=====================
BlueTooth Low Energy
=====================
.. raw:: html

   <script type="text/javascript">
   window.location.href = "BLE_GATT_Throughput_Example.html"
   </script>

.. toctree::
   :hidden:
      
   BLE_GATT_Throughput_Example.rst
   BLE_Environmental_Sensing_Profile_Example.rst
   BLE_GAP_Peripheral-GATT_Client_with_Current_Time_Service.rst
   BLE_GAP_Peripheral-GATT_Server_with_Current_Time_Service.rst
   BLE_Beacon_Example.rst
   BLE_Battery_Server.rst
   