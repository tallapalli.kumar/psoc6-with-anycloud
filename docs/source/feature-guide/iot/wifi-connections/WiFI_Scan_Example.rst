==================
WiFi Scan Example
==================

This example demonstrates how to configure different scan filters provided in the Wi-Fi Connection Manager (WCM) middleware and scan for the available Wi-Fi networks. 

.. raw:: html

   <a href="https://github.com/cypresssemiconductorco/mtb-example-anycloud-wifi-scan" target="_blank">Click here to be taken to the project on GitHub.</a><br><br>
