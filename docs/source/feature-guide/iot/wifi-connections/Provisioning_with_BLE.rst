======================
Provisioning with BLE
======================

This example uses the Arm\ :sup:`®` Cortex :sup:`®`-M4 (CM4) CPU of PSoC :sup:`®` 6 MCU to communicate with the CYW43xxx combo devices and control the Wi-Fi and BLE functionality. It uses BLE on the combo device to help connect the Wi-Fi to the AP. It also enables low-power mode on Wi-Fi and BLE. 

.. raw:: html

   <a href="https://github.com/cypresssemiconductorco/mtb-example-anycloud-ble-wifi-onboarding" target="_blank">Click here to be taken to the project on GitHub.</a><br><br>
