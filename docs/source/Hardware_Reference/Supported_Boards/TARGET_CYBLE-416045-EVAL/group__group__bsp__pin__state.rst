===========
Pin States
===========


.. doxygendefine:: CYBSP_LED_STATE_ON
   :project: TARGET_CYBLE-416045-EVAL
   
.. doxygendefine:: CYBSP_LED_STATE_OFF
   :project: TARGET_CYBLE-416045-EVAL
   
.. doxygendefine:: CYBSP_BTN_PRESSED
   :project: TARGET_CYBLE-416045-EVAL

.. doxygendefine:: CYBSP_BTN_OFF
   :project: TARGET_CYBLE-416045-EVAL
   


  

