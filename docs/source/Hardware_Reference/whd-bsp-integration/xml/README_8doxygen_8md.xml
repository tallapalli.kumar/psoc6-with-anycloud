<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="README_8doxygen_8md" kind="file" language="Markdown">
    <compoundname>README.doxygen.md</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">#<sp />WiFi<sp />Host<sp />Driver<sp />Board<sp />Support<sp />Package<sp />Integration</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Overview</highlight></codeline>
<codeline />
<codeline><highlight class="normal">This<sp />library<sp />provides<sp />some<sp />convenience<sp />functions<sp />for<sp />connecting<sp />the<sp />WiFi<sp />Host<sp />Driver<sp />(WHD)<sp />library<sp />to<sp />a<sp />Board<sp />Support<sp />Package<sp />(BSP)<sp />that<sp />includes<sp />a<sp />WLAN<sp />chip.<sp />This<sp />library<sp />initializes<sp />the<sp />hardware<sp />and<sp />passes<sp />a<sp />reference<sp />to<sp />the<sp />communication<sp />interface<sp />on<sp />the<sp />board<sp />into<sp />WHD.<sp />It<sp />also<sp />sets<sp />up<sp />the<sp />LwIP<sp />based<sp />network<sp />buffers<sp />to<sp />be<sp />used<sp />for<sp />sending<sp />packets<sp />back<sp />and<sp />forth.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Getting<sp />Started</highlight></codeline>
<codeline />
<codeline><highlight class="normal">To<sp />use<sp />this<sp />library:</highlight></codeline>
<codeline><highlight class="normal">1.<sp />Implement<sp />the<sp />cy_network_process_ethernet_data()<sp />function.<sp />This<sp />should<sp />be<sp />something<sp />similar<sp />to<sp />the<sp />example<sp />below.</highlight></codeline>
<codeline><highlight class="normal">```cpp</highlight></codeline>
<codeline><highlight class="normal">/*<sp />This<sp />needs<sp />to<sp />be<sp />the<sp />same<sp />item<sp />as<sp />passed<sp />to<sp />netifapi_netif_add()<sp />*/</highlight></codeline>
<codeline><highlight class="normal">static<sp />struct<sp />netif<sp />*default_interface<sp />=<sp />NULL;</highlight></codeline>
<codeline />
<codeline><highlight class="normal">void<sp />cy_network_process_ethernet_data(whd_interface_t<sp />iface,<sp />whd_buffer_t<sp />buf)</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />if<sp />(default_interface<sp />!=<sp />NULL)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />if<sp />(default_interface-&gt;input(buf,<sp />default_interface)<sp />!=<sp />ERR_OK)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cy_buffer_release(buf,<sp />WHD_NETWORK_RX);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />else</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cy_buffer_release(buf,<sp />WHD_NETWORK_RX);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
<codeline><highlight class="normal">```</highlight></codeline>
<codeline><highlight class="normal">2.<sp />Include<sp />a<sp />reference<sp />to<sp />`cybsp_wifi.h`.</highlight></codeline>
<codeline><highlight class="normal">3.<sp />Call<sp />cybsp_wifi_init_primary()<sp />to<sp />initialize<sp />the<sp />interface.<sp />This<sp />needs<sp />to<sp />be<sp />done<sp />after<sp />having<sp />called<sp />cybsp_init().</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Features</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />APIs<sp />for<sp />setting<sp />up<sp />the<sp />WHD<sp />interface<sp />with<sp />the<sp />BSP's<sp />SDIO<sp />interface.</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />Initialize<sp />a<sp />primary<sp />WiFi<sp />interface</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />Initialize<sp />an<sp />optional<sp />secondary<sp />WiFi<sp />interface</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />*<sp />Cleanup<sp />interfaces</highlight></codeline>
<codeline><highlight class="normal">*<sp />APIs<sp />for<sp />connecting<sp />WHD<sp />to<sp />LwIP<sp />memory<sp />buffers<sp />(whd_buffer_funcs_t)</highlight></codeline>
<codeline><highlight class="normal">*<sp />Framework<sp />for<sp />connecting<sp />WHD<sp />to<sp />LwIP<sp />network<sp />interface<sp />(whd_netif_funcs_t)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />More<sp />information</highlight></codeline>
<codeline><highlight class="normal">*<sp />[API<sp />Reference<sp />Guide](https://cypresssemiconductorco.github.io/whd-bsp-integration/html/modules.html)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor,<sp />an<sp />Infineon<sp />Technologies<sp />Company](http://www.cypress.com)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor<sp />GitHub](https://github.com/cypresssemiconductorco)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[ModusToolbox](https://www.cypress.com/products/modustoolbox-software-environment)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">---</highlight></codeline>
<codeline><highlight class="normal">©<sp />Cypress<sp />Semiconductor<sp />Corporation,<sp />2019-2020.</highlight></codeline>
    </programlisting>
    <location file="bsp/libs/whd-bsp-integration/README.doxygen.md" />
  </compounddef>
</doxygen>