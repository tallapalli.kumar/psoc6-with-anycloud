<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cybsp__wifi_8h" kind="file" language="C++">
    <compoundname>cybsp_wifi.h</compoundname>
    <includes local="yes">cy_result.h</includes>
    <includes local="yes">whd_wifi_api.h</includes>
    <includedby local="yes" refid="cybsp__wifi_8c">cybsp_wifi.c</includedby>
    <incdepgraph>
      <node id="23">
        <label>cy_result.h</label>
      </node>
      <node id="22">
        <label>cybsp_wifi.h</label>
        <link refid="cybsp__wifi_8h" />
        <childnode refid="23" relation="include">
        </childnode>
        <childnode refid="24" relation="include">
        </childnode>
      </node>
      <node id="24">
        <label>whd_wifi_api.h</label>
      </node>
    </incdepgraph>
      <sectiondef kind="define">
      <memberdef id="group__group__bsp__wifi_1ga1ddf3533adfa2e8f298f7edab7a8fa0c" kind="define" prot="public" static="no">
        <name>CYBSP_RSLT_WIFI_INIT_FAILED</name>
        <initializer>(CY_RSLT_CREATE(CY_RSLT_TYPE_ERROR, CY_RSLT_MODULE_BOARD_LIB_WHD_INTEGRATION, 0))</initializer>
        <briefdescription>
<para>Initialization of the WiFi driver failed. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.h" bodystart="43" column="9" file="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.h" line="43" />
      </memberdef>
      <memberdef id="group__group__bsp__wifi_1gab868840379127847a984d02327620dd4" kind="define" prot="public" static="no">
        <name>CYBSP_RSLT_WIFI_SDIO_ENUM_TIMEOUT</name>
        <initializer>(CY_RSLT_CREATE(CY_RSLT_TYPE_ERROR, CY_RSLT_MODULE_BOARD_LIB_WHD_INTEGRATION, 1))</initializer>
        <briefdescription>
<para>SDIO enumeration failed. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.h" bodystart="46" column="9" file="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.h" line="46" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__bsp__wifi_1ga7f491cc7f4a7ecf4b752d0970a1f58f3" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cybsp_wifi_init_primary_extended</definition>
        <argsstring>(whd_interface_t *interface, whd_resource_source_t *resource_if, whd_buffer_funcs_t *buffer_if, whd_netif_funcs_t *netif_if)</argsstring>
        <name>cybsp_wifi_init_primary_extended</name>
        <param>
          <type>whd_interface_t *</type>
          <declname>interface</declname>
        </param>
        <param>
          <type>whd_resource_source_t *</type>
          <declname>resource_if</declname>
        </param>
        <param>
          <type>whd_buffer_funcs_t *</type>
          <declname>buffer_if</declname>
        </param>
        <param>
          <type>whd_netif_funcs_t *</type>
          <declname>netif_if</declname>
        </param>
        <briefdescription>
<para>Initializes the primary interface for the WiFi driver on the board. </para>        </briefdescription>
        <detaileddescription>
<para>This sets up the WHD interface to use the <ref kindref="compound" refid="group__group__bsp__network__buffer">Buffer management</ref> APIs and to communicate over the SDIO interface on the board. This function does the following:<linebreak />
 1) Initializes the WiFi driver.<linebreak />
 2) Turns on the WiFi chip.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function cannot be called multiple times. If the interface needs to be reinitialized, &lt;a href="group__group__bsp__wifi.html#group__group__bsp__wifi_1ga77555750b014230bd78143aefb9f379b"&gt;cybsp_wifi_deinit&lt;/a&gt; must be called before calling this function again.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">interface</parametername>
</parameternamelist>
<parameterdescription>
<para>Interface to be initialized </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">resource_if</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to resource interface to provide resources to the driver initialization process. Passing NULL will use the default. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">buffer_if</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a buffer interface to provide buffer related services to the driver instance. Passing NULL will use the default. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">netif_if</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a whd_netif_funcs_t to provide network stack services to the driver instance. Passing NULL will use the default. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>CY_RSLT_SUCCESS for successful initialization or error if initialization failed. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="236" bodyfile="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.c" bodystart="210" column="1" file="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.h" line="64" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__bsp__wifi_1gafc78b1c0b031052308051088ee2021b7" inline="yes" kind="function" prot="public" static="yes" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>static cy_rslt_t cybsp_wifi_init_primary</definition>
        <argsstring>(whd_interface_t *interface)</argsstring>
        <name>cybsp_wifi_init_primary</name>
        <param>
          <type>whd_interface_t *</type>
          <declname>interface</declname>
        </param>
        <briefdescription>
<para>Initializes the primary interface for the WiFi driver on the board using the default resource, buffer, and network interfaces. </para>        </briefdescription>
        <detaileddescription>
<para>See <ref kindref="member" refid="group__group__bsp__wifi_1ga7f491cc7f4a7ecf4b752d0970a1f58f3">cybsp_wifi_init_primary_extended()</ref> for more details.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">interface</parametername>
</parameternamelist>
<parameterdescription>
<para>Interface to be initialized </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>CY_RSLT_SUCCESS for successful initialization or error if initialization failed. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="77" bodyfile="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.h" bodystart="74" column="1" file="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.h" line="74" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__bsp__wifi_1ga13b5648cec341b96068e2b372388eea4" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cybsp_wifi_init_secondary</definition>
        <argsstring>(whd_interface_t *interface, whd_mac_t *mac_address)</argsstring>
        <name>cybsp_wifi_init_secondary</name>
        <param>
          <type>whd_interface_t *</type>
          <declname>interface</declname>
        </param>
        <param>
          <type>whd_mac_t *</type>
          <declname>mac_address</declname>
        </param>
        <briefdescription>
<para>This function initializes and adds a secondary interface to the WiFi driver. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function does not initialize the WiFi driver or turn on the WiFi chip. That is required to be done by first calling &lt;a href="group__group__bsp__wifi.html#group__group__bsp__wifi_1gafc78b1c0b031052308051088ee2021b7"&gt;cybsp_wifi_init_primary&lt;/a&gt;.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">interface</parametername>
</parameternamelist>
<parameterdescription>
<para>Interface to be initialized </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">mac_address</parametername>
</parameternamelist>
<parameterdescription>
<para>Mac address for secondary interface </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>CY_RSLT_SUCCESS for successful initialization or error if initialization failed. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="241" bodyfile="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.c" bodystart="238" column="1" file="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.h" line="87" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__bsp__wifi_1ga77555750b014230bd78143aefb9f379b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cybsp_wifi_deinit</definition>
        <argsstring>(whd_interface_t interface)</argsstring>
        <name>cybsp_wifi_deinit</name>
        <param>
          <type>whd_interface_t</type>
          <declname>interface</declname>
        </param>
        <briefdescription>
<para>De-initializes all WiFi interfaces and the WiFi driver. </para>        </briefdescription>
        <detaileddescription>
<para>This function does the following:<linebreak />
 1) Deinitializes all WiFi interfaces and WiFi driver.<linebreak />
 2) Turns off the WiFi chip.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">interface</parametername>
</parameternamelist>
<parameterdescription>
<para>Interface to be de-initialized. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>CY_RSLT_SUCCESS for successful de-initialization or error if de-initialization failed. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="255" bodyfile="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.c" bodystart="243" column="1" file="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.h" line="97" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__bsp__wifi_1ga9f2fb2176d21e958727a75821b346e47" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>whd_driver_t</type>
        <definition>whd_driver_t cybsp_get_wifi_driver</definition>
        <argsstring>(void)</argsstring>
        <name>cybsp_get_wifi_driver</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Gets the wifi driver instance initialized by the driver. </para>        </briefdescription>
        <detaileddescription>
<para>This should only be called after being initialized by <ref kindref="member" refid="group__group__bsp__wifi_1gafc78b1c0b031052308051088ee2021b7">cybsp_wifi_init_primary()</ref> and before being deinitialized by <ref kindref="member" refid="group__group__bsp__wifi_1ga77555750b014230bd78143aefb9f379b">cybsp_wifi_deinit()</ref>. This is also the only time where the driver reference is valid.</para><para><simplesect kind="return"><para>Wifi driver instance pointer. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="260" bodyfile="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.c" bodystart="257" column="1" file="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.h" line="106" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>Basic abstraction layer for dealing with boards containing a Cypress MCU. </para>    </briefdescription>
    <detaileddescription>
<para>This API provides convenience methods for initializing and manipulating different hardware found on the board.</para><para><simplesect kind="copyright"><para>Copyright 2018-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para></simplesect>
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at <verbatim>http://www.apache.org/licenses/LICENSE-2.0
</verbatim></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight></codeline>
<codeline lineno="33"><highlight class="preprocessor">#pragma<sp />once</highlight><highlight class="normal" /></codeline>
<codeline lineno="34"><highlight class="normal" /></codeline>
<codeline lineno="35"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_result.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="36"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"whd_wifi_api.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="37"><highlight class="normal" /></codeline>
<codeline lineno="38"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="39"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline lineno="40"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="41"><highlight class="normal" /></codeline>
<codeline lineno="43"><highlight class="preprocessor">#define<sp />CYBSP_RSLT_WIFI_INIT_FAILED<sp />(CY_RSLT_CREATE(CY_RSLT_TYPE_ERROR,<sp />CY_RSLT_MODULE_BOARD_LIB_WHD_INTEGRATION,<sp />0))</highlight><highlight class="normal" /></codeline>
<codeline lineno="44"><highlight class="normal" /></codeline>
<codeline lineno="46"><highlight class="preprocessor">#define<sp />CYBSP_RSLT_WIFI_SDIO_ENUM_TIMEOUT<sp />(CY_RSLT_CREATE(CY_RSLT_TYPE_ERROR,<sp />CY_RSLT_MODULE_BOARD_LIB_WHD_INTEGRATION,<sp />1))</highlight><highlight class="normal" /></codeline>
<codeline lineno="47"><highlight class="normal" /></codeline>
<codeline lineno="64"><highlight class="normal">cy_rslt_t<sp /><ref kindref="member" refid="group__group__bsp__wifi_1ga7f491cc7f4a7ecf4b752d0970a1f58f3">cybsp_wifi_init_primary_extended</ref>(whd_interface_t*<sp />interface,<sp />whd_resource_source_t<sp />*resource_if,<sp />whd_buffer_funcs_t<sp />*buffer_if,</highlight></codeline>
<codeline lineno="65"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />whd_netif_funcs_t<sp />*netif_if);</highlight></codeline>
<codeline lineno="66"><highlight class="normal" /></codeline>
<codeline lineno="74"><highlight class="keyword">static</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">inline</highlight><highlight class="normal"><sp />cy_rslt_t<sp /><ref kindref="member" refid="group__group__bsp__wifi_1gafc78b1c0b031052308051088ee2021b7">cybsp_wifi_init_primary</ref>(whd_interface_t*<sp />interface)</highlight></codeline>
<codeline lineno="75"><highlight class="normal">{</highlight></codeline>
<codeline lineno="76"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__bsp__wifi_1ga7f491cc7f4a7ecf4b752d0970a1f58f3">cybsp_wifi_init_primary_extended</ref>(interface,<sp />NULL,<sp />NULL,<sp />NULL);</highlight></codeline>
<codeline lineno="77"><highlight class="normal">}</highlight></codeline>
<codeline lineno="78"><highlight class="normal" /></codeline>
<codeline lineno="87"><highlight class="normal">cy_rslt_t<sp /><ref kindref="member" refid="group__group__bsp__wifi_1ga13b5648cec341b96068e2b372388eea4">cybsp_wifi_init_secondary</ref>(whd_interface_t*<sp />interface,<sp />whd_mac_t*<sp />mac_address);</highlight></codeline>
<codeline lineno="88"><highlight class="normal" /></codeline>
<codeline lineno="97"><highlight class="normal">cy_rslt_t<sp /><ref kindref="member" refid="group__group__bsp__wifi_1ga77555750b014230bd78143aefb9f379b">cybsp_wifi_deinit</ref>(whd_interface_t<sp />interface);</highlight></codeline>
<codeline lineno="98"><highlight class="normal" /></codeline>
<codeline lineno="106"><highlight class="normal">whd_driver_t<sp /><ref kindref="member" refid="group__group__bsp__wifi_1ga9f2fb2176d21e958727a75821b346e47">cybsp_get_wifi_driver</ref>(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">);</highlight></codeline>
<codeline lineno="107"><highlight class="normal" /></codeline>
<codeline lineno="108"><highlight class="normal" /><highlight class="preprocessor">#ifdef<sp />__cplusplus</highlight><highlight class="normal" /></codeline>
<codeline lineno="109"><highlight class="normal">}</highlight></codeline>
<codeline lineno="110"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />__cplusplus<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="111"><highlight class="normal" /></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_WHD_GLUE/whd-bsp-integration/cybsp_wifi.h" />
  </compounddef>
</doxygen>