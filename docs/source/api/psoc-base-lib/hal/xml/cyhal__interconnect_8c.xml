<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cyhal__interconnect_8c" kind="file" language="C++">
    <compoundname>cyhal_interconnect.c</compoundname>
    <includes local="yes" refid="cyhal__interconnect_8h">cyhal_interconnect.h</includes>
    <includes local="yes" refid="cyhal__gpio__impl_8h">cyhal_gpio_impl.h</includes>
    <briefdescription>
<para>Provides a high level interface for interacting with the internal digital routing on the chip. </para>    </briefdescription>
    <detaileddescription>
<para>This is a wrapper around the lower level PDL API.</para><para><simplesect kind="copyright"><para>Copyright 2018-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para></simplesect>
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at <verbatim>http://www.apache.org/licenses/LICENSE-2.0
</verbatim></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight></codeline>
<codeline lineno="26"><highlight class="preprocessor">#include<sp />"cyhal_interconnect.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="27"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_gpio_impl.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="28"><highlight class="normal" /></codeline>
<codeline lineno="29"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(CY_IP_MXPERI)<sp />||<sp />defined(CY_IP_M0S8PERI)</highlight><highlight class="normal" /></codeline>
<codeline lineno="30"><highlight class="normal" /></codeline>
<codeline lineno="31"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="32"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal" /></codeline>
<codeline lineno="33"><highlight class="normal">{</highlight></codeline>
<codeline lineno="34"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="35"><highlight class="normal" /></codeline>
<codeline lineno="36"><highlight class="normal"><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp /><ref kindref="member" refid="group__group__hal__interconnect_1gaeb7156c33f00a7c21867a46e401d59ef">cyhal_connect_pin</ref>(</highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__pin__mapping__t">cyhal_resource_pin_mapping_t</ref><sp />*pin_connection)</highlight></codeline>
<codeline lineno="37"><highlight class="normal">{</highlight></codeline>
<codeline lineno="38"><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref><sp />pin<sp />=<sp />pin_connection-&gt;<ref kindref="member" refid="structcyhal__resource__pin__mapping__t_1a61f6033fdaae54a06ac8e63656b2a37a">pin</ref>;</highlight></codeline>
<codeline lineno="39"><highlight class="normal"><sp /><sp /><sp /><sp />GPIO_PRT_Type<sp />*port<sp />=<sp />Cy_GPIO_PortToAddr(<ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga015f256578abd5638668ff19b9dc89d5">CYHAL_GET_PORT</ref>(pin));</highlight></codeline>
<codeline lineno="40"><highlight class="normal"><sp /><sp /><sp /><sp />en_hsiom_sel_t<sp />hsiom<sp />=<sp />pin_connection-&gt;<ref kindref="member" refid="structcyhal__resource__pin__mapping__t_1ad4514be01ab83c7248c320b3232145c1">hsiom</ref>;</highlight></codeline>
<codeline lineno="41"><highlight class="normal"><sp /><sp /><sp /><sp />uint8_t<sp />mode<sp />=<sp />pin_connection-&gt;<ref kindref="member" refid="structcyhal__resource__pin__mapping__t_1a01c7dc6cfa4fca67252a14571b98a3ff">drive_mode</ref>;</highlight></codeline>
<codeline lineno="42"><highlight class="normal" /></codeline>
<codeline lineno="43"><highlight class="normal"><sp /><sp /><sp /><sp />Cy_GPIO_Pin_FastInit(port,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gae0af2ee8c5a2a2e6661962b368d1f2ba">CYHAL_GET_PIN</ref>(pin),<sp />mode,<sp />1,<sp />hsiom);</highlight></codeline>
<codeline lineno="44"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">//<sp />Force<sp />output<sp />to<sp />enable<sp />pulls.</highlight><highlight class="normal" /></codeline>
<codeline lineno="45"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">switch</highlight><highlight class="normal"><sp />(mode)<sp />{</highlight></codeline>
<codeline lineno="46"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">case</highlight><highlight class="normal"><sp />CY_GPIO_DM_PULLUP:</highlight></codeline>
<codeline lineno="47"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />Cy_GPIO_Write(port,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gae0af2ee8c5a2a2e6661962b368d1f2ba">CYHAL_GET_PIN</ref>(pin),<sp />1);</highlight></codeline>
<codeline lineno="48"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">break</highlight><highlight class="normal">;</highlight></codeline>
<codeline lineno="49"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">case</highlight><highlight class="normal"><sp />CY_GPIO_DM_PULLDOWN:</highlight></codeline>
<codeline lineno="50"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />Cy_GPIO_Write(port,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gae0af2ee8c5a2a2e6661962b368d1f2ba">CYHAL_GET_PIN</ref>(pin),<sp />0);</highlight></codeline>
<codeline lineno="51"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">break</highlight><highlight class="normal">;</highlight></codeline>
<codeline lineno="52"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">default</highlight><highlight class="normal">:</highlight></codeline>
<codeline lineno="53"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />do<sp />nothing<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="54"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">break</highlight><highlight class="normal">;</highlight></codeline>
<codeline lineno="55"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="56"><highlight class="normal" /></codeline>
<codeline lineno="57"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>;</highlight></codeline>
<codeline lineno="58"><highlight class="normal">}</highlight></codeline>
<codeline lineno="59"><highlight class="normal" /></codeline>
<codeline lineno="60"><highlight class="normal"><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp /><ref kindref="member" refid="group__group__hal__interconnect_1ga62898739384ebb41a8b3e2c4d491a0cc">cyhal_disconnect_pin</ref>(<ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref><sp />pin)</highlight></codeline>
<codeline lineno="61"><highlight class="normal">{</highlight></codeline>
<codeline lineno="62"><highlight class="normal"><sp /><sp /><sp /><sp />GPIO_PRT_Type<sp />*port<sp />=<sp />Cy_GPIO_PortToAddr(<ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga015f256578abd5638668ff19b9dc89d5">CYHAL_GET_PORT</ref>(pin));</highlight></codeline>
<codeline lineno="63"><highlight class="normal"><sp /><sp /><sp /><sp />Cy_GPIO_Pin_FastInit(port,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gae0af2ee8c5a2a2e6661962b368d1f2ba">CYHAL_GET_PIN</ref>(pin),<sp />CY_GPIO_DM_HIGHZ,<sp />1,<sp />HSIOM_SEL_GPIO);</highlight></codeline>
<codeline lineno="64"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>;</highlight></codeline>
<codeline lineno="65"><highlight class="normal">}</highlight></codeline>
<codeline lineno="66"><highlight class="normal" /></codeline>
<codeline lineno="67"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="68"><highlight class="normal">}</highlight></codeline>
<codeline lineno="69"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="70"><highlight class="normal" /></codeline>
<codeline lineno="71"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />defined(CY_IP_MXPERI)<sp />||<sp />defined(CY_IP_M0S8PERI)<sp />*/</highlight><highlight class="preprocessor" /></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_interconnect.c" />
  </compounddef>
</doxygen>