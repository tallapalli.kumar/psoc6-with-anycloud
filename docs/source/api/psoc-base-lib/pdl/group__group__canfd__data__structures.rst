================
Data Structures
================

.. doxygengroup:: group_canfd_data_structures
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: