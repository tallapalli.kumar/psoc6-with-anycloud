===========================
SIO input buffer trip-point
===========================

.. doxygengroup:: group_gpio_sioVtrip
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: