=====================================
Number of days in month definitions
=====================================



.. doxygengroup:: group_rtc_days_in_month
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: