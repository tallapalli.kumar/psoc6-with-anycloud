=================
Enumerated Types
=================

.. doxygengroup:: group_ipc_sema_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   