==============================
The Power Mode Status Defines
==============================

.. doxygengroup:: group_syspm_return_status
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: