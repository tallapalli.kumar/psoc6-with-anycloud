======
Macros
======

API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__i2s__macros__interrupt__masks.rst
   group__group__i2s__macros__current__state.rst

.. doxygengroup:: group_i2s_macros
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


