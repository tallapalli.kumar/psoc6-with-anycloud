================
Enumerated Types
================

.. doxygengroup:: group_flash_enumerated_types
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: