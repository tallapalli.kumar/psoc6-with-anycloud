==============
PWM kill modes
==============

.. doxygengroup:: group_tcpwm_pwm_kill_modes
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: