===========================
Port Interrupt Functions
===========================

.. doxygengroup:: group_gpio_functions_interrupt
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
  