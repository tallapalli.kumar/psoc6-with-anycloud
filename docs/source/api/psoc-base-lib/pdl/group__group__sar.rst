========================
SAR (SAR ADC Subsystem)
========================


.. doxygengroup:: group_sar
   :project: pdl

.. toctree::
   
   group__group__sar__macros.rst
   group__group__sar__functions.rst
   group__group__sar__data__structures.rst
   group__group__sar__enums.rst
   
   
   
