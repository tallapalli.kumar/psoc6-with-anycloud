================
Enumerated Types
================

.. doxygengroup:: group_syslib_enumerated_types
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: