======================================
LPM (Link Power Management) Functions
======================================

.. doxygengroup:: group_usbfs_dev_drv_functions_lpm
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: