<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__seglcd__functions__config" kind="group">
    <compoundname>group_seglcd_functions_config</compoundname>
    <title>Block Configuration Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__seglcd__functions__config_1gae5bce733739f6bbe0a2aa9d2959f2a50" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__seglcd__enums_1gaf81660a051e97187b6617ae3f709b047">cy_en_seglcd_status_t</ref></type>
        <definition>cy_en_seglcd_status_t Cy_SegLCD_Init</definition>
        <argsstring>(LCD_Type *base, cy_stc_seglcd_config_t const *config)</argsstring>
        <name>Cy_SegLCD_Init</name>
        <param>
          <type>LCD_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__seglcd__config__t">cy_stc_seglcd_config_t</ref> const *</type>
          <declname>config</declname>
        </param>
        <briefdescription>
<para>Initializes/restores the default Segment LCD block configuration. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The base pointer to the LCD instance registers. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>config</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to a configuration structure. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__seglcd__enums_1gaf81660a051e97187b6617ae3f709b047">cy_en_seglcd_status_t</ref>.</para></simplesect>
Side Effects: The block is disabled to change the settings.</para><para><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="comment">/*<sp />Scenario:<sp />Enable<sp />an<sp />LCD<sp />block<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><ref kindref="compound" refid="structcy__stc__seglcd__config__t">cy_stc_seglcd_config_t</ref><sp />config<sp />=</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.speed<sp />=<sp /><ref kindref="member" refid="group__group__seglcd__enums_1ggab22a15df01ec018165a1a7e812f70bb0a280e1ebe0dce993105ffd2d6209055bb">CY_SEGLCD_SPEED_HIGH</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.wave<sp />=<sp /><ref kindref="member" refid="group__group__seglcd__enums_1gga255aa427b265f88cc6e618cdc5491248a5c1a7cd720aedee901d6527dd617f57b">CY_SEGLCD_TYPE_B</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.drive<sp />=<sp /><ref kindref="member" refid="group__group__seglcd__enums_1ggaff012223bec16a8c88dfdb39bd304893a1e4f051430063f293bdfc4ddbe32a27f">CY_SEGLCD_PWM</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.bias<sp />=<sp /><ref kindref="member" refid="group__group__seglcd__enums_1gga2b57420866b0307ca04c1e033790661ca3bac132675f4842763ee46417d83f3a1">CY_SEGLCD_BIAS_FOURTH</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.lsClk<sp />=<sp /><ref kindref="member" refid="group__group__seglcd__enums_1gga4e8bfe5d2149ea8d82ac609367709c95abadf62ab3037855e61b95e5a13be87bb">CY_SEGLCD_LSCLK_LF</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.comNum<sp />=<sp />8,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.frRate<sp />=<sp />70,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.contrast<sp />=<sp />70,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /></highlight><highlight class="comment">/*.clkFreq<sp />is<sp />unknown<sp />here<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal">};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keyword">const</highlight><highlight class="normal"><sp />uint32_t<sp />commons[8]<sp />=</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_0,<sp />0UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_1,<sp />1UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_2,<sp />2UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_3,<sp />3UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_4,<sp />4UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_5,<sp />5UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_6,<sp />6UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_7,<sp />7UL)</highlight></codeline>
<codeline><highlight class="normal">};</highlight></codeline>
</programlisting><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Then<sp />in<sp />executable<sp />code:<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />frequency<sp />of<sp />the<sp />assigned<sp />peripheral<sp />clock<sp />divider<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />config.<ref kindref="member" refid="structcy__stc__seglcd__config__t_1ad72440f94cfffb60d7223157722e5769">clkFreq</ref><sp /><sp />=<sp /><ref kindref="member" refid="group__group__sysclk__clk__peripheral__funcs_1ga366428a7c17da00f7a7ae025aeda2e23">Cy_SysClk_PeriphGetFrequency</ref>(<ref kindref="member" refid="group__group__sysclk__clk__peripheral__enums_1gga06138349be16d91fd5d00ded2f4592b8ad2299deae6e36a070de5a60368b319ad">CY_SYSCLK_DIV_8_BIT</ref>,<sp />0U);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__seglcd__enums_1ggaf81660a051e97187b6617ae3f709b047a3fac00a061e1cd84f5e0e13289944cb7">CY_SEGLCD_SUCCESS</ref><sp />==<sp /><ref kindref="member" refid="group__group__seglcd__functions__config_1gae5bce733739f6bbe0a2aa9d2959f2a50">Cy_SegLCD_Init</ref>(LCD0,<sp />&amp;config))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__seglcd__enums_1ggaf81660a051e97187b6617ae3f709b047a3fac00a061e1cd84f5e0e13289944cb7">CY_SEGLCD_SUCCESS</ref><sp />==<sp /><ref kindref="member" refid="group__group__seglcd__functions__frame_1ga7f6bb5486ade2f8fe07ae883b1e59be2">Cy_SegLCD_ClrFrame</ref>(LCD0,<sp />commons))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__functions__config_1ga55eb3d853498593da4eb3cddaca073df">Cy_SegLCD_Enable</ref>(LCD0);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Now<sp />the<sp />block<sp />generates<sp />LCD<sp />signals<sp />(all<sp />the<sp />pixels<sp />are<sp />off)<sp />and<sp />is<sp />ready<sp />to<sp />turn<sp />on<sp />any<sp />pixel</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*<sp />(or<sp />many<sp />pixels)<sp />using<sp />any<sp />of<sp />the<sp />frame/pixel/character/display<sp />management<sp />API<sp />functions.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="419" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_seglcd.c" bodystart="375" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_seglcd.h" line="562" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__seglcd__functions__config_1ga73c8ea0f6e72d690a34c1ad175ff21aa" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__seglcd__enums_1gaf81660a051e97187b6617ae3f709b047">cy_en_seglcd_status_t</ref></type>
        <definition>cy_en_seglcd_status_t Cy_SegLCD_Contrast</definition>
        <argsstring>(LCD_Type *base, uint32_t contrast, cy_stc_seglcd_config_t *config)</argsstring>
        <name>Cy_SegLCD_Contrast</name>
        <param>
          <type>LCD_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>contrast</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__seglcd__config__t">cy_stc_seglcd_config_t</ref> *</type>
          <declname>config</declname>
        </param>
        <briefdescription>
<para>Sets a specified contrast. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The base pointer to the LCD instance registers. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>contrast</parametername>
</parameternamelist>
<parameterdescription>
<para>The contrast value to be set. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>config</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to a configuration structure. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__seglcd__enums_1gaf81660a051e97187b6617ae3f709b047">cy_en_seglcd_status_t</ref>.</para></simplesect>
Side Effects: The configuration structure contrast value is also updated.</para><para><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />contrast<sp />=<sp />70UL;<sp /></highlight><highlight class="comment">/*<sp />Contrast<sp />value<sp />from<sp />0<sp />to<sp />100<sp />*/</highlight><highlight class="normal" /></codeline>
</programlisting><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Adjust<sp />the<sp />LCD<sp />contrast<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__seglcd__enums_1ggaf81660a051e97187b6617ae3f709b047a3fac00a061e1cd84f5e0e13289944cb7">CY_SEGLCD_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__seglcd__functions__config_1ga73c8ea0f6e72d690a34c1ad175ff21aa">Cy_SegLCD_Contrast</ref>(LCD0,<sp />contrast,<sp />&amp;config))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="459" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_seglcd.c" bodystart="440" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_seglcd.h" line="563" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__seglcd__functions__config_1ga60b86978cdba06fe799591e0822dcc55" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SegLCD_Deinit</definition>
        <argsstring>(LCD_Type *base)</argsstring>
        <name>Cy_SegLCD_Deinit</name>
        <param>
          <type>LCD_Type *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>De-initializes the LCD block (resets the block registers to default state). </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The base pointer to the LCD instance registers.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Reset<sp />all<sp />the<sp />block<sp />registers<sp />to<sp />their<sp />default<sp />values<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__functions__config_1ga60b86978cdba06fe799591e0822dcc55">Cy_SegLCD_Deinit</ref>(LCD0);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="482" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_seglcd.c" bodystart="474" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_seglcd.h" line="564" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__seglcd__functions__config_1ga55eb3d853498593da4eb3cddaca073df" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SegLCD_Enable</definition>
        <argsstring>(LCD_Type *base)</argsstring>
        <name>Cy_SegLCD_Enable</name>
        <param>
          <type>LCD_Type *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Enables the Segment LCD block. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The base pointer to the LCD instance registers.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="comment">/*<sp />Scenario:<sp />Enable<sp />an<sp />LCD<sp />block<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><ref kindref="compound" refid="structcy__stc__seglcd__config__t">cy_stc_seglcd_config_t</ref><sp />config<sp />=</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.<ref kindref="member" refid="structcy__stc__seglcd__config__t_1a1be9b9b30d3113fe5c2ce54e6b837740">speed</ref><sp />=<sp /><ref kindref="member" refid="group__group__seglcd__enums_1ggab22a15df01ec018165a1a7e812f70bb0a280e1ebe0dce993105ffd2d6209055bb">CY_SEGLCD_SPEED_HIGH</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.wave<sp />=<sp /><ref kindref="member" refid="group__group__seglcd__enums_1gga255aa427b265f88cc6e618cdc5491248a5c1a7cd720aedee901d6527dd617f57b">CY_SEGLCD_TYPE_B</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.drive<sp />=<sp /><ref kindref="member" refid="group__group__seglcd__enums_1ggaff012223bec16a8c88dfdb39bd304893a1e4f051430063f293bdfc4ddbe32a27f">CY_SEGLCD_PWM</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.bias<sp />=<sp /><ref kindref="member" refid="group__group__seglcd__enums_1gga2b57420866b0307ca04c1e033790661ca3bac132675f4842763ee46417d83f3a1">CY_SEGLCD_BIAS_FOURTH</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.lsClk<sp />=<sp /><ref kindref="member" refid="group__group__seglcd__enums_1gga4e8bfe5d2149ea8d82ac609367709c95abadf62ab3037855e61b95e5a13be87bb">CY_SEGLCD_LSCLK_LF</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.comNum<sp />=<sp />8,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.frRate<sp />=<sp />70,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.contrast<sp />=<sp />70,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /></highlight><highlight class="comment">/*.clkFreq<sp />is<sp />unknown<sp />here<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal">};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keyword">const</highlight><highlight class="normal"><sp />uint32_t<sp />commons[8]<sp />=</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_0,<sp />0UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_1,<sp />1UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_2,<sp />2UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_3,<sp />3UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_4,<sp />4UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_5,<sp />5UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_6,<sp />6UL),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__macros_1ga5da32950a854641868e98c302afe2fe5">CY_SEGLCD_COMMON</ref>(LCD_COM_7,<sp />7UL)</highlight></codeline>
<codeline><highlight class="normal">};</highlight></codeline>
</programlisting><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Then<sp />in<sp />executable<sp />code:<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />frequency<sp />of<sp />the<sp />assigned<sp />peripheral<sp />clock<sp />divider<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />config.<ref kindref="member" refid="structcy__stc__seglcd__config__t_1ad72440f94cfffb60d7223157722e5769">clkFreq</ref><sp /><sp />=<sp /><ref kindref="member" refid="group__group__sysclk__clk__peripheral__funcs_1ga366428a7c17da00f7a7ae025aeda2e23">Cy_SysClk_PeriphGetFrequency</ref>(<ref kindref="member" refid="group__group__sysclk__clk__peripheral__enums_1gga06138349be16d91fd5d00ded2f4592b8ad2299deae6e36a070de5a60368b319ad">CY_SYSCLK_DIV_8_BIT</ref>,<sp />0U);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__seglcd__enums_1ggaf81660a051e97187b6617ae3f709b047a3fac00a061e1cd84f5e0e13289944cb7">CY_SEGLCD_SUCCESS</ref><sp />==<sp /><ref kindref="member" refid="group__group__seglcd__functions__config_1gae5bce733739f6bbe0a2aa9d2959f2a50">Cy_SegLCD_Init</ref>(LCD0,<sp />&amp;config))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__seglcd__enums_1ggaf81660a051e97187b6617ae3f709b047a3fac00a061e1cd84f5e0e13289944cb7">CY_SEGLCD_SUCCESS</ref><sp />==<sp /><ref kindref="member" refid="group__group__seglcd__functions__frame_1ga7f6bb5486ade2f8fe07ae883b1e59be2">Cy_SegLCD_ClrFrame</ref>(LCD0,<sp />commons))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__functions__config_1ga55eb3d853498593da4eb3cddaca073df">Cy_SegLCD_Enable</ref>(LCD0);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Now<sp />the<sp />block<sp />generates<sp />LCD<sp />signals<sp />(all<sp />the<sp />pixels<sp />are<sp />off)<sp />and<sp />is<sp />ready<sp />to<sp />turn<sp />on<sp />any<sp />pixel</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*<sp />(or<sp />many<sp />pixels)<sp />using<sp />any<sp />of<sp />the<sp />frame/pixel/character/display<sp />management<sp />API<sp />functions.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="519" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_seglcd.c" bodystart="498" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_seglcd.h" line="565" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__seglcd__functions__config_1gafca1fc7166fcf468d61813ec0507fd7d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SegLCD_Disable</definition>
        <argsstring>(LCD_Type *base)</argsstring>
        <name>Cy_SegLCD_Disable</name>
        <param>
          <type>LCD_Type *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Disables the Segment LCD block. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The base pointer to the LCD instance registers.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Stop<sp />the<sp />LCD<sp />signal<sp />generation<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__seglcd__functions__config_1gafca1fc7166fcf468d61813ec0507fd7d">Cy_SegLCD_Disable</ref>(LCD0);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="561" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_seglcd.c" bodystart="534" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_seglcd.h" line="566" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>