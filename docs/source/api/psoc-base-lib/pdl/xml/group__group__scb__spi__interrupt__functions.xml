<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__scb__spi__interrupt__functions" kind="group">
    <compoundname>group_scb_spi_interrupt_functions</compoundname>
    <title>Interrupt</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__scb__spi__interrupt__functions_1ga0adeb497479d79c9ecea8169cfaff114" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_SPI_Interrupt</definition>
        <argsstring>(CySCB_Type *base, cy_stc_scb_spi_context_t *context)</argsstring>
        <name>Cy_SCB_SPI_Interrupt</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__spi__context__t">cy_stc_scb_spi_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>This is the interrupt function for the SCB configured in the SPI mode. </para>        </briefdescription>
        <detaileddescription>
<para>This function must be called inside the user-defined interrupt service routine for <ref kindref="member" refid="group__group__scb__spi__high__level__functions_1ga6fdbb98ef7faddc5025fab510fb1617e">Cy_SCB_SPI_Transfer</ref> to work.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__spi__context__t">cy_stc_scb_spi_context_t</ref> allocated by the user. The structure is used during the SPI operation for internal configuration and data retention. The user must not modify anything in this structure. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="883" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_spi.c" bodystart="807" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="623" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__interrupt__functions_1ga5594391e0d6a0b020355761826de2e78" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SCB_SPI_RegisterCallback</definition>
        <argsstring>(CySCB_Type const *base, cy_cb_scb_spi_handle_events_t callback, cy_stc_scb_spi_context_t *context)</argsstring>
        <name>Cy_SCB_SPI_RegisterCallback</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__scb__spi__data__structures_1gaa02c98c323acc350f3b4780f7833785f">cy_cb_scb_spi_handle_events_t</ref></type>
          <declname>callback</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__spi__context__t">cy_stc_scb_spi_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Registers a callback function, which notifies that <ref kindref="compound" refid="group__group__scb__spi__macros__callback__events">SPI Callback Events</ref> occurred in the <ref kindref="member" refid="group__group__scb__spi__interrupt__functions_1ga0adeb497479d79c9ecea8169cfaff114">Cy_SCB_SPI_Interrupt</ref>. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>callback</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the callback function. See <ref kindref="member" refid="group__group__scb__spi__data__structures_1gaa02c98c323acc350f3b4780f7833785f">cy_cb_scb_spi_handle_events_t</ref> for the function prototype.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__spi__context__t">cy_stc_scb_spi_context_t</ref> allocated by the user. The structure is used during the SPI operation for internal configuration and data retention. The user should not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;To remove the callback, pass NULL as the pointer to the callback function. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1453" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" bodystart="1446" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="625" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>