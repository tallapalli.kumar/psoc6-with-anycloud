<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__sd__host__high__level__functions" kind="group">
    <compoundname>group_sd_host_high_level_functions</compoundname>
    <title>High-Level</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__sd__host__high__level__functions_1ga9d72dcc2426210eef77fa5b95eefb986" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__sd__host__enums_1gabbf5bb6a131cd8b9c529b7e36b35fe23">cy_en_sd_host_status_t</ref></type>
        <definition>cy_en_sd_host_status_t Cy_SD_Host_InitCard</definition>
        <argsstring>(SDHC_Type *base, cy_stc_sd_host_sd_card_config_t *config, cy_stc_sd_host_context_t *context)</argsstring>
        <name>Cy_SD_Host_InitCard</name>
        <param>
          <type>SDHC_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__sd__host__sd__card__config__t">cy_stc_sd_host_sd_card_config_t</ref> *</type>
          <declname>config</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__sd__host__context__t">cy_stc_sd_host_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Initializes a card if it is connected. </para>        </briefdescription>
        <detaileddescription>
<para>After this function is called, the card is in the transfer state.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>*base</parametername>
</parameternamelist>
<parameterdescription>
<para>The SD host registers structure pointer.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>*config</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SD card configuration structure.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__sd__host__context__t">cy_stc_sd_host_context_t</ref> allocated by the user. The structure is used during the SD host operation for internal configuration and data retention. The user must not modify anything in this structure. If only the SD host functions which do not require context will be used, pass NULL as the pointer to the context.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__sd__host__enums_1gabbf5bb6a131cd8b9c529b7e36b35fe23">cy_en_sd_host_status_t</ref></para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;When lowVoltageSignaling is True, this function negotiates with the card to change the bus signaling level to 1.8V. The dedicated io_volt_sel pin is used to change the regulator supplying voltage to the VDDIO of the SD block in order to operate at 1.8V. To configure the custom IO pin in order to control (using the GPIO driver) the regulator supplying voltage, the user must implement weak &lt;a href="group__group__sd__host__low__level__functions.html#group__group__sd__host__low__level__functions_1ga307ae5aea7a503f9cac590dd895a8d36"&gt;Cy_SD_Host_ChangeIoVoltage()&lt;/a&gt;. Also, this function must set the SIGNALING_EN bit of the SDHC_CORE_HOST_CTRL2_R register when ioVoltage = CY_SD_HOST_IO_VOLT_1_8V.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;After calling this function, the SD Host is configured in Default Speed mode (for the SD card), or SDR12 Speed mode (when lowVoltageSignaling is true), or eMMC legacy (for the eMMC card) with SD clock = 25 MHz. The Power Limit and Driver Strength functions of the CMD6 command are set into the default state (0.72 W and Type B). It is the user's responsibility to set Power Limit and Driver Strength depending on the capacitance load of the host system. To change Speed mode, the user must call the &lt;a href="group__group__sd__host__low__level__functions.html#group__group__sd__host__low__level__functions_1ga0b48a86672163e3be31362d7b3de0730"&gt;Cy_SD_Host_SetBusSpeedMode()&lt;/a&gt; and Cy_SD_Host_SdCardChangeClock() functions. Additionally, SD SDR25, SD SDR50, eMMC High Speed SDR modes require settings CLOCK_OUT_DLY and CLOCK_IN_DLY bit-fields of the GP_OUT_R register. For more information about Speed modes, refer to Part 1 Physical Layer SD Specification. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="850" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sd_host.c" bodystart="619" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sd_host.h" line="1639" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sd__host__high__level__functions_1ga651eeea3b4c0a433db932db34ba7a222" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__sd__host__enums_1gabbf5bb6a131cd8b9c529b7e36b35fe23">cy_en_sd_host_status_t</ref></type>
        <definition>cy_en_sd_host_status_t Cy_SD_Host_Read</definition>
        <argsstring>(SDHC_Type *base, cy_stc_sd_host_write_read_config_t *config, cy_stc_sd_host_context_t const *context)</argsstring>
        <name>Cy_SD_Host_Read</name>
        <param>
          <type>SDHC_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__sd__host__write__read__config__t">cy_stc_sd_host_write_read_config_t</ref> *</type>
          <declname>config</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__sd__host__context__t">cy_stc_sd_host_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Reads single- or multiple-block data from the SD card / eMMC. </para>        </briefdescription>
        <detaileddescription>
<para>If DMA is not used this function blocks until all data is read. If DMA is used all data is read when the Transfer complete event is set. It is the user responsibility to check and reset the transfer complete event (using <ref kindref="member" refid="group__group__sd__host__interrupt__functions_1gad4a09951afc276a483c93e5e158528f3">Cy_SD_Host_GetNormalInterruptStatus</ref> and <ref kindref="member" refid="group__group__sd__host__interrupt__functions_1ga95975e199a0669a600e2fe5ada69b892">Cy_SD_Host_ClearNormalInterruptStatus</ref> functions).</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>*base</parametername>
</parameternamelist>
<parameterdescription>
<para>The SD host registers structure pointer.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>*config</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SD card read-write structure.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__sd__host__context__t">cy_stc_sd_host_context_t</ref> allocated by the user. The structure is used during the SD host operation for internal configuration and data retention. The user must not modify anything in this structure. If only the SD host functions which do not require context will be used, pass NULL as the pointer to the context.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__sd__host__enums_1gabbf5bb6a131cd8b9c529b7e36b35fe23">cy_en_sd_host_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="979" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sd_host.c" bodystart="881" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sd_host.h" line="1642" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sd__host__high__level__functions_1gaacf4fbad1af632a2b7fe82980b15e8ae" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__sd__host__enums_1gabbf5bb6a131cd8b9c529b7e36b35fe23">cy_en_sd_host_status_t</ref></type>
        <definition>cy_en_sd_host_status_t Cy_SD_Host_Write</definition>
        <argsstring>(SDHC_Type *base, cy_stc_sd_host_write_read_config_t *config, cy_stc_sd_host_context_t const *context)</argsstring>
        <name>Cy_SD_Host_Write</name>
        <param>
          <type>SDHC_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__sd__host__write__read__config__t">cy_stc_sd_host_write_read_config_t</ref> *</type>
          <declname>config</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__sd__host__context__t">cy_stc_sd_host_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Writes single- or multiple-block data to the SD card / eMMC. </para>        </briefdescription>
        <detaileddescription>
<para>If DMA is not used this function blocks until all data is written. If DMA is used all data is written when the Transfer complete event is set. It is the user responsibility to check and reset the transfer complete event (using <ref kindref="member" refid="group__group__sd__host__interrupt__functions_1gad4a09951afc276a483c93e5e158528f3">Cy_SD_Host_GetNormalInterruptStatus</ref> and <ref kindref="member" refid="group__group__sd__host__interrupt__functions_1ga95975e199a0669a600e2fe5ada69b892">Cy_SD_Host_ClearNormalInterruptStatus</ref> functions).</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>*base</parametername>
</parameternamelist>
<parameterdescription>
<para>The SD host registers structure pointer.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>*config</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SD card read-write structure.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__sd__host__context__t">cy_stc_sd_host_context_t</ref> allocated by the user. The structure is used during the SD host operation for internal configuration and data retention. The user must not modify anything in this structure. If only the SD host functions which do not require context will be used, pass NULL as the pointer to the context.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__sd__host__enums_1gabbf5bb6a131cd8b9c529b7e36b35fe23">cy_en_sd_host_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1109" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sd_host.c" bodystart="1010" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sd_host.h" line="1645" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sd__host__high__level__functions_1gabfb5430ccafd3a7a84b3bdf7eb106c98" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__sd__host__enums_1gabbf5bb6a131cd8b9c529b7e36b35fe23">cy_en_sd_host_status_t</ref></type>
        <definition>cy_en_sd_host_status_t Cy_SD_Host_Erase</definition>
        <argsstring>(SDHC_Type *base, uint32_t startAddr, uint32_t endAddr, cy_en_sd_host_erase_type_t eraseType, cy_stc_sd_host_context_t const *context)</argsstring>
        <name>Cy_SD_Host_Erase</name>
        <param>
          <type>SDHC_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>startAddr</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>endAddr</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__sd__host__enums_1gafcc446fe80b86a86f74ba7794d1e3a8d">cy_en_sd_host_erase_type_t</ref></type>
          <declname>eraseType</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__sd__host__context__t">cy_stc_sd_host_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Erases the number block data of the SD card / eMMC. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function starts the erase operation end exits. It is the user's responsibility to check when the erase operation completes. The erase operation completes when ref\ Cy_SD_Host_GetCardStatus returns the status value where both ready-for-data (CY_SD_HOST_CMD13_READY_FOR_DATA) and card-transition (CY_SD_HOST_CARD_TRAN) bits are set. Also it is the user's responsibility to clear the CY_SD_HOST_CMD_COMPLETE flag after calling this function.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>*base</parametername>
</parameternamelist>
<parameterdescription>
<para>The SD host registers structure pointer.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>startAddr</parametername>
</parameternamelist>
<parameterdescription>
<para>The address to start erasing from.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>endAddr</parametername>
</parameternamelist>
<parameterdescription>
<para>The address to stop erasing.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>eraseType</parametername>
</parameternamelist>
<parameterdescription>
<para>Specifies the erase type (FULE, DISCARD).</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__sd__host__context__t">cy_stc_sd_host_context_t</ref> allocated by the user. The structure is used during the SD host operation for internal configuration and data retention. The user must not modify anything in this structure. If only the SD host functions which do not require context will be used, pass NULL as the pointer to the context.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__sd__host__enums_1gabbf5bb6a131cd8b9c529b7e36b35fe23">cy_en_sd_host_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1266" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sd_host.c" bodystart="1149" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sd_host.h" line="1648" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>