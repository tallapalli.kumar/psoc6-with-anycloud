<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__sysclk__pm__funcs" kind="group">
    <compoundname>group_sysclk_pm_funcs</compoundname>
    <title>Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__sysclk__pm__funcs_1ga34bd0e837cdf7d0953a7e0f5219905b5" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref></type>
        <definition>cy_en_syspm_status_t Cy_SysClk_DeepSleepCallback</definition>
        <argsstring>(cy_stc_syspm_callback_params_t *callbackParams, cy_en_syspm_callback_mode_t mode)</argsstring>
        <name>Cy_SysClk_DeepSleepCallback</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref> *</type>
          <declname>callbackParams</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></type>
          <declname>mode</declname>
        </param>
        <briefdescription>
<para>Callback function to be used when entering system Deep Sleep mode. </para>        </briefdescription>
        <detaileddescription>
<para>This function is applicable if:<itemizedlist>
<listitem><para>The FLL is enabled</para></listitem><listitem><para>The PLL is enabled and is driven by ECO</para></listitem></itemizedlist>
</para><para>This function performs the following:</para><para><orderedlist>
<listitem><para>Before entering Deep Sleep, the clock configuration is saved in SRAM. If the FLL/PLL source is the ECO, then the FLL/PLL is bypassed and the source is changed to IMO. <linebreak />
 If the FLL is enabled - it is just bypassed.</para></listitem><listitem><para>Upon wakeup from Deep Sleep, the function waits for ECO stabilization, then restores the configuration and waits for the FLL/PLL to regain their frequency locks. <linebreak />
 If ECO is not used and FLL is enabled - it waits for FLL lock and unbypasses it.</para></listitem></orderedlist>
</para><para>The function prevents entry into Deep Sleep mode if the measurement counters are currently counting; see <ref kindref="member" refid="group__group__sysclk__calclk__funcs_1ga0a87e123411d5711344780ddfa492f37">Cy_SysClk_StartClkMeasurementCounters</ref>.</para><para>This function can be called during execution of <ref kindref="member" refid="group__group__syspm__functions__power_1ga5150c28fe4d2626720c1fbf74b3111ca">Cy_SysPm_CpuEnterDeepSleep</ref>. To do so, register this function as a callback before calling <ref kindref="member" refid="group__group__syspm__functions__power_1ga5150c28fe4d2626720c1fbf74b3111ca">Cy_SysPm_CpuEnterDeepSleep</ref> - specify <ref kindref="member" refid="group__group__syspm__data__enumerates_1gga8c2960c0164ead1cfa86e7d6846b6ff0abc51d74deff0ceea4304b01b2d57bd9d">CY_SYSPM_DEEPSLEEP</ref> as the callback type and call <ref kindref="member" refid="group__group__syspm__functions__callback_1ga0d58b00c8dc764a6371590f70e2f73c7">Cy_SysPm_RegisterCallback</ref>.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function is recommended to be the last callback that is registered. Doing so minimizes the time spent on low power mode entry and exit. &lt;br /&gt;This function implements all four SysPm callback modes &lt;a href="group__group__syspm__data__enumerates.html#group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63"&gt;cy_en_syspm_callback_mode_t&lt;/a&gt;. So the &lt;a href="structcy__stc__syspm__callback__t.html#structcy__stc__syspm__callback__t_1a1b49c2454ac29e52261e28eb9071413b"&gt;cy_stc_syspm_callback_t::skipMode&lt;/a&gt; must be set to 0UL. &lt;br /&gt;This function does not support such cases as, for example, FLL is enabled but bypassed by user code before entering Deep Sleep. &lt;br /&gt;You can use this callback implementation as an example to design custom low-power callbacks for certain user application.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>callbackParams</parametername>
</parameternamelist>
<parameterdescription>
<para>structure with the syspm callback parameters, see <ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mode</parametername>
</parameternamelist>
<parameterdescription>
<para>Callback mode, see <ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Error / status code; see <ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref>. Pass if not doing a clock measurement, otherwise Fail. Timeout if timeout waiting for ECO, FLL or PLL to get stable / regain its frequency lock. CY_SYSCLK_INVALID_STATE - ECO already enabled For the PSoC 64 devices there are possible situations when function returns the PRA error status code. This is because for PSoC 64 devices the function uses the PRA driver to change the protected registers. Refer to <ref kindref="member" refid="group__group__pra__enums_1ga60be13e12e82986f8c0d6c6a6d4f12c5">cy_en_pra_status_t</ref> for more details.</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />application<sp />uses<sp />FLL/PLL<sp />sourced<sp />from<sp />the<sp />ECO.<sp />The<sp />device</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />needs<sp />to<sp />enter<sp />chip<sp />Deep<sp />Sleep.<sp />The<sp />ECO<sp />and<sp />FLL/PLL<sp />must<sp />be</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />suitably<sp />prepared<sp />before<sp />entering<sp />low<sp />power<sp />mode.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Register<sp />all<sp />the<sp />Deep<sp />Sleep<sp />callbacks<sp />for<sp />other<sp />peripherals.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp />The<sp />SysClk<sp />callback<sp />is<sp />recommended<sp />to<sp />be<sp />registered<sp />last<sp />to<sp />minimize<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp />the<sp />low<sp />power<sp />mode<sp />entry<sp />and<sp />wakeup<sp />timings.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref><sp />clkCallbackParams<sp />=<sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.base<sp /><sp /><sp /><sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />NULL,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.context<sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />NULL</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__syspm__callback__t">cy_stc_syspm_callback_t</ref><sp />clkCallback<sp />=<sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.callback<sp /><sp /><sp /><sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />&amp;<ref kindref="member" refid="group__group__sysclk__pm__funcs_1ga34bd0e837cdf7d0953a7e0f5219905b5">Cy_SysClk_DeepSleepCallback</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.type<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__syspm__data__enumerates_1gga8c2960c0164ead1cfa86e7d6846b6ff0abc51d74deff0ceea4304b01b2d57bd9d">CY_SYSPM_DEEPSLEEP</ref>,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.skipMode<sp /><sp /><sp /><sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />0UL,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.callbackParams<sp />=*/</highlight><highlight class="normal"><sp />&amp;clkCallbackParams,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.prevItm<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />NULL,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.nextItm<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />NULL,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.order<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />0</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Register<sp />the<sp />clock<sp />callback<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(!<ref kindref="member" refid="group__group__syspm__functions__callback_1ga0d58b00c8dc764a6371590f70e2f73c7">Cy_SysPm_RegisterCallback</ref>(&amp;clkCallback))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Could<sp />not<sp />register<sp />callback.<sp />Examine<sp />the<sp />number<sp />of<sp />registered<sp />callbacks<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Ensure<sp />that<sp />the<sp />clock<sp />measurement<sp />is<sp />not<sp />running<sp />and<sp />enter<sp />chip<sp />Deep<sp />Sleep<sp />*/</highlight><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2120" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_sysclk.c" bodystart="1923" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="1631" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>