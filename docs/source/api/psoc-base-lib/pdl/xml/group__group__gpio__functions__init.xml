<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__gpio__functions__init" kind="group">
    <compoundname>group_gpio_functions_init</compoundname>
    <title>Initialization Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__gpio__functions__init_1gad61553f65d4e6bd827eb6464a7913461" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__gpio__enums_1ga0ba12c6f18fa9e356ceea0218beb7259">cy_en_gpio_status_t</ref></type>
        <definition>cy_en_gpio_status_t Cy_GPIO_Pin_Init</definition>
        <argsstring>(GPIO_PRT_Type *base, uint32_t pinNum, const cy_stc_gpio_pin_config_t *config)</argsstring>
        <name>Cy_GPIO_Pin_Init</name>
        <param>
          <type><ref kindref="compound" refid="struct_g_p_i_o___p_r_t___type">GPIO_PRT_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>pinNum</declname>
        </param>
        <param>
          <type>const <ref kindref="compound" refid="structcy__stc__gpio__pin__config__t">cy_stc_gpio_pin_config_t</ref> *</type>
          <declname>config</declname>
        </param>
        <briefdescription>
<para>Initializes all pin configuration settings for the specified pin. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the pin's port register base address</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>pinNum</parametername>
</parameternamelist>
<parameterdescription>
<para>Position of the pin bit-field within the port register</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>config</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the pin config structure base address</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Initialization status</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function modifies port registers in read-modify-write operations. It is not thread safe as the resource is shared among multiple pins on a port.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__gpio__pin__config__t">cy_stc_gpio_pin_config_t</ref><sp />pinConfig<sp />=<sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.outVal<sp />=*/</highlight><highlight class="normal"><sp />1UL,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Output<sp />=<sp />High<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.driveMode<sp />=*/</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__gpio__drive_modes_1ga822f7d73072811b69a754d70806247b9">CY_GPIO_DM_PULLUP</ref>,<sp /></highlight><highlight class="comment">/*<sp />Resistive<sp />pull-up,<sp />input<sp />buffer<sp />on<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.hsiom<sp />=*/</highlight><highlight class="normal"><sp />P0_3_GPIO,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Software<sp />controlled<sp />pin<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.intEdge<sp />=*/</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__gpio__interrupt_trigger_1gafbaa3f5dff9b5689cdb43bb07c7c6fef">CY_GPIO_INTR_RISING</ref>,<sp /></highlight><highlight class="comment">/*<sp />Rising<sp />edge<sp />interrupt<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.intMask<sp />=*/</highlight><highlight class="normal"><sp />1UL,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />port<sp />interrupt<sp />for<sp />this<sp />pin<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.vtrip<sp />=*/</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__gpio__vtrip_1ga0eb9d3f41338feae28103e8a8c302ba0">CY_GPIO_VTRIP_CMOS</ref>,<sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />CMOS<sp />voltage<sp />trip<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.slewRate<sp />=*/</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__gpio__slew_rate_1gabb43620358101afd1663376cb3ba19b4">CY_GPIO_SLEW_FAST</ref>,<sp /><sp /></highlight><highlight class="comment">/*<sp />Fast<sp />slew<sp />rate<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.driveSel<sp />=*/</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__gpio__drive_strength_1ga203ca4e69600122e76e45dbaa2f013fb">CY_GPIO_DRIVE_FULL</ref>,<sp /></highlight><highlight class="comment">/*<sp />Full<sp />drive<sp />strength<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.vregEn<sp />=*/</highlight><highlight class="normal"><sp />0UL,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />SIO-specific<sp />setting<sp />-<sp />ignored<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.ibufMode<sp />=*/</highlight><highlight class="normal"><sp />0UL,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />SIO-specific<sp />setting<sp />-<sp />ignored<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.vtripSel<sp />=*/</highlight><highlight class="normal"><sp />0UL,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />SIO-specific<sp />setting<sp />-<sp />ignored<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.vrefSel<sp />=*/</highlight><highlight class="normal"><sp />0UL,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />SIO-specific<sp />setting<sp />-<sp />ignored<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.vohSel<sp />=*/</highlight><highlight class="normal"><sp />0UL<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />SIO-specific<sp />setting<sp />-<sp />ignored<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />};</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />pin<sp />P0.3<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__gpio__enums_1gga0ba12c6f18fa9e356ceea0218beb7259ac12fe3dac92e654617ce1a0cda34c0b0">CY_GPIO_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__gpio__functions__init_1gad61553f65d4e6bd827eb6464a7913461">Cy_GPIO_Pin_Init</ref>(P0_3_PORT,<sp />P0_3_NUM,<sp />&amp;pinConfig))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="123" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_gpio.c" bodystart="70" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_gpio.h" line="549" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__gpio__functions__init_1gaece2166923613cf7abb536d8a05bfd45" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__gpio__enums_1ga0ba12c6f18fa9e356ceea0218beb7259">cy_en_gpio_status_t</ref></type>
        <definition>cy_en_gpio_status_t Cy_GPIO_Port_Init</definition>
        <argsstring>(GPIO_PRT_Type *base, const cy_stc_gpio_prt_config_t *config)</argsstring>
        <name>Cy_GPIO_Port_Init</name>
        <param>
          <type><ref kindref="compound" refid="struct_g_p_i_o___p_r_t___type">GPIO_PRT_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>const <ref kindref="compound" refid="structcy__stc__gpio__prt__config__t">cy_stc_gpio_prt_config_t</ref> *</type>
          <declname>config</declname>
        </param>
        <briefdescription>
<para>Initialize a complete port of pins from a single init structure. </para>        </briefdescription>
        <detaileddescription>
<para>The configuration structure used in this function has a 1:1 mapping to the GPIO and HSIOM registers. Refer to the device Technical Reference Manual (TRM) for the register details on how to populate them.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the pin's port register base address</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>config</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the pin config structure base address</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Initialization status</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;If using the PSoC Creator IDE, there is no need to initialize the pins when using the GPIO component on the schematic. Ports are configured in Cy_SystemInit() before main() entry.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__gpio__prt__config__t">cy_stc_gpio_prt_config_t</ref><sp />portConfig<sp />=<sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.out<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />0x00000008u,<sp /><sp /></highlight><highlight class="comment">/*<sp />PX.3<sp />output<sp />=<sp />1<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.intrMask<sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />0x00000008u,<sp /><sp /></highlight><highlight class="comment">/*<sp />PX.3<sp />interrupt<sp />enabled<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.intrCfg<sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />0x00000040u,<sp /><sp /></highlight><highlight class="comment">/*<sp />PX.3<sp />rising<sp />edge<sp />interrupt<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.cfg<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />0x0000A000u,<sp /><sp /></highlight><highlight class="comment">/*<sp />PX.3<sp />resistive<sp />pull-up<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.cfgIn<sp /><sp /><sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />0x00000000u,<sp /><sp /></highlight><highlight class="comment">/*<sp />PX[7:0]<sp />CMOS<sp />trip<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.cfgOut<sp /><sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />0x00000000u,<sp /><sp /></highlight><highlight class="comment">/*<sp />PX[7:0]<sp />Fast<sp />slew<sp />rate,<sp />full<sp />drive<sp />strength<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.cfgSIO<sp /><sp /><sp /><sp /><sp />=*/</highlight><highlight class="normal"><sp />0x00000000u,<sp /><sp /></highlight><highlight class="comment">/*<sp />PX[7:0]<sp />ignored<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.sel0Active<sp />=*/</highlight><highlight class="normal"><sp />0x00000000u,<sp /><sp /></highlight><highlight class="comment">/*<sp />PX[3:0]<sp />Software<sp />controlled<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*.sel1Active<sp />=*/</highlight><highlight class="normal"><sp />0x00000000u,<sp /><sp /></highlight><highlight class="comment">/*<sp />PX[7:4]<sp />Software<sp />controlled<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />GPIO<sp />port<sp />0<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__gpio__enums_1gga0ba12c6f18fa9e356ceea0218beb7259ac12fe3dac92e654617ce1a0cda34c0b0">CY_GPIO_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__gpio__functions__init_1gaece2166923613cf7abb536d8a05bfd45">Cy_GPIO_Port_Init</ref>(GPIO_PRT0,<sp />&amp;portConfig))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="187" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_gpio.c" bodystart="154" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_gpio.h" line="550" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__gpio__functions__init_1gaf57c501727276013d3e8974a9fb7d0a7" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_GPIO_Pin_FastInit</definition>
        <argsstring>(GPIO_PRT_Type *base, uint32_t pinNum, uint32_t driveMode, uint32_t outVal, en_hsiom_sel_t hsiom)</argsstring>
        <name>Cy_GPIO_Pin_FastInit</name>
        <param>
          <type><ref kindref="compound" refid="struct_g_p_i_o___p_r_t___type">GPIO_PRT_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>pinNum</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>driveMode</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>outVal</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__gpio__enums_1ga678dc02e490d04efdcfec78648899ce4">en_hsiom_sel_t</ref></type>
          <declname>hsiom</declname>
        </param>
        <briefdescription>
<para>Initialize the most common configuration settings for all pin types. </para>        </briefdescription>
        <detaileddescription>
<para>These include, drive mode, initial output value, and HSIOM connection.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the pin's port register base address</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>pinNum</parametername>
</parameternamelist>
<parameterdescription>
<para>Position of the pin bit-field within the port register</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>driveMode</parametername>
</parameternamelist>
<parameterdescription>
<para>Pin drive mode. Options are detailed in <ref kindref="compound" refid="group__group__gpio__drive_modes">Pin drive mode</ref> macros</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>outVal</parametername>
</parameternamelist>
<parameterdescription>
<para>Logic state of the output buffer driven to the pin (1 or 0)</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>hsiom</parametername>
</parameternamelist>
<parameterdescription>
<para>HSIOM input selection</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function modifies port registers in read-modify-write operations. It is not thread safe as the resource is shared among multiple pins on a port. You can use the &lt;a href="group__group__syslib__functions.html#group__group__syslib__functions_1gae679f15a1702c159c105b596a8801105"&gt;Cy_SysLib_EnterCriticalSection()&lt;/a&gt; and &lt;a href="group__group__syslib__functions.html#group__group__syslib__functions_1ga8c16343c075610b2888b0693f972b555"&gt;Cy_SysLib_ExitCriticalSection()&lt;/a&gt; functions to ensure that &lt;a href="group__group__gpio__functions__init.html#group__group__gpio__functions__init_1gaf57c501727276013d3e8974a9fb7d0a7"&gt;Cy_GPIO_Pin_FastInit()&lt;/a&gt; function execution is not interrupted.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Quickly<sp />initialize<sp />pin<sp />P0.3<sp />(e.g.<sp />quickly<sp />set<sp />up<sp />a<sp />test<sp />LED)<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__gpio__functions__init_1gaf57c501727276013d3e8974a9fb7d0a7">Cy_GPIO_Pin_FastInit</ref>(P0_3_PORT,<sp />P0_3_NUM,<sp /><ref kindref="member" refid="group__group__gpio__drive_modes_1ga822f7d73072811b69a754d70806247b9">CY_GPIO_DM_PULLUP</ref>,<sp />1UL,<sp />P0_3_GPIO);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="241" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_gpio.c" bodystart="224" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_gpio.h" line="551" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__gpio__functions__init_1gad996e3e1fc44d28611e9b83fc025994b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_GPIO_Port_Deinit</definition>
        <argsstring>(GPIO_PRT_Type *base)</argsstring>
        <name>Cy_GPIO_Port_Deinit</name>
        <param>
          <type><ref kindref="compound" refid="struct_g_p_i_o___p_r_t___type">GPIO_PRT_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Reset a complete port of pins back to power on reset defaults. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the pin's port register base address</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />GPIO<sp />port<sp />0<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__gpio__functions__init_1gaece2166923613cf7abb536d8a05bfd45">Cy_GPIO_Port_Init</ref>(GPIO_PRT0,<sp />&amp;portConfig);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />port<sp />is<sp />no<sp />longer<sp />used<sp />or<sp />needs<sp />to<sp />be<sp />disabled<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Reset<sp />the<sp />GPIO<sp />port<sp />0<sp />to<sp />Power-On-Reset<sp />values<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__gpio__functions__init_1gad996e3e1fc44d28611e9b83fc025994b">Cy_GPIO_Port_Deinit</ref>(GPIO_PRT0);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="274" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_gpio.c" bodystart="257" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_gpio.h" line="552" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__gpio__functions__init_1ga83a06264feed0e1042671a74339ea155" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_GPIO_SetHSIOM</definition>
        <argsstring>(GPIO_PRT_Type *base, uint32_t pinNum, en_hsiom_sel_t value)</argsstring>
        <name>Cy_GPIO_SetHSIOM</name>
        <param>
          <type><ref kindref="compound" refid="struct_g_p_i_o___p_r_t___type">GPIO_PRT_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>pinNum</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__gpio__enums_1ga678dc02e490d04efdcfec78648899ce4">en_hsiom_sel_t</ref></type>
          <declname>value</declname>
        </param>
        <briefdescription>
<para>Configures the HSIOM connection to the pin. </para>        </briefdescription>
        <detaileddescription>
<para>Connects the specified High-Speed Input Output Multiplexer (HSIOM) selection to the pin.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the pin's port register base address</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>pinNum</parametername>
</parameternamelist>
<parameterdescription>
<para>Position of the pin bit-field within the port register</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>value</parametername>
</parameternamelist>
<parameterdescription>
<para>HSIOM input selection</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function modifies a port register in a read-modify-write operation. It is not thread safe as the resource is shared among multiple pins on a port.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Retrieve<sp />the<sp />HSIOM<sp />connection<sp />to<sp />P0.3<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(P0_3_GPIO<sp />==<sp /><ref kindref="member" refid="group__group__gpio__functions__init_1ga5f0929f9d81dc941016b55e227c68a69">Cy_GPIO_GetHSIOM</ref>(P0_3_PORT,<sp />P0_3_NUM))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Connect<sp />P0.3<sp />to<sp />AMUXA<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__gpio__functions__init_1ga83a06264feed0e1042671a74339ea155">Cy_GPIO_SetHSIOM</ref>(P0_3_PORT,<sp />P0_3_NUM,<sp />P0_3_AMUXA);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="680" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_gpio.h" bodystart="657" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_gpio.h" line="553" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__gpio__functions__init_1ga5f0929f9d81dc941016b55e227c68a69" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__gpio__enums_1ga678dc02e490d04efdcfec78648899ce4">en_hsiom_sel_t</ref></type>
        <definition>__STATIC_INLINE en_hsiom_sel_t Cy_GPIO_GetHSIOM</definition>
        <argsstring>(GPIO_PRT_Type *base, uint32_t pinNum)</argsstring>
        <name>Cy_GPIO_GetHSIOM</name>
        <param>
          <type><ref kindref="compound" refid="struct_g_p_i_o___p_r_t___type">GPIO_PRT_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>pinNum</declname>
        </param>
        <briefdescription>
<para>Returns the current HSIOM multiplexer connection to the pin. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the pin's port register base address</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>pinNum</parametername>
</parameternamelist>
<parameterdescription>
<para>Position of the pin bit-field within the port register</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>HSIOM input selection</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Retrieve<sp />the<sp />HSIOM<sp />connection<sp />to<sp />P0.3<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(P0_3_GPIO<sp />==<sp /><ref kindref="member" refid="group__group__gpio__functions__init_1ga5f0929f9d81dc941016b55e227c68a69">Cy_GPIO_GetHSIOM</ref>(P0_3_PORT,<sp />P0_3_NUM))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Connect<sp />P0.3<sp />to<sp />AMUXA<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__gpio__functions__init_1ga83a06264feed0e1042671a74339ea155">Cy_GPIO_SetHSIOM</ref>(P0_3_PORT,<sp />P0_3_NUM,<sp />P0_3_AMUXA);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="724" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_gpio.h" bodystart="702" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_gpio.h" line="554" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__gpio__functions__init_1gab0eeafea970eadb4e147ff6d0e3804dc" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="compound" refid="struct_g_p_i_o___p_r_t___type">GPIO_PRT_Type</ref> *</type>
        <definition>__STATIC_INLINE GPIO_PRT_Type * Cy_GPIO_PortToAddr</definition>
        <argsstring>(uint32_t portNum)</argsstring>
        <name>Cy_GPIO_PortToAddr</name>
        <param>
          <type>uint32_t</type>
          <declname>portNum</declname>
        </param>
        <briefdescription>
<para>Retrieves the port address based on the given port number. </para>        </briefdescription>
        <detaileddescription>
<para>This is a helper function to calculate the port base address when given a port number. It is to be used when pin access needs to be calculated at runtime.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>portNum</parametername>
</parameternamelist>
<parameterdescription>
<para>Port number</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Base address of the port register structure</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />portCnt<sp /><sp />=<sp />0UL;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />pinCnt<sp /><sp /><sp />=<sp />3UL;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />pinState<sp />=<sp />1UL;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Calculate<sp />the<sp />location<sp />and<sp />state<sp />of<sp />a<sp />pin<sp />to<sp />control<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Control<sp />which<sp />pin<sp />to<sp />set/clear<sp />using<sp />parameters<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__gpio__functions__gpio_1ga849c813d6771bf8d3c59b89b28a07bca">Cy_GPIO_Write</ref>(<ref kindref="member" refid="group__group__gpio__functions__init_1gab0eeafea970eadb4e147ff6d0e3804dc">Cy_GPIO_PortToAddr</ref>(portCnt),<sp />pinCnt,<sp />pinState);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="761" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_gpio.h" bodystart="746" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_gpio.h" line="555" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>