<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__scb__spi__high__level__functions" kind="group">
    <compoundname>group_scb_spi_high_level_functions</compoundname>
    <title>High-Level</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__scb__spi__high__level__functions_1ga6fdbb98ef7faddc5025fab510fb1617e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__scb__spi__enums_1ga8ad22e7a296b64de0d41c983979d53b3">cy_en_scb_spi_status_t</ref></type>
        <definition>cy_en_scb_spi_status_t Cy_SCB_SPI_Transfer</definition>
        <argsstring>(CySCB_Type *base, void *txBuffer, void *rxBuffer, uint32_t size, cy_stc_scb_spi_context_t *context)</argsstring>
        <name>Cy_SCB_SPI_Transfer</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>txBuffer</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>rxBuffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__spi__context__t">cy_stc_scb_spi_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>This function starts an SPI transfer operation. </para>        </briefdescription>
        <detaileddescription>
<para>It configures transmit and receive buffers for an SPI transfer. If the data that will be received is not important, pass NULL as rxBuffer. If the data that will be transmitted is not important, pass NULL as txBuffer and then the <ref kindref="member" refid="group__group__scb__spi__macros_1ga01cb858faea2bc203ce96b9500d63776">CY_SCB_SPI_DEFAULT_TX</ref> is sent out as each data element. Note that passing NULL as rxBuffer and txBuffer are considered invalid cases.</para><para>After the function configures TX and RX interrupt sources, it returns and <ref kindref="member" refid="group__group__scb__spi__interrupt__functions_1ga0adeb497479d79c9ecea8169cfaff114">Cy_SCB_SPI_Interrupt</ref> manages further data transfer.</para><para><itemizedlist>
<listitem><para>In the master mode, the transfer operation starts after calling this function</para></listitem><listitem><para>In the slave mode, the transfer registers and will start when the master request arrives.</para></listitem></itemizedlist>
</para><para>When the transfer operation is completed (requested number of data elements sent and received), the <ref kindref="member" refid="group__group__scb__spi__macros__xfer__status_1gab024f7d6ed7cbaa36c80c904a6179de7">CY_SCB_SPI_TRANSFER_ACTIVE</ref> status is cleared and the <ref kindref="member" refid="group__group__scb__spi__macros__callback__events_1gafe7ca0de9c979f366dcd59bdd98f2165">CY_SCB_SPI_TRANSFER_CMPLT_EVENT</ref> event is generated.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>txBuffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer of the buffer with data to transmit. The element size is defined by the data type that depends on the configured TX data width.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>rxBuffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the buffer to store received data. The element size is defined by the data type that depends on the configured RX data width.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of data elements to transmit and receive.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__spi__context__t">cy_stc_scb_spi_context_t</ref> allocated by the user. The structure is used during the SPI operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__scb__spi__enums_1ga8ad22e7a296b64de0d41c983979d53b3">cy_en_scb_spi_status_t</ref></para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;The buffers must not be modified and must stay allocated until the end of the transfer.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;This function overrides all RX and TX FIFO interrupt sources and changes the RX and TX FIFO level. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="667" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_spi.c" bodystart="604" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="583" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__high__level__functions_1ga31e7821f7edc301ea6cf195da3c51498" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_SPI_AbortTransfer</definition>
        <argsstring>(CySCB_Type *base, cy_stc_scb_spi_context_t *context)</argsstring>
        <name>Cy_SCB_SPI_AbortTransfer</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__spi__context__t">cy_stc_scb_spi_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Aborts the current SPI transfer. </para>        </briefdescription>
        <detaileddescription>
<para>It disables the TX and RX interrupt sources, clears the TX and RX FIFOs and the status.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__spi__context__t">cy_stc_scb_spi_context_t</ref> allocated by the user. The structure is used during the SPI operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;If slave aborts transfer and the master is still transferring data, that data will be placed in the RX FIFO, and the TX underflow will be set. To drop data received into the RX FIFO, RX FIFO must be cleared when the transfer is complete. Otherwise, received data will be kept and copied to the buffer when &lt;a href="group__group__scb__spi__high__level__functions.html#group__group__scb__spi__high__level__functions_1ga6fdbb98ef7faddc5025fab510fb1617e"&gt;Cy_SCB_SPI_Transfer&lt;/a&gt; is called.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Side Effects</title><para>The transmit FIFO clear operation also clears the shift register, so that the shifter can be cleared in the middle of a data element transfer, corrupting it. The data element corruption means that all bits that have not been transmitted are transmitted as "ones" on the bus. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="723" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_spi.c" bodystart="701" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="585" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__high__level__functions_1ga95069817e25be749ae989e5d7131f8d0" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_SPI_GetTransferStatus</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_spi_context_t const *context)</argsstring>
        <name>Cy_SCB_SPI_GetTransferStatus</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__spi__context__t">cy_stc_scb_spi_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the status of the transfer operation started by <ref kindref="member" refid="group__group__scb__spi__high__level__functions_1ga6fdbb98ef7faddc5025fab510fb1617e">Cy_SCB_SPI_Transfer</ref>. </para>        </briefdescription>
        <detaileddescription>
<para>This status is a bit mask and the value returned may have a multiple-bit set.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__spi__context__t">cy_stc_scb_spi_context_t</ref> allocated by the user. The structure is used during the SPI operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="compound" refid="group__group__scb__spi__macros__xfer__status">SPI Transfer Status</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The status is cleared by calling &lt;a href="group__group__scb__spi__high__level__functions.html#group__group__scb__spi__high__level__functions_1ga6fdbb98ef7faddc5025fab510fb1617e"&gt;Cy_SCB_SPI_Transfer&lt;/a&gt; or &lt;a href="group__group__scb__spi__high__level__functions.html#group__group__scb__spi__high__level__functions_1ga31e7821f7edc301ea6cf195da3c51498"&gt;Cy_SCB_SPI_AbortTransfer&lt;/a&gt;. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="786" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_spi.c" bodystart="780" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="586" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__high__level__functions_1ga2736527b851afea1bc901581768ca566" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_SPI_GetNumTransfered</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_spi_context_t const *context)</argsstring>
        <name>Cy_SCB_SPI_GetNumTransfered</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__spi__context__t">cy_stc_scb_spi_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the number of data elements transferred since the last call to <ref kindref="member" refid="group__group__scb__spi__high__level__functions_1ga6fdbb98ef7faddc5025fab510fb1617e">Cy_SCB_SPI_Transfer</ref>. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the SPI SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__spi__context__t">cy_stc_scb_spi_context_t</ref> allocated by the user. The structure is used during the SPI operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The number of data elements transferred. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="752" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_spi.c" bodystart="746" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="587" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>