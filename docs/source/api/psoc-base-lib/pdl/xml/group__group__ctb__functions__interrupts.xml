<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ctb__functions__interrupts" kind="group">
    <compoundname>group_ctb_functions_interrupts</compoundname>
    <title>Interrupt Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__interrupts_1gaa05d09b4e77c18c5279aa3353875d85c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_CTB_GetInterruptStatus</definition>
        <argsstring>(const CTBM_Type *base, cy_en_ctb_opamp_sel_t compNum)</argsstring>
        <name>Cy_CTB_GetInterruptStatus</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga2175f16ceffc2774b50643be95f32137">cy_en_ctb_opamp_sel_t</ref></type>
          <declname>compNum</declname>
        </param>
        <briefdescription>
<para>Return the status of the interrupt when the configured comparator edge is detected. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>compNum</parametername>
</parameternamelist>
<parameterdescription>
<para><ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a715789db816ed8db3729da6e57aacdcb">CY_CTB_OPAMP_0</ref>, <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a9318dd62afb23fc0c4dac2abcfcea5f0">CY_CTB_OPAMP_1</ref>, or <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a4c859afffdcf6dcf16c4350906759488">CY_CTB_OPAMP_BOTH</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The interrupt status. If compNum is <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a4c859afffdcf6dcf16c4350906759488">CY_CTB_OPAMP_BOTH</ref>, cast the returned status to <ref kindref="member" refid="group__group__ctb__enums_1ga2175f16ceffc2774b50643be95f32137">cy_en_ctb_opamp_sel_t</ref> to determine which comparator edge (or both) was detected.<itemizedlist>
<listitem><para>0: Edge was not detected</para></listitem><listitem><para>Non-zero: Configured edge type was detected</para></listitem></itemizedlist>
</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Both<sp />opamps<sp />have<sp />been<sp />configured<sp />as<sp />comparators<sp />with<sp />interrupts<sp />enabled.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Retrieve<sp />the<sp />interrupt<sp />status<sp />to<sp />determine<sp />which<sp />interrupt<sp />was<sp />triggered.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />intrStatus;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intrStatus<sp />=<sp /><ref kindref="member" refid="group__group__ctb__functions__interrupts_1gaa05d09b4e77c18c5279aa3353875d85c">Cy_CTB_GetInterruptStatus</ref>(CTBM0,<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a4c859afffdcf6dcf16c4350906759488">CY_CTB_OPAMP_BOTH</ref>);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(CY_CTB_OPAMP_0<sp />==<sp />(intrStatus<sp />&amp;<sp />CY_CTB_OPAMP_0))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Do<sp />something<sp />when<sp />comparator<sp />0<sp />interrupt<sp />occurs.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a9318dd62afb23fc0c4dac2abcfcea5f0">CY_CTB_OPAMP_1</ref><sp />==<sp />(intrStatus<sp />&amp;<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a9318dd62afb23fc0c4dac2abcfcea5f0">CY_CTB_OPAMP_1</ref>))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Do<sp />something<sp />when<sp />comparator<sp />1<sp />interrupt<sp />occurs.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1254" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" bodystart="1249" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1100" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__interrupts_1gad8ad429bd10d7fae38c5a95a77785a60" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTB_ClearInterrupt</definition>
        <argsstring>(CTBM_Type *base, cy_en_ctb_opamp_sel_t compNum)</argsstring>
        <name>Cy_CTB_ClearInterrupt</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga2175f16ceffc2774b50643be95f32137">cy_en_ctb_opamp_sel_t</ref></type>
          <declname>compNum</declname>
        </param>
        <briefdescription>
<para>Clear the CTB comparator triggered interrupt. </para>        </briefdescription>
        <detaileddescription>
<para>The interrupt must be cleared with this function so that the hardware can set subsequent interrupts and those interrupts can be forwarded to the interrupt controller, if enabled.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>compNum</parametername>
</parameternamelist>
<parameterdescription>
<para><ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a715789db816ed8db3729da6e57aacdcb">CY_CTB_OPAMP_0</ref>, <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a9318dd62afb23fc0c4dac2abcfcea5f0">CY_CTB_OPAMP_1</ref>, or <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a4c859afffdcf6dcf16c4350906759488">CY_CTB_OPAMP_BOTH</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1282" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" bodystart="1274" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1101" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__interrupts_1gacaf04d0238203abed108dcc5ae7e765d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTB_SetInterrupt</definition>
        <argsstring>(CTBM_Type *base, cy_en_ctb_opamp_sel_t compNum)</argsstring>
        <name>Cy_CTB_SetInterrupt</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga2175f16ceffc2774b50643be95f32137">cy_en_ctb_opamp_sel_t</ref></type>
          <declname>compNum</declname>
        </param>
        <briefdescription>
<para>Force the CTB interrupt to trigger using software. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>compNum</parametername>
</parameternamelist>
<parameterdescription>
<para><ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a715789db816ed8db3729da6e57aacdcb">CY_CTB_OPAMP_0</ref>, <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a9318dd62afb23fc0c4dac2abcfcea5f0">CY_CTB_OPAMP_1</ref>, or <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a4c859afffdcf6dcf16c4350906759488">CY_CTB_OPAMP_BOTH</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1304" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" bodystart="1299" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1102" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__interrupts_1ga77961fdd9eef9504ff17e70577b5ea07" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTB_SetInterruptMask</definition>
        <argsstring>(CTBM_Type *base, cy_en_ctb_opamp_sel_t compNum)</argsstring>
        <name>Cy_CTB_SetInterruptMask</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga2175f16ceffc2774b50643be95f32137">cy_en_ctb_opamp_sel_t</ref></type>
          <declname>compNum</declname>
        </param>
        <briefdescription>
<para>Configure the CTB comparator edge interrupt to be forwarded to the CPU interrupt controller. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>compNum</parametername>
</parameternamelist>
<parameterdescription>
<para><ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a9e99c0e580cf26beea9ba5a088d20738">CY_CTB_OPAMP_NONE</ref>, <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a715789db816ed8db3729da6e57aacdcb">CY_CTB_OPAMP_0</ref>, <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a9318dd62afb23fc0c4dac2abcfcea5f0">CY_CTB_OPAMP_1</ref>, or <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a4c859afffdcf6dcf16c4350906759488">CY_CTB_OPAMP_BOTH</ref>. Calling this function with CY_CTB_OPAMP_NONE will disable all interrupt requests.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Enable<sp />interrupt<sp />request<sp />for<sp />both<sp />comparators.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__interrupts_1ga77961fdd9eef9504ff17e70577b5ea07">Cy_CTB_SetInterruptMask</ref>(CTBM0,<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a4c859afffdcf6dcf16c4350906759488">CY_CTB_OPAMP_BOTH</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1332" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" bodystart="1327" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1103" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__interrupts_1ga41785a38fe4f39c2060c4126061ac349" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_CTB_GetInterruptMask</definition>
        <argsstring>(const CTBM_Type *base, cy_en_ctb_opamp_sel_t compNum)</argsstring>
        <name>Cy_CTB_GetInterruptMask</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga2175f16ceffc2774b50643be95f32137">cy_en_ctb_opamp_sel_t</ref></type>
          <declname>compNum</declname>
        </param>
        <briefdescription>
<para>Return whether the CTB comparator edge interrupt output is forwarded to the CPU interrupt controller as configured by <ref kindref="member" refid="group__group__ctb__functions__interrupts_1ga77961fdd9eef9504ff17e70577b5ea07">Cy_CTB_SetInterruptMask</ref>. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>compNum</parametername>
</parameternamelist>
<parameterdescription>
<para><ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a715789db816ed8db3729da6e57aacdcb">CY_CTB_OPAMP_0</ref>, <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a9318dd62afb23fc0c4dac2abcfcea5f0">CY_CTB_OPAMP_1</ref>, or <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a4c859afffdcf6dcf16c4350906759488">CY_CTB_OPAMP_BOTH</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The interrupt mask. If compNum is <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a4c859afffdcf6dcf16c4350906759488">CY_CTB_OPAMP_BOTH</ref>, cast the returned mask to <ref kindref="member" refid="group__group__ctb__enums_1ga2175f16ceffc2774b50643be95f32137">cy_en_ctb_opamp_sel_t</ref> to determine which comparator interrupt output (or both) is forwarded.<itemizedlist>
<listitem><para>0: Interrupt output not forwarded to interrupt controller</para></listitem><listitem><para>Non-zero: Interrupt output forwarded to interrupt controller</para></listitem></itemizedlist>
</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Check<sp />if<sp />comparator<sp />0<sp />interrupts<sp />are<sp />enabled.<sp />If<sp />not,<sp />enable<sp />it.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />intrMask;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intrMask<sp />=<sp /><ref kindref="member" refid="group__group__ctb__functions__interrupts_1ga41785a38fe4f39c2060c4126061ac349">Cy_CTB_GetInterruptMask</ref>(CTBM0,<sp />CY_CTB_OPAMP_0);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(0UL<sp />==<sp />intrMask)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__interrupts_1ga77961fdd9eef9504ff17e70577b5ea07">Cy_CTB_SetInterruptMask</ref>(CTBM0,<sp />CY_CTB_OPAMP_0);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1366" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" bodystart="1361" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1104" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__interrupts_1ga3da4b3a6ad917138d0b9fef001c8db76" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_CTB_GetInterruptStatusMasked</definition>
        <argsstring>(const CTBM_Type *base, cy_en_ctb_opamp_sel_t compNum)</argsstring>
        <name>Cy_CTB_GetInterruptStatusMasked</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga2175f16ceffc2774b50643be95f32137">cy_en_ctb_opamp_sel_t</ref></type>
          <declname>compNum</declname>
        </param>
        <briefdescription>
<para>Return the CTB comparator edge output interrupt state after being masked. </para>        </briefdescription>
        <detaileddescription>
<para>This is the bitwise AND of <ref kindref="member" refid="group__group__ctb__functions__interrupts_1gaa05d09b4e77c18c5279aa3353875d85c">Cy_CTB_GetInterruptStatus</ref> and <ref kindref="member" refid="group__group__ctb__functions__interrupts_1ga41785a38fe4f39c2060c4126061ac349">Cy_CTB_GetInterruptMask</ref>.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>compNum</parametername>
</parameternamelist>
<parameterdescription>
<para><ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a715789db816ed8db3729da6e57aacdcb">CY_CTB_OPAMP_0</ref>, <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a9318dd62afb23fc0c4dac2abcfcea5f0">CY_CTB_OPAMP_1</ref>, or <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a4c859afffdcf6dcf16c4350906759488">CY_CTB_OPAMP_BOTH</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>If compNum is <ref kindref="member" refid="group__group__ctb__enums_1gga2175f16ceffc2774b50643be95f32137a4c859afffdcf6dcf16c4350906759488">CY_CTB_OPAMP_BOTH</ref>, cast the returned value to <ref kindref="member" refid="group__group__ctb__enums_1ga2175f16ceffc2774b50643be95f32137">cy_en_ctb_opamp_sel_t</ref> to determine which comparator interrupt output (or both) is detected and masked.<itemizedlist>
<listitem><para>0: Configured edge not detected or not masked</para></listitem><listitem><para>Non-zero: Configured edge type detected and masked </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1394" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" bodystart="1389" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1105" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This set of functions is related to the comparator interrupts. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>