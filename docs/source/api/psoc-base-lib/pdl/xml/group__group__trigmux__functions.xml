<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__trigmux__functions" kind="group">
    <compoundname>group_trigmux_functions</compoundname>
    <title>Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__trigmux__functions_1ga3671fac144b75c3b3eddc5ab46ae96f6" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__trigmux__enums_1ga30a0d3f7caf6a59d3fb7a618a95ed6ae">cy_en_trigmux_status_t</ref></type>
        <definition>cy_en_trigmux_status_t Cy_TrigMux_Connect</definition>
        <argsstring>(uint32_t inTrig, uint32_t outTrig, bool invert, en_trig_type_t trigType)</argsstring>
        <name>Cy_TrigMux_Connect</name>
        <param>
          <type>uint32_t</type>
          <declname>inTrig</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>outTrig</declname>
        </param>
        <param>
          <type>bool</type>
          <declname>invert</declname>
        </param>
        <param>
          <type>en_trig_type_t</type>
          <declname>trigType</declname>
        </param>
        <briefdescription>
<para>Connects an input trigger source and output trigger. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>inTrig</parametername>
</parameternamelist>
<parameterdescription>
<para>An input selection for the trigger mux.<itemizedlist>
<listitem><para>Bit 30 should be cleared.</para></listitem><listitem><para>Bit 12 should be cleared.</para></listitem><listitem><para>Bits 11:8 represent the trigger group selection.</para></listitem><listitem><para>Bits 7:0 select the input trigger signal for the specified trigger multiplexer.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>outTrig</parametername>
</parameternamelist>
<parameterdescription>
<para>The output of the trigger mux. This refers to the consumer of the trigger mux.<itemizedlist>
<listitem><para>Bit 30 should be set.</para></listitem><listitem><para>Bit 12 should be cleared.</para></listitem><listitem><para>Bits 11:8 represent the trigger group selection.<linebreak />
 For PERI_ver1:</para></listitem><listitem><para>Bits 6:0 select the output trigger number in the trigger group.<linebreak />
 For PERI_ver2:</para></listitem><listitem><para>Bits 7:0 select the output trigger number in the trigger group.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>invert</parametername>
</parameternamelist>
<parameterdescription>
<para><itemizedlist>
<listitem><para>true: The output trigger is inverted.</para></listitem><listitem><para>false: The output trigger is not inverted.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>trigType</parametername>
</parameternamelist>
<parameterdescription>
<para>The trigger signal type.<itemizedlist>
<listitem><para>TRIGGER_TYPE_EDGE: The trigger is synchronized to the consumer blocks clock and a two-cycle pulse is generated on this clock.</para></listitem><listitem><para>TRIGGER_TYPE_LEVEL: The trigger is a simple level output.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>status:<itemizedlist>
<listitem><para>CY_TRIGMUX_SUCCESS: The connection is made successfully.</para></listitem><listitem><para>CY_TRIGMUX_BAD_PARAM: Some parameter is invalid.</para></listitem></itemizedlist>
</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Setup<sp />a<sp />trigger<sp />multiplexer<sp />connection<sp />from<sp />TCPWM0<sp />counter<sp />0<sp />overflow<sp />output<sp />to<sp />DataWire<sp />0<sp />channel<sp />0<sp />trigger<sp />input.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Also<sp />for<sp />trigger<sp />signal<sp />evidence<sp />setup<sp />two<sp />additional<sp />trigger<sp />multiplexer<sp />connections:</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />-<sp />from<sp />the<sp />same<sp />TCPWM0<sp />counter<sp />0<sp />overflow<sp />output<sp />to<sp />HSIOM<sp />trigger<sp />IO<sp />output<sp />0<sp />(GPIO<sp />port<sp />0.4)</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />-<sp />from<sp />the<sp />DataWire<sp />0<sp />channel<sp />0<sp />trigger<sp />output<sp />to<sp />HSIOM<sp />trigger<sp />IO<sp />output<sp />1<sp />(GPIO<sp />port<sp />0.5)</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#if<sp />(CY_IP_MXPERI_VERSION<sp />==<sp />1U)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__trigmux__functions_1ga3671fac144b75c3b3eddc5ab46ae96f6">Cy_TrigMux_Connect</ref>(TRIG11_IN_TCPWM0_TR_OVERFLOW0,<sp />TRIG11_OUT_TR_GROUP0_INPUT9,<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">,<sp />TRIGGER_TYPE_LEVEL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__trigmux__functions_1ga3671fac144b75c3b3eddc5ab46ae96f6">Cy_TrigMux_Connect</ref>(TRIG0_IN_TR_GROUP11_OUTPUT0,<sp />TRIG0_OUT_CPUSS_DW0_TR_IN0,<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">,<sp />TRIGGER_TYPE_EDGE);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__trigmux__functions_1ga3671fac144b75c3b3eddc5ab46ae96f6">Cy_TrigMux_Connect</ref>(TRIG11_IN_TCPWM0_TR_OVERFLOW0,<sp />TRIG11_OUT_TR_GROUP8_INPUT9,<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">,<sp />TRIGGER_TYPE_LEVEL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__trigmux__functions_1ga3671fac144b75c3b3eddc5ab46ae96f6">Cy_TrigMux_Connect</ref>(TRIG8_IN_TR_GROUP11_OUTPUT0,<sp />TRIG8_OUT_PERI_TR_IO_OUTPUT0,<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">,<sp />TRIGGER_TYPE_EDGE);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__trigmux__functions_1ga3671fac144b75c3b3eddc5ab46ae96f6">Cy_TrigMux_Connect</ref>(TRIG10_IN_CPUSS_DW0_TR_OUT0,<sp />TRIG10_OUT_TR_GROUP8_INPUT1,<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">,<sp />TRIGGER_TYPE_LEVEL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__trigmux__functions_1ga3671fac144b75c3b3eddc5ab46ae96f6">Cy_TrigMux_Connect</ref>(TRIG8_IN_TR_GROUP10_OUTPUT0,<sp />TRIG8_OUT_PERI_TR_IO_OUTPUT1,<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">,<sp />TRIGGER_TYPE_EDGE);</highlight></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#else<sp /></highlight><highlight class="comment">/*<sp />CY_IP_MXPERI_VERSION<sp />&gt;<sp />1<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__trigmux__functions_1ga3671fac144b75c3b3eddc5ab46ae96f6">Cy_TrigMux_Connect</ref>(TRIG_IN_MUX_0_TCPWM0_TR_OVERFLOW0,<sp />TRIG_OUT_MUX_0_PDMA0_TR_IN0,<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">,<sp />TRIGGER_TYPE_EDGE);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__trigmux__functions_1ga3671fac144b75c3b3eddc5ab46ae96f6">Cy_TrigMux_Connect</ref>(TRIG_IN_MUX_4_TCPWM0_TR_OVERFLOW0,<sp />TRIG_OUT_MUX_4_HSIOM_TR_IO_OUTPUT0,<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">,<sp />TRIGGER_TYPE_EDGE);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__trigmux__functions_1ga3671fac144b75c3b3eddc5ab46ae96f6">Cy_TrigMux_Connect</ref>(TRIG_IN_MUX_4_PDMA0_TR_OUT0,<sp />TRIG_OUT_MUX_4_HSIOM_TR_IO_OUTPUT1,<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">,<sp />TRIGGER_TYPE_EDGE);</highlight></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />CY_IP_MXPERI_VERSION<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Note:<sp />the<sp />Cy_TrigMux_Connect<sp />return<sp />status<sp />is<sp />ignored<sp />here<sp />because<sp />the<sp />provided<sp />parameters<sp />are<sp />correct.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />It<sp />is<sp />made<sp />here<sp />just<sp />to<sp />reduce<sp />the<sp />code<sp />snippet<sp />redundancy,<sp />it<sp />is<sp />not<sp />recommended<sp />to<sp />do<sp />so<sp />in<sp />the<sp />user<sp />code.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Now<sp />initialize<sp />the<sp />correspondent<sp />GPIO<sp />pins<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__gpio__functions__init_1gaf57c501727276013d3e8974a9fb7d0a7">Cy_GPIO_Pin_FastInit</ref>(GPIO_PRT0,<sp />4UL,<sp /><ref kindref="member" refid="group__group__gpio__drive_modes_1gacebd8bea6222d742bdfbfd86dabab940">CY_GPIO_DM_STRONG_IN_OFF</ref>,<sp />0UL,<sp />P0_4_PERI_TR_IO_OUTPUT0);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__gpio__functions__init_1gaf57c501727276013d3e8974a9fb7d0a7">Cy_GPIO_Pin_FastInit</ref>(GPIO_PRT0,<sp />5UL,<sp /><ref kindref="member" refid="group__group__gpio__drive_modes_1gacebd8bea6222d742bdfbfd86dabab940">CY_GPIO_DM_STRONG_IN_OFF</ref>,<sp />0UL,<sp />P0_5_PERI_TR_IO_OUTPUT1);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Now<sp />the<sp />TCPWM0<sp />counter<sp />0<sp />periodically<sp />triggers<sp />the<sp />DW0<sp />channel<sp />0<sp />(each<sp />time<sp />when<sp />overflow<sp />event<sp />occurs).</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Also<sp />the<sp />TCPWM<sp />and<sp />DMA<sp />output<sp />trigger<sp />signals<sp />can<sp />be<sp />observed<sp />with<sp />oscilloscope<sp />on<sp />pins<sp />P0.4<sp />and<sp />P0.5<sp />correspondingly.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="124" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_trigmux.c" bodystart="98" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_trigmux.h" line="321" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__trigmux__functions_1gad3c1d26d25a47bc4beca499bf0407c80" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__trigmux__enums_1ga30a0d3f7caf6a59d3fb7a618a95ed6ae">cy_en_trigmux_status_t</ref></type>
        <definition>cy_en_trigmux_status_t Cy_TrigMux_SwTrigger</definition>
        <argsstring>(uint32_t trigLine, uint32_t cycles)</argsstring>
        <name>Cy_TrigMux_SwTrigger</name>
        <param>
          <type>uint32_t</type>
          <declname>trigLine</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>cycles</declname>
        </param>
        <briefdescription>
<para>This function generates a software trigger on an input trigger line. </para>        </briefdescription>
        <detaileddescription>
<para>All output triggers connected to this input trigger will be triggered. The function also verifies that there is no activated trigger before generating another activation.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>trigLine</parametername>
</parameternamelist>
<parameterdescription>
<para>The input of the trigger mux.<itemizedlist>
<listitem><para>Bit 30 represents if the signal is an input/output. When this bit is set, the trigger activation is for an output trigger from the trigger multiplexer. When this bit is reset, the trigger activation is for an input trigger to the trigger multiplexer.</para></listitem><listitem><para>Bits 12:8 represent the trigger group selection.<linebreak />
 In case of output trigger line (bit 30 is set): For PERI_ver1:</para></listitem><listitem><para>Bits 6:0 select the output trigger number in the trigger group.<linebreak />
 For PERI_ver2:</para></listitem><listitem><para>Bits 7:0 select the output trigger number in the trigger group. In case of input trigger line (bit 30 is unset):</para></listitem><listitem><para>Bits 7:0 select the input trigger signal for the trigger multiplexer.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>cycles</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of "Clk_Peri" cycles during which the trigger remains activated.<linebreak />
 For PERI_ver1: The valid range of cycles is 1 ... 254.<linebreak />
 For PERI_ver2: The only valid value of cycles is 2 (<ref kindref="member" refid="group__group__trigmux__macros_1ga9347a341f6903dee749841fae0dcb4a5">CY_TRIGGER_TWO_CYCLES</ref>).<linebreak />
 Also there are special values (supported with both PERI_ver1 and PERI_ver2):<itemizedlist>
<listitem><para>CY_TRIGGER_INFINITE - trigger remains activated until the user deactivates it by calling this function with CY_TRIGGER_DEACTIVATE parameter.</para></listitem><listitem><para>CY_TRIGGER_DEACTIVATE - this is used to deactivate the trigger activated by calling this function with CY_TRIGGER_INFINITE parameter.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>status:<itemizedlist>
<listitem><para>CY_TRIGMUX_SUCCESS: The trigger is successfully activated/deactivated.</para></listitem><listitem><para>CY_TRIGMUX_INVALID_STATE: The trigger is already activated/not active.</para></listitem><listitem><para>CY_TRIGMUX_BAD_PARAM: Some parameter is invalid.</para></listitem></itemizedlist>
</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Software<sp />Trigger<sp />the<sp />DW0<sp />channel<sp />0<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#if<sp />(CY_IP_MXPERI_VERSION<sp />==<sp />1U)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__trigmux__enums_1gga30a0d3f7caf6a59d3fb7a618a95ed6aea19e80892aa7755dd38c708994502d02c">CY_TRIGMUX_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__trigmux__functions_1gad3c1d26d25a47bc4beca499bf0407c80">Cy_TrigMux_SwTrigger</ref>(TRIG0_OUT_CPUSS_DW0_TR_IN0,<sp /><ref kindref="member" refid="group__group__trigmux__macros_1ga9347a341f6903dee749841fae0dcb4a5">CY_TRIGGER_TWO_CYCLES</ref>))</highlight></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#else<sp /></highlight><highlight class="comment">/*<sp />CY_IP_MXPERI_VERSION<sp />&gt;<sp />1<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__trigmux__enums_1gga30a0d3f7caf6a59d3fb7a618a95ed6aea19e80892aa7755dd38c708994502d02c">CY_TRIGMUX_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__trigmux__functions_1gad3c1d26d25a47bc4beca499bf0407c80">Cy_TrigMux_SwTrigger</ref>(TRIG_OUT_MUX_0_PDMA0_TR_IN0,<sp /><ref kindref="member" refid="group__group__trigmux__macros_1ga9347a341f6903dee749841fae0dcb4a5">CY_TRIGGER_TWO_CYCLES</ref>))</highlight></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />CY_IP_MXPERI_VERSION<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="378" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_trigmux.c" bodystart="328" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_trigmux.h" line="322" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__trigmux__functions_1gab7106b8fe037e2ebbbb996289ad1fb00" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__trigmux__enums_1ga30a0d3f7caf6a59d3fb7a618a95ed6ae">cy_en_trigmux_status_t</ref></type>
        <definition>cy_en_trigmux_status_t Cy_TrigMux_Select</definition>
        <argsstring>(uint32_t outTrig, bool invert, en_trig_type_t trigType)</argsstring>
        <name>Cy_TrigMux_Select</name>
        <param>
          <type>uint32_t</type>
          <declname>outTrig</declname>
        </param>
        <param>
          <type>bool</type>
          <declname>invert</declname>
        </param>
        <param>
          <type>en_trig_type_t</type>
          <declname>trigType</declname>
        </param>
        <briefdescription>
<para>Enables and configures the specified 1-to-1 trigger line. </para>        </briefdescription>
        <detaileddescription>
<para>For PERI_ver2 only.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>outTrig</parametername>
</parameternamelist>
<parameterdescription>
<para>The 1to1 trigger line.<itemizedlist>
<listitem><para>Bit 30 should be set.</para></listitem><listitem><para>Bit 12 should be set.</para></listitem><listitem><para>Bits 11:8 represent the 1-to-1 trigger group selection.</para></listitem><listitem><para>Bits 7:0 select the trigger line number in the trigger group.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>invert</parametername>
</parameternamelist>
<parameterdescription>
<para><itemizedlist>
<listitem><para>true: The trigger signal is inverted.</para></listitem><listitem><para>false: The trigger signal is not inverted.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>trigType</parametername>
</parameternamelist>
<parameterdescription>
<para>The trigger signal type.<itemizedlist>
<listitem><para>TRIGGER_TYPE_EDGE: The trigger is synchronized to the consumer blocks clock and a two-cycle pulse is generated on this clock.</para></listitem><listitem><para>TRIGGER_TYPE_LEVEL: The trigger is a simple level output.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>status:<itemizedlist>
<listitem><para>CY_TRIGMUX_SUCCESS: The selection is made successfully.</para></listitem><listitem><para>CY_TRIGMUX_BAD_PARAM: Some parameter is invalid.</para></listitem></itemizedlist>
</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Set<sp />up<sp />a<sp />1<sp />to<sp />1<sp />connection<sp />between<sp />two<sp />peripherals;<sp />in<sp />this<sp />case<sp />from<sp />SCB0<sp />TX<sp />Trigger<sp />Output<sp />to<sp />DW0<sp />channel<sp />16<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__trigmux__enums_1gga30a0d3f7caf6a59d3fb7a618a95ed6aea19e80892aa7755dd38c708994502d02c">CY_TRIGMUX_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__trigmux__functions_1gab7106b8fe037e2ebbbb996289ad1fb00">Cy_TrigMux_Select</ref>(TRIG_OUT_1TO1_0_SCB0_TX_TO_PDMA0_TR_IN16,<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">,<sp />TRIGGER_TYPE_LEVEL))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="183" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_trigmux.c" bodystart="157" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_trigmux.h" line="323" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__trigmux__functions_1ga9cf035a60df4c7073280b8ac21666b22" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__trigmux__enums_1ga30a0d3f7caf6a59d3fb7a618a95ed6ae">cy_en_trigmux_status_t</ref></type>
        <definition>cy_en_trigmux_status_t Cy_TrigMux_Deselect</definition>
        <argsstring>(uint32_t outTrig)</argsstring>
        <name>Cy_TrigMux_Deselect</name>
        <param>
          <type>uint32_t</type>
          <declname>outTrig</declname>
        </param>
        <briefdescription>
<para>Disables the specified 1-to-1 trigger line. </para>        </briefdescription>
        <detaileddescription>
<para>For PERI_ver2 only.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>outTrig</parametername>
</parameternamelist>
<parameterdescription>
<para>The 1to1 trigger line.<itemizedlist>
<listitem><para>Bit 30 should be set.</para></listitem><listitem><para>Bit 12 should be set.</para></listitem><listitem><para>Bits 11:8 represent the 1-to-1 trigger group selection.</para></listitem><listitem><para>Bits 7:0 select the trigger line number in the trigger group.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>status:<itemizedlist>
<listitem><para>CY_TRIGMUX_SUCCESS: The deselection is made successfully.</para></listitem><listitem><para>CY_TRIGMUX_BAD_PARAM: Some parameter is invalid.</para></listitem></itemizedlist>
</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Disable<sp />a<sp />1<sp />to<sp />1<sp />connection,<sp />using<sp />SCB0<sp />TX<sp />Trigger<sp />Output<sp />to<sp />DataWire<sp />0<sp />channel<sp />16<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__trigmux__enums_1gga30a0d3f7caf6a59d3fb7a618a95ed6aea19e80892aa7755dd38c708994502d02c">CY_TRIGMUX_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__trigmux__functions_1ga9cf035a60df4c7073280b8ac21666b22">Cy_TrigMux_Deselect</ref>(TRIG_OUT_1TO1_0_SCB0_TX_TO_PDMA0_TR_IN16))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="229" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_trigmux.c" bodystart="207" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_trigmux.h" line="324" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__trigmux__functions_1ga797820d8e7c746129d354888da382498" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__trigmux__enums_1ga30a0d3f7caf6a59d3fb7a618a95ed6ae">cy_en_trigmux_status_t</ref></type>
        <definition>cy_en_trigmux_status_t Cy_TrigMux_SetDebugFreeze</definition>
        <argsstring>(uint32_t outTrig, bool enable)</argsstring>
        <name>Cy_TrigMux_SetDebugFreeze</name>
        <param>
          <type>uint32_t</type>
          <declname>outTrig</declname>
        </param>
        <param>
          <type>bool</type>
          <declname>enable</declname>
        </param>
        <briefdescription>
<para>Enables/disables the Debug Freeze feature for the specified trigger multiplexer or 1-to-1 trigger line. </para>        </briefdescription>
        <detaileddescription>
<para>For PERI_ver2 only.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>outTrig</parametername>
</parameternamelist>
<parameterdescription>
<para>The output of the trigger mux or dedicated 1-to-1 trigger line.<itemizedlist>
<listitem><para>Bit 30 should be set.</para></listitem><listitem><para>Bits 12:8 represent the trigger group selection.</para></listitem><listitem><para>Bits 7:0 select the output trigger number in the trigger group.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>enable</parametername>
</parameternamelist>
<parameterdescription>
<para><itemizedlist>
<listitem><para>true: The Debug Freeze feature is enabled.</para></listitem><listitem><para>false: The Debug Freeze feature is disabled.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>status:<itemizedlist>
<listitem><para>CY_TRIGMUX_SUCCESS: The operation is made successfully.</para></listitem><listitem><para>CY_TRIGMUX_BAD_PARAM: The outTrig parameter is invalid.</para></listitem></itemizedlist>
</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Enable<sp />a<sp />Debug<sp />Freeze<sp />feature<sp />using<sp />DW0<sp />channel<sp />0<sp />trigger<sp />input<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__trigmux__enums_1gga30a0d3f7caf6a59d3fb7a618a95ed6aea19e80892aa7755dd38c708994502d02c">CY_TRIGMUX_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__trigmux__functions_1ga797820d8e7c746129d354888da382498">Cy_TrigMux_SetDebugFreeze</ref>(TRIG_OUT_MUX_0_PDMA0_TR_IN0,<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Now<sp />the<sp />DW0<sp />channel<sp />0<sp />trigger<sp />input<sp />will<sp />be<sp />frozen<sp />when</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />the<sp />tr_dbg_freeze<sp />signal<sp />is<sp />activated.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Usually<sp />the<sp />tr_dbg_freeze<sp />signal<sp />is<sp />intended<sp />to<sp />be<sp />activated<sp />from</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />the<sp />cpuss.cti_tr_out[x]<sp />signal,<sp />for<sp />example:</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__trigmux__functions_1ga3671fac144b75c3b3eddc5ab46ae96f6">Cy_TrigMux_Connect</ref>(TRIG_IN_MUX_7_CTI_TR_OUT0,<sp />TRIG_OUT_MUX_7_DEBUG_FREEZE_TR_IN,<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">,<sp />TRIGGER_TYPE_LEVEL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />However<sp />it<sp />can<sp />be<sp />activated<sp />just<sp />with<sp />software<sp />triggering:<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__trigmux__functions_1gad3c1d26d25a47bc4beca499bf0407c80">Cy_TrigMux_SwTrigger</ref>(TRIG_OUT_MUX_7_DEBUG_FREEZE_TR_IN,<sp /><ref kindref="member" refid="group__group__trigmux__macros_1gab3a2b0e75a195d5fb7020cecc2caf948">CY_TRIGGER_INFINITE</ref>);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Disable<sp />a<sp />Debug<sp />Freeze<sp />feature<sp />using<sp />DW0<sp />channel<sp />0<sp />trigger<sp />input<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__trigmux__enums_1gga30a0d3f7caf6a59d3fb7a618a95ed6aea19e80892aa7755dd38c708994502d02c">CY_TRIGMUX_SUCCESS</ref><sp />!=<sp /><ref kindref="member" refid="group__group__trigmux__functions_1ga797820d8e7c746129d354888da382498">Cy_TrigMux_SetDebugFreeze</ref>(TRIG_OUT_MUX_0_PDMA0_TR_IN0,<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Insert<sp />error<sp />handling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="282" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_trigmux.c" bodystart="257" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_trigmux.h" line="325" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>