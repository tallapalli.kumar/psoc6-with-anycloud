<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__scb__i2c__low__power__functions" kind="group">
    <compoundname>group_scb_i2c_low_power_functions</compoundname>
    <title>Low Power Callbacks</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__low__power__functions_1gaf2044790a92ba63cb11616f2e80d3d1f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref></type>
        <definition>cy_en_syspm_status_t Cy_SCB_I2C_DeepSleepCallback</definition>
        <argsstring>(cy_stc_syspm_callback_params_t *callbackParams, cy_en_syspm_callback_mode_t mode)</argsstring>
        <name>Cy_SCB_I2C_DeepSleepCallback</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref> *</type>
          <declname>callbackParams</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></type>
          <declname>mode</declname>
        </param>
        <briefdescription>
<para>This function handles the transition of the I2C SCB into and out of Deep Sleep mode. </para>        </briefdescription>
        <detaileddescription>
<para>It prevents the device from entering Deep Sleep mode if the I2C slave or master is actively communicating. The behavior of the I2C SCB in Deep Sleep depends on whether the SCB block is wakeup-capable or not:<itemizedlist>
<listitem><para><bold>Wakeup-capable</bold>: during Deep Sleep mode on incoming I2C slave address the slave receives address and stretches the clock until the device is awoken from Deep Sleep mode. If the slave address occurs before the device enters Deep Sleep mode, the device will not enter Deep Sleep mode. Only the I2C slave can be configured to be a wakeup source from Deep Sleep mode.</para></listitem><listitem><para><bold>Not wakeup-capable</bold>: the SCB is disabled in Deep Sleep mode. It is re-enabled if the device fails to enter Deep Sleep mode or when the device is awoken from Deep Sleep mode. While the SCB is disabled it stops driving the outputs and ignores the inputs. The slave NACKs all incoming addresses.</para></listitem></itemizedlist>
</para><para>This function must be called during execution of <ref kindref="member" refid="group__group__syspm__functions__power_1ga5150c28fe4d2626720c1fbf74b3111ca">Cy_SysPm_CpuEnterDeepSleep</ref>. To do it, register this function as a callback before calling <ref kindref="member" refid="group__group__syspm__functions__power_1ga5150c28fe4d2626720c1fbf74b3111ca">Cy_SysPm_CpuEnterDeepSleep</ref> : specify <ref kindref="member" refid="group__group__syspm__data__enumerates_1gga8c2960c0164ead1cfa86e7d6846b6ff0abc51d74deff0ceea4304b01b2d57bd9d">CY_SYSPM_DEEPSLEEP</ref> as the callback type and call <ref kindref="member" refid="group__group__syspm__functions__callback_1ga0d58b00c8dc764a6371590f70e2f73c7">Cy_SysPm_RegisterCallback</ref>.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>callbackParams</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the callback parameters structure <ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mode</parametername>
</parameternamelist>
<parameterdescription>
<para>Callback mode, see <ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref></para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Only applicable for &lt;b&gt;rev-08 of the CY8CKIT-062-BLE&lt;/b&gt;. For proper operation, when the I2C slave is configured to be a wakeup source from Deep Sleep mode, this function must be copied and modified by the user. The I2C clock disable code must be inserted in the &lt;a href="group__group__syspm__data__enumerates.html#group__group__syspm__data__enumerates_1ggae06cd8869fe61d709ad6145ca9f3cd63a7d302375276b3b5f250a8208c999b558"&gt;CY_SYSPM_BEFORE_TRANSITION&lt;/a&gt; and clock enable code in the &lt;a href="group__group__syspm__data__enumerates.html#group__group__syspm__data__enumerates_1ggae06cd8869fe61d709ad6145ca9f3cd63aafd1db1f7f86ac7bbd18d59b78af5693"&gt;CY_SYSPM_AFTER_TRANSITION&lt;/a&gt; mode processing. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="417" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="277" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="725" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__low__power__functions_1gaf480cdce47050c37a3558285d6a1c2a7" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref></type>
        <definition>cy_en_syspm_status_t Cy_SCB_I2C_HibernateCallback</definition>
        <argsstring>(cy_stc_syspm_callback_params_t *callbackParams, cy_en_syspm_callback_mode_t mode)</argsstring>
        <name>Cy_SCB_I2C_HibernateCallback</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref> *</type>
          <declname>callbackParams</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></type>
          <declname>mode</declname>
        </param>
        <briefdescription>
<para>This function handles the transition of the I2C SCB block into Hibernate mode. </para>        </briefdescription>
        <detaileddescription>
<para>It prevents the device from entering Hibernate mode if the I2C slave or master is actively communicating. If the I2C is ready to enter Hibernate mode it is disabled. If the device failed to enter Hibernate mode, the SCB is enabled. After the SCB is disabled, it stops driving the outputs and ignores the inputs. The slave NACKs all incoming addresses.</para><para>This function must be called during execution of <ref kindref="member" refid="group__group__syspm__functions__power_1gae97647a28c370674ba57d451d21d1c51">Cy_SysPm_SystemEnterHibernate</ref>. To do it, register this function as a callback before calling <ref kindref="member" refid="group__group__syspm__functions__power_1gae97647a28c370674ba57d451d21d1c51">Cy_SysPm_SystemEnterHibernate</ref> : specify <ref kindref="member" refid="group__group__syspm__data__enumerates_1gga8c2960c0164ead1cfa86e7d6846b6ff0a613e2f83e3ab88e3569cf34ff0fa5912">CY_SYSPM_HIBERNATE</ref> as the callback type and call <ref kindref="member" refid="group__group__syspm__functions__callback_1ga0d58b00c8dc764a6371590f70e2f73c7">Cy_SysPm_RegisterCallback</ref>.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>callbackParams</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the callback parameters structure <ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mode</parametername>
</parameternamelist>
<parameterdescription>
<para>Callback mode, see <ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="508" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="448" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="726" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>