<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__profile__functions__general" kind="group">
    <compoundname>group_profile_functions_general</compoundname>
    <title>General Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__profile__functions__general_1ga3a4f9cb2191f4c950821aca1c56766b2" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_Profile_Init</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_Profile_Init</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Initializes and enables the profile hardware. </para>        </briefdescription>
        <detaileddescription>
<para>This function must be called once when energy profiling is desired. The operation does not start a profiling session.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The profile interrupt must also be configured. &lt;a href="group__group__profile__functions__interrupt.html#group__group__profile__functions__interrupt_1gaf530fa03abe1f90c9788ed4ee1ed140f"&gt;Cy_Profile_ISR()&lt;/a&gt; can be used as its handler.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />Energy<sp />Profiler<sp />block<sp />needs<sp />to<sp />be<sp />initialized<sp />before<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />configuring<sp />the<sp />profile<sp />counters.<sp />The<sp />profile<sp />interrupt<sp />is</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />configured<sp />using<sp />the<sp />sysint<sp />driver.<sp />Its<sp />handler<sp />function<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />is<sp />Cy_Profile_ISR().<sp />It<sp />is<sp />enabled<sp />using<sp />NVIC_EnableIRQ().<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />the<sp />energy<sp />profiler<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__profile__functions__general_1ga3a4f9cb2191f4c950821aca1c56766b2">Cy_Profile_Init</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Clear<sp />all<sp />counter<sp />configurations<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__profile__functions__counter_1gaa727295348b428f46dc3d0a7e1f1681f">Cy_Profile_ClearConfiguration</ref>();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="409" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_profile.h" bodystart="404" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_profile.h" line="378" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__profile__functions__general_1gaf60450e95c581d875f6eed5fa01f84eb" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_Profile_DeInit</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_Profile_DeInit</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Clears the interrupt mask and disables the profile hardware. </para>        </briefdescription>
        <detaileddescription>
<para>This function should be called when energy profiling is no longer desired.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The profile interrupt is not disabled by this operation and must be disabled separately.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />energy<sp />profiler<sp />is<sp />no<sp />longer<sp />required<sp />and<sp />needs<sp />to<sp />be</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />turned<sp />off.<sp />All<sp />profile<sp />counters<sp />are<sp />disabled.<sp />The<sp />profile<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />interrupt<sp />is<sp />also<sp />disabled<sp />using<sp />NVIC_DisableIRQ().<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Stop<sp />profiling<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__profile__functions__general_1ga4a4a1151a6c68338ab316b8119ae91ec">Cy_Profile_StopProfiling</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Turn<sp />off<sp />the<sp />energy<sp />profiler<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__profile__functions__general_1gaf60450e95c581d875f6eed5fa01f84eb">Cy_Profile_DeInit</ref>();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="431" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_profile.h" bodystart="427" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_profile.h" line="379" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__profile__functions__general_1gad8b4c8660845665052ffc891d9b1cd50" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_Profile_StartProfiling</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_Profile_StartProfiling</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Starts the profiling/measurement window. </para>        </briefdescription>
        <detaileddescription>
<para>This operation allows the enabled profile counters to start counting.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The profile interrupt should be enabled before calling this function for the firmware to be notified when a counter overflow occurs.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />CM0<sp />activity<sp />needs<sp />to<sp />be<sp />monitored<sp />during<sp />a<sp />fixed</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />window.<sp />The<sp />profile<sp />interrupt<sp />is<sp />configured<sp />using<sp />the<sp />sysint<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />driver.<sp />Its<sp />handler<sp />function<sp />is<sp />Cy_Profile_ISR().<sp />The<sp />interrupt</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />is<sp />enabled<sp />using<sp />NVIC_EnableIRQ().<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />List<sp />of<sp />handles<sp />for<sp />allocated<sp />counters<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__profile__ctr__t">cy_stc_profile_ctr_ptr_t</ref><sp />cntHandle[PROFILE_PRFL_CNT_NR]<sp />=<sp />{0UL};</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />the<sp />energy<sp />profiler<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__profile__functions__general_1ga3a4f9cb2191f4c950821aca1c56766b2">Cy_Profile_Init</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__profile__functions__counter_1gaa727295348b428f46dc3d0a7e1f1681f">Cy_Profile_ClearConfiguration</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Configure<sp />a<sp />profile<sp />counter<sp />to<sp />monitor<sp />the<sp />CM0<sp />activity<sp />and<sp />enable<sp />it<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cntHandle[0]<sp />=<sp /><ref kindref="member" refid="group__group__profile__functions__counter_1ga077b14aae89e02d6e01699e873fe3a9b">Cy_Profile_ConfigureCounter</ref>(CPUSS_MONITOR_CM0,<sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />CM0<sp />active<sp />cycles<sp />count<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__profile__enums_1gga82efb19440328ed10ceb3ae66ac4e6f3a4164d3e39093109cb37d74adac47c6c3">CY_PROFILE_EVENT</ref>,<sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Count<sp />number<sp />of<sp />active<sp />cycles<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__profile__enums_1ggaf4167388440a79a2671d7c1bea86785cab734f4cdfa75f56ad1bbecdfd22dc137">CY_PROFILE_CLK_TIMER</ref>,<sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Ignored<sp />for<sp />"event"<sp />counting<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />10UL);<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Weight<sp />of<sp />10<sp />(example)<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />the<sp />assigned<sp />counter<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<ref kindref="member" refid="group__group__profile__functions__counter_1gadb0af4c3dfab0094b3c37192f600f351">Cy_Profile_EnableCounter</ref>(cntHandle[0]);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Start<sp />the<sp />profiling<sp />window.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__profile__functions__general_1gad8b4c8660845665052ffc891d9b1cd50">Cy_Profile_StartProfiling</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />When<sp />ready<sp />to<sp />read<sp />the<sp />result,<sp />stop<sp />the<sp />profiling<sp />window<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="144" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_profile.c" bodystart="136" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_profile.h" line="380" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__profile__functions__general_1ga4a4a1151a6c68338ab316b8119ae91ec" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_Profile_StopProfiling</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_Profile_StopProfiling</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Stops the profiling/measurement window. </para>        </briefdescription>
        <detaileddescription>
<para>This operation prevents the enabled profile counters from counting.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The profile interrupt should be disabled before calling this function.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />weighted<sp />average<sp />of<sp />all<sp />the<sp />counter<sp />values<sp />in<sp />the<sp />profiling</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />window<sp />needs<sp />to<sp />be<sp />read.<sp />The<sp />energy<sp />profiler<sp />and<sp />the<sp />profile</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />interrupt<sp />are<sp />initialized<sp />and<sp />enabled.<sp />The<sp />profile<sp />counters</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />are<sp />configured<sp />and<sp />enabled.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />List<sp />of<sp />handles<sp />for<sp />allocated<sp />counters<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__profile__ctr__t">cy_stc_profile_ctr_ptr_t</ref><sp />cntHandle[PROFILE_PRFL_CNT_NR]<sp />=<sp />{0UL};</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Start<sp />the<sp />profiling<sp />window<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__profile__functions__general_1gad8b4c8660845665052ffc891d9b1cd50">Cy_Profile_StartProfiling</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Perform<sp />some<sp />activity...<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />When<sp />ready<sp />to<sp />read<sp />the<sp />results<sp />of<sp />the<sp />counter<sp />values,<sp />stop<sp />the<sp />profiling<sp />window.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__profile__functions__general_1ga4a4a1151a6c68338ab316b8119ae91ec">Cy_Profile_StopProfiling</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Energy<sp />limit<sp />(example)<sp />of<sp />a<sp />profiling<sp />window<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint64_t<sp />energyLimit<sp />=<sp /><sp />0x00100000UL;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />weighted<sp />average<sp />of<sp />all<sp />the<sp />counter<sp />values<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint64_t<sp />weightedAvg<sp />=<sp /><ref kindref="member" refid="group__group__profile__functions__calculation_1gaf7064fd5a7d413a42fb9d3053cf8f8ea">Cy_Profile_GetSumWeightedCounts</ref>(cntHandle,<sp />PROFILE_PRFL_CNT_NR);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(weightedAvg<sp />&gt;<sp />energyLimit)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />The<sp />energy<sp />consumption<sp />is<sp />more<sp />than<sp />expected<sp />for<sp />this<sp />profiling<sp />window.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />Take<sp />appropriate<sp />measures<sp />to<sp />reduce<sp />the<sp />energy<sp />consumption.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="451" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_profile.h" bodystart="448" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_profile.h" line="381" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__profile__functions__general_1ga941794243fc79c08dd1f644bf916cc18" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_Profile_IsProfiling</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_Profile_IsProfiling</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Reports the active status of the profiling window. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>0 = profiling is not active; 1 = profiling is active</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />status<sp />of<sp />the<sp />profiling<sp />window<sp />needs<sp />to<sp />be<sp />known<sp />to<sp />make</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />a<sp />determination<sp />as<sp />to<sp />whether<sp />to<sp />start<sp />a<sp />profiling<sp />window</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />or<sp />to<sp />stop<sp />it.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(1UL<sp />==<sp /><ref kindref="member" refid="group__group__profile__functions__general_1ga941794243fc79c08dd1f644bf916cc18">Cy_Profile_IsProfiling</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Stop<sp />the<sp />profiling<sp />window<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__profile__functions__general_1ga4a4a1151a6c68338ab316b8119ae91ec">Cy_Profile_StopProfiling</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Start<sp />the<sp />profiling<sp />window<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__profile__functions__general_1gad8b4c8660845665052ffc891d9b1cd50">Cy_Profile_StartProfiling</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="469" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_profile.h" bodystart="466" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_profile.h" line="382" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>