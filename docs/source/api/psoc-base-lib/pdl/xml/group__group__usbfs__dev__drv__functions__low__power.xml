<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__usbfs__dev__drv__functions__low__power" kind="group">
    <compoundname>group_usbfs_dev_drv_functions_low_power</compoundname>
    <title>Low Power Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__drv__functions__low__power_1gabd7fa97f9881ff3992851fecb9fa6ca2" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE bool</type>
        <definition>__STATIC_INLINE bool Cy_USBFS_Dev_Drv_CheckActivity</definition>
        <argsstring>(USBFS_Type *base)</argsstring>
        <name>Cy_USBFS_Dev_Drv_CheckActivity</name>
        <param>
          <type>USBFS_Type *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Returns the activity status of the bus. </para>        </briefdescription>
        <detaileddescription>
<para>It clears the hardware status to provide an updated status on the next call of this function. This function is useful to determine whether there is any USB bus activity between function calls. A typical use case is to determine whether the USB suspend conditions are met.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The bus activity since the last call. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1965" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" bodystart="1956" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1350" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__drv__functions__low__power_1gaeb6514f79104fe1ad21cd68a155808b5" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_USBFS_Dev_Drv_Force</definition>
        <argsstring>(USBFS_Type *base, cy_en_usbfs_dev_drv_force_bus_state_t state)</argsstring>
        <name>Cy_USBFS_Dev_Drv_Force</name>
        <param>
          <type>USBFS_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1ga460ea6ba6179dd6fad514ba26c982e92">cy_en_usbfs_dev_drv_force_bus_state_t</ref></type>
          <declname>state</declname>
        </param>
        <briefdescription>
<para>Forces a USB J, K, or SE0 state on the USB lines. </para>        </briefdescription>
        <detaileddescription>
<para>A typical use case is to signal a Remote Wakeup condition on the USB bus.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>state</parametername>
</parameternamelist>
<parameterdescription>
<para>The desired bus state. See <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1ga460ea6ba6179dd6fad514ba26c982e92">cy_en_usbfs_dev_drv_force_bus_state_t</ref> for the set of constants. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1987" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" bodystart="1983" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1351" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__drv__functions__low__power_1ga1e254e18f758dbc14cb74b6ef93fc705" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_USBFS_Dev_Drv_Suspend</definition>
        <argsstring>(USBFS_Type *base, cy_stc_usbfs_dev_drv_context_t *context)</argsstring>
        <name>Cy_USBFS_Dev_Drv_Suspend</name>
        <param>
          <type>USBFS_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Prepares the USBFS component to enter Deep Sleep mode. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> allocated by the user. The structure is used during the USBFS Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;After entering low-power mode, the data that is left in the IN or OUT endpoint buffers is not restored after a wakeup, and is lost. Therefore, it should be stored in the SRAM for OUT endpoint or read by the host for the IN endpoint before entering low-power mode. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1255" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_usbfs_dev_drv.c" bodystart="1244" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1352" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__drv__functions__low__power_1ga4f96e219508bc0ea6d645d79ebb75c34" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_USBFS_Dev_Drv_Resume</definition>
        <argsstring>(USBFS_Type *base, cy_stc_usbfs_dev_drv_context_t *context)</argsstring>
        <name>Cy_USBFS_Dev_Drv_Resume</name>
        <param>
          <type>USBFS_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Prepares the USBFS component for operation after exiting Deep Sleep mode. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> allocated by the user. The structure is used during the USBFS Device operation for internal configuration and data retention. The user must not modify anything in this structure. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1297" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_usbfs_dev_drv.c" bodystart="1274" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1353" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>The Low-power functions provide an API to implement Low-power callback at the application level. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>