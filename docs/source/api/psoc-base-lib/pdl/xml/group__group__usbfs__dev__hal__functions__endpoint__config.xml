<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__usbfs__dev__hal__functions__endpoint__config" kind="group">
    <compoundname>group_usbfs_dev_hal_functions_endpoint_config</compoundname>
    <title>Data Endpoint Configuration Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__hal__functions__endpoint__config_1ga81b07ede74c6ad6b99e424afed5fffe7" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref></type>
        <definition>__STATIC_INLINE cy_en_usbfs_dev_drv_status_t Cy_USBFS_Dev_Drv_AddEndpoint</definition>
        <argsstring>(USBFS_Type *base, cy_stc_usb_dev_ep_config_t const *config, cy_stc_usbfs_dev_drv_context_t *context)</argsstring>
        <name>Cy_USBFS_Dev_Drv_AddEndpoint</name>
        <param>
          <type>USBFS_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usb__dev__ep__config__t">cy_stc_usb_dev_ep_config_t</ref> const *</type>
          <declname>config</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Configures a data endpoint for the following operation (allocates hardware resources for data endpoint). </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>config</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to data endpoint configuration <ref kindref="compound" refid="structcy__stc__usb__dev__ep__config__t">cy_stc_usb_dev_ep_config_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> allocated by the user. The structure is used during the USBFS Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status code of the function execution <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2122" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" bodystart="2107" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1250" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__hal__functions__endpoint__config_1ga732e3eb9471c9587f3305eaacb4cd1a2" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref></type>
        <definition>cy_en_usbfs_dev_drv_status_t Cy_USBFS_Dev_Drv_RemoveEndpoint</definition>
        <argsstring>(USBFS_Type *base, uint32_t endpointAddr, cy_stc_usbfs_dev_drv_context_t *context)</argsstring>
        <name>Cy_USBFS_Dev_Drv_RemoveEndpoint</name>
        <param>
          <type>USBFS_Type *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>endpointAddr</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Removes a data endpoint (release hardware resources allocated by data endpoint). </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>endpointAddr</parametername>
</parameternamelist>
<parameterdescription>
<para>The data endpoint address (7 bit - direction, 3-0 bits - endpoint number).</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> allocated by the user. The structure is used during the USBFS Device operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Status code of the function execution <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1gace2573b482baf5a2a0f796b72cc244ac">cy_en_usbfs_dev_drv_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="473" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_usbfs_dev_drv_io.c" bodystart="441" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1254" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__usbfs__dev__hal__functions__endpoint__config_1ga73f805a814fa4a1c66dc4224d2002319" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_USBFS_Dev_Drv_OverwriteMemcpy</definition>
        <argsstring>(USBFS_Type const *base, uint32_t endpoint, cy_fn_usbfs_dev_drv_memcpy_ptr_t memcpyFunc, cy_stc_usbfs_dev_drv_context_t *context)</argsstring>
        <name>Cy_USBFS_Dev_Drv_OverwriteMemcpy</name>
        <param>
          <type>USBFS_Type const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>endpoint</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__usbfs__dev__drv__data__structures_1gaece00619a6241ca4ed9b05bd937269c1">cy_fn_usbfs_dev_drv_memcpy_ptr_t</ref></type>
          <declname>memcpyFunc</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Overwrites the memory copy (memcpy) function used to copy data with the user- implemented: </para>        </briefdescription>
        <detaileddescription>
<para><itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__usbfs__dev__hal__functions__data__xfer_1ga3699408b8336670b36c2e7a3fa6949d7">Cy_USBFS_Dev_Drv_ReadOutEndpoint</ref> copies data from from the internal buffer to the application buffer for OUT endpoint.</para></listitem><listitem><para><ref kindref="member" refid="group__group__usbfs__dev__hal__functions__data__xfer_1ga65db7019eafec3e38e8fbca316f92673">Cy_USBFS_Dev_Drv_LoadInEndpoint</ref> copies data from the application buffer for IN endpoint to the the internal buffer. Only applicable when endpoint management mode is <ref kindref="member" refid="group__group__usbfs__dev__drv__enums_1ggaaf6cf71cc366279eb82aefed4f81e2d1aad338d6b20d33a6cff7f47e2b883ddec">CY_USBFS_DEV_DRV_EP_MANAGEMENT_DMA_AUTO</ref>.</para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the USBFS instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>endpoint</parametername>
</parameternamelist>
<parameterdescription>
<para>The data endpoint number.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>memcpyFunc</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the function that copies data.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__usbfs__dev__drv__context__t">cy_stc_usbfs_dev_drv_context_t</ref> allocated by the user. The structure is used during the USBFS Device operation for internal configuration and data retention. The user must not modify anything in this structure. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2166" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" bodystart="2154" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_usbfs_dev_drv.h" line="1258" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>The Data Endpoint Configuration Functions provide an API to allocate and release hardware resources and override the memcpy function for the data endpoints. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>