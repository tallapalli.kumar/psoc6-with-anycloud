<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__scb__uart__high__level__functions" kind="group">
    <compoundname>group_scb_uart_high_level_functions</compoundname>
    <title>High-Level</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__scb__uart__high__level__functions_1gaf31baac4127fbab570f27bde1df9c622" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_UART_StartRingBuffer</definition>
        <argsstring>(CySCB_Type *base, void *buffer, uint32_t size, cy_stc_scb_uart_context_t *context)</argsstring>
        <name>Cy_SCB_UART_StartRingBuffer</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Starts the receive ring buffer operation. </para>        </briefdescription>
        <detaileddescription>
<para>The RX interrupt source is configured to get data from the RX FIFO and put into the ring buffer.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the UART SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to the user defined ring buffer. The element size is defined by the data type, which depends on the configured data width.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of the receive ring buffer. Note that one data element is used for internal use, so if the size is 32, then only 31 data elements are used for data storage.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> allocated by the user. The structure is used during the UART operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;The buffer must not be modified and stay allocated while the ring buffer operates.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;This function overrides the RX interrupt sources and changes the RX FIFO level. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="476" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="454" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="595" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__uart__high__level__functions_1gab759a9b7c7d5406699833c049fdaa72c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_UART_StopRingBuffer</definition>
        <argsstring>(CySCB_Type *base, cy_stc_scb_uart_context_t *context)</argsstring>
        <name>Cy_SCB_UART_StopRingBuffer</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Stops receiving data into the ring buffer and clears the ring buffer. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the UART SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> allocated by the user. The structure is used during the UART operation for internal configuration and data retention. The user must not modify anything in this structure. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="521" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="514" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="597" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__uart__high__level__functions_1gaa497f01cc0fdc16bc050869160138954" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_UART_GetNumInRingBuffer</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_uart_context_t const *context)</argsstring>
        <name>Cy_SCB_UART_GetNumInRingBuffer</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the number of data elements in the ring buffer. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the UART SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> allocated by the user. The structure is used during the UART operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The number of data elements in the receive ring buffer.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;One data element is used for internal use, so when the buffer is full, this function returns (Ring Buffer size - 1). &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="565" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="547" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="598" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__uart__high__level__functions_1ga4fe298d9d23ced2826998a44dde923c1" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_UART_ClearRingBuffer</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_uart_context_t *context)</argsstring>
        <name>Cy_SCB_UART_ClearRingBuffer</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Clears the ring buffer. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the UART SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> allocated by the user. The structure is used during the UART operation for internal configuration and data retention. The user must not modify anything in this structure. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="590" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="584" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="599" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__uart__high__level__functions_1gace7421c664f582ab4ad4bade0683f209" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__scb__uart__enums_1gac72413306ca26b7b8174526e3fc539df">cy_en_scb_uart_status_t</ref></type>
        <definition>cy_en_scb_uart_status_t Cy_SCB_UART_Receive</definition>
        <argsstring>(CySCB_Type *base, void *buffer, uint32_t size, cy_stc_scb_uart_context_t *context)</argsstring>
        <name>Cy_SCB_UART_Receive</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>This function starts a UART receive operation. </para>        </briefdescription>
        <detaileddescription>
<para>It configures the receive interrupt sources to get data available in the receive FIFO and returns. The <ref kindref="member" refid="group__group__scb__uart__interrupt__functions_1ga3e1928d8f20b2685aad02df29b658e3f">Cy_SCB_UART_Interrupt</ref> manages the further data transfer.</para><para>If the ring buffer is enabled, this function first reads data from the ring buffer. If there is more data to receive, it configures the receive interrupt sources to copy the remaining bytes from the RX FIFO when they arrive.</para><para>When the receive operation is completed (requested number of data elements received) the <ref kindref="member" refid="group__group__scb__uart__macros__receive__status_1ga5ddb60f6cf8432fb0e2bc15b1a353902">CY_SCB_UART_RECEIVE_ACTIVE</ref> status is cleared and the <ref kindref="member" refid="group__group__scb__uart__macros__callback__events_1ga027091599ffb1698dc4c9e2877e17103">CY_SCB_UART_RECEIVE_DONE_EVENT</ref> event is generated.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the UART SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to buffer to store received data. The element size is defined by the data type, which depends on the configured data width.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of data elements to receive.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> allocated by the user. The structure is used during the UART operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__scb__uart__enums_1gac72413306ca26b7b8174526e3fc539df">cy_en_scb_uart_status_t</ref></para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;The buffer must not be modified and stay allocated until end of the receive operation.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;This function overrides the RX interrupt sources and changes the RX FIFO level. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="752" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="637" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="601" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__uart__high__level__functions_1gaf55cfa886e03cf485b6491d2f8ecfb30" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_UART_AbortReceive</definition>
        <argsstring>(CySCB_Type *base, cy_stc_scb_uart_context_t *context)</argsstring>
        <name>Cy_SCB_UART_AbortReceive</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Abort the current receive operation by clearing the receive status. </para>        </briefdescription>
        <detaileddescription>
<para><itemizedlist>
<listitem><para>If the ring buffer is disabled, the receive interrupt sources are disabled.</para></listitem><listitem><para>If the ring buffer is enabled, the receive interrupt source is configured to get data from the receive FIFO and put it into the ring buffer.</para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the UART SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> allocated by the user. The structure is used during the UART operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;The RX FIFO and ring buffer are not cleared after abort of receive operation.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;If after the abort of the receive operation the transmitter continues sending data, it gets into the RX FIFO. To drop this data, the RX FIFO and ring buffer (if enabled) must be cleared when the transmitter stops sending data. Otherwise, received data will be kept and copied to the buffer when &lt;a href="group__group__scb__uart__high__level__functions.html#group__group__scb__uart__high__level__functions_1gace7421c664f582ab4ad4bade0683f209"&gt;Cy_SCB_UART_Receive&lt;/a&gt; is called. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="794" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="783" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="603" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__uart__high__level__functions_1ga4cc7bd2f28ef5e170e663414dba66f8c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_UART_GetReceiveStatus</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_uart_context_t const *context)</argsstring>
        <name>Cy_SCB_UART_GetReceiveStatus</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the status of the receive operation. </para>        </briefdescription>
        <detaileddescription>
<para>This status is a bit mask and the value returned may have multiple bits set.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the UART SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> allocated by the user. The structure is used during the UART operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="compound" refid="group__group__scb__uart__macros__receive__status">UART Receive Statuses</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The status is only cleared by calling &lt;a href="group__group__scb__uart__high__level__functions.html#group__group__scb__uart__high__level__functions_1gace7421c664f582ab4ad4bade0683f209"&gt;Cy_SCB_UART_Receive&lt;/a&gt; again. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="855" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="849" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="604" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__uart__high__level__functions_1ga0f3adeb3117ac83fb4ab75a5be72a44f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_UART_GetNumReceived</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_uart_context_t const *context)</argsstring>
        <name>Cy_SCB_UART_GetNumReceived</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the number of data elements received since the last call to <ref kindref="member" refid="group__group__scb__uart__high__level__functions_1gace7421c664f582ab4ad4bade0683f209">Cy_SCB_UART_Receive</ref>. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the UART SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> allocated by the user. The structure is used during the UART operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The number of data elements received. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="823" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="817" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="605" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__uart__high__level__functions_1ga12cc7712f9cb5e26b28d1d9a3a9576b4" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__scb__uart__enums_1gac72413306ca26b7b8174526e3fc539df">cy_en_scb_uart_status_t</ref></type>
        <definition>cy_en_scb_uart_status_t Cy_SCB_UART_Transmit</definition>
        <argsstring>(CySCB_Type *base, void *buffer, uint32_t size, cy_stc_scb_uart_context_t *context)</argsstring>
        <name>Cy_SCB_UART_Transmit</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>This function starts a UART transmit operation. </para>        </briefdescription>
        <detaileddescription>
<para>It configures the transmit interrupt sources and returns. The <ref kindref="member" refid="group__group__scb__uart__interrupt__functions_1ga3e1928d8f20b2685aad02df29b658e3f">Cy_SCB_UART_Interrupt</ref> manages the further data transfer.</para><para>When the transmit operation is completed (requested number of data elements sent on the bus), the <ref kindref="member" refid="group__group__scb__uart__macros__transmit__status_1gaa640df2a6637fd851cfcce6665ba93f1">CY_SCB_UART_TRANSMIT_ACTIVE</ref> status is cleared and the <ref kindref="member" refid="group__group__scb__uart__macros__callback__events_1gaf7e87969a0f31bbcd1bcc84d8266c7b9">CY_SCB_UART_TRANSMIT_DONE_EVENT</ref> event is generated.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the UART SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to user data to place in transmit buffer. The element size is defined by the data type, which depends on the configured data width.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of data elements to transmit.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> allocated by the user. The structure is used during the UART operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__scb__uart__enums_1gac72413306ca26b7b8174526e3fc539df">cy_en_scb_uart_status_t</ref></para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;The buffer must not be modified and must stay allocated until its content is copied into the TX FIFO.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;This function overrides the TX FIFO interrupt sources and changes the TX FIFO level. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="935" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="897" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="607" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__uart__high__level__functions_1ga2e860593a2acc245b432ebd59e5780b7" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_UART_AbortTransmit</definition>
        <argsstring>(CySCB_Type *base, cy_stc_scb_uart_context_t *context)</argsstring>
        <name>Cy_SCB_UART_AbortTransmit</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Aborts the current transmit operation. </para>        </briefdescription>
        <detaileddescription>
<para>It disables the transmit interrupt sources and clears the transmit FIFO and status.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the UART SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> allocated by the user. The structure is used during the UART operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="par"><title>Side Effects</title><para>The transmit FIFO clear operation also clears the shift register, so that the shifter can be cleared in the middle of a data element transfer, corrupting it. The data element corruption means that all bits that have not been transmitted are transmitted as "ones" on the bus. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="972" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="962" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="609" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__uart__high__level__functions_1ga92ea29d41db81f995b0ff442d6639191" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_UART_GetTransmitStatus</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_uart_context_t const *context)</argsstring>
        <name>Cy_SCB_UART_GetTransmitStatus</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the status of the transmit operation. </para>        </briefdescription>
        <detaileddescription>
<para>This status is a bit mask and the value returned may have multiple bits set.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the UART SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> allocated by the user. The structure is used during the UART operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="compound" refid="group__group__scb__uart__macros__transmit__status">UART Transmit Status</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The status is only cleared by calling &lt;a href="group__group__scb__uart__high__level__functions.html#group__group__scb__uart__high__level__functions_1ga12cc7712f9cb5e26b28d1d9a3a9576b4"&gt;Cy_SCB_UART_Transmit&lt;/a&gt; or &lt;a href="group__group__scb__uart__high__level__functions.html#group__group__scb__uart__high__level__functions_1ga2e860593a2acc245b432ebd59e5780b7"&gt;Cy_SCB_UART_AbortTransmit&lt;/a&gt;. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1034" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="1028" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="610" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__uart__high__level__functions_1gaa718862ebaad835b183ed04d7bc65f2d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_UART_GetNumLeftToTransmit</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_uart_context_t const *context)</argsstring>
        <name>Cy_SCB_UART_GetNumLeftToTransmit</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the number of data elements left to transmit since the last call to <ref kindref="member" refid="group__group__scb__uart__high__level__functions_1ga12cc7712f9cb5e26b28d1d9a3a9576b4">Cy_SCB_UART_Transmit</ref>. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the UART SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__uart__context__t">cy_stc_scb_uart_context_t</ref> allocated by the user. The structure is used during the UART operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The number of data elements left to transmit. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1001" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="995" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="611" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>