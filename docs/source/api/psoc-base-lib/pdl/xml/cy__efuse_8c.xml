<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cy__efuse_8c" kind="file" language="C++">
    <compoundname>cy_efuse.c</compoundname>
    <includes local="yes" refid="cy__efuse_8h">cy_efuse.h</includes>
    <includes local="yes" refid="cy__ipc__drv_8h">cy_ipc_drv.h</includes>
      <sectiondef kind="var">
      <memberdef id="cy__efuse_8c_1aa9dc8e1ad5deca480c8b2c0dc41a5b6e" kind="variable" mutable="no" prot="public" static="yes">
        <type>uint32_t</type>
        <definition>volatile uint32_t opcode</definition>
        <argsstring />
        <name>opcode</name>
        <briefdescription>
        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" bodystart="43" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" line="43" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="cy__efuse_8c_1a6d0fa926090f8b5eb7dc50117a058dc0" inline="no" kind="function" prot="public" static="yes" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref></type>
        <definition>static cy_en_efuse_status_t ProcessOpcode</definition>
        <argsstring>(void)</argsstring>
        <name>ProcessOpcode</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Converts System Call returns to the eFuse driver return defines. </para>        </briefdescription>
        <detaileddescription>
<para>If an unknown error was returned, the error code can be accessed via the <ref kindref="member" refid="group__group__efuse__functions_1ga19b1ee83f64f5405681b37266941c3e5">Cy_EFUSE_GetExternalStatus()</ref> function.</para><para><simplesect kind="return"><para><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="234" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" bodystart="210" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" line="45" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__efuse__functions_1ga2dc80ca522388529fe573f5c46694f2f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref></type>
        <definition>cy_en_efuse_status_t Cy_EFUSE_GetEfuseBit</definition>
        <argsstring>(uint32_t bitNum, bool *bitVal)</argsstring>
        <name>Cy_EFUSE_GetEfuseBit</name>
        <param>
          <type>uint32_t</type>
          <declname>bitNum</declname>
        </param>
        <param>
          <type>bool *</type>
          <declname>bitVal</declname>
        </param>
        <briefdescription>
<para>Reports the current state of a given eFuse bit-number. </para>        </briefdescription>
        <detaileddescription>
<para>Consult the device TRM to determine the target fuse bit number.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;An attempt to read an eFuse data from a protected memory region will generate a HardFault.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>bitNum</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of the bit to read. The valid range of the bit number is from 0 to EFUSE_EFUSE_NR * 32 * 8 - 1 where:<itemizedlist>
<listitem><para>EFUSE_EFUSE_NR is number of efuse macros in the selected device series,</para></listitem><listitem><para>32 is a number of fuse bytes in one efuse macro,</para></listitem><listitem><para>8 is a number of fuse bits in the byte.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
The EFUSE_EFUSE_NR macro is defined in the series-specific header file, e.g <emphasis>&lt;PDL_DIR&gt;/devices/include/psoc6_01_config</emphasis>.<emphasis>h</emphasis> </para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>bitVal</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the location to store the bit value.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref></para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para>The example below shows how to read device life-cycle register bits in PSoC 6: <programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />offset<sp />=<sp />offsetof(<ref kindref="compound" refid="structcy__stc__efuse__data__t">cy_stc_efuse_data_t</ref>,<sp />LIFECYCLE_STAGE.SECURE);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />bitVal;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<sp /><ref kindref="member" refid="group__group__efuse__functions_1ga2dc80ca522388529fe573f5c46694f2f">Cy_EFUSE_GetEfuseBit</ref>(offset,<sp />&amp;bitVal);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="100" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" bodystart="79" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" line="79" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__efuse__functions_1ga975d7896953da9ad0c2543f481cbfcc6" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref></type>
        <definition>cy_en_efuse_status_t Cy_EFUSE_GetEfuseByte</definition>
        <argsstring>(uint32_t offset, uint8_t *byteVal)</argsstring>
        <name>Cy_EFUSE_GetEfuseByte</name>
        <param>
          <type>uint32_t</type>
          <declname>offset</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>byteVal</declname>
        </param>
        <briefdescription>
<para>Reports the current state of the eFuse byte. </para>        </briefdescription>
        <detaileddescription>
<para>If the offset parameter is beyond the available quantities, zeroes will be stored to the byteVal parameter. Consult the device TRM to determine the target fuse byte offset.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;An attempt to read an eFuse data from a protected memory region will generate a HardFault.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>offset</parametername>
</parameternamelist>
<parameterdescription>
<para>The offset of the byte to read. The valid range of the byte offset is from 0 to EFUSE_EFUSE_NR * 32 - 1 where:<itemizedlist>
<listitem><para>EFUSE_EFUSE_NR is a number of efuse macros in the selected device series,</para></listitem><listitem><para>32 is a number of fuse bytes in one efuse macro.</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
The EFUSE_EFUSE_NR macro is defined in the series-specific header file, e.g <emphasis>&lt;PDL_DIR&gt;/devices/include/psoc6_01_config</emphasis>.<emphasis>h</emphasis> </para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>byteVal</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the location to store eFuse data.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref></para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para>The example below shows how to read a device life-cycle stage register in PSoC 6: <programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Determine<sp />the<sp />offset<sp />of<sp />the<sp />byte<sp />to<sp />read.<sp />Divide<sp />by<sp />8<sp />since<sp />the<sp />one<sp />byte<sp />in</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp />cy_stc_efuse_data_t<sp />struct<sp />corresponds<sp />to<sp />the<sp />one<sp />bit<sp />in<sp />the<sp />eFuse<sp />memory.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />offset<sp />=<sp />offsetof(<ref kindref="compound" refid="structcy__stc__efuse__data__t">cy_stc_efuse_data_t</ref>,<sp />LIFECYCLE_STAGE.NORMAL)<sp />/<sp /><ref kindref="member" refid="group__group__efuse__macros_1ga34bda47f01021424e981b32de0d8a7f6">CY_EFUSE_BITS_PER_BYTE</ref>;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint8_t<sp />byteVal;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(void)<sp /><ref kindref="member" refid="group__group__efuse__functions_1ga975d7896953da9ad0c2543f481cbfcc6">Cy_EFUSE_GetEfuseByte</ref>(offset,<sp />&amp;byteVal);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="171" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" bodystart="136" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" line="136" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__efuse__functions_1ga19b1ee83f64f5405681b37266941c3e5" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_EFUSE_GetExternalStatus</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_EFUSE_GetExternalStatus</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This function handles the case where a module such as a security image captures a system call from this driver and reports its own status or error code, for example, protection violation. </para>        </briefdescription>
        <detaileddescription>
<para>In that case, a function from this driver returns an unknown error (see <ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref>). After receipt of an unknown error, the user may call this function to get the status of the capturing module.</para><para>The user is responsible for parsing the content of the returned value and casting it to the appropriate enumeration.</para><para><simplesect kind="return"><para>The error code of the previous efuse operation. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="195" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" bodystart="192" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" line="192" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>Provides API implementation of the eFuse driver. </para>    </briefdescription>
    <detaileddescription>
<para><simplesect kind="version"><para>1.10.3</para></simplesect>
<simplesect kind="copyright"><para>Copyright 2017-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para></simplesect>
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at <verbatim>http://www.apache.org/licenses/LICENSE-2.0
</verbatim></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight></codeline>
<codeline lineno="26"><highlight class="preprocessor">#include<sp />"cy_efuse.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="27"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_ipc_drv.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="28"><highlight class="normal" /></codeline>
<codeline lineno="29"><highlight class="normal" /><highlight class="preprocessor">#ifdef<sp />CY_IP_MXEFUSE</highlight><highlight class="normal" /></codeline>
<codeline lineno="30"><highlight class="normal" /></codeline>
<codeline lineno="32"><highlight class="preprocessor">#define<sp />CY_EFUSE_OPCODE_SUCCESS<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(0xA0000000UL)<sp /><sp /><sp /><sp /></highlight></codeline>
<codeline lineno="33"><highlight class="preprocessor">#define<sp />CY_EFUSE_OPCODE_STS_Msk<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(0xF0000000UL)<sp /><sp /><sp /><sp /></highlight></codeline>
<codeline lineno="34"><highlight class="preprocessor">#define<sp />CY_EFUSE_OPCODE_INV_PROT<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(0xF0000001UL)<sp /><sp /><sp /><sp /></highlight></codeline>
<codeline lineno="35"><highlight class="preprocessor">#define<sp />CY_EFUSE_OPCODE_INV_ADDR<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(0xF0000002UL)<sp /><sp /><sp /><sp /></highlight></codeline>
<codeline lineno="36"><highlight class="preprocessor">#define<sp />CY_EFUSE_OPCODE_READ_FUSE_BYTE<sp /><sp /><sp /><sp /><sp /><sp />(0x03000000UL)<sp /><sp /><sp /><sp /></highlight></codeline>
<codeline lineno="37"><highlight class="preprocessor">#define<sp />CY_EFUSE_OPCODE_OFFSET_Pos<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(8UL)<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline lineno="38"><highlight class="preprocessor">#define<sp />CY_EFUSE_OPCODE_DATA_Msk<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(0xFFUL)<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline lineno="39"><highlight class="preprocessor">#define<sp />CY_EFUSE_IPC_STRUCT<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(Cy_IPC_Drv_GetIpcBaseAddress(CY_IPC_CHAN_SYSCALL))<sp /></highlight></codeline>
<codeline lineno="40"><highlight class="preprocessor">#define<sp />CY_EFUSE_IPC_NOTIFY_STRUCT0<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(0x1UL<sp />&lt;&lt;<sp />CY_IPC_INTR_SYSCALL1)<sp /></highlight></codeline>
<codeline lineno="43"><highlight class="preprocessor">static<sp />volatile<sp />uint32_t<sp />opcode;</highlight><highlight class="normal" /></codeline>
<codeline lineno="44"><highlight class="normal" /></codeline>
<codeline lineno="45"><highlight class="normal" /><highlight class="keyword">static</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref><sp />ProcessOpcode(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">);</highlight></codeline>
<codeline lineno="46"><highlight class="normal" /></codeline>
<codeline lineno="47"><highlight class="normal" /><highlight class="comment">/*******************************************************************************</highlight></codeline>
<codeline lineno="48"><highlight class="comment">*<sp />Function<sp />Name:<sp />Cy_EFUSE_GetEfuseBit</highlight></codeline>
<codeline lineno="49"><highlight class="comment">****************************************************************************/</highlight></codeline>
<codeline lineno="79"><highlight class="normal"><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref><sp /><ref kindref="member" refid="group__group__efuse__functions_1ga2dc80ca522388529fe573f5c46694f2f">Cy_EFUSE_GetEfuseBit</ref>(uint32_t<sp />bitNum,<sp /></highlight><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />*bitVal)</highlight></codeline>
<codeline lineno="80"><highlight class="normal">{</highlight></codeline>
<codeline lineno="81"><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__efuse__enumerated__types_1ggad6c57e8e4b9500208ff1f6d27f6055b0a568115accc57066afda6e297b3036b87">CY_EFUSE_BAD_PARAM</ref>;</highlight></codeline>
<codeline lineno="82"><highlight class="normal" /></codeline>
<codeline lineno="83"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(bitVal<sp />!=<sp />NULL)</highlight></codeline>
<codeline lineno="84"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="85"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint32_t<sp />offset<sp />=<sp />bitNum<sp />/<sp /><ref kindref="member" refid="group__group__efuse__macros_1ga34bda47f01021424e981b32de0d8a7f6">CY_EFUSE_BITS_PER_BYTE</ref>;</highlight></codeline>
<codeline lineno="86"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint8_t<sp />byteVal;</highlight></codeline>
<codeline lineno="87"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*bitVal<sp />=<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">;</highlight></codeline>
<codeline lineno="88"><highlight class="normal" /></codeline>
<codeline lineno="89"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Read<sp />the<sp />eFuse<sp />byte<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="90"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__efuse__functions_1ga975d7896953da9ad0c2543f481cbfcc6">Cy_EFUSE_GetEfuseByte</ref>(offset,<sp />&amp;byteVal);</highlight></codeline>
<codeline lineno="91"><highlight class="normal" /></codeline>
<codeline lineno="92"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(result<sp />==<sp /><ref kindref="member" refid="group__group__efuse__enumerated__types_1ggad6c57e8e4b9500208ff1f6d27f6055b0a0dcde15a6d117de55a450ab911bcfef3">CY_EFUSE_SUCCESS</ref>)</highlight></codeline>
<codeline lineno="93"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="94"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint32_t<sp />bitPos<sp />=<sp />bitNum<sp />%<sp /><ref kindref="member" refid="group__group__efuse__macros_1ga34bda47f01021424e981b32de0d8a7f6">CY_EFUSE_BITS_PER_BYTE</ref>;</highlight></codeline>
<codeline lineno="95"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Extract<sp />the<sp />bit<sp />from<sp />the<sp />byte<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="96"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*bitVal<sp />=<sp />(((byteVal<sp />&gt;&gt;<sp />bitPos)<sp />&amp;<sp />0x01U)<sp />!=<sp />0U);</highlight></codeline>
<codeline lineno="97"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="98"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="99"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />(result);</highlight></codeline>
<codeline lineno="100"><highlight class="normal">}</highlight></codeline>
<codeline lineno="101"><highlight class="normal" /></codeline>
<codeline lineno="102"><highlight class="normal" /></codeline>
<codeline lineno="103"><highlight class="normal" /><highlight class="comment">/*******************************************************************************</highlight></codeline>
<codeline lineno="104"><highlight class="comment">*<sp />Function<sp />Name:<sp />Cy_EFUSE_GetEfuseByte</highlight></codeline>
<codeline lineno="105"><highlight class="comment">****************************************************************************/</highlight></codeline>
<codeline lineno="136"><highlight class="normal"><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref><sp /><ref kindref="member" refid="group__group__efuse__functions_1ga975d7896953da9ad0c2543f481cbfcc6">Cy_EFUSE_GetEfuseByte</ref>(uint32_t<sp />offset,<sp />uint8_t<sp />*byteVal)</highlight></codeline>
<codeline lineno="137"><highlight class="normal">{</highlight></codeline>
<codeline lineno="138"><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__efuse__enumerated__types_1ggad6c57e8e4b9500208ff1f6d27f6055b0a568115accc57066afda6e297b3036b87">CY_EFUSE_BAD_PARAM</ref>;</highlight></codeline>
<codeline lineno="139"><highlight class="normal" /></codeline>
<codeline lineno="140"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(byteVal<sp />!=<sp />NULL)</highlight></codeline>
<codeline lineno="141"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="142"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Prepare<sp />opcode<sp />before<sp />calling<sp />the<sp />SROM<sp />API<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="143"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />opcode<sp />=<sp />CY_EFUSE_OPCODE_READ_FUSE_BYTE<sp />|<sp />(offset<sp />&lt;&lt;<sp />CY_EFUSE_OPCODE_OFFSET_Pos);</highlight></codeline>
<codeline lineno="144"><highlight class="normal" /></codeline>
<codeline lineno="145"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Send<sp />the<sp />IPC<sp />message<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="146"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__ipc__functions_1gab9e1b6079cd81d9b918b354d9437744d">Cy_IPC_Drv_SendMsgPtr</ref>(CY_EFUSE_IPC_STRUCT,<sp />CY_EFUSE_IPC_NOTIFY_STRUCT0,<sp />(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">*)&amp;opcode)<sp />==<sp /><ref kindref="member" refid="group__group__ipc__enums_1ggab838549fe808bd6cb1bfc52d2b5cbf25a8748b3bf3a2b39bd469c0d41217a2cb2">CY_IPC_DRV_SUCCESS</ref>)</highlight></codeline>
<codeline lineno="147"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="148"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Wait<sp />until<sp />the<sp />IPC<sp />structure<sp />is<sp />locked<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="149"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">while</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__ipc__functions_1ga1ed2f8f6a0343bdeedbe4f81c0df5e1a">Cy_IPC_Drv_IsLockAcquired</ref>(CY_EFUSE_IPC_STRUCT)<sp />!=<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">)</highlight></codeline>
<codeline lineno="150"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="151"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="152"><highlight class="normal" /></codeline>
<codeline lineno="153"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />The<sp />result<sp />of<sp />the<sp />SROM<sp />API<sp />call<sp />is<sp />returned<sp />to<sp />the<sp />opcode<sp />variable<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="154"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />((opcode<sp />&amp;<sp />CY_EFUSE_OPCODE_STS_Msk)<sp />==<sp />CY_EFUSE_OPCODE_SUCCESS)</highlight></codeline>
<codeline lineno="155"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="156"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*byteVal<sp />=<sp />(uint8_t)(opcode<sp />&amp;<sp />CY_EFUSE_OPCODE_DATA_Msk);</highlight></codeline>
<codeline lineno="157"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__efuse__enumerated__types_1ggad6c57e8e4b9500208ff1f6d27f6055b0a0dcde15a6d117de55a450ab911bcfef3">CY_EFUSE_SUCCESS</ref>;</highlight></codeline>
<codeline lineno="158"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="159"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline lineno="160"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="161"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp />ProcessOpcode();</highlight></codeline>
<codeline lineno="162"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*byteVal<sp />=<sp />0U;</highlight></codeline>
<codeline lineno="163"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="164"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="165"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">else</highlight><highlight class="normal" /></codeline>
<codeline lineno="166"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="167"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__efuse__enumerated__types_1ggad6c57e8e4b9500208ff1f6d27f6055b0accd064505cd2fd71c40eb2f535920a16">CY_EFUSE_IPC_BUSY</ref>;</highlight></codeline>
<codeline lineno="168"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="169"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="170"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />(result);</highlight></codeline>
<codeline lineno="171"><highlight class="normal">}</highlight></codeline>
<codeline lineno="172"><highlight class="normal" /></codeline>
<codeline lineno="173"><highlight class="normal" /></codeline>
<codeline lineno="174"><highlight class="normal" /><highlight class="comment">/*******************************************************************************</highlight></codeline>
<codeline lineno="175"><highlight class="comment">*<sp />Function<sp />Name:<sp />Cy_EFUSE_GetExternalStatus</highlight></codeline>
<codeline lineno="176"><highlight class="comment">****************************************************************************/</highlight></codeline>
<codeline lineno="192"><highlight class="normal">uint32_t<sp /><ref kindref="member" refid="group__group__efuse__functions_1ga19b1ee83f64f5405681b37266941c3e5">Cy_EFUSE_GetExternalStatus</ref>(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">)</highlight></codeline>
<codeline lineno="193"><highlight class="normal">{</highlight></codeline>
<codeline lineno="194"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />(opcode);</highlight></codeline>
<codeline lineno="195"><highlight class="normal">}</highlight></codeline>
<codeline lineno="196"><highlight class="normal" /></codeline>
<codeline lineno="197"><highlight class="normal" /></codeline>
<codeline lineno="198"><highlight class="normal" /><highlight class="comment">/*******************************************************************************</highlight></codeline>
<codeline lineno="199"><highlight class="comment">*<sp />Function<sp />Name:<sp />ProcessOpcode</highlight></codeline>
<codeline lineno="200"><highlight class="comment">****************************************************************************/</highlight></codeline>
<codeline lineno="210"><highlight class="keyword">static</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref><sp />ProcessOpcode(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">)</highlight></codeline>
<codeline lineno="211"><highlight class="normal">{</highlight></codeline>
<codeline lineno="212"><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__efuse__enumerated__types_1gad6c57e8e4b9500208ff1f6d27f6055b0">cy_en_efuse_status_t</ref><sp />result;</highlight></codeline>
<codeline lineno="213"><highlight class="normal" /></codeline>
<codeline lineno="214"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">switch</highlight><highlight class="normal">(opcode)</highlight></codeline>
<codeline lineno="215"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="216"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">case</highlight><highlight class="normal"><sp />CY_EFUSE_OPCODE_INV_PROT<sp />:</highlight></codeline>
<codeline lineno="217"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="218"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__efuse__enumerated__types_1ggad6c57e8e4b9500208ff1f6d27f6055b0af2414df94d160b87b7b5dedeff58472a">CY_EFUSE_INVALID_PROTECTION</ref>;</highlight></codeline>
<codeline lineno="219"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">break</highlight><highlight class="normal">;</highlight></codeline>
<codeline lineno="220"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="221"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">case</highlight><highlight class="normal"><sp />CY_EFUSE_OPCODE_INV_ADDR<sp />:</highlight></codeline>
<codeline lineno="222"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="223"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__efuse__enumerated__types_1ggad6c57e8e4b9500208ff1f6d27f6055b0aba76e5a4762395aa3448ed99ef77cd0a">CY_EFUSE_INVALID_FUSE_ADDR</ref>;</highlight></codeline>
<codeline lineno="224"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">break</highlight><highlight class="normal">;</highlight></codeline>
<codeline lineno="225"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="226"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />default<sp />:</highlight></codeline>
<codeline lineno="227"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="228"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__efuse__enumerated__types_1ggad6c57e8e4b9500208ff1f6d27f6055b0a7f40519e81c80b25cc4dc9dfbbf35cb6">CY_EFUSE_ERR_UNC</ref>;</highlight></codeline>
<codeline lineno="229"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">break</highlight><highlight class="normal">;</highlight></codeline>
<codeline lineno="230"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="231"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="232"><highlight class="normal" /></codeline>
<codeline lineno="233"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />(result);</highlight></codeline>
<codeline lineno="234"><highlight class="normal">}</highlight></codeline>
<codeline lineno="235"><highlight class="normal" /></codeline>
<codeline lineno="236"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />#ifdef<sp />CY_IP_MXEFUSE<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="237"><highlight class="normal" /></codeline>
<codeline lineno="238"><highlight class="normal" /><highlight class="comment">/*<sp />[]<sp />END<sp />OF<sp />FILE<sp />*/</highlight><highlight class="normal" /></codeline>
    </programlisting>
    <location file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_efuse.c" />
  </compounddef>
</doxygen>