<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__scb__i2c__slave__functions" kind="group">
    <compoundname>group_scb_i2c_slave_functions</compoundname>
    <title>Slave</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__slave__functions_1ga179b4495d7255567bb8b166e16e5a073" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_I2C_SlaveConfigReadBuf</definition>
        <argsstring>(CySCB_Type const *base, uint8_t *buffer, uint32_t size, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_SlaveConfigReadBuf</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Configures the buffer pointer and the read buffer size. </para>        </briefdescription>
        <detaileddescription>
<para>This is the buffer from which the master reads data. After this function is called, data transfer from the read buffer to the master is handled by <ref kindref="member" refid="group__group__scb__i2c__interrupt__functions_1ga729b2fa4de4d44ea2e8995d7ca6a0c24">Cy_SCB_I2C_Interrupt</ref>.</para><para>When the Read transaction is completed (master generated Stop, ReStart or error occurred), the <ref kindref="member" refid="group__group__scb__i2c__macros__slave__status_1ga2e400c423f7250710ddda059ff554c10">CY_SCB_I2C_SLAVE_RD_BUSY</ref> status is cleared and the <ref kindref="member" refid="group__group__scb__i2c__macros__slave__status_1ga8445a80c064b85b48f4cb89afa4880ae">CY_SCB_I2C_SLAVE_RD_CMPLT</ref> is set. Also the <ref kindref="member" refid="group__group__scb__i2c__macros__callback__events_1gae44345d38014821cc5d5c918dd074324">CY_SCB_I2C_SLAVE_RD_CMPLT_EVENT</ref> event is generated.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the buffer with data to be read by the master.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>Size of the buffer.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;The Read buffer must not be modified and stay allocated until it has been read by the master.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;If this function has not been called, and the master tries to read data from the slave a &lt;a href="group__group__scb__i2c__macros.html#group__group__scb__i2c__macros_1gad56a26de596ed39516b91adc50db4bf9"&gt;CY_SCB_I2C_DEFAULT_TX&lt;/a&gt; is returned to the master.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;If the master tries to read more bytes than available in the Read buffer, a &lt;a href="group__group__scb__i2c__macros__callback__events.html#group__group__scb__i2c__macros__callback__events_1gada6c481df3423c3c0d7db05fd626534a"&gt;CY_SCB_I2C_SLAVE_RD_BUF_EMPTY_EVENT&lt;/a&gt; event occurs. The &lt;a href="group__group__scb__i2c__macros.html#group__group__scb__i2c__macros_1gad56a26de596ed39516b91adc50db4bf9"&gt;CY_SCB_I2C_DEFAULT_TX&lt;/a&gt; is returned to the master if the buffer remains empty after an event notification. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="851" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="839" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="663" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__slave__functions_1ga1c9f6c807f4758016bf90cc8f69f96b3" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_I2C_SlaveAbortRead</definition>
        <argsstring>(CySCB_Type *base, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_SlaveAbortRead</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Aborts the configured slave read buffer to be read by the master. </para>        </briefdescription>
        <detaileddescription>
<para>If the master reads and "abort operation" is requested, the <ref kindref="member" refid="group__group__scb__i2c__macros__callback__events_1gada6c481df3423c3c0d7db05fd626534a">CY_SCB_I2C_SLAVE_RD_BUF_EMPTY_EVENT</ref> event occurs. The <ref kindref="member" refid="group__group__scb__i2c__macros_1gad56a26de596ed39516b91adc50db4bf9">CY_SCB_I2C_DEFAULT_TX</ref> is returned to the master if the buffer remains empty after the event notification.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="par"><title>Side Effects</title><para>If the TX FIFO is used, this function clears it. The TX FIFO clear operation also clears the shift register, thus the shifter can be cleared in the middle of a data element transfer, corrupting it. The data element corruption means that all bits that have not been transmitted are transmitted as "ones" on the bus. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="901" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="881" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="665" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__slave__functions_1gaaf2d216e9cb246dd2256521cb7f50170" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_I2C_SlaveConfigWriteBuf</definition>
        <argsstring>(CySCB_Type const *base, uint8_t *buffer, uint32_t size, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_SlaveConfigWriteBuf</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Configures the buffer pointer and size of the write buffer. </para>        </briefdescription>
        <detaileddescription>
<para>This is the buffer that the master writes data to. After this function is called data transfer from the master into the write buffer is handled by <ref kindref="member" refid="group__group__scb__i2c__interrupt__functions_1ga729b2fa4de4d44ea2e8995d7ca6a0c24">Cy_SCB_I2C_Interrupt</ref>.</para><para>When write transaction is completed (master generated Stop, ReStart or error occurred) the <ref kindref="member" refid="group__group__scb__i2c__macros__slave__status_1ga53cab9166005ca6c89ed24a4871bb5bd">CY_SCB_I2C_SLAVE_WR_BUSY</ref> status is cleared and the <ref kindref="member" refid="group__group__scb__i2c__macros__slave__status_1ga0e917e7b1f8b297bb97716a0a742a14a">CY_SCB_I2C_SLAVE_WR_CMPLT</ref> is set, also the <ref kindref="member" refid="group__group__scb__i2c__macros__callback__events_1gaa509ff3cd20d1b1d31e3c22e7c28871f">CY_SCB_I2C_SLAVE_WR_CMPLT_EVENT</ref> event is generated.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to buffer to store data written by the master.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>Size of the buffer.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;The write buffer must not be modified and must stay allocated until it has been written by the master.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;If this function has not been called and the master tries to write data, the first byte is NAKed and discarded.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;If the master writes more bytes than the slave can store in the write buffer, the &lt;a href="group__group__scb__i2c__macros__slave__status.html#group__group__scb__i2c__macros__slave__status_1ga72fb4c9b77ebeb06e36f7b74eb9f0a6f"&gt;CY_SCB_I2C_SLAVE_WR_OVRFL&lt;/a&gt; status is set and the slave will NACK last byte, unless the RX FIFO is used. Then the slave will NAK only after RX FIFO becomes full.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;If the RX FIFO is used, the minimum write buffer size is automatically the size of the RX FIFO. If a write buffer is less than the RX FIFO size, extra bytes are ACKed and stored into RX FIFO but ignored by firmware. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1029" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="1018" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="666" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__slave__functions_1ga50157a71ba992433bd4fde948d25d44e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_I2C_SlaveAbortWrite</definition>
        <argsstring>(CySCB_Type *base, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_SlaveAbortWrite</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Aborts the configured slave write buffer to be written by the master. </para>        </briefdescription>
        <detaileddescription>
<para>If master writes and an "abort operation" is requested, the next incoming byte will be NAKed.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;If the RX FIFO is used, the NAK will not be sent until RX FIFO becomes full, however bytes accepted after an abort request are ignored. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1077" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="1054" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="668" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__slave__functions_1gab3e1c474d6a5c7c5547ea08aa5d59e22" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_I2C_SlaveGetStatus</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_i2c_context_t const *context)</argsstring>
        <name>Cy_SCB_I2C_SlaveGetStatus</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the current I2C slave status. </para>        </briefdescription>
        <detaileddescription>
<para>This status is a bit mask and the value returned may have multiple bits set.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="compound" refid="group__group__scb__i2c__macros__slave__status">I2C Slave Status</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="796" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="790" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="670" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__slave__functions_1ga9499272c3ec7013ec130291239793199" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_I2C_SlaveClearReadStatus</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_SlaveClearReadStatus</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Clears the read status and error conditions flags and returns their values. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="compound" refid="group__group__scb__i2c__macros__slave__status">I2C Slave Status</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The &lt;a href="group__group__scb__i2c__macros__slave__status.html#group__group__scb__i2c__macros__slave__status_1ga2e400c423f7250710ddda059ff554c10"&gt;CY_SCB_I2C_SLAVE_RD_BUSY&lt;/a&gt; flag is not cleared. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="973" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="962" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="671" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__slave__functions_1ga73636c22f0db8c09d790f387aa35e6be" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_I2C_SlaveClearWriteStatus</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_SlaveClearWriteStatus</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Clears the write status flags and error condition flags and returns their values. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="compound" refid="group__group__scb__i2c__macros__slave__status">I2C Slave Status</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The &lt;a href="group__group__scb__i2c__macros__slave__status.html#group__group__scb__i2c__macros__slave__status_1ga53cab9166005ca6c89ed24a4871bb5bd"&gt;CY_SCB_I2C_SLAVE_WR_BUSY&lt;/a&gt; flag is not cleared. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1150" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="1139" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="672" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__slave__functions_1gaf8f2f880c12a526b2e459f3137c45d5c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_I2C_SlaveGetReadTransferCount</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_i2c_context_t const *context)</argsstring>
        <name>Cy_SCB_I2C_SlaveGetReadTransferCount</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the number of bytes read by the master since the last time <ref kindref="member" refid="group__group__scb__i2c__slave__functions_1ga179b4495d7255567bb8b166e16e5a073">Cy_SCB_I2C_SlaveConfigReadBuf</ref> was called. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The number of bytes read by the master.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;This function returns an invalid value if a read transaction was aborted or any listed event occurs during the transaction: &lt;a href="group__group__scb__i2c__macros__slave__status.html#group__group__scb__i2c__macros__slave__status_1ga63fcc61bddf39d12a85172f776a34d7b"&gt;CY_SCB_I2C_SLAVE_ARB_LOST&lt;/a&gt;, &lt;a href="group__group__scb__i2c__macros__slave__status.html#group__group__scb__i2c__macros__slave__status_1ga70d17df38c669816f83e69ac8ab02753"&gt;CY_SCB_I2C_SLAVE_BUS_ERR&lt;/a&gt;.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;This number is updated only when a transaction completes, either through an error or successfully. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="937" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="931" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="674" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__slave__functions_1ga3cf4dc1cc3f14c3ba9aa4679ca25c186" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_I2C_SlaveGetWriteTransferCount</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_i2c_context_t const *context)</argsstring>
        <name>Cy_SCB_I2C_SlaveGetWriteTransferCount</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the number of bytes written by the master since the last time <ref kindref="member" refid="group__group__scb__i2c__slave__functions_1gaaf2d216e9cb246dd2256521cb7f50170">Cy_SCB_I2C_SlaveConfigWriteBuf</ref> was called. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Number of bytes written by the master.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;This function returns an invalid value if write transaction was aborted or any listed event occurs during the transaction: &lt;a href="group__group__scb__i2c__macros__slave__status.html#group__group__scb__i2c__macros__slave__status_1ga63fcc61bddf39d12a85172f776a34d7b"&gt;CY_SCB_I2C_SLAVE_ARB_LOST&lt;/a&gt;, &lt;a href="group__group__scb__i2c__macros__slave__status.html#group__group__scb__i2c__macros__slave__status_1ga70d17df38c669816f83e69ac8ab02753"&gt;CY_SCB_I2C_SLAVE_BUS_ERR&lt;/a&gt;.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;This number is updated only when the transaction completes, either through an error or successfully. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1113" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="1107" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="675" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>