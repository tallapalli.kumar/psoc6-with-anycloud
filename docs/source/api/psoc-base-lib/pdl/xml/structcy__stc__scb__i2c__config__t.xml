<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="structcy__stc__scb__i2c__config__t" kind="struct" language="C++" prot="public">
    <compoundname>cy_stc_scb_i2c_config_t</compoundname>
    <includes local="no">cy_scb_i2c.h</includes>
      <sectiondef kind="public-attrib">
      <memberdef id="structcy__stc__scb__i2c__config__t_1ab317c1252433320bfd76d2b8629dac23" kind="variable" mutable="no" prot="public" static="no">
        <type><ref kindref="member" refid="group__group__scb__i2c__enums_1ga848386100a4d9700bf5bb790b3db3ed2">cy_en_scb_i2c_mode_t</ref></type>
        <definition>cy_en_scb_i2c_mode_t cy_stc_scb_i2c_config_t::i2cMode</definition>
        <argsstring />
        <name>i2cMode</name>
        <briefdescription>
<para>Specifies the mode of operation. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" bodystart="431" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="431" />
      </memberdef>
      <memberdef id="structcy__stc__scb__i2c__config__t_1a5051507a6a9adb8b9db39572c5f11340" kind="variable" mutable="no" prot="public" static="no">
        <type>bool</type>
        <definition>bool cy_stc_scb_i2c_config_t::useRxFifo</definition>
        <argsstring />
        <name>useRxFifo</name>
        <briefdescription>
<para>The SCB provides an RX FIFO in hardware (consult the selected device datasheet to get the actual FIFO size). </para>        </briefdescription>
        <detaileddescription>
<para>The useRxFifo field defines how the driver firmware reads data from the RX FIFO:<itemizedlist>
<listitem><para>If this option is enabled, the hardware is configured to automatically ACK incoming data, and interrupt is enabled to take data out of the RX FIFO when it has some number of bytes (typically, when it is half full).</para></listitem><listitem><para>If this option is disabled, the interrupt is enabled to take data out of the RX FIFO when a byte is available. Also, hardware does not automatically ACK the data. Firmware must tell the hardware to ACK the byte (so each byte requires interrupt processing). <linebreak />
 <bold>Typically, this option should be enabled</bold> to configure hardware to automatically ACK incoming data. Otherwise hardware might not get the command to ACK or NACK a byte fast enough, and clock stretching is applied (the transaction is delayed) until the command is set. When this option is enabled, the number of interrupts required to process the transaction is significantly reduced because several bytes are handled at once. <linebreak />
 <bold>However, there is a side effect:</bold></para></listitem><listitem><para>For master mode, the drawback is that the master may receive more data than desired due to the interrupt latency. An interrupt fires when the second-to-last byte has been received. This interrupt tells the hardware to stop receiving data. If the latency of this interrupt is longer than one transaction of the byte on the I2C bus, then the hardware automatically ACKs the following bytes until the interrupt is serviced or the RX FIFO becomes full.</para></listitem><listitem><para>For slave mode, the drawback is that the slave only NACKs the master when the RX FIFO becomes full, NOT when the slave write firmware buffer becomes full. <linebreak />
 In either master or slave mode, all received extra bytes are dropped. <verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The useRxFifo option is not available if acceptAddrInFifo is true. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para></listitem></itemizedlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" bodystart="464" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="464" />
      </memberdef>
      <memberdef id="structcy__stc__scb__i2c__config__t_1a2c4bded993c3b334fbc2321952ab99f5" kind="variable" mutable="no" prot="public" static="no">
        <type>bool</type>
        <definition>bool cy_stc_scb_i2c_config_t::useTxFifo</definition>
        <argsstring />
        <name>useTxFifo</name>
        <briefdescription>
<para>The SCB provides a TX FIFO in hardware (consult the selected device datasheet to get the actual FIFO size). </para>        </briefdescription>
        <detaileddescription>
<para>The useTxFifo option defines how the driver firmware loads data into the TX FIFO:<itemizedlist>
<listitem><para>If this option is enabled, the TX FIFO is fully loaded with data and the interrupt is enabled to keep the TX FIFO loaded until the end of the transaction.</para></listitem><listitem><para>If this option is disabled, a single byte is loaded into the TX FIFO and the interrupt enabled to load the next byte when the TX FIFO becomes empty (so each byte requires interrupt processing). <linebreak />
 <bold>Typically, this option should be enabled</bold> to keep the TX FIFO loaded with data and reduce the probability of clock stretching. When there is no data to transfer, clock stretching is applied (the transaction is delayed) until the data is loaded. When this option is enabled, the number of interrupts required to process the transaction is significantly reduced because several bytes are handled at once. <linebreak />
 <bold>The drawback of enabling useTxFifo</bold> is that the abort operation clears the TX FIFO. The TX FIFO clear operation also clears the shift register. As a result the shifter may be cleared in the middle of a byte transaction, corrupting it. The remaining bits to transaction within the corrupted byte are complemented with 1s. If this is an issue, then do not enable this option. </para></listitem></itemizedlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" bodystart="488" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="488" />
      </memberdef>
      <memberdef id="structcy__stc__scb__i2c__config__t_1a9e0bf167c747280bb8a1c2f4f134889b" kind="variable" mutable="no" prot="public" static="no">
        <type>uint8_t</type>
        <definition>uint8_t cy_stc_scb_i2c_config_t::slaveAddress</definition>
        <argsstring />
        <name>slaveAddress</name>
        <briefdescription>
<para>The 7-bit right justified slave address, used only for the slave mode. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" bodystart="493" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="493" />
      </memberdef>
      <memberdef id="structcy__stc__scb__i2c__config__t_1a3578fa3852e87ead95db3c0c13064d71" kind="variable" mutable="no" prot="public" static="no">
        <type>uint8_t</type>
        <definition>uint8_t cy_stc_scb_i2c_config_t::slaveAddressMask</definition>
        <argsstring />
        <name>slaveAddressMask</name>
        <briefdescription>
<para>The slave address mask is used to mask bits of the slave address during the address match procedure (it is used only for the slave mode). </para>        </briefdescription>
        <detaileddescription>
<para>Bit 0 of the address mask corresponds to the read/write direction bit and is always a do not care in the address match therefore must be set 0. Bit value 0 - excludes bit from address comparison. Bit value 1 - the bit needs to match with the corresponding bit of the I2C slave address. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" bodystart="504" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="504" />
      </memberdef>
      <memberdef id="structcy__stc__scb__i2c__config__t_1a9074db87cce95b20f6adbaf905e05f98" kind="variable" mutable="no" prot="public" static="no">
        <type>bool</type>
        <definition>bool cy_stc_scb_i2c_config_t::acceptAddrInFifo</definition>
        <argsstring />
        <name>acceptAddrInFifo</name>
        <briefdescription>
<para>True - the slave address is accepted in the RX FIFO, false - the slave addresses are not accepted in the RX FIFO. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" bodystart="510" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="510" />
      </memberdef>
      <memberdef id="structcy__stc__scb__i2c__config__t_1a7a705499e24c5dade7143d1c339389af" kind="variable" mutable="no" prot="public" static="no">
        <type>bool</type>
        <definition>bool cy_stc_scb_i2c_config_t::ackGeneralAddr</definition>
        <argsstring />
        <name>ackGeneralAddr</name>
        <briefdescription>
<para>True - accept the general call address; false - ignore the general call address. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" bodystart="516" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="516" />
      </memberdef>
      <memberdef id="structcy__stc__scb__i2c__config__t_1a5349cece2450d79a40dbe36dda778bd7" kind="variable" mutable="no" prot="public" static="no">
        <type>bool</type>
        <definition>bool cy_stc_scb_i2c_config_t::enableWakeFromSleep</definition>
        <argsstring />
        <name>enableWakeFromSleep</name>
        <briefdescription>
<para>When set, the slave will wake the device from Deep Sleep on an address match (the device datasheet must be consulted to determine which SCBs support this mode) </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" bodystart="523" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="523" />
      </memberdef>
      <memberdef id="structcy__stc__scb__i2c__config__t_1a1414232f520fb70d07f67218c627a586" kind="variable" mutable="no" prot="public" static="no">
        <type>bool</type>
        <definition>bool cy_stc_scb_i2c_config_t::enableDigitalFilter</definition>
        <argsstring />
        <name>enableDigitalFilter</name>
        <briefdescription>
<para>Enables a digital 3-tap median filter to be applied to the inputs of filter glitches on the lines. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" bodystart="529" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="529" />
      </memberdef>
      <memberdef id="structcy__stc__scb__i2c__config__t_1a8b742ccfd88a2a9216e9ad7b018a6d51" kind="variable" mutable="no" prot="public" static="no">
        <type>uint32_t</type>
        <definition>uint32_t cy_stc_scb_i2c_config_t::lowPhaseDutyCycle</definition>
        <argsstring />
        <name>lowPhaseDutyCycle</name>
        <briefdescription>
<para>The number of SCB clock cycles in the low phase of SCL. </para>        </briefdescription>
        <detaileddescription>
<para>Only applicable in master modes. The valid range is 7 to 16. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" bodystart="535" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="535" />
      </memberdef>
      <memberdef id="structcy__stc__scb__i2c__config__t_1a275201ed69def1d0e7432eef35cfd64c" kind="variable" mutable="no" prot="public" static="no">
        <type>uint32_t</type>
        <definition>uint32_t cy_stc_scb_i2c_config_t::highPhaseDutyCycle</definition>
        <argsstring />
        <name>highPhaseDutyCycle</name>
        <briefdescription>
<para>The number of SCB clock cycles in the high phase of SCL. </para>        </briefdescription>
        <detaileddescription>
<para>Only applicable in master modes. The valid range is 5 to 16. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" bodystart="541" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="541" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>I2C configuration structure. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <location bodyend="543" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" bodystart="428" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="429" />
    <listofallmembers>
      <member prot="public" refid="structcy__stc__scb__i2c__config__t_1a9074db87cce95b20f6adbaf905e05f98" virt="non-virtual"><scope>cy_stc_scb_i2c_config_t</scope><name>acceptAddrInFifo</name></member>
      <member prot="public" refid="structcy__stc__scb__i2c__config__t_1a7a705499e24c5dade7143d1c339389af" virt="non-virtual"><scope>cy_stc_scb_i2c_config_t</scope><name>ackGeneralAddr</name></member>
      <member prot="public" refid="structcy__stc__scb__i2c__config__t_1a1414232f520fb70d07f67218c627a586" virt="non-virtual"><scope>cy_stc_scb_i2c_config_t</scope><name>enableDigitalFilter</name></member>
      <member prot="public" refid="structcy__stc__scb__i2c__config__t_1a5349cece2450d79a40dbe36dda778bd7" virt="non-virtual"><scope>cy_stc_scb_i2c_config_t</scope><name>enableWakeFromSleep</name></member>
      <member prot="public" refid="structcy__stc__scb__i2c__config__t_1a275201ed69def1d0e7432eef35cfd64c" virt="non-virtual"><scope>cy_stc_scb_i2c_config_t</scope><name>highPhaseDutyCycle</name></member>
      <member prot="public" refid="structcy__stc__scb__i2c__config__t_1ab317c1252433320bfd76d2b8629dac23" virt="non-virtual"><scope>cy_stc_scb_i2c_config_t</scope><name>i2cMode</name></member>
      <member prot="public" refid="structcy__stc__scb__i2c__config__t_1a8b742ccfd88a2a9216e9ad7b018a6d51" virt="non-virtual"><scope>cy_stc_scb_i2c_config_t</scope><name>lowPhaseDutyCycle</name></member>
      <member prot="public" refid="structcy__stc__scb__i2c__config__t_1a9e0bf167c747280bb8a1c2f4f134889b" virt="non-virtual"><scope>cy_stc_scb_i2c_config_t</scope><name>slaveAddress</name></member>
      <member prot="public" refid="structcy__stc__scb__i2c__config__t_1a3578fa3852e87ead95db3c0c13064d71" virt="non-virtual"><scope>cy_stc_scb_i2c_config_t</scope><name>slaveAddressMask</name></member>
      <member prot="public" refid="structcy__stc__scb__i2c__config__t_1a5051507a6a9adb8b9db39572c5f11340" virt="non-virtual"><scope>cy_stc_scb_i2c_config_t</scope><name>useRxFifo</name></member>
      <member prot="public" refid="structcy__stc__scb__i2c__config__t_1a2c4bded993c3b334fbc2321952ab99f5" virt="non-virtual"><scope>cy_stc_scb_i2c_config_t</scope><name>useTxFifo</name></member>
    </listofallmembers>
  </compounddef>
</doxygen>