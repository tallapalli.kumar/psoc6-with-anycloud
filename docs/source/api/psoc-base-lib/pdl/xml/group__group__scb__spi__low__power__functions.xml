<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__scb__spi__low__power__functions" kind="group">
    <compoundname>group_scb_spi_low_power_functions</compoundname>
    <title>Low Power Callbacks</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__power__functions_1gabf413c42a9ba8a20c38f2491ae2e36c0" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref></type>
        <definition>cy_en_syspm_status_t Cy_SCB_SPI_DeepSleepCallback</definition>
        <argsstring>(cy_stc_syspm_callback_params_t *callbackParams, cy_en_syspm_callback_mode_t mode)</argsstring>
        <name>Cy_SCB_SPI_DeepSleepCallback</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref> *</type>
          <declname>callbackParams</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></type>
          <declname>mode</declname>
        </param>
        <briefdescription>
<para>This function handles the transition of the SCB SPI into and out of Deep Sleep mode. </para>        </briefdescription>
        <detaileddescription>
<para>It prevents the device from entering Deep Sleep mode if the SPI slave or master is actively communicating, or there is any data in the TX or RX FIFOs. The following behavior of the SPI SCB depends on whether the SCB block is wakeup-capable or not:<itemizedlist>
<listitem><para><bold>Wakeup-capable</bold>: any transfer intended to the slave wakes up the device from Deep Sleep mode. The slave responds with 0xFF to the transfer and incoming data is ignored. If the transfer occurs before the device enters Deep Sleep mode, the device will not enter Deep Sleep mode and incoming data is stored in the RX FIFO. The SCB clock is disabled before entering Deep Sleep and enabled after the device exits Deep Sleep mode. The SCB clock disabling may lead to corrupted data in the RX FIFO. Clear the RX FIFO after this callback is executed. Note that for proper SPI operation after Deep Sleep the source of hf_clk[0] must be stable, this includes the FLL/PLL. The SysClk callback ensures that hf_clk[0] gets stable and it must be called before <ref kindref="member" refid="group__group__scb__spi__low__power__functions_1gabf413c42a9ba8a20c38f2491ae2e36c0">Cy_SCB_SPI_DeepSleepCallback</ref>. Only the SPI slave can be configured to be a wakeup source from Deep Sleep mode.</para></listitem><listitem><para><bold>Not wakeup-capable</bold>: the SPI is disabled. It is enabled when the device fails to enter Deep Sleep mode or it is awakened from Deep Sleep mode. While the SPI is disabled, it stops driving the outputs and ignores the inputs. Any incoming data is ignored. Refer to section <ref kindref="member" refid="group__group__scb__spi_1group_scb_spi_lp">Low Power Support</ref> for more information about SPI pins when SCB disabled.</para></listitem></itemizedlist>
</para><para>This function must be called during execution of <ref kindref="member" refid="group__group__syspm__functions__power_1ga5150c28fe4d2626720c1fbf74b3111ca">Cy_SysPm_CpuEnterDeepSleep</ref>. To do it, register this function as a callback before calling <ref kindref="member" refid="group__group__syspm__functions__power_1ga5150c28fe4d2626720c1fbf74b3111ca">Cy_SysPm_CpuEnterDeepSleep</ref> : specify <ref kindref="member" refid="group__group__syspm__data__enumerates_1gga8c2960c0164ead1cfa86e7d6846b6ff0abc51d74deff0ceea4304b01b2d57bd9d">CY_SYSPM_DEEPSLEEP</ref> as the callback type and call <ref kindref="member" refid="group__group__syspm__functions__callback_1ga0d58b00c8dc764a6371590f70e2f73c7">Cy_SysPm_RegisterCallback</ref>.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>callbackParams</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the callback parameters structure <ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mode</parametername>
</parameternamelist>
<parameterdescription>
<para>Callback mode, see <ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref></para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Only applicable for &lt;b&gt;rev-08 of the CY8CKIT-062-BLE&lt;/b&gt;. For proper operation, when the SPI slave is configured to be a wakeup source from Deep Sleep mode, this function must be copied and modified by the user. The SPI clock disable code must be inserted in the &lt;a href="group__group__syspm__data__enumerates.html#group__group__syspm__data__enumerates_1ggae06cd8869fe61d709ad6145ca9f3cd63a7d302375276b3b5f250a8208c999b558"&gt;CY_SYSPM_BEFORE_TRANSITION&lt;/a&gt; and clock enable code in the &lt;a href="group__group__syspm__data__enumerates.html#group__group__syspm__data__enumerates_1ggae06cd8869fe61d709ad6145ca9f3cd63aafd1db1f7f86ac7bbd18d59b78af5693"&gt;CY_SYSPM_AFTER_TRANSITION&lt;/a&gt; mode processing. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="437" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_spi.c" bodystart="297" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="633" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__spi__low__power__functions_1ga1bf16639bec6ac8209621227bd5ac0e3" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref></type>
        <definition>cy_en_syspm_status_t Cy_SCB_SPI_HibernateCallback</definition>
        <argsstring>(cy_stc_syspm_callback_params_t *callbackParams, cy_en_syspm_callback_mode_t mode)</argsstring>
        <name>Cy_SCB_SPI_HibernateCallback</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref> *</type>
          <declname>callbackParams</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></type>
          <declname>mode</declname>
        </param>
        <briefdescription>
<para>This function handles the transition of the SCB SPI into Hibernate mode. </para>        </briefdescription>
        <detaileddescription>
<para>It prevents the device from entering Hibernate mode if the SPI slave or master is actively communicating, or there is any data in the TX or RX FIFOs. If the SPI is ready to enter Hibernate mode, it is disabled. If the device failed to enter Hibernate mode, the SPI is re-enabled. While the SPI is disabled, it stops driving the outputs and ignores the inputs. Any incoming data is ignored. Refer to section <ref kindref="member" refid="group__group__scb__spi_1group_scb_spi_lp">Low Power Support</ref> for more information about SPI pins when SCB disabled.</para><para>This function must be called during execution of <ref kindref="member" refid="group__group__syspm__functions__power_1gae97647a28c370674ba57d451d21d1c51">Cy_SysPm_SystemEnterHibernate</ref>. To do it, register this function as a callback before calling <ref kindref="member" refid="group__group__syspm__functions__power_1gae97647a28c370674ba57d451d21d1c51">Cy_SysPm_SystemEnterHibernate</ref> : specify <ref kindref="member" refid="group__group__syspm__data__enumerates_1gga8c2960c0164ead1cfa86e7d6846b6ff0a613e2f83e3ab88e3569cf34ff0fa5912">CY_SYSPM_HIBERNATE</ref> as the callback type and call <ref kindref="member" refid="group__group__syspm__functions__callback_1ga0d58b00c8dc764a6371590f70e2f73c7">Cy_SysPm_RegisterCallback</ref>.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>callbackParams</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the callback parameters structure <ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mode</parametername>
</parameternamelist>
<parameterdescription>
<para>Callback mode, see <ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="538" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_spi.c" bodystart="469" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_spi.h" line="634" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>