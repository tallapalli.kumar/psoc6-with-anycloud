=============================
Shift Register output invert
=============================

.. doxygengroup:: group_tcpwm_shiftreg_output_invert
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   