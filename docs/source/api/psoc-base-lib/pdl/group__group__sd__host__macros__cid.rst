===================
CID Register Masks
===================


.. doxygengroup:: group_sd_host_macros_cid
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: