==============================
Shift Register Disabled Output
==============================

.. doxygengroup:: group_tcpwm_shiftreg_output_on_disable
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   