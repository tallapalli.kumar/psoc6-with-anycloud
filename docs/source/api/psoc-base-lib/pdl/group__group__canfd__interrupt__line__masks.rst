===============================
Interrupt line selection masks
===============================

.. doxygengroup:: group_canfd_interrupt_line_masks
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

