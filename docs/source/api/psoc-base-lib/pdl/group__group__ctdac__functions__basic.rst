==============================
Basic Configuration Functions
==============================

.. doxygengroup:: group_ctdac_functions_basic
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: