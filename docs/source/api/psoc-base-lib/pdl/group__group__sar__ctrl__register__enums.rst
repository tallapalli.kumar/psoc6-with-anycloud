=======================
Control Register Enums
=======================




.. doxygengroup:: group_sar_ctrl_register_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: