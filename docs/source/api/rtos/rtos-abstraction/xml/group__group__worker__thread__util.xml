<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__worker__thread__util" kind="group">
    <compoundname>group_worker_thread_util</compoundname>
    <title>Worker Thread Utility</title>
    <innerclass prot="public" refid="structcy__worker__thread__params__t">cy_worker_thread_params_t</innerclass>
    <innerclass prot="public" refid="structcy__worker__thread__info__t">cy_worker_thread_info_t</innerclass>
      <sectiondef kind="enum">
      <memberdef id="group__group__worker__thread__util_1ga514bc54aa543a69ce1768903b9851473" kind="enum" prot="public" static="no" strong="no">
        <type />
        <name>cy_worker_thread_state_t</name>
        <enumvalue id="group__group__worker__thread__util_1gga514bc54aa543a69ce1768903b9851473a0d938b2d088126d6d47c936040e843c2" prot="public">
          <name>CY_WORKER_THREAD_INVALID</name>
          <briefdescription>
<para>Worker Thread is in invalid state. </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <enumvalue id="group__group__worker__thread__util_1gga514bc54aa543a69ce1768903b9851473aec127b9642db106c61c3c32106fb69fb" prot="public">
          <name>CY_WORKER_THREAD_VALID</name>
          <briefdescription>
<para>Worker Thread is in valid state. </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <enumvalue id="group__group__worker__thread__util_1gga514bc54aa543a69ce1768903b9851473a79db794eee56f8188a116ed31ae5b1d4" prot="public">
          <name>CY_WORKER_THREAD_TERMINATING</name>
          <briefdescription>
<para>Worker Thread is starting to terminate. </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <enumvalue id="group__group__worker__thread__util_1gga514bc54aa543a69ce1768903b9851473a8685271e73d4fc9bc13e62a58a8431c8" prot="public">
          <name>CY_WORKER_THREAD_JOIN_COMPLETE</name>
          <briefdescription>
<para>Worker Thread join is complete. </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <briefdescription>
<para><bold>cy_worker_thread_state_t: </bold><linebreak />Thread state enumeration.</para></briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="68" bodyfile="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cy_worker_thread.h" bodystart="62" column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cy_worker_thread.h" line="63" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="typedef">
      <memberdef id="group__group__worker__thread__util_1gaeb4fb0e41f7d0092cc135f7cc5e41250" kind="typedef" prot="public" static="no">
        <type>void()</type>
        <definition>typedef void() cy_worker_thread_func_t(void *arg)</definition>
        <argsstring>(void *arg)</argsstring>
        <name>cy_worker_thread_func_t</name>
        <briefdescription>
<para>Worker thread function call prototype. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cy_worker_thread.h" bodystart="59" column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cy_worker_thread.h" line="59" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__worker__thread__util_1ga383f418c20f7ae9d2eb184722b7bbd04" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_worker_thread_create</definition>
        <argsstring>(cy_worker_thread_info_t *new_worker, const cy_worker_thread_params_t *params)</argsstring>
        <name>cy_worker_thread_create</name>
        <param>
          <type><ref kindref="compound" refid="structcy__worker__thread__info__t">cy_worker_thread_info_t</ref> *</type>
          <declname>new_worker</declname>
        </param>
        <param>
          <type>const <ref kindref="compound" refid="structcy__worker__thread__params__t">cy_worker_thread_params_t</ref> *</type>
          <declname>params</declname>
        </param>
        <briefdescription>
<para>Create worker thread to handle running callbacks in a separate thread. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Calling this function twice on the same thread object ( &lt;a href="structcy__worker__thread__info__t.html#"&gt;cy_worker_thread_info_t&lt;/a&gt;) without calling &lt;a href="group__group__worker__thread__util.html#group__group__worker__thread__util_1gae4a9787d87626fefc16e328754182c37"&gt;cy_worker_thread_delete&lt;/a&gt; will cause memory leakage.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">new_worker</parametername>
</parameternamelist>
<parameterdescription>
<para>pointer to <ref kindref="compound" refid="structcy__worker__thread__info__t">cy_worker_thread_info_t</ref> structure to be filled when created. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">params</parametername>
</parameternamelist>
<parameterdescription>
<para>pointer to requested parameters for starting worker thread.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the worker thread creation request. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="104" bodyfile="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/source/cy_worker_thread.c" bodystart="74" column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cy_worker_thread.h" line="102" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__worker__thread__util_1gae4a9787d87626fefc16e328754182c37" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_worker_thread_delete</definition>
        <argsstring>(cy_worker_thread_info_t *old_worker)</argsstring>
        <name>cy_worker_thread_delete</name>
        <param>
          <type><ref kindref="compound" refid="structcy__worker__thread__info__t">cy_worker_thread_info_t</ref> *</type>
          <declname>old_worker</declname>
        </param>
        <briefdescription>
<para>Delete worker thread. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function will wait for the thread to complete all pending work in the queue and exit before returning.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">old_worker</parametername>
</parameternamelist>
<parameterdescription>
<para>pointer to <ref kindref="compound" refid="structcy__worker__thread__info__t">cy_worker_thread_info_t</ref> structure to be deleted.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the deletion of the worker thread. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="147" bodyfile="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/source/cy_worker_thread.c" bodystart="106" column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cy_worker_thread.h" line="113" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__worker__thread__util_1ga4a582d807e3b7d4fa0bf7ee770af9015" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_worker_thread_enqueue</definition>
        <argsstring>(cy_worker_thread_info_t *worker_info, cy_worker_thread_func_t *work_func, void *arg)</argsstring>
        <name>cy_worker_thread_enqueue</name>
        <param>
          <type><ref kindref="compound" refid="structcy__worker__thread__info__t">cy_worker_thread_info_t</ref> *</type>
          <declname>worker_info</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__worker__thread__util_1gaeb4fb0e41f7d0092cc135f7cc5e41250">cy_worker_thread_func_t</ref> *</type>
          <declname>work_func</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>arg</declname>
        </param>
        <briefdescription>
<para>Queue work on a worker thread. </para>        </briefdescription>
        <detaileddescription>
<para>Call the given function in the worker thread context.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;If the thread priority is below that of the current thread, you must yield to allow the worker thread to run. This can be done by calling &lt;a href="group__group__abstraction__rtos__time.html#group__group__abstraction__rtos__time_1gaa33d9f3026f722ac92950c6215e4283a"&gt;cy_rtos_delay_milliseconds&lt;/a&gt; or by waiting on an RTOS object in all higher priority threads.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">worker_info</parametername>
</parameternamelist>
<parameterdescription>
<para>pointer to worker_thread used to run function </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">work_func</parametername>
</parameternamelist>
<parameterdescription>
<para>function to run </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">arg</parametername>
</parameternamelist>
<parameterdescription>
<para>opaque arg to be used in function call</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the queueing of work. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="167" bodyfile="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/source/cy_worker_thread.c" bodystart="149" column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cy_worker_thread.h" line="129" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="define">
      <memberdef id="group__group__worker__thread__util_1gae45bcc8cd62f58eb236ef486b4d5558c" kind="define" prot="public" static="no">
        <name>CY_WORKER_THREAD_DEFAULT_NAME</name>
        <initializer>"CYWorker"</initializer>
        <briefdescription>
<para>&lt; Default worker thread name </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cy_worker_thread.h" bodystart="48" column="9" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cy_worker_thread.h" line="48" />
      </memberdef>
      <memberdef id="group__group__worker__thread__util_1gae442fac806ebf74e3ba623a2c2e2ae1b" kind="define" prot="public" static="no">
        <name>CY_WORKER_DEFAULT_ENTRIES</name>
        <initializer>(16)</initializer>
        <briefdescription>
<para>Default number of work items in the queue. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cy_worker_thread.h" bodystart="50" column="9" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cy_worker_thread.h" line="50" />
      </memberdef>
      <memberdef id="group__group__worker__thread__util_1gaad1bf39d72a75351866a1871779dcaf8" kind="define" prot="public" static="no">
        <name>CY_WORKER_THREAD_ERR_THREAD_INVALID</name>
        <initializer>CY_RSLT_CREATE(CY_RSLT_TYPE_ERROR, CY_RSLT_MODULE_ABSTRACTION_OS, 32)</initializer>
        <briefdescription>
<para>Additional work cannot be enqueued because the worker thread has been terminated. </para>        </briefdescription>
        <detaileddescription>
<para>This can occur if <ref kindref="member" refid="group__group__worker__thread__util_1ga383f418c20f7ae9d2eb184722b7bbd04">cy_worker_thread_create</ref> was not called or <ref kindref="member" refid="group__group__worker__thread__util_1gae4a9787d87626fefc16e328754182c37">cy_worker_thread_delete</ref> was called before calling <ref kindref="member" refid="group__group__worker__thread__util_1ga4a582d807e3b7d4fa0bf7ee770af9015">cy_worker_thread_enqueue</ref> </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cy_worker_thread.h" bodystart="56" column="9" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cy_worker_thread.h" line="56" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>Worker thread utility that allows functions to be run a different thread context. </para>    </briefdescription>
    <detaileddescription>
<para>This utility can be used to delegate work that is not timing critical. For example, scheduling work in interrupt handlers to keep handler execution times low or if some work needs to be done at a different priority. </para>    </detaileddescription>
  </compounddef>
</doxygen>