========================
cy_worker_thread_info_t
========================

.. doxygenstruct:: cy_worker_thread_info_t
   :project: rtos

Public Attributes
------------------

.. doxygenvariable:: event_queue
   :project: rtos

.. doxygenvariable:: thread
   :project: rtos

.. doxygenvariable:: state
   :project: rtos
