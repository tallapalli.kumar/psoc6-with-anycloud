======
CMSIS
======

Documentation for CMSIS can be found on the project's site.  

.. raw:: html

   <a href="https://www.keil.com/pack/doc/CMSIS/General/html/index.html" target="_blank">Click here to be taken to the documentation.</a><br><br>
