Enumerated types
==================

.. doxygengroup:: group_secure_sockets_enums
   :project: secure-sockets	
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   