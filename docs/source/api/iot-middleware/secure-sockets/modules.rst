=======================
API Reference
=======================

         The following provides a list of driver API documentation

      

         +----------------------------------+----------------------------------+
         | `Secure Sockets                  | The Secure Sockets API provides  |
         | API <group_                      | functions to communicate over    |
         | _group__secure__sockets.html>`__ | the IP network                   |
         +----------------------------------+----------------------------------+
         | `Message Sequence                |                                  |
         | Charts <group__group             |                                  |
         | __secure__sockets__mscs.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Macros <group__group__          |                                  |
         | secure__sockets__macros.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Secure Sockets results/error    | Secure Sockets Library APIs      |
         | codes <group__group__s           | return results of type cy_rslt_t |
         | ecure__sockets__results.html>`__ | and consist of three parts:      |
         +----------------------------------+----------------------------------+
         | `Typedefs <group__group__se      |                                  |
         | cure__sockets__typedefs.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Enumerated                      |                                  |
         | types <group__group_             |                                  |
         | _secure__sockets__enums.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Structures <group__             |                                  |
         | group__secu                      |                                  |
         | re__sockets__structures.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Functions <group__group__sec    | All API functions except         |
         | ure__sockets__functions.html>`__ | `cy_s                            |
         |                                  | ocket_init <group__group__secure |
         |                                  | __sockets__functions.html#ga49a4 |
         |                                  | 9b4e3b4293aa3855f47ce831ebe2>`__ |
         |                                  | and                              |
         |                                  | `cy_soc                          |
         |                                  | ket_deinit <group__group__secure |
         |                                  | __sockets__functions.html#ga0eb3 |
         |                                  | e8e31c5a1c11c08ac35807bb5680>`__ |
         |                                  | are thread-safe                  |
         +----------------------------------+----------------------------------+
         | `TLS API                         | The TLS API provides functions   |
         | <group__group__cy__tls.html>`__  | for secure communication over    |
         |                                  | the IP network                   |
         +----------------------------------+----------------------------------+
         | `Enumerations <group             |                                  |
         | __group__cy__tls__enums.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Typedefs <group__g              |                                  |
         | roup__cy__tls__typedefs.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Structures <group__gro          |                                  |
         | up__cy__tls__structures.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Functions <group__gr            | All the API functions except     |
         | oup__cy__tls__functions.html>`__ | `cy_tls_init <group__group       |
         |                                  | __cy__tls__functions.html#gaa78e |
         |                                  | 119aa59d26d68ff2e879d55cdac8>`__ |
         |                                  | `cy_tls_deinit <group__group     |
         |                                  | __cy__tls__functions.html#gaccea |
         |                                  | 15eaf0ad36001b914bd53b327a84>`__ |
         |                                  | `cy_tls_load_global_ro           |
         |                                  | ot_ca_certificates <group__group |
         |                                  | __cy__tls__functions.html#ga1869 |
         |                                  | 18ab7a9c658fb7e3cc6f00e0d6d9>`__ |
         |                                  | and                              |
         |                                  | `cy_tls_release_global_ro        |
         |                                  | ot_ca_certificates <group__group |
         |                                  | __cy__tls__functions.html#ga5e76 |
         |                                  | c0d24388071a0ba60b38a9367bd0>`__ |
         |                                  | are thread-safe                  |
         +----------------------------------+----------------------------------+


.. toctree::
   :hidden:

   group__group__secure__sockets.rst
   group__group__cy__tls.rst
   
