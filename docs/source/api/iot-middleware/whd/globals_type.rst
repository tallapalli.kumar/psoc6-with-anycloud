==========
Typesdefs
==========

-  sup_auth_status_t :
   `whd_events.h <whd__events_8h.html#a4c17ec19ca52ebe4bbad3ea977108d0b>`__
-  whd_ap_info_t :
   `whd_types.h <whd__types_8h.html#ab850fbf60dfa93cd732f4e36a1e134cf>`__
-  whd_btc_lescan_params_t :
   `whd_types.h <whd__types_8h.html#a98dae084aa15394bf6f8e907e09233a4>`__
-  whd_buffer_funcs_t :
   `whd.h <whd_8h.html#a44a6e9abc68a4322a3958bdc24ae9981>`__
-  whd_buffer_queue_ptr_t :
   `whd_types.h <whd__types_8h.html#a0767ab3fb805f164a11b58473c308053>`__
-  whd_coex_config_t :
   `whd_types.h <whd__types_8h.html#a1cb4da9972e0e9317a6e26e2fd460902>`__
-  whd_driver_t :
   `whd.h <whd_8h.html#ac45015d82e65db891b463066873eca4f>`__
-  whd_event_eth_hdr_t :
   `whd_events.h <whd__events_8h.html#a8e7e9d5ab8841a160aa9e2420c636864>`__
-  whd_event_ether_header_t :
   `whd_events.h <whd__events_8h.html#a31915a18541e6182e926247a3b8ed3dc>`__
-  whd_event_handler_t :
   `whd_events.h <group__event.html#ga9c74af353c83a8f77097acbc65362b07>`__
-  whd_event_t :
   `whd_events.h <whd__events_8h.html#a9d6929e68c05572695ee040da171d508>`__
-  whd_init_config_t :
   `whd.h <whd_8h.html#a933f0025533b9b88ecb77b651db29250>`__
-  whd_interface_t :
   `whd.h <whd_8h.html#a2e544c482ddbb690bbb95ce7174e79a0>`__
-  whd_netif_funcs_t :
   `whd.h <whd_8h.html#ac35b975959ed585acf554535b502ce68>`__
-  whd_oob_config_t :
   `whd_types.h <whd__types_8h.html#ac07b13dcb5ec0e66a707dcf8a539c97e>`__
-  whd_resource_source_t :
   `whd.h <whd_8h.html#a05847ad8fa418d69d5f51fe654835b8b>`__
-  whd_result_t :
   `whd_types.h <whd__types_8h.html#add62f4b5040a2451e23869d2f9e1ae05>`__
-  whd_scan_result_callback_t :
   `whd_wifi_api.h <group__wifijoin.html#ga9f8c6096922212981dd2101a17aec471>`__
-  whd_scan_result_t :
   `whd_types.h <whd__types_8h.html#a58298b1b5c2f2425d5aed061de48aaa7>`__
-  whd_sdio_config_t :
   `whd_types.h <whd__types_8h.html#a2b3fb82834e1fbcbbf1f51e83c675edd>`__
-  whd_sdio_funcs_t :
   `whd.h <whd_8h.html#af5e9a147a89968d2026fd7720a519bbd>`__
-  whd_spi_config_t :
   `whd_types.h <whd__types_8h.html#a2f5c5d3c7f74ce9a86595fe220c860d3>`__
-  whd_spi_funcs_t :
   `whd.h <whd_8h.html#a7373cb0b0ef75e1b740fd53c77bda5e4>`__
-  whd_sync_scan_result_t :
   `whd_types.h <whd__types_8h.html#ab30a697b3ebbf2007dc09194b54b1a1b>`__
-  whd_time_t :
   `whd_types.h <whd__types_8h.html#aaf5901182e9ac5462ba6d4ada56e54d0>`__
-  wl_bss_info_t :
   `whd_types.h <whd__types_8h.html#a92c6377f96c792ee4d94bb36b7777ea6>`__
-  wl_chanspec_t :
   `whd_types.h <whd__types_8h.html#ac2f5aa33ad4da263645133854e489c76>`__

