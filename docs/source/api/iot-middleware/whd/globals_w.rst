===
w
===

Here is a list of all documented functions, variables, defines, enums,
and typedefs with links to the documentation:

.. rubric:: - w -
   :name: w--

-  WEP_ENABLED :
   `whd_types.h <whd__types_8h.html#a4199a1b37dba92f482ff0b9eb406c323>`__
-  WHD_802_11_BAND_2_4GHZ :
   `whd_types.h <whd__types_8h.html#aa57cbd1826f7022123be8ec0ac352d85ad83834fecf6a59269c7bdbc02bf0ce2f>`__
-  WHD_802_11_BAND_5GHZ :
   `whd_types.h <whd__types_8h.html#aa57cbd1826f7022123be8ec0ac352d85a0af1540533ecaf4c2511d1953c766119>`__
-  whd_802_11_band_t :
   `whd_types.h <whd__types_8h.html#aa57cbd1826f7022123be8ec0ac352d85>`__
-  WHD_ACCESS_POINT_NOT_FOUND :
   `whd_types.h <whd__types_8h.html#a38cbe87adc541246f2eae5534501231a>`__
-  WHD_ADD_CUSTOM_IE :
   `whd_types.h <whd__types_8h.html#a8be1026494a86f0ceeebb2dcbf092cbda79a530656dc2116ab992378d57256869>`__
-  whd_add_secondary_interface() :
   `whd_wifi_api.h <group__wifimanagement.html#ga553c009e9f6f856dfe243d745e21bbf9>`__
-  WHD_ADDRESS_ALREADY_REGISTERED :
   `whd_types.h <whd__types_8h.html#a7aecc5610dbe6e1584b349bfb202a2a7>`__
-  WHD_AP_ALREADY_UP :
   `whd_types.h <whd__types_8h.html#a3ed786d3514cdb56daecc1713feb8199>`__
-  whd_ap_info_t :
   `whd_types.h <whd__types_8h.html#ab850fbf60dfa93cd732f4e36a1e134cf>`__
-  whd_arp_arpoe_get() :
   `whd_wifi_api.h <group__wifiutilities.html#ga22d76514755f2dfde0ee6acbc193787a>`__
-  whd_arp_arpoe_set() :
   `whd_wifi_api.h <group__wifiutilities.html#gaa30d9e2a761de17962312d818f941c05>`__
-  whd_arp_cache_clear() :
   `whd_wifi_api.h <group__wifiutilities.html#ga01ece00a357914d02fa5a181fbddbf8c>`__
-  whd_arp_features_get() :
   `whd_wifi_api.h <group__wifiutilities.html#ga54728d3d6a714ef9264d6e25df9a6722>`__
-  whd_arp_features_print() :
   `whd_wifi_api.h <group__wifiutilities.html#gaf70b6becc3e4ecc551f5bf5278d7ad93>`__
-  whd_arp_features_set() :
   `whd_wifi_api.h <group__wifiutilities.html#gab4ebcd7fdd7b328e2f4e10b79197cfca>`__
-  whd_arp_hostip_list_add() :
   `whd_wifi_api.h <group__wifiutilities.html#ga417b404b9892862a572802a4ea38eb79>`__
-  whd_arp_hostip_list_add_string() :
   `whd_wifi_api.h <group__wifiutilities.html#ga61135784226a59af098384f4a071af02>`__
-  whd_arp_hostip_list_clear() :
   `whd_wifi_api.h <group__wifiutilities.html#gaaf3cec4a4896670049ec3802c91a2d66>`__
-  whd_arp_hostip_list_clear_id() :
   `whd_wifi_api.h <group__wifiutilities.html#ga15cfc56bd481a4bc02c6dc93bfa6883c>`__
-  whd_arp_hostip_list_clear_id_string() :
   `whd_wifi_api.h <group__wifiutilities.html#gaa38f97cc9800e7355d96d833b32b1c1c>`__
-  whd_arp_hostip_list_get() :
   `whd_wifi_api.h <group__wifiutilities.html#ga4287cc85981a511399ea927c84f61330>`__
-  whd_arp_peerage_get() :
   `whd_wifi_api.h <group__wifiutilities.html#ga2cd55bdc13221177c61310ecaa6f4bd7>`__
-  whd_arp_peerage_set() :
   `whd_wifi_api.h <group__wifiutilities.html#gab61ae9878853c02b4cf194c2c3a7377f>`__
-  whd_arp_stats_clear() :
   `whd_wifi_api.h <group__wifiutilities.html#ga564e95d3343802ed33d1b2c6fb531aeb>`__
-  whd_arp_stats_get() :
   `whd_wifi_api.h <group__wifiutilities.html#ga9d65fe0a82dee5edf65eb6f1efe9df52>`__
-  whd_arp_stats_print() :
   `whd_wifi_api.h <group__wifiutilities.html#ga37714a68f350e5a9e24dd6aeaea0ae98>`__
-  whd_arp_version() :
   `whd_wifi_api.h <group__wifiutilities.html#gae6bf707d845518cba9a8256fb5681f4a>`__
-  WHD_BADARG :
   `whd_types.h <whd__types_8h.html#a572bf9261bd3f234bd804d55b284f55d>`__
-  whd_bool_t :
   `whd_types.h <whd__types_8h.html#a7cd94a03f2e7e6aab7217ed559c7a0ac>`__
-  WHD_BSS_TYPE_ADHOC :
   `whd_types.h <whd__types_8h.html#a3070614a06a8989b9eb207e9f1286c5fac4c317dde2a56f56571b0258e6038675>`__
-  WHD_BSS_TYPE_ANY :
   `whd_types.h <whd__types_8h.html#a3070614a06a8989b9eb207e9f1286c5faeae98beecab71ed2434077afc97721d5>`__
-  WHD_BSS_TYPE_INFRASTRUCTURE :
   `whd_types.h <whd__types_8h.html#a3070614a06a8989b9eb207e9f1286c5fa7caa0abe03eadaee7aa958827e2d5365>`__
-  WHD_BSS_TYPE_MESH :
   `whd_types.h <whd__types_8h.html#a3070614a06a8989b9eb207e9f1286c5faa32f92242f0ac074d88260eb2fa76a2f>`__
-  whd_bss_type_t :
   `whd_types.h <whd__types_8h.html#a3070614a06a8989b9eb207e9f1286c5f>`__
-  WHD_BSS_TYPE_UNKNOWN :
   `whd_types.h <whd__types_8h.html#a3070614a06a8989b9eb207e9f1286c5fa6711a29485a893344ddebf8fa099ec48>`__
-  whd_btc_lescan_params_t :
   `whd_types.h <whd__types_8h.html#a98dae084aa15394bf6f8e907e09233a4>`__
-  WHD_BUFFER_ALLOC_FAIL :
   `whd_types.h <whd__types_8h.html#a18719c46ce4cff61ff3c4d50309261e2>`__
-  whd_buffer_dir_t :
   `whd_network_types.h <group__buffif.html#ga44a64c51498b204ceef5555209e29452>`__
-  whd_buffer_funcs_t :
   `whd.h <whd_8h.html#a44a6e9abc68a4322a3958bdc24ae9981>`__
-  WHD_BUFFER_POINTER_MOVE_ERROR :
   `whd_types.h <whd__types_8h.html#a4453a7518a649b88128f21a5af2a8755>`__
-  whd_buffer_queue_ptr_t :
   `whd_types.h <whd__types_8h.html#a0767ab3fb805f164a11b58473c308053>`__
-  WHD_BUFFER_SIZE_SET_ERROR :
   `whd_types.h <whd__types_8h.html#af7974469bcf8a4bad24d339d623297e7>`__
-  WHD_BUFFER_UNAVAILABLE_PERMANENT :
   `whd_types.h <whd__types_8h.html#a730333b8281a84dcda0985b757b97823>`__
-  WHD_BUFFER_UNAVAILABLE_TEMPORARY :
   `whd_types.h <whd__types_8h.html#a94f122e793a4a91c8ac044366d1611a9>`__
-  WHD_BUS_READ_REGISTER_ERROR :
   `whd_types.h <whd__types_8h.html#a998914de59ed4ad6f510c063fee877f1>`__
-  whd_bus_sdio_attach() :
   `whd_wifi_api.h <group__busapi.html#ga9f08c5241843cbe2ef06ffa8bfb5118d>`__
-  whd_bus_sdio_detach() :
   `whd_wifi_api.h <group__busapi.html#gadeed66e792eed64cbb6a0deef1dbe2da>`__
-  whd_bus_spi_attach() :
   `whd_wifi_api.h <group__busapi.html#ga16b799bb65db52fb4518e1fbc203fe53>`__
-  whd_bus_spi_detach() :
   `whd_wifi_api.h <group__busapi.html#gae83aaeef8c3d922cbb839b75ab289d13>`__
-  whd_bus_transfer_direction_t :
   `whd_types.h <whd__types_8h.html#afc8e8073d434bf124933526a8184313a>`__
-  WHD_BUS_WRITE_REGISTER_ERROR :
   `whd_types.h <whd__types_8h.html#a659306d5da66cf4bb2d2a5a8c7f530a2>`__
-  WHD_CLM_BLOB_DLOAD_ERROR :
   `whd_types.h <whd__types_8h.html#a6dd36bd14965f3601fee55c6e799dfa1>`__
-  whd_coex_config_t :
   `whd_types.h <whd__types_8h.html#a1cb4da9972e0e9317a6e26e2fd460902>`__
-  WHD_CONNECTION_LOST :
   `whd_types.h <whd__types_8h.html#a11801e020e110714a7a21527b18bc5c0>`__
-  WHD_CORE_CLOCK_NOT_ENABLED :
   `whd_types.h <whd__types_8h.html#af182b141bdde3c09af9849ca2f6f4598>`__
-  WHD_CORE_IN_RESET :
   `whd_types.h <whd__types_8h.html#adcd663e17b745c4fde47a59bd4ae3886>`__
-  WHD_COUNTRY_AFGHANISTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae770f53071110d4c62c45f3185212a2c>`__
-  WHD_COUNTRY_ALBANIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad6f4894e386515835c1754f164e9bcc6>`__
-  WHD_COUNTRY_ALGERIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa62c554f0e94ff63ef545d067a8f6fd4>`__
-  WHD_COUNTRY_AMERICAN_SAMOA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af16efa365354075d9f24cd783bd3b692>`__
-  WHD_COUNTRY_ANGOLA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a563ec781c9ac04774bd120cdb03213b3>`__
-  WHD_COUNTRY_ANGUILLA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aeb5d277f1549df165d7b220151347e93>`__
-  WHD_COUNTRY_ANTIGUA_AND_BARBUDA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a75088930532c4c414b7ebe2207fd39c1>`__
-  WHD_COUNTRY_ARGENTINA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa9086183077304caa5c0997b083aa09f>`__
-  WHD_COUNTRY_ARMENIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1ca3dac8fb93d58c216e94d87562fe0c>`__
-  WHD_COUNTRY_ARUBA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602afa17325e3a4447caf37db22e4d8dbd35>`__
-  WHD_COUNTRY_AUSTRALIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aebbf99f30ed79facff3fdee81fccb539>`__
-  WHD_COUNTRY_AUSTRIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af82950fa63c8d479999335fe1dd81d20>`__
-  WHD_COUNTRY_AZERBAIJAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602affa44f6b56c1b7a070185fe01040e758>`__
-  WHD_COUNTRY_BAHAMAS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac6076e03149c89cc2bcd76c4e05dbc4d>`__
-  WHD_COUNTRY_BAHRAIN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad32bdf453acfa3bef198aedfa190565b>`__
-  WHD_COUNTRY_BAKER_ISLAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6223c070c73257a38b1deec92a246245>`__
-  WHD_COUNTRY_BANGLADESH :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abaab712c7d16e5675e302e702ed9e09a>`__
-  WHD_COUNTRY_BARBADOS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a2191b0d5b008ee5b736722a57bded49d>`__
-  WHD_COUNTRY_BELARUS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602adbde897af63475f3377fde09ebdf18ae>`__
-  WHD_COUNTRY_BELGIUM :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae2d2eec5fc2a62bdd7e5423df889d70c>`__
-  WHD_COUNTRY_BELIZE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab08aca5d4ac515c418393cf8b105c1fa>`__
-  WHD_COUNTRY_BENIN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8b5718faa1aa137578cac467fd146c98>`__
-  WHD_COUNTRY_BERMUDA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7c7bda7e8a07ebb79e29856559fccd2e>`__
-  WHD_COUNTRY_BHUTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abc34c698a3a5368366885bcb628afb96>`__
-  WHD_COUNTRY_BOLIVIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae2ed98e109b1f8cd9e09c94d53ec2a87>`__
-  WHD_COUNTRY_BOSNIA_AND_HERZEGOVINA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1e8a3ee4ffdf908b2537918b60627e35>`__
-  WHD_COUNTRY_BOTSWANA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8f31c5978a56922f8f15273a057ecc7b>`__
-  WHD_COUNTRY_BRAZIL :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad991ba1abd3a98b073b1ccc426d8bb51>`__
-  WHD_COUNTRY_BRITISH_INDIAN_OCEAN_TERRITORY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a75e7187a26194e37529b76b424874f5b>`__
-  WHD_COUNTRY_BRUNEI_DARUSSALAM :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad58d8ffd6db58aa29f6c0fbee553e81b>`__
-  WHD_COUNTRY_BULGARIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a801c4ec653ee9bd12fb1becbe2a394c0>`__
-  WHD_COUNTRY_BURKINA_FASO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af06898be718ec7dca62de26b7900c8fd>`__
-  WHD_COUNTRY_BURUNDI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a66177cc6dcd277ba8c2c694502180dc7>`__
-  WHD_COUNTRY_CAMBODIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a516f67438861b15a55e22d46235e8e69>`__
-  WHD_COUNTRY_CAMEROON :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af61ade757f15f4d2999d45e5ea2a7baa>`__
-  WHD_COUNTRY_CANADA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae73dbffc9994566b8811401bb7b50cab>`__
-  WHD_COUNTRY_CANADA_REV950 :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a86b6fed33793c1b9295aeb75cb51431d>`__
-  WHD_COUNTRY_CAPE_VERDE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a4e952962e2677ca1ec9014f469bc9924>`__
-  WHD_COUNTRY_CAYMAN_ISLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6132980ec6e1d1964aa5e9187961acc5>`__
-  WHD_COUNTRY_CENTRAL_AFRICAN_REPUBLIC :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1b053b731226015cd1f7a435e232abe1>`__
-  WHD_COUNTRY_CHAD :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a015236ede99e9f4b5e1075c9e03e3e0a>`__
-  WHD_COUNTRY_CHILE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a19b99f0a9db2c89518ddd5635d7fa100>`__
-  WHD_COUNTRY_CHINA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5ed5b8e7c2c1f819e3845d35c4e20e84>`__
-  WHD_COUNTRY_CHRISTMAS_ISLAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aecfe598cebf2b72d3cfe77fb5b999244>`__
-  whd_country_code_t :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602>`__
-  WHD_COUNTRY_COLOMBIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a34e4e66cb208eaad502aff27339c5e1e>`__
-  WHD_COUNTRY_COMOROS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae3bc50d4684d980480c8e77f5cddbf22>`__
-  WHD_COUNTRY_CONGO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac003ca4c4b6ff3363414a5af32d3708f>`__
-  WHD_COUNTRY_CONGO_THE_DEMOCRATIC_REPUBLIC_OF_THE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a85e2ad1d9cb9db255046ec24e173d23e>`__
-  WHD_COUNTRY_COSTA_RICA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a726a776bbd473697c7d712c77fec4641>`__
-  WHD_COUNTRY_COTE_DIVOIRE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad093bb4e8acab3f68409bd3e1880f08e>`__
-  WHD_COUNTRY_CROATIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602afe6c1511f2547395ce73ed2ee5be0775>`__
-  WHD_COUNTRY_CUBA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad2c7ee32538bd6cfa74c95759530ca3c>`__
-  WHD_COUNTRY_CYPRUS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1de20aee06c515b04d36a6deccc21fd0>`__
-  WHD_COUNTRY_CZECH_REPUBLIC :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a3625aa03434d5542977a73e76562fbb9>`__
-  WHD_COUNTRY_DENMARK :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a123e23a550e7a2090f6778e209df88b0>`__
-  WHD_COUNTRY_DJIBOUTI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6966612a794d39ad699302b447e6d286>`__
-  WHD_COUNTRY_DOMINICA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad80f276af148804fe969d9cfc30afca8>`__
-  WHD_COUNTRY_DOMINICAN_REPUBLIC :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a97e88a10f482d24184d47fd1c8505f7c>`__
-  WHD_COUNTRY_DOWN_UNDER :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a84eec6d380810cde17e1ed34878ccf6d>`__
-  WHD_COUNTRY_ECUADOR :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae3f3dbde00549fab0b967a026488f6c3>`__
-  WHD_COUNTRY_EGYPT :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a3b16a609ff768c913a88797441df6107>`__
-  WHD_COUNTRY_EL_SALVADOR :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aba94b6ef5f7bcbdc0a196e5c3568162f>`__
-  WHD_COUNTRY_EQUATORIAL_GUINEA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6dd76befbc9318e3506cc44bf08cb8d1>`__
-  WHD_COUNTRY_ERITREA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5ab25febbbe2582c9ad218f869badcc3>`__
-  WHD_COUNTRY_ESTONIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a221e5b5eebcc3071540f6e6fdd692f30>`__
-  WHD_COUNTRY_ETHIOPIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af716cd38b781b1098ca4c2770d8c69c2>`__
-  WHD_COUNTRY_EUROPEAN_WIDE_REV895 :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a557dc78eb4d648cf1bbdfe588e2f2912>`__
-  WHD_COUNTRY_FALKLAND_ISLANDS_MALVINAS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a733054fabf650944bd38e92f280a044b>`__
-  WHD_COUNTRY_FAROE_ISLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa83846c28f7996a7b182499c7efd271e>`__
-  WHD_COUNTRY_FIJI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0fa63a76100a3a47a94577c07c4fc766>`__
-  WHD_COUNTRY_FINLAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae356b11ca5a6bddbc7a0c905803308b1>`__
-  WHD_COUNTRY_FRANCE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a747f19b0963ae45c5c344b90f0be09ea>`__
-  WHD_COUNTRY_FRENCH_GUINA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aef39f8df3efe46b64764eefb3267ae36>`__
-  WHD_COUNTRY_FRENCH_POLYNESIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a03b5d72ec6b661e620591f22bae08f4a>`__
-  WHD_COUNTRY_FRENCH_SOUTHERN_TERRITORIES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abf314a956f934d7795fca3c84bcd21e0>`__
-  WHD_COUNTRY_GABON :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602acb40ac85e59a92fea17ea304d17efff9>`__
-  WHD_COUNTRY_GAMBIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae6800f8e2ad045af81da3c898f6497ce>`__
-  WHD_COUNTRY_GEORGIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5c2ad817523941a21074a3e377fa480d>`__
-  WHD_COUNTRY_GERMANY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7791d7ac26d9d4343c0496c2e159190b>`__
-  WHD_COUNTRY_GHANA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac6ea83c4e447e3521ac8d8e5d898c200>`__
-  WHD_COUNTRY_GIBRALTAR :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a557f77ffbd0cf43e84cf85121e990fed>`__
-  WHD_COUNTRY_GREECE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad26b379054843ecf7e70127b29a58976>`__
-  WHD_COUNTRY_GRENADA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a35f98e8e0ca205e9a7060ce475a2aa40>`__
-  WHD_COUNTRY_GUADELOUPE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6706d8c6d48a30baf6f96207bfb04efd>`__
-  WHD_COUNTRY_GUAM :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a98ef170fc691d938ad04b7df7e584a01>`__
-  WHD_COUNTRY_GUATEMALA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0b9d8febff8bd8b6f4617ccd21a805eb>`__
-  WHD_COUNTRY_GUERNSEY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a9b7ec6307cd5ef266db43bb71cb7f128>`__
-  WHD_COUNTRY_GUINEA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa73ce4ffc17a7591a371155e4d4b6109>`__
-  WHD_COUNTRY_GUINEA_BISSAU :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac0ec790014f14166e36698205dfba57e>`__
-  WHD_COUNTRY_GUYANA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac8afd8e2064f40716297eb88a12edadb>`__
-  WHD_COUNTRY_HAITI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aaf070b0358c343df358068652afcce68>`__
-  WHD_COUNTRY_HOLY_SEE_VATICAN_CITY_STATE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aea3bf67e3565999f055885608c7c4daf>`__
-  WHD_COUNTRY_HONDURAS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a21e299a7a875490886684f9e4dba7576>`__
-  WHD_COUNTRY_HONG_KONG :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8f05d8daae41dacec62c08cd35d38706>`__
-  WHD_COUNTRY_HUNGARY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a41dc8df1b22373ccefcfffac5fb922e0>`__
-  WHD_COUNTRY_ICELAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aad262a8e2fc5122082aec4edb2fd14d5>`__
-  WHD_COUNTRY_INDIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af465e8755d5762ca58ba23e2bf1f533f>`__
-  WHD_COUNTRY_INDONESIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abdf6018376c85284c09ffed41e5dd9ca>`__
-  WHD_COUNTRY_IRAN_ISLAMIC_REPUBLIC_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae4eecfdb70f4da9290fa8f6b0eb834c2>`__
-  WHD_COUNTRY_IRAQ :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a294d8250b0b00730df005a1cc005c7f6>`__
-  WHD_COUNTRY_IRELAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5e5fe6acc6fa80f06e269b673b943e20>`__
-  WHD_COUNTRY_ISRAEL :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a3b2c184733b592037f45faad93efa8cc>`__
-  WHD_COUNTRY_ITALY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a9e18703f874e4d1536686ae5852114e2>`__
-  WHD_COUNTRY_JAMAICA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af35909c9e6d81e88969fd94ea556f2ce>`__
-  WHD_COUNTRY_JAPAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae0b960378ac4240b1687552a2a79bea9>`__
-  WHD_COUNTRY_JERSEY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a72c36ec2c352536afb89aaa3b1406391>`__
-  WHD_COUNTRY_JORDAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac52c278360c46c8fb831604c3333adf6>`__
-  WHD_COUNTRY_KAZAKHSTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af33a3ab91c5fd1285254748d5a809f68>`__
-  WHD_COUNTRY_KENYA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a208d456b89f5fbc1c2da13b3ada26291>`__
-  WHD_COUNTRY_KIRIBATI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a36804c3a53fc28ada67114eecf9e11d7>`__
-  WHD_COUNTRY_KOREA_REPUBLIC_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae545ebb8fee76591be2faada7da6bdac>`__
-  WHD_COUNTRY_KOSOVO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a228d180b77b08c9bdcc42dff20c70cdf>`__
-  WHD_COUNTRY_KUWAIT :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aee9c8a1ad5b822ff69c51a8566a08c24>`__
-  WHD_COUNTRY_KYRGYZSTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac3abc7083fa3c72445a9f41944d2864a>`__
-  WHD_COUNTRY_LAO_PEOPLES_DEMOCRATIC_REPUBIC :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1c3b3f7fd37f28f55ac4b6a447bb7003>`__
-  WHD_COUNTRY_LATVIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab8f7eb7a7adad5c463f41159749350d3>`__
-  WHD_COUNTRY_LEBANON :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a519a0d9341a05a24f93e5ca13b0d3d92>`__
-  WHD_COUNTRY_LESOTHO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8d2cc9cb1e8a9d86ca261da4de20d8a2>`__
-  WHD_COUNTRY_LIBERIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a148c223dec8f4b7f24c6c84ff679a5e3>`__
-  WHD_COUNTRY_LIBYAN_ARAB_JAMAHIRIYA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a084316071a4390d666555196182e4cdd>`__
-  WHD_COUNTRY_LIECHTENSTEIN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7106096dfeaa18acc3bc56aac6b4db4d>`__
-  WHD_COUNTRY_LITHUANIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a9946d1d75b70b438d0de9571303b0a96>`__
-  WHD_COUNTRY_LUXEMBOURG :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a22c88aec1158e2a34abb255170bf1245>`__
-  WHD_COUNTRY_MACAO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa6d065b2a35b3b3fe4547b04b97e8f1d>`__
-  WHD_COUNTRY_MACEDONIA_FORMER_YUGOSLAV_REPUBLIC_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602acfc2f020b3295cd704cfa2ff2a667eee>`__
-  WHD_COUNTRY_MADAGASCAR :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa5d0401f6610b50f594272a66ead46e3>`__
-  WHD_COUNTRY_MALAWI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ace2810c6a0a379bea125a63a0b8fa0af>`__
-  WHD_COUNTRY_MALAYSIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a82d471d6333b18fd0d290a8b375033e2>`__
-  WHD_COUNTRY_MALDIVES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab965a8aeb2862f58fb27eeaa67f3d04f>`__
-  WHD_COUNTRY_MALI :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abfbb05227a672d8d3fecf071fe427161>`__
-  WHD_COUNTRY_MALTA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602acfb6daa860d8e5cd60801237bf25aedf>`__
-  WHD_COUNTRY_MAN_ISLE_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a869f7c0d7efda7142f47a7e88bd5e344>`__
-  WHD_COUNTRY_MARTINIQUE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a076de94a02a472d0fd8162f48921c018>`__
-  WHD_COUNTRY_MAURITANIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5d9d1c3cd6cd3aa6206d756a1c915c81>`__
-  WHD_COUNTRY_MAURITIUS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa5178f7f44391b25f409a75b37cd0cdc>`__
-  WHD_COUNTRY_MAYOTTE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a96454a25ed1b69b7f0f0d92601ed9987>`__
-  WHD_COUNTRY_MEXICO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a95eef4bf4935992c4297f8d5bee6dbae>`__
-  WHD_COUNTRY_MICRONESIA_FEDERATED_STATES_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad60eb0ac67cf8cc0881c9170f7eb80d8>`__
-  WHD_COUNTRY_MOLDOVA_REPUBLIC_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad2e6f80c3b8389267daf5573df4779d2>`__
-  WHD_COUNTRY_MONACO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a18059d3339f927a34115b35520162c26>`__
-  WHD_COUNTRY_MONGOLIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a2d81e9ec568f1df7a72d2d23eb23f258>`__
-  WHD_COUNTRY_MONTENEGRO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a39adf99ed1f2478a0739346f1d222a7c>`__
-  WHD_COUNTRY_MONTSERRAT :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac637c256031eecbb8a885af1d4a47fb3>`__
-  WHD_COUNTRY_MOROCCO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a2ed09927575bce96192c493fba569987>`__
-  WHD_COUNTRY_MOZAMBIQUE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a77be84b525fc558598d9fd5ba5b81706>`__
-  WHD_COUNTRY_MYANMAR :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a194bf205760d740770b47f1e2dbb1782>`__
-  WHD_COUNTRY_NAMIBIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8ea6a47eca07425b9418899dd083801c>`__
-  WHD_COUNTRY_NAURU :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a674644e323d1b20022c50686b21d67f4>`__
-  WHD_COUNTRY_NEPAL :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a87c031d1c96980cdbbc4e1df6af14b45>`__
-  WHD_COUNTRY_NETHERLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab88b8fb0927f56275ad1dbc1263bd7f2>`__
-  WHD_COUNTRY_NETHERLANDS_ANTILLES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602acc1a6ae1a250255331d3566a8ec1d67c>`__
-  WHD_COUNTRY_NEW_CALEDONIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a84403d618acd04b681e0c749e7ffacb2>`__
-  WHD_COUNTRY_NEW_ZEALAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1c1aee2de9eaffcdfdfbe6c1f78d0637>`__
-  WHD_COUNTRY_NICARAGUA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a43dfe0f1a2e6638f1275bfd973f7a389>`__
-  WHD_COUNTRY_NIGER :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aabe3b5667f6a0bd781a8ba2d9430ca01>`__
-  WHD_COUNTRY_NIGERIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a442808483cd3fdb7c928cb428e92080e>`__
-  WHD_COUNTRY_NORFOLK_ISLAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a1623028336473099ee7a101ec8a521b5>`__
-  WHD_COUNTRY_NORTHERN_MARIANA_ISLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aab8168ab9ea38a7873e32eed16e3e8b3>`__
-  WHD_COUNTRY_NORWAY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab3e3b29686764f32fbe45a7246e00953>`__
-  WHD_COUNTRY_OMAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a99c85200419992bd4eeb3d244d6ac631>`__
-  WHD_COUNTRY_PAKISTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a4580c23e0c842b0ab42f390f301c8079>`__
-  WHD_COUNTRY_PALAU :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602addfc3a89a81a980fb05ac51c57d6df16>`__
-  WHD_COUNTRY_PANAMA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aba97b764cd570b1592a685270463ab86>`__
-  WHD_COUNTRY_PAPUA_NEW_GUINEA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6a9a094f05b9cee9b5033a2d815866c2>`__
-  WHD_COUNTRY_PARAGUAY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a877c185178c8c8a4a14fffad2ea352b9>`__
-  WHD_COUNTRY_PERU :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a4811b46e2cedb4b3707cbbb7f9d373e7>`__
-  WHD_COUNTRY_PHILIPPINES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a9ff1e9fdba8d5e35a8e5447cbeb587fd>`__
-  WHD_COUNTRY_POLAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ace120d194662e88e238af89d55f28fc3>`__
-  WHD_COUNTRY_PORTUGAL :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae21bd728e2fa7610b73c3e7a5e8c3020>`__
-  WHD_COUNTRY_PUETO_RICO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602adbcf2230a367ee819d3d1f5fd8229656>`__
-  WHD_COUNTRY_QATAR :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0878d0014862206af7cac468a3972727>`__
-  WHD_COUNTRY_REUNION :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a14a0ff94a7eee02e70b3de50ab8eb7a3>`__
-  WHD_COUNTRY_ROMANIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa2ab39eef5becb203a96138c3c820c1b>`__
-  WHD_COUNTRY_RUSSIAN_FEDERATION :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a33aa2fbce6047122fd93158b35ea0b05>`__
-  WHD_COUNTRY_RWANDA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7d7367cc418bdf56d8501fc947c1ec68>`__
-  WHD_COUNTRY_SAINT_KITTS_AND_NEVIS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0daf67a923f71fb27ca2030c912004f7>`__
-  WHD_COUNTRY_SAINT_LUCIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a54824ebf705be95da5afceef14ba7f04>`__
-  WHD_COUNTRY_SAINT_PIERRE_AND_MIQUELON :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac0682dfa09b0d3fd562f273acd0d4bab>`__
-  WHD_COUNTRY_SAINT_VINCENT_AND_THE_GRENADINES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac4b57b5f4f2a11929c50ac6294803363>`__
-  WHD_COUNTRY_SAMOA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa765025ec6762254e8174229f85edf48>`__
-  WHD_COUNTRY_SANIT_MARTIN_SINT_MARTEEN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab22c39087bcf2366631c55d19257aef9>`__
-  WHD_COUNTRY_SAO_TOME_AND_PRINCIPE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a724068ed705cd4d0786735529e72aa8a>`__
-  WHD_COUNTRY_SAUDI_ARABIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6180fa8d8a4c429b1b0f1164bbb74780>`__
-  WHD_COUNTRY_SENEGAL :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a428c7a86948575df4c6ce6fa18c90da8>`__
-  WHD_COUNTRY_SERBIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af81575db32ea976752b28b6a765f8a79>`__
-  WHD_COUNTRY_SEYCHELLES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0966efe7527ffd00cb54a61b30880763>`__
-  WHD_COUNTRY_SIERRA_LEONE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8b0888491f1d99fa9e0098313ab4a457>`__
-  WHD_COUNTRY_SINGAPORE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7ceec39dcab743bda2fd3af599579696>`__
-  WHD_COUNTRY_SLOVAKIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a675936d175b7a1d64b9f3bbbd8a46cf1>`__
-  WHD_COUNTRY_SLOVENIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a48e7173849521e008b96e6808cac6042>`__
-  WHD_COUNTRY_SOLOMON_ISLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a74863678fb25be3b767188a861091468>`__
-  WHD_COUNTRY_SOMALIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0b2df2e6288d95b93887f501295240bb>`__
-  WHD_COUNTRY_SOUTH_AFRICA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a07bee2924ee54d1c74f80190d5e7e7a9>`__
-  WHD_COUNTRY_SPAIN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a37a91d2fca382aacac6a409768592453>`__
-  WHD_COUNTRY_SRI_LANKA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af2dfe3f5a822d08a6e15438e71116ca3>`__
-  WHD_COUNTRY_SURINAME :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a9a7652d4a0e94aec5b347f06adde9dd0>`__
-  WHD_COUNTRY_SWAZILAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abb595de433f8d521c19b29e7df568f10>`__
-  WHD_COUNTRY_SWEDEN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a169ff9217bce13d40b61c7981be6500c>`__
-  WHD_COUNTRY_SWITZERLAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab49759646877a2f88dfcf21024882024>`__
-  WHD_COUNTRY_SYRIAN_ARAB_REPUBLIC :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a11fa3c6d43509b7108e52c0591792695>`__
-  WHD_COUNTRY_TAIWAN_PROVINCE_OF_CHINA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af46c80327d3dd82d77f871ca3b7a8412>`__
-  WHD_COUNTRY_TAJIKISTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0490ac27378c2aac97428c87e6c22eb3>`__
-  WHD_COUNTRY_TANZANIA_UNITED_REPUBLIC_OF :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a63c8174222e8251ca58b296afe4fb0b2>`__
-  WHD_COUNTRY_THAILAND :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a2b7d2bd6cd4caa9aefa2e830a64dcd7a>`__
-  WHD_COUNTRY_TOGO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a44bbe0126f99fdcd6a31fa6f8f4ff10c>`__
-  WHD_COUNTRY_TONGA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602abcaab8b85a785f167e4db8f9eca42eeb>`__
-  WHD_COUNTRY_TRINIDAD_AND_TOBAGO :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5f064f5a7a02bc8c51a8fdd4b6b3f34e>`__
-  WHD_COUNTRY_TUNISIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae593d1db3974edc9f2929bfce1a60f9f>`__
-  WHD_COUNTRY_TURKEY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602afd09a8ca5076e0fc26af0ba15519d12c>`__
-  WHD_COUNTRY_TURKMENISTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab1db73e8a705bdff1ae4d5f32d23e76c>`__
-  WHD_COUNTRY_TURKS_AND_CAICOS_ISLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a0226d0b32ce0ae55beb66b6984e183b1>`__
-  WHD_COUNTRY_TUVALU :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac6a05fb5107eae3d7abc5d44c768c467>`__
-  WHD_COUNTRY_UGANDA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac5a999f73d5f6068f3051188dcfdf484>`__
-  WHD_COUNTRY_UKRAINE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a52653ed0fa500aec109e48062b37628a>`__
-  WHD_COUNTRY_UNITED_ARAB_EMIRATES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab483ff06b30aed116e2c304a81a50991>`__
-  WHD_COUNTRY_UNITED_KINGDOM :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7f380c41dc00c60e605977eecefc12d8>`__
-  WHD_COUNTRY_UNITED_STATES :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a5916c7373b3c5c51a37408b7e6fc9a11>`__
-  WHD_COUNTRY_UNITED_STATES_MINOR_OUTLYING_ISLANDS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8c5cc2e06d48a055e87dc50dd476bb8f>`__
-  WHD_COUNTRY_UNITED_STATES_NO_DFS :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a7978297eadb16cdcc60ce94eb2243514>`__
-  WHD_COUNTRY_UNITED_STATES_REV4 :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a2a146a0f3575c98e2de26e96d3cfe411>`__
-  WHD_COUNTRY_UNITED_STATES_REV931 :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a6139ef6c6fc963d6f65606011593abfa>`__
-  WHD_COUNTRY_URUGUAY :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a8a30da815d77006bd79261a1a8f6ba13>`__
-  WHD_COUNTRY_UZBEKISTAN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a29a6b48684d5f13913e074d5eff373ef>`__
-  WHD_COUNTRY_VANUATU :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a42567f62fb3db9b331d667cadf521c4b>`__
-  WHD_COUNTRY_VENEZUELA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a32f35514879a6d2388dd287f2b733a95>`__
-  WHD_COUNTRY_VIET_NAM :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ad6a9f5318cc8cbff8f62ebc055ea5bc6>`__
-  WHD_COUNTRY_VIRGIN_ISLANDS_BRITISH :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a51b412f5fbbc776aaba60e6539f09960>`__
-  WHD_COUNTRY_VIRGIN_ISLANDS_US :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ac6c0b157b4dd0c702f8b8efbabe72e3a>`__
-  WHD_COUNTRY_WALLIS_AND_FUTUNA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a4ead1c340c1b0b803a1e7229ad2d8c6e>`__
-  WHD_COUNTRY_WEST_BANK :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a62b82918fc05f1161b649ba7868b9cb1>`__
-  WHD_COUNTRY_WESTERN_SAHARA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ab84d497102e7afac1a6d976bc2e1f52f>`__
-  WHD_COUNTRY_WORLD_WIDE_XV_REV983 :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602af3850579e2b2432fc0e1747d33bbec02>`__
-  WHD_COUNTRY_WORLD_WIDE_XX :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a16ebc6cfb59f870500fc7339ae75b0c8>`__
-  WHD_COUNTRY_WORLD_WIDE_XX_REV17 :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602aa474c04ef58a2de055b26a4894f73dcc>`__
-  WHD_COUNTRY_YEMEN :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602ae2d344ec27a73529f793ff47ad765f90>`__
-  WHD_COUNTRY_ZAMBIA :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602afe0f095c0ee7807b48694026f7689e0b>`__
-  WHD_COUNTRY_ZIMBABWE :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602a4337f5e3720a79a511cae64e63123293>`__
-  whd_custom_ie_action_t :
   `whd_types.h <whd__types_8h.html#a8be1026494a86f0ceeebb2dcbf092cbd>`__
-  whd_deinit() :
   `whd_wifi_api.h <group__wifimanagement.html#gab7d6e59874922e5646e08e52082e32f5>`__
-  WHD_DELAY_TOO_LONG :
   `whd_types.h <whd__types_8h.html#a7c128bcb61d6db9a8d58a2f5b9ed55a2>`__
-  WHD_DELAY_TOO_SHORT :
   `whd_types.h <whd__types_8h.html#a34cc0bb8645a8046708428c134c76f14>`__
-  WHD_DOES_NOT_EXIST :
   `whd_types.h <whd__types_8h.html#abb82350191c4b2751119d4206044c676>`__
-  WHD_DOT11_RC_RESERVED :
   `whd_types.h <whd__types_8h.html#ac81b31559ee1db82f01e8acfb8eea55daf2b4dc47610f370bd154aa27c2434398>`__
-  WHD_DOT11_RC_UNSPECIFIED :
   `whd_types.h <whd__types_8h.html#ac81b31559ee1db82f01e8acfb8eea55da03fab52dfc684e41485202d5a72588d1>`__
-  whd_dot11_reason_code_t :
   `whd_types.h <whd__types_8h.html#ac81b31559ee1db82f01e8acfb8eea55d>`__
-  whd_driver_t :
   `whd.h <whd_8h.html#ac45015d82e65db891b463066873eca4f>`__
-  WHD_EAPOL_KEY_FAILURE :
   `whd_types.h <whd__types_8h.html#a11f0c32e1137f71695fca3ffcfba3225>`__
-  WHD_EAPOL_KEY_PACKET_G1_TIMEOUT :
   `whd_types.h <whd__types_8h.html#a2b501e596df5290737987b146f4f8ea7>`__
-  WHD_EAPOL_KEY_PACKET_M1_TIMEOUT :
   `whd_types.h <whd__types_8h.html#a433165f9b0097e6c60d21ae88f2f16f6>`__
-  WHD_EAPOL_KEY_PACKET_M3_TIMEOUT :
   `whd_types.h <whd__types_8h.html#a6ebde3f52c86c797f2a6f2e1366febac>`__
-  WHD_ETHERNET_SIZE :
   `whd_types.h <whd__types_8h.html#ac7923e15c2e0a935cb7c8cad13b9dc6e>`__
-  whd_event_eth_hdr_t :
   `whd_events.h <whd__events_8h.html#a8e7e9d5ab8841a160aa9e2420c636864>`__
-  whd_event_ether_header_t :
   `whd_events.h <whd__events_8h.html#a31915a18541e6182e926247a3b8ed3dc>`__
-  whd_event_handler_t :
   `whd_events.h <group__event.html#ga9c74af353c83a8f77097acbc65362b07>`__
-  whd_event_t :
   `whd_events.h <whd__events_8h.html#a9d6929e68c05572695ee040da171d508>`__
-  WHD_FALSE :
   `whd_types.h <whd__types_8h.html#a7cd94a03f2e7e6aab7217ed559c7a0aca13563ade40eb686d1a1d81372cb31412>`__
-  WHD_FILTER_NOT_FOUND :
   `whd_types.h <whd__types_8h.html#a91089285a820e59587c544de682420e3>`__
-  WHD_FLOW_CONTROLLED :
   `whd_types.h <whd__types_8h.html#a23f5c05cf88f6ed945cf15c111c778cb>`__
-  WHD_HAL_ERROR :
   `whd_types.h <whd__types_8h.html#a68e4cdc07dccd14d0cc6c6216ff37fb2>`__
-  WHD_HANDLER_ALREADY_REGISTERED :
   `whd_types.h <whd__types_8h.html#a18206b25da666ffc407fcf2b207a1739>`__
-  WHD_HWTAG_MISMATCH :
   `whd_types.h <whd__types_8h.html#abdd206e05538f81fcb9dca59d4f4404e>`__
-  whd_ie_packet_flag_t :
   `whd_types.h <whd__types_8h.html#a07b4f24f4e2abacc45a9ad29f008488f>`__
-  whd_init() :
   `whd_wifi_api.h <group__wifimanagement.html#ga286dde2ee65ac3ea5ae67a6e6ef25f0a>`__
-  whd_init_config_t :
   `whd.h <whd_8h.html#a933f0025533b9b88ecb77b651db29250>`__
-  WHD_INTERFACE_NOT_UP :
   `whd_types.h <whd__types_8h.html#a32f993fc79964aeb95864885ab604a20>`__
-  whd_interface_t :
   `whd.h <whd_8h.html#a2e544c482ddbb690bbb95ce7174e79a0>`__
-  WHD_INVALID_DUTY_CYCLE :
   `whd_types.h <whd__types_8h.html#a8ef95acfbfd3ccbb9912af09bc4d0784>`__
-  WHD_INVALID_INTERFACE :
   `whd_types.h <whd__types_8h.html#a17a759d5ba4e5b5cfc51a5ea89e16ad0>`__
-  WHD_INVALID_JOIN_STATUS :
   `whd_types.h <whd__types_8h.html#ab0c582f87b55cbcbbfdcdd1551f6be1b>`__
-  WHD_INVALID_KEY :
   `whd_types.h <whd__types_8h.html#a63b3cf80bafef7f1f255c0c826bceb10>`__
-  WHD_IOCTL_FAIL :
   `whd_types.h <whd__types_8h.html#ac9f58fcc05846835a544720f7a80e9f7>`__
-  WHD_IOVAR_GET_LISTEN_INTERVAL :
   `whd_types.h <whd__types_8h.html#adc3a23bad78e0110546c9edc00456dafa335ed12c48af483e1c930e5c7b9f5d57>`__
-  WHD_IOVAR_GET_MAC_ADDRESS :
   `whd_types.h <whd__types_8h.html#adc3a23bad78e0110546c9edc00456dafa5ebc26219966e0db67e2cdbb8f89d986>`__
-  WHD_JOIN_IN_PROGRESS :
   `whd_types.h <whd__types_8h.html#a39d85692e57da297005929f28430cf8e>`__
-  WHD_LINK_HEADER :
   `whd_types.h <whd__types_8h.html#a19b63b93a33a8d9f330925e1f0e87a5d>`__
-  WHD_LINK_MTU :
   `whd_types.h <whd__types_8h.html#a6c25e15fb4270bbd5e77169d9ce1da59>`__
-  WHD_LISTEN_INTERVAL_TIME_UNIT_BEACON :
   `whd_types.h <whd__types_8h.html#a98ac1a2590358bafac6a2da287034638ab1001ef79a4819c782281e4fb275a53b>`__
-  WHD_LISTEN_INTERVAL_TIME_UNIT_DTIM :
   `whd_types.h <whd__types_8h.html#a98ac1a2590358bafac6a2da287034638ae1f7bbf9c410906413d74238699e277c>`__
-  whd_listen_interval_time_unit_t :
   `whd_types.h <whd__types_8h.html#a98ac1a2590358bafac6a2da287034638>`__
-  WHD_MALLOC_FAILURE :
   `whd_types.h <whd__types_8h.html#a6450f155199992d564d036f64963cd54>`__
-  WHD_MSG_IFNAME_MAX :
   `whd_events.h <whd__events_8h.html#ac94cc32da29a68641af2b000d883cd6a>`__
-  whd_netif_funcs_t :
   `whd.h <whd_8h.html#ac35b975959ed585acf554535b502ce68>`__
-  whd_network_get_bsscfgidx_from_ifp() :
   `whd_wifi_api.h <group__dbg.html#ga84376c0830fbdb04c67812243539d4c6>`__
-  whd_network_get_ifidx_from_ifp() :
   `whd_wifi_api.h <group__dbg.html#ga01abfdd80ff65c7cac64e0d7c2f93c60>`__
-  WHD_NETWORK_NOT_FOUND :
   `whd_types.h <whd__types_8h.html#a27ea061512955c1fa154666b11346771>`__
-  WHD_NETWORK_RX :
   `whd_network_types.h <group__buffif.html#gga44a64c51498b204ceef5555209e29452a11af875b5bd8b18c4a0c878af601d1ca>`__
-  whd_network_send_ethernet_data() :
   `whd_network_types.h <group__netif.html#ga3642a8b0fe0df7449730bec92e09c7d4>`__
-  WHD_NETWORK_TX :
   `whd_network_types.h <group__buffif.html#gga44a64c51498b204ceef5555209e29452a3d24bf5423cc175fe6ee80bd359dc5bd>`__
-  WHD_NO_CREDITS :
   `whd_types.h <whd__types_8h.html#a6270efc8ded415e7d0e1059b3d5eff1f>`__
-  WHD_NO_PACKET_TO_RECEIVE :
   `whd_types.h <whd__types_8h.html#a5a5122cc33024c33eaa9dcca1d6b2917>`__
-  WHD_NO_PACKET_TO_SEND :
   `whd_types.h <whd__types_8h.html#a1e3927ec83cfaac801196210b8a9af45>`__
-  WHD_NOT_AUTHENTICATED :
   `whd_types.h <whd__types_8h.html#ac2e279a3f388f66c076d4dd73310a27b>`__
-  WHD_NOT_KEYED :
   `whd_types.h <whd__types_8h.html#a4377fd0c321dbebed159e8c84858a1e4>`__
-  WHD_NULL_PTR_ARG :
   `whd_types.h <whd__types_8h.html#ab7612a8165b6e28255c4a2ef78627454>`__
-  whd_oob_config_t :
   `whd_types.h <whd__types_8h.html#ac07b13dcb5ec0e66a707dcf8a539c97e>`__
-  WHD_OUT_OF_EVENT_HANDLER_SPACE :
   `whd_types.h <whd__types_8h.html#a24f120d05f15b5c5564c8fc7098516f3>`__
-  WHD_PACKET_FILTER_RULE_NEGATIVE_MATCHING :
   `whd_types.h <whd__types_8h.html#a36b806b9b0e986bf290ae005d1bae038a1f6065677744b90d146bafbcb3832cac>`__
-  WHD_PACKET_FILTER_RULE_POSITIVE_MATCHING :
   `whd_types.h <whd__types_8h.html#a36b806b9b0e986bf290ae005d1bae038a75352695a5824e5b1759743869bca6d9>`__
-  whd_packet_filter_rule_t :
   `whd_types.h <whd__types_8h.html#a36b806b9b0e986bf290ae005d1bae038>`__
-  WHD_PARTIAL_RESULTS :
   `whd_types.h <whd__types_8h.html#a1b8d28e95bef4c55d0cdbc2ffc6c9ef7>`__
-  WHD_PAYLOAD_MTU :
   `whd_types.h <whd__types_8h.html#a90b4f488c8f53596cffcb88fe6111fcc>`__
-  WHD_PENDING :
   `whd_types.h <whd__types_8h.html#a8c29570e0fc44f87bdea3b6a438efe1c>`__
-  whd_pf_add_packet_filter() :
   `whd_wifi_api.h <group__wifiutilities.html#gae81aead624f8e374970e1aa3f330ea30>`__
-  whd_pf_disable_packet_filter() :
   `whd_wifi_api.h <group__wifiutilities.html#ga6af0c7e202709713fa5d06e71a585e28>`__
-  whd_pf_enable_packet_filter() :
   `whd_wifi_api.h <group__wifiutilities.html#ga3e70cd84bf811b869c955d75fa80590d>`__
-  whd_pf_get_packet_filter_mask_and_pattern() :
   `whd_wifi_api.h <group__wifiutilities.html#gacd9d57c8febb11cc240db23f5de92cd9>`__
-  whd_pf_get_packet_filter_stats() :
   `whd_wifi_api.h <group__wifiutilities.html#ga26172adb5ec181bb8f2009e47eb252cf>`__
-  whd_pf_remove_packet_filter() :
   `whd_wifi_api.h <group__wifiutilities.html#gaff414d31f3406ec5d1af54b889f4849a>`__
-  WHD_PHYSICAL_HEADER :
   `whd_types.h <whd__types_8h.html#a65859cd60e3f43138360dfa07e2d0b30>`__
-  WHD_PMK_WRONG_LENGTH :
   `whd_types.h <whd__types_8h.html#a9691a3d9b9499bc36d73483bdf824ab7>`__
-  whd_print_stats() :
   `whd_wifi_api.h <group__dbg.html#gab6dca3a6d7a90a1f9d8fb18660048c18>`__
-  WHD_QUEUE_ERROR :
   `whd_types.h <whd__types_8h.html#a0c06e71abd08027b33cc93a684c04cd5>`__
-  WHD_REMOVE_CUSTOM_IE :
   `whd_types.h <whd__types_8h.html#a8be1026494a86f0ceeebb2dcbf092cbdadc803bf5bf9286741e59da1b850561ce>`__
-  whd_resource_source_t :
   `whd.h <whd_8h.html#a05847ad8fa418d69d5f51fe654835b8b>`__
-  whd_resource_type_t :
   `whd_resource_api.h <whd__resource__api_8h.html#a26c678527cf0cb5196be511d9ad62c0c>`__
-  WHD_RESOURCE_WLAN_CLM :
   `whd_resource_api.h <whd__resource__api_8h.html#a26c678527cf0cb5196be511d9ad62c0cad763d3e34be0df4a36b630bb7c37e51c>`__
-  WHD_RESOURCE_WLAN_FIRMWARE :
   `whd_resource_api.h <whd__resource__api_8h.html#a26c678527cf0cb5196be511d9ad62c0caf1635251dcc37bfa67af783a53f0acf3>`__
-  WHD_RESOURCE_WLAN_NVRAM :
   `whd_resource_api.h <whd__resource__api_8h.html#a26c678527cf0cb5196be511d9ad62c0ca397297fd6225d6eae30e7fa0d40a94f7>`__
-  WHD_RESULT_CREATE :
   `whd_types.h <whd__types_8h.html#adf4a5004401ae1772bf5b33a5e19e108>`__
-  whd_result_t :
   `whd_types.h <whd__types_8h.html#add62f4b5040a2451e23869d2f9e1ae05>`__
-  WHD_RESULT_TYPE :
   `whd_types.h <whd__types_8h.html#a24950ac00b520d360c9d6092be69ee68>`__
-  WHD_RTOS_ERROR :
   `whd_types.h <whd__types_8h.html#a2ef366a6a9ac07e3087461c1946499dd>`__
-  WHD_RTOS_STATIC_MEM_LIMIT :
   `whd_types.h <whd__types_8h.html#a6499f6328208f184b4f716a910d7cdf6>`__
-  WHD_RX_BUFFER_ALLOC_FAIL :
   `whd_types.h <whd__types_8h.html#a7609ba95d8336c6d5a3a790b0b61b590>`__
-  WHD_SCAN_ABORTED :
   `whd_types.h <whd__types_8h.html#a34d5a5749a0bcc00b7249108a8670adaa83e78acc28b9e74f743db6deb743472b>`__
-  WHD_SCAN_COMPLETED_SUCCESSFULLY :
   `whd_types.h <whd__types_8h.html#a34d5a5749a0bcc00b7249108a8670adaafca7e19867e47de64de826d409bc7c7d>`__
-  WHD_SCAN_INCOMPLETE :
   `whd_types.h <whd__types_8h.html#a34d5a5749a0bcc00b7249108a8670adaa5c525f0cbf37c43871a67556a2581dcd>`__
-  whd_scan_result_callback_t :
   `whd_wifi_api.h <group__wifijoin.html#ga9f8c6096922212981dd2101a17aec471>`__
-  WHD_SCAN_RESULT_FLAG_BEACON :
   `whd_types.h <whd__types_8h.html#a4bb9a3034dfb9a44507ef6201842edb7a7c6e2726f436bdc8b7d45f7cc34c504b>`__
-  WHD_SCAN_RESULT_FLAG_RSSI_OFF_CHANNEL :
   `whd_types.h <whd__types_8h.html#a4bb9a3034dfb9a44507ef6201842edb7a5239fb226233ffc16252907b91fa8be7>`__
-  whd_scan_result_flag_t :
   `whd_types.h <whd__types_8h.html#a4bb9a3034dfb9a44507ef6201842edb7>`__
-  whd_scan_result_t :
   `whd_types.h <whd__types_8h.html#a58298b1b5c2f2425d5aed061de48aaa7>`__
-  whd_scan_status_t :
   `whd_types.h <whd__types_8h.html#a34d5a5749a0bcc00b7249108a8670ada>`__
-  WHD_SCAN_TYPE_ACTIVE :
   `whd_types.h <whd__types_8h.html#af7ac1b0be4ce67f3e82b876c2f27fd3aaac68ee09cf61767ddf825d3164fc48f6>`__
-  WHD_SCAN_TYPE_NO_BSSID_FILTER :
   `whd_types.h <whd__types_8h.html#af7ac1b0be4ce67f3e82b876c2f27fd3aa3973e30c4f21443ba51cb43a60e80927>`__
-  WHD_SCAN_TYPE_PASSIVE :
   `whd_types.h <whd__types_8h.html#af7ac1b0be4ce67f3e82b876c2f27fd3aa1f46026d7581b09ec416e4e33dbdeb0e>`__
-  WHD_SCAN_TYPE_PNO :
   `whd_types.h <whd__types_8h.html#af7ac1b0be4ce67f3e82b876c2f27fd3aa6906a0f7483557038c547223181666f1>`__
-  WHD_SCAN_TYPE_PROHIBITED_CHANNELS :
   `whd_types.h <whd__types_8h.html#af7ac1b0be4ce67f3e82b876c2f27fd3aae2c9c7e9d61ed8493aef4ae4fef5f742>`__
-  whd_scan_type_t :
   `whd_types.h <whd__types_8h.html#af7ac1b0be4ce67f3e82b876c2f27fd3a>`__
-  WHD_SDIO_BUS_UP_FAIL :
   `whd_types.h <whd__types_8h.html#ac73dc3debf5093500f5fcd0b1750e8e7>`__
-  whd_sdio_config_t :
   `whd_types.h <whd__types_8h.html#a2b3fb82834e1fbcbbf1f51e83c675edd>`__
-  whd_sdio_funcs_t :
   `whd.h <whd_8h.html#af5e9a147a89968d2026fd7720a519bbd>`__
-  WHD_SDIO_RETRIES_EXCEEDED :
   `whd_types.h <whd__types_8h.html#ac5725175be9d81f698084386eb44688e>`__
-  WHD_SDIO_RX_FAIL :
   `whd_types.h <whd__types_8h.html#a276b9d9307ec24da1cd6ed1da2f0feaf>`__
-  WHD_SECURITY_FORCE_32_BIT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059aa6ab763d4d6b3fcffd944d14b3edd1f4>`__
-  WHD_SECURITY_IBSS_OPEN :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059aa9ded800120d37ad54c4467688416abc>`__
-  WHD_SECURITY_OPEN :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a2b5521d2587803363849e1987775e78b>`__
-  whd_security_t :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059>`__
-  WHD_SECURITY_UNKNOWN :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059aebaa913f60b77b94277287056775c27d>`__
-  WHD_SECURITY_WEP_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059abe451579838fff23cd0a1fc1d1ff313d>`__
-  WHD_SECURITY_WEP_SHARED :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a25a16cfc764ee18ff73d440b9a307228>`__
-  WHD_SECURITY_WPA2_AES_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a278600fd506f0522cd923a8ee066c3f4>`__
-  WHD_SECURITY_WPA2_AES_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a81b7002ff8c0d488af8525ae614b6cf5>`__
-  WHD_SECURITY_WPA2_FBT_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a843c4f3ffe1f3e006cde49b7bdee5a09>`__
-  WHD_SECURITY_WPA2_FBT_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059ae380b414a2bc8a9343d0a6f4a71c0c0c>`__
-  WHD_SECURITY_WPA2_MIXED_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059af37aae67b60c8d1cfc83be2bbbeeb894>`__
-  WHD_SECURITY_WPA2_MIXED_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a602d04db8143c505cc674c738bf20150>`__
-  WHD_SECURITY_WPA2_TKIP_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059ad3888b236e75c3d5d5e491d01de5cd7f>`__
-  WHD_SECURITY_WPA2_TKIP_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a901ffc3e307d8b44bc80150e7cea8fbb>`__
-  WHD_SECURITY_WPA2_WPA_AES_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a2ecbc670d4cb7b82a2fbe4ee9af3a217>`__
-  WHD_SECURITY_WPA2_WPA_MIXED_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a769e1d8325cd5eda1363e1d2b9157235>`__
-  WHD_SECURITY_WPA3_SAE :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a9d550fa7cc0ea96ac1e9a4de5223d2b0>`__
-  WHD_SECURITY_WPA3_WPA2_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059af4d58bc2335113f0c70abdbd3019bb61>`__
-  WHD_SECURITY_WPA_AES_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a45b9dd3606cbe683b3db7fd13b392ec2>`__
-  WHD_SECURITY_WPA_AES_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a8905c2aea364d45edf8496e5190effd0>`__
-  WHD_SECURITY_WPA_MIXED_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a67aad7934dfb63684dcf3859f3dd1d88>`__
-  WHD_SECURITY_WPA_MIXED_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059afd3dd8c5b97f85f737177097fc0481b4>`__
-  WHD_SECURITY_WPA_TKIP_ENT :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a8331ab29a560b0936a5f16ed2745b550>`__
-  WHD_SECURITY_WPA_TKIP_PSK :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059ad73ab4a21acbd437cc43c3b58ef45771>`__
-  WHD_SECURITY_WPS_SECURE :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059a2d39ccad06b7d7225b03186160144195>`__
-  WHD_SEMAPHORE_ERROR :
   `whd_types.h <whd__types_8h.html#a465f9477fbe7df4860470bf262a20e81>`__
-  WHD_SET_BLOCK_ACK_WINDOW_FAIL :
   `whd_types.h <whd__types_8h.html#a8aedd7c7219349b28905c47b547e3419>`__
-  WHD_SLEEP_ERROR :
   `whd_types.h <whd__types_8h.html#ad506512b8a302ce61df402f59bc7504c>`__
-  whd_spi_config_t :
   `whd_types.h <whd__types_8h.html#a2f5c5d3c7f74ce9a86595fe220c860d3>`__
-  whd_spi_funcs_t :
   `whd.h <whd_8h.html#a7373cb0b0ef75e1b740fd53c77bda5e4>`__
-  WHD_SPI_ID_READ_FAIL :
   `whd_types.h <whd__types_8h.html#a0ed6696f986b3d35b4306c06a708db19>`__
-  WHD_SPI_SIZE_MISMATCH :
   `whd_types.h <whd__types_8h.html#a6e7d6fcd9f48e9df5bd8ed66f88d05f3>`__
-  WHD_SUCCESS :
   `whd_types.h <whd__types_8h.html#a8aaeff5d2f1bfaeeb2931c401ac40a3f>`__
-  whd_sync_scan_result_t :
   `whd_types.h <whd__types_8h.html#ab30a697b3ebbf2007dc09194b54b1a1b>`__
-  WHD_THREAD_CREATE_FAILED :
   `whd_types.h <whd__types_8h.html#ab7fb839bb825a42085305032735fcfff>`__
-  WHD_THREAD_DELETE_FAIL :
   `whd_types.h <whd__types_8h.html#a4deb31cfc8310b82d3b6359b526f7e9b>`__
-  WHD_THREAD_FINISH_FAIL :
   `whd_types.h <whd__types_8h.html#a3be54cee48ade84eff99df8a29979958>`__
-  WHD_THREAD_STACK_NULL :
   `whd_types.h <whd__types_8h.html#a9c694e98139f96882d3098802ec9a947>`__
-  whd_time_t :
   `whd_types.h <whd__types_8h.html#aaf5901182e9ac5462ba6d4ada56e54d0>`__
-  WHD_TIMEOUT :
   `whd_types.h <whd__types_8h.html#a7eee7c29f665510d218cea3c620b3150>`__
-  whd_tko_get_FW_connect() :
   `whd_wifi_api.h <group__wifiutilities.html#gacc2882e3a308f7b2b938bc5741ab43c3>`__
-  whd_tko_get_status() :
   `whd_wifi_api.h <group__wifiutilities.html#gae6b46bcc1a3c7d708abc71ae6a123481>`__
-  whd_tko_max_assoc() :
   `whd_wifi_api.h <group__wifiutilities.html#ga70cacca6b62ca658882d9070c49b6343>`__
-  whd_tko_param() :
   `whd_wifi_api.h <group__wifiutilities.html#ga482c8ffcb1166f2f161e4ad67f8ef9d5>`__
-  whd_tko_toggle() :
   `whd_wifi_api.h <group__wifiutilities.html#ga189ed043d5ce9cf8d1a0845484a96d6b>`__
-  WHD_TRUE :
   `whd_types.h <whd__types_8h.html#a7cd94a03f2e7e6aab7217ed559c7a0aca665c5318c953dbd96c759579729c87b6>`__
-  WHD_UNFINISHED :
   `whd_types.h <whd__types_8h.html#a9a3c109ff2d06746031090b108e9540e>`__
-  WHD_UNKNOWN_INTERFACE :
   `whd_types.h <whd__types_8h.html#ad6972e6627af27af729d9441ac57dbac>`__
-  WHD_UNKNOWN_SECURITY_TYPE :
   `whd_types.h <whd__types_8h.html#a7904772fe0ca7e2de9843ee118def04c>`__
-  WHD_UNSUPPORTED :
   `whd_types.h <whd__types_8h.html#ac555243f1fc71fb3a5037d57d5f70768>`__
-  whd_usr_ioctl_get_list_t :
   `whd_types.h <whd__types_8h.html#aaf3febc3b0392a63822949dc85ed1b0c>`__
-  whd_usr_ioctl_set_list_t :
   `whd_types.h <whd__types_8h.html#a316541b19d3e8071b691a4baccbb17c8>`__
-  whd_usr_iovar_get_list_t :
   `whd_types.h <whd__types_8h.html#adc3a23bad78e0110546c9edc00456daf>`__
-  whd_usr_iovar_set_list_t :
   `whd_types.h <whd__types_8h.html#a9911a5f34d75551d1c25fa68396eafc4>`__
-  WHD_WAIT_ABORTED :
   `whd_types.h <whd__types_8h.html#aa716e92104149dfba1cd732a92ddd04f>`__
-  WHD_WEP_KEYLEN_BAD :
   `whd_types.h <whd__types_8h.html#ae33bbaaf58019659592cd22800fc20e0>`__
-  WHD_WEP_NOT_ALLOWED :
   `whd_types.h <whd__types_8h.html#af35d1a67b9c58a0dd84bc13a4fa63e67>`__
-  whd_wifi_ap_get_max_assoc() :
   `whd_wifi_api.h <group__wifisoftap.html#ga32982684d093a173a6e578856b581d29>`__
-  whd_wifi_ap_set_beacon_interval() :
   `whd_wifi_api.h <group__wifisoftap.html#gaf2758fcc1028704d801b16d77b96a345>`__
-  whd_wifi_ap_set_dtim_interval() :
   `whd_wifi_api.h <group__wifisoftap.html#ga57c57ad4d5acf499e90ff8db020fa2d9>`__
-  whd_wifi_clear_packet_filter_stats() :
   `whd_wifi_api.h <group__wifiutilities.html#gab3e9371ccaa61f1f40e0246a66db42a6>`__
-  whd_wifi_deauth_sta() :
   `whd_wifi_api.h <group__wifisoftap.html#gad9ad2649c18db8773351a5938e7930e4>`__
-  whd_wifi_deregister_event_handler() :
   `whd_events.h <whd__events_8h.html#ae415f1f3fcbbfe506aa0e656bf8debb3>`__
-  whd_wifi_disable_powersave() :
   `whd_wifi_api.h <group__wifipowersave.html#gab9af7ef8dac544f2bb8871259f85c437>`__
-  whd_wifi_enable_powersave() :
   `whd_wifi_api.h <group__wifipowersave.html#ga05cfafe07ad2c208acfdee29f7c6e626>`__
-  whd_wifi_enable_powersave_with_throughput() :
   `whd_wifi_api.h <group__wifipowersave.html#ga6152d966f454fe8e8836472b802401c3>`__
-  whd_wifi_enable_sup_set_passphrase() :
   `whd_wifi_api.h <group__wifiutilities.html#gae118219bea1b82ae76a5c1ee5f7f302c>`__
-  whd_wifi_enable_supplicant() :
   `whd_wifi_api.h <group__wifiutilities.html#gad2daa1aba4dccd3f0506950ccee42113>`__
-  whd_wifi_get_acparams() :
   `whd_wifi_api.h <group__wifiutilities.html#gafe61d474712d3ce7e15ef371d87f54e5>`__
-  whd_wifi_get_ap_client_rssi() :
   `whd_wifi_api.h <group__wifiutilities.html#gab571b303f19131fd032fba4b744dbf20>`__
-  whd_wifi_get_ap_info() :
   `whd_wifi_api.h <group__wifisoftap.html#ga95c40af4be45d119b737c0113d9a038e>`__
-  whd_wifi_get_associated_client_list() :
   `whd_wifi_api.h <group__wifisoftap.html#ga9e974ab0fcc24698d01a5abc3e2c1dbb>`__
-  whd_wifi_get_bss_info() :
   `whd_wifi_api.h <group__dbg.html#gab4a8c97085b18321d8d682b1578edf7d>`__
-  whd_wifi_get_bssid() :
   `whd_wifi_api.h <group__wifiutilities.html#ga52d3190589758d9fd36a7b27a9d08f28>`__
-  whd_wifi_get_channel() :
   `whd_wifi_api.h <group__wifiutilities.html#gab81522c25d723fbe18b90871a278ba9d>`__
-  whd_wifi_get_clm_version() :
   `whd_wifi_api.h <group__dbg.html#ga1e8a85564910052e82332a89db4d9ff5>`__
-  whd_wifi_get_ioctl_buffer() :
   `whd_wifi_api.h <group__wifiioctl.html#gab1a4c2ae9163408ae92b3f292ee2807f>`__
-  whd_wifi_get_ioctl_value() :
   `whd_wifi_api.h <group__wifiioctl.html#ga817655c1372b8c87608b204105f06937>`__
-  whd_wifi_get_iovar_buffer_with_param() :
   `whd_wifi_api.h <group__wifiioctl.html#ga2b1cdfc36df80ca4700076fd2747b34e>`__
-  whd_wifi_get_listen_interval() :
   `whd_wifi_api.h <group__wifiutilities.html#ga5a62f0854ad148dde280b4f75596a309>`__
-  whd_wifi_get_mac_address() :
   `whd_wifi_api.h <group__wifiutilities.html#ga8a634822973adc75a61a05eb2b664ef5>`__
-  whd_wifi_get_powersave_mode() :
   `whd_wifi_api.h <group__wifipowersave.html#ga3eed267d42020440a077d7649cde0567>`__
-  whd_wifi_get_rssi() :
   `whd_wifi_api.h <group__wifiutilities.html#gaf82eda2ff9979b1bc053a6d7f1f4b125>`__
-  whd_wifi_get_wifi_version() :
   `whd_wifi_api.h <group__dbg.html#ga334e8f04bdbc06da6eadf29cea997b99>`__
-  whd_wifi_init_ap() :
   `whd_wifi_api.h <group__wifisoftap.html#ga647aebc1d86708017fd11029e2e1a51c>`__
-  whd_wifi_is_ready_to_transceive() :
   `whd_wifi_api.h <group__wifiutilities.html#ga07144bbc68755481708752962538ee6f>`__
-  whd_wifi_join() :
   `whd_wifi_api.h <group__wifijoin.html#gac767814ae445f735b51686db20c780be>`__
-  whd_wifi_join_specific() :
   `whd_wifi_api.h <group__wifijoin.html#gae41c4ab0b2f3ecc433a6c4e89238b066>`__
-  whd_wifi_leave() :
   `whd_wifi_api.h <group__wifijoin.html#ga971ed7d8e459c715aa9bf3c52b563d8b>`__
-  whd_wifi_manage_custom_ie() :
   `whd_wifi_api.h <group__wifiutilities.html#gaaf7be7d8099507426f52ecff8f716543>`__
-  whd_wifi_off() :
   `whd_wifi_api.h <group__wifimanagement.html#ga422587a9b104a871233a42aceb2141d2>`__
-  whd_wifi_on() :
   `whd_wifi_api.h <group__wifimanagement.html#ga1c8bf41b593cb947266f8690d495f381>`__
-  whd_wifi_print_whd_log() :
   `whd_wifi_api.h <group__dbg.html#ga6435c76ac1eae5af952053d76d9c67ab>`__
-  whd_wifi_read_wlan_log() :
   `whd_wifi_api.h <group__dbg.html#ga7f089a4c933c9c05a61e2cfc00fcb4f8>`__
-  whd_wifi_register_multicast_address() :
   `whd_wifi_api.h <group__wifiutilities.html#gab36ff07aa1d825c0366c82ce003dd9f6>`__
-  whd_wifi_sae_password() :
   `whd_wifi_api.h <group__wifiutilities.html#ga977fe2ff7c3f6966ffc40fda0e9694a8>`__
-  whd_wifi_scan() :
   `whd_wifi_api.h <group__wifijoin.html#ga82d8c980d15acf0e6e18b56423f4e52b>`__
-  whd_wifi_scan_synch() :
   `whd_wifi_api.h <group__wifijoin.html#gaef3dc76f682c6c193df94897e9d0b71f>`__
-  whd_wifi_send_action_frame() :
   `whd_wifi_api.h <group__wifiutilities.html#ga2cf8ecd7c319660833bd30a060990dca>`__
-  whd_wifi_set_channel() :
   `whd_wifi_api.h <group__wifiutilities.html#ga168fb83b839bab61d2c9d3b1f42a6782>`__
-  whd_wifi_set_coex_config() :
   `whd_wifi_api.h <group__wifiutilities.html#ga95237c0e6182bfb176519421ac9287b4>`__
-  whd_wifi_set_down() :
   `whd_wifi_api.h <group__wifimanagement.html#ga412c2d4653a7c36329c6cf78be344336>`__
-  whd_wifi_set_event_handler() :
   `whd_events.h <group__event.html#ga4175c3ef9bb16601fabc605fadeaf8d8>`__
-  whd_wifi_set_ioctl_buffer() :
   `whd_wifi_api.h <group__wifiioctl.html#ga5d7aec7622caacf4a0642cf1da1fa647>`__
-  whd_wifi_set_ioctl_value() :
   `whd_wifi_api.h <group__wifiioctl.html#gad54ac84589a14cc97429bf138fa32639>`__
-  whd_wifi_set_listen_interval() :
   `whd_wifi_api.h <group__wifiutilities.html#gac93f69169a725f277e955eec0bf528db>`__
-  whd_wifi_set_passphrase() :
   `whd_wifi_api.h <group__wifiutilities.html#ga893ccaef6ed5d0afe95a96bbe89e8a80>`__
-  whd_wifi_set_up() :
   `whd_wifi_api.h <group__wifimanagement.html#ga925da3b1ed914d7bb661d3cb2af50680>`__
-  whd_wifi_start_ap() :
   `whd_wifi_api.h <group__wifisoftap.html#ga3c9aa99add3f6a6d13e9092bd6e1246b>`__
-  whd_wifi_stop_ap() :
   `whd_wifi_api.h <group__wifisoftap.html#ga2c6c28512678dc57dabb641c41e30d41>`__
-  whd_wifi_stop_scan() :
   `whd_wifi_api.h <group__wifijoin.html#gacf8632bb68cef4d831083a66bee7b8fa>`__
-  whd_wifi_toggle_packet_filter() :
   `whd_wifi_api.h <group__wifiutilities.html#ga35860e8c45162735f5ef3d93ed262d90>`__
-  whd_wifi_unregister_multicast_address() :
   `whd_wifi_api.h <group__wifiutilities.html#gad092a543b73e91c7090875ccb940e01f>`__
-  WHD_WLAN_ACM_NOTSUPPORTED :
   `whd_types.h <whd__types_8h.html#a0ed279e8ccaefdcb4347649c08591280>`__
-  WHD_WLAN_ASSOCIATED :
   `whd_types.h <whd__types_8h.html#afa0df2e6c420b3d8bef2baf60d2addea>`__
-  WHD_WLAN_BAD_VERSION :
   `whd_types.h <whd__types_8h.html#ad06624a14de68f3ceb4464bc2c38ab97>`__
-  WHD_WLAN_BADADDR :
   `whd_types.h <whd__types_8h.html#a1412d0482b0178a4c05c76edb14148af>`__
-  WHD_WLAN_BADARG :
   `whd_types.h <whd__types_8h.html#a8014f6266d91d9393e04bbc9a9b9080b>`__
-  WHD_WLAN_BADBAND :
   `whd_types.h <whd__types_8h.html#a3da74a46eebc41e14e7c25815c1381b1>`__
-  WHD_WLAN_BADCHAN :
   `whd_types.h <whd__types_8h.html#a18200ecab39e4cdedafa57f74be50009>`__
-  WHD_WLAN_BADKEYIDX :
   `whd_types.h <whd__types_8h.html#aa0ab7a6987319fd5e0fd5e3cd1176be2>`__
-  WHD_WLAN_BADLEN :
   `whd_types.h <whd__types_8h.html#afd54025f38ebd9a069a768c74da6d80a>`__
-  WHD_WLAN_BADOPTION :
   `whd_types.h <whd__types_8h.html#ae8f8c4c8662920789e013f15ee23f242>`__
-  WHD_WLAN_BADRATESET :
   `whd_types.h <whd__types_8h.html#a904ea4607ee1a485d849ef38118986e0>`__
-  WHD_WLAN_BADSSIDLEN :
   `whd_types.h <whd__types_8h.html#ac3d931142254845104478204c9d1f87f>`__
-  WHD_WLAN_BUFTOOLONG :
   `whd_types.h <whd__types_8h.html#a0be1d9ad73a6e99f93858cc5b62b37cc>`__
-  WHD_WLAN_BUFTOOSHORT :
   `whd_types.h <whd__types_8h.html#a8a8bbb7fc3c39acc127eb738ad8e41e1>`__
-  WHD_WLAN_BUSY :
   `whd_types.h <whd__types_8h.html#ac1e0aee10477b784b711951247bcf799>`__
-  WHD_WLAN_DISABLED :
   `whd_types.h <whd__types_8h.html#ae42dbb304386a4e16435d6fbbf1e5c55>`__
-  WHD_WLAN_EPERM :
   `whd_types.h <whd__types_8h.html#abb313ccaa4a5cb84f6d14affad96a300>`__
-  WHD_WLAN_ERROR :
   `whd_types.h <whd__types_8h.html#ac23d24b8da4c424644151d0893c83a6d>`__
-  WHD_WLAN_INVALID :
   `whd_types.h <whd__types_8h.html#a03251fa1c42118f23bfb5e8377e2ed3d>`__
-  WHD_WLAN_NOBAND :
   `whd_types.h <whd__types_8h.html#ae528496ed94368d53b938f40aa8f089b>`__
-  WHD_WLAN_NOCLK :
   `whd_types.h <whd__types_8h.html#a8a32264cc4534666f4538abf1b3279b3>`__
-  WHD_WLAN_NODEVICE :
   `whd_types.h <whd__types_8h.html#a0d1ab8d1ea6eb43f00a2a7a55a67016f>`__
-  WHD_WLAN_NOFUNCTION :
   `whd_types.h <whd__types_8h.html#a4e2fb5743b973f4d26dfa641ce7a7d16>`__
-  WHD_WLAN_NOMEM :
   `whd_types.h <whd__types_8h.html#a9ed1ef0145feb5c599c0ffe521cf7016>`__
-  WHD_WLAN_NONRESIDENT :
   `whd_types.h <whd__types_8h.html#aecc3e43dc3a59b8d6f452d6a2972c3f8>`__
-  WHD_WLAN_NORESOURCE :
   `whd_types.h <whd__types_8h.html#a7df68783c789fc1ba43a37b4b06e65f5>`__
-  WHD_WLAN_NOT_WME_ASSOCIATION :
   `whd_types.h <whd__types_8h.html#a7a72b67344e174d59d9da91884500c1a>`__
-  WHD_WLAN_NOTAP :
   `whd_types.h <whd__types_8h.html#a5e8feb3e343b95b26599bf953f8511c3>`__
-  WHD_WLAN_NOTASSOCIATED :
   `whd_types.h <whd__types_8h.html#a9b985926cd74ad21525001e31e37dc60>`__
-  WHD_WLAN_NOTBANDLOCKED :
   `whd_types.h <whd__types_8h.html#ada99a8b76a743e8f4d919b111583303d>`__
-  WHD_WLAN_NOTDOWN :
   `whd_types.h <whd__types_8h.html#a73c623ff49884d56e536136962111df4>`__
-  WHD_WLAN_NOTFOUND :
   `whd_types.h <whd__types_8h.html#ae98f0303ab1877e4e4cda0dd803d6383>`__
-  WHD_WLAN_NOTREADY :
   `whd_types.h <whd__types_8h.html#a7811e88b62f2f8b51d6818c05325fda9>`__
-  WHD_WLAN_NOTSTA :
   `whd_types.h <whd__types_8h.html#a13bd8edb5c29960413f87f584a508810>`__
-  WHD_WLAN_NOTUP :
   `whd_types.h <whd__types_8h.html#aca39c37d54a0b1e2fc06ccb347ff9a6d>`__
-  WHD_WLAN_OUTOFRANGECHAN :
   `whd_types.h <whd__types_8h.html#af790269c01083f486ba9e4e6df6bfccb>`__
-  WHD_WLAN_RADIOOFF :
   `whd_types.h <whd__types_8h.html#afb565415b3fc8dc00b2681e202ed16fa>`__
-  WHD_WLAN_RANGE :
   `whd_types.h <whd__types_8h.html#a10597a7175433ccd2c519babf24d50ce>`__
-  WHD_WLAN_RXFAIL :
   `whd_types.h <whd__types_8h.html#aaa963717ba6115b3a909f7162436c919>`__
-  WHD_WLAN_SDIO_ERROR :
   `whd_types.h <whd__types_8h.html#a634bebec2a4dba9371c7c0168cf9c081>`__
-  WHD_WLAN_TSPEC_NOTFOUND :
   `whd_types.h <whd__types_8h.html#a28b3339dce5b5c2044c5875d6902e964>`__
-  WHD_WLAN_TXFAIL :
   `whd_types.h <whd__types_8h.html#afd4b4619382d94a5e5fa9675bd92f802>`__
-  WHD_WLAN_UNFINISHED :
   `whd_types.h <whd__types_8h.html#a4fa6337820d22f0cfa922f27bbf50250>`__
-  WHD_WLAN_UNSUPPORTED :
   `whd_types.h <whd__types_8h.html#ad74bbd2e81cb23c0ecc35fefda6f4478>`__
-  WHD_WLAN_WLAN_DOWN :
   `whd_types.h <whd__types_8h.html#a2ab103bac375d60d8ae2d942b324376e>`__
-  WHD_WLAN_WME_NOT_ENABLED :
   `whd_types.h <whd__types_8h.html#a34acd1ff790f822872ed736945f683ab>`__
-  WHD_WPA_KEYLEN_BAD :
   `whd_types.h <whd__types_8h.html#a379efc2cb11128835c418e615adfcefc>`__
-  WIFI_IE_OUI_LENGTH :
   `whd_types.h <whd__types_8h.html#ae6328bba53e322f1332e7b7b2d7cc128>`__
-  wl_bss_info_t :
   `whd_types.h <whd__types_8h.html#a92c6377f96c792ee4d94bb36b7777ea6>`__
-  wl_chanspec_t :
   `whd_types.h <whd__types_8h.html#ac2f5aa33ad4da263645133854e489c76>`__
-  WL_MFP_CAPABLE :
   `whd_types.h <whd__types_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba51d019c133381c69b48678f6aee69c14>`__
-  WL_MFP_NONE :
   `whd_types.h <whd__types_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba0843901f508ccc7996257015fb07c850>`__
-  WL_MFP_REQUIRED :
   `whd_types.h <whd__types_8h.html#a06fc87d81c62e9abb8790b6e5713c55ba3ed2ab2dcadf0adaa51abb1e5af9a786>`__
-  WLAN_ENUM_OFFSET :
   `whd_types.h <whd__types_8h.html#a01539df5d3e26227c354161d9eda58f8>`__
-  WLC_E_ACTION_FRAME :
   `whd_events.h <whd__events_8h.html#a3c6a8365f82bedb7412bebde8311bee4>`__
-  WLC_E_ACTION_FRAME_COMPLETE :
   `whd_events.h <whd__events_8h.html#a5741fe8a095d2c7721fd5126b8acea56>`__
-  WLC_E_ASSOC :
   `whd_events.h <whd__events_8h.html#a8c9fd116781000929a8b09de58f6288e>`__
-  WLC_E_ASSOC_IND :
   `whd_events.h <whd__events_8h.html#adfb692b8b02e3082657cefc92f6f216d>`__
-  WLC_E_AUTH :
   `whd_events.h <whd__events_8h.html#a2e0540e8fe3f44be75575247648e1c7f>`__
-  WLC_E_DEAUTH :
   `whd_events.h <whd__events_8h.html#a04b3c3a873e93afd4a51643707a20120>`__
-  WLC_E_DEAUTH_IND :
   `whd_events.h <whd__events_8h.html#a6fbce5e7ab3f5f4cde0ef8c11ff90e05>`__
-  WLC_E_DISASSOC :
   `whd_events.h <whd__events_8h.html#a9b9413ac981ea6e20b8f0f80523235c0>`__
-  WLC_E_DISASSOC_IND :
   `whd_events.h <whd__events_8h.html#ae9c1ad1200aaa8eec7c96e4fc3438593>`__
-  WLC_E_ESCAN_RESULT :
   `whd_events.h <whd__events_8h.html#a1b04aa17aa56325d7ac03dbf6495d53b>`__
-  WLC_E_LINK :
   `whd_events.h <whd__events_8h.html#adf984d77e19b6e3a023dd27216599456>`__
-  WLC_E_NONE :
   `whd_events.h <whd__events_8h.html#a5608426af7320ce9e54a8a500fc044e9>`__
-  WLC_E_PROBREQ_MSG :
   `whd_events.h <whd__events_8h.html#a26d330cb0796c551edda9c848f1d826c>`__
-  WLC_E_PSK_SUP :
   `whd_events.h <whd__events_8h.html#a73ab299b66cc3915b300b6f9360ae4be>`__
-  WLC_E_REASSOC :
   `whd_events.h <whd__events_8h.html#a87f949345de32ebd84030a1cc9c9159f>`__
-  WLC_E_REASSOC_IND :
   `whd_events.h <whd__events_8h.html#a8ddd3a253d9fd56a5704d9f3b1dd9b8a>`__
-  WLC_E_SET_SSID :
   `whd_events.h <whd__events_8h.html#ae370e84bd9ed3b3cde25919287673298>`__
-  WLC_E_STATUS_11HQUIET :
   `whd_events.h <whd__events_8h.html#a0ff5a599549f180cca6ae50f4c404bb9>`__
-  WLC_E_STATUS_ABORT :
   `whd_events.h <whd__events_8h.html#a7efd6bc08b75e44e74cfad92084020c3>`__
-  WLC_E_STATUS_ATTEMPT :
   `whd_events.h <whd__events_8h.html#ae66d4ccc84bc98c3018b049d865a4e56>`__
-  WLC_E_STATUS_CCXFASTRM :
   `whd_events.h <whd__events_8h.html#ac2937cd5a00b77988fdeb91aa06e0542>`__
-  WLC_E_STATUS_CS_ABORT :
   `whd_events.h <whd__events_8h.html#ad7ce31ac63e0e91bca76c2f0bbc0e92c>`__
-  WLC_E_STATUS_ERROR :
   `whd_events.h <whd__events_8h.html#ab5a63329b542569285c035ec864322b8>`__
-  WLC_E_STATUS_FAIL :
   `whd_events.h <whd__events_8h.html#a7bbe432d919eb9e29cca5bbeff6f2d22>`__
-  WLC_E_STATUS_INVALID :
   `whd_events.h <whd__events_8h.html#ac597fcbff37082363669370113a7628b>`__
-  WLC_E_STATUS_NEWASSOC :
   `whd_events.h <whd__events_8h.html#ac60199a213be6784ece10816f189e6df>`__
-  WLC_E_STATUS_NEWSCAN :
   `whd_events.h <whd__events_8h.html#ace9d1ee71e3e6a1fd0e5c816347cef85>`__
-  WLC_E_STATUS_NO_ACK :
   `whd_events.h <whd__events_8h.html#a88664d38d341c3cc57dfd901f6bb6010>`__
-  WLC_E_STATUS_NO_NETWORKS :
   `whd_events.h <whd__events_8h.html#a3ba3ab746af65eea3d54945c491e75b5>`__
-  WLC_E_STATUS_NOCHANS :
   `whd_events.h <whd__events_8h.html#a8f01683977e56ff6cf81c9dbef1cb1b5>`__
-  WLC_E_STATUS_PARTIAL :
   `whd_events.h <whd__events_8h.html#aec78b245d5a001d341e370f9a246542a>`__
-  WLC_E_STATUS_SUCCESS :
   `whd_events.h <whd__events_8h.html#a36a1db14fab1a7d56c5d9c2a57cf030f>`__
-  WLC_E_STATUS_SUPPRESS :
   `whd_events.h <whd__events_8h.html#a95517d8b374126d53040e871e0430299>`__
-  WLC_E_STATUS_TIMEOUT :
   `whd_events.h <whd__events_8h.html#a59959134916adbb98a9875a8c7462474>`__
-  WLC_E_STATUS_UNSOLICITED :
   `whd_events.h <whd__events_8h.html#a5a7e4c4f047fff0443e0e7037e35babe>`__
-  WLC_SUP_AUTHENTICATED :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4caa4fb668202b9e57bbfaedf0b20304a6e>`__
-  WLC_SUP_AUTHENTICATING :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4cab0fea5debce5e8abf993c6a4e6aa2cce>`__
-  WLC_SUP_CONNECTING :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca8ac4d7e7fb55b33bb65316c9ba42f147>`__
-  WLC_SUP_DISCONNECTED :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4cac9bc0eb3bd21bc5189a4a46793ea7680>`__
-  WLC_SUP_IDREQUIRED :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca6d0a906929483eb4d9633c875cffd6ce>`__
-  WLC_SUP_KEYED :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4cab04882e234f679807f486fcd34ee9a1a>`__
-  WLC_SUP_KEYXCHANGE :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca47709bf89f9482d964fb8f80110b9938>`__
-  WLC_SUP_KEYXCHANGE_PREP_G2 :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4caa1a479ce249334165efc2abb7a417303>`__
-  WLC_SUP_KEYXCHANGE_PREP_M2 :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca82ffd4e7e1eda9cde4a2e4f93a625623>`__
-  WLC_SUP_KEYXCHANGE_PREP_M4 :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4caf6103c24206afe80f6f3918a5f139772>`__
-  WLC_SUP_KEYXCHANGE_WAIT_G1 :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca8f648a422bf489e97c275b0e81b4e69a>`__
-  WLC_SUP_KEYXCHANGE_WAIT_M1 :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca05d3731c95722d8e0499673a12697abe>`__
-  WLC_SUP_KEYXCHANGE_WAIT_M3 :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4caefe14a3fa955320276622a8c2a3df3f6>`__
-  WLC_SUP_LAST_BASIC_STATE :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca949fb8270a4429fe0bdd53eb654d10df>`__
-  WLC_SUP_STATUS_OFFSET :
   `whd_events.h <whd__events_8h.html#ac97c29fe35bab5af9cc08a9eb1fedffc>`__
-  WLC_SUP_TIMEOUT :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4ca9e0cfe079f25369646bb199aba3f1240>`__
-  WPA2_SECURITY :
   `whd_types.h <whd__types_8h.html#a8875737a0403d2136a69bbc96401cccf>`__
-  WPA3_SECURITY :
   `whd_types.h <whd__types_8h.html#ad175824d1581f69f5a725e4c9171aa79>`__
-  WPA_SECURITY :
   `whd_types.h <whd__types_8h.html#aa7ccd472bacbd8ee01f31f0f0e2ce3dc>`__
-  WPS_ENABLED :
   `whd_types.h <whd__types_8h.html#aaae7e8a0eb357cae8f130f9099d8e7b8>`__

