=============
Enumerations
=============

-  sup_auth_status :
   `whd_events.h <whd__events_8h.html#aba9543ff6a1542d81c7adc66bb339c4c>`__
-  whd_802_11_band_t :
   `whd_types.h <whd__types_8h.html#aa57cbd1826f7022123be8ec0ac352d85>`__
-  whd_bool_t :
   `whd_types.h <whd__types_8h.html#a7cd94a03f2e7e6aab7217ed559c7a0ac>`__
-  whd_bss_type_t :
   `whd_types.h <whd__types_8h.html#a3070614a06a8989b9eb207e9f1286c5f>`__
-  whd_buffer_dir_t :
   `whd_network_types.h <group__buffif.html#ga44a64c51498b204ceef5555209e29452>`__
-  whd_bus_transfer_direction_t :
   `whd_types.h <whd__types_8h.html#afc8e8073d434bf124933526a8184313a>`__
-  whd_country_code_t :
   `whd_types.h <whd__types_8h.html#a5adb0953a8527552bc47001673830602>`__
-  whd_custom_ie_action_t :
   `whd_types.h <whd__types_8h.html#a8be1026494a86f0ceeebb2dcbf092cbd>`__
-  whd_dot11_reason_code_t :
   `whd_types.h <whd__types_8h.html#ac81b31559ee1db82f01e8acfb8eea55d>`__
-  whd_ie_packet_flag_t :
   `whd_types.h <whd__types_8h.html#a07b4f24f4e2abacc45a9ad29f008488f>`__
-  whd_listen_interval_time_unit_t :
   `whd_types.h <whd__types_8h.html#a98ac1a2590358bafac6a2da287034638>`__
-  whd_packet_filter_rule_t :
   `whd_types.h <whd__types_8h.html#a36b806b9b0e986bf290ae005d1bae038>`__
-  whd_resource_type_t :
   `whd_resource_api.h <whd__resource__api_8h.html#a26c678527cf0cb5196be511d9ad62c0c>`__
-  whd_scan_result_flag_t :
   `whd_types.h <whd__types_8h.html#a4bb9a3034dfb9a44507ef6201842edb7>`__
-  whd_scan_status_t :
   `whd_types.h <whd__types_8h.html#a34d5a5749a0bcc00b7249108a8670ada>`__
-  whd_scan_type_t :
   `whd_types.h <whd__types_8h.html#af7ac1b0be4ce67f3e82b876c2f27fd3a>`__
-  whd_security_t :
   `whd_types.h <whd__types_8h.html#aaeeea5666743710aa4f01ff264b27059>`__
-  whd_usr_ioctl_get_list_t :
   `whd_types.h <whd__types_8h.html#aaf3febc3b0392a63822949dc85ed1b0c>`__
-  whd_usr_ioctl_set_list_t :
   `whd_types.h <whd__types_8h.html#a316541b19d3e8071b691a4baccbb17c8>`__
-  whd_usr_iovar_get_list_t :
   `whd_types.h <whd__types_8h.html#adc3a23bad78e0110546c9edc00456daf>`__
-  whd_usr_iovar_set_list_t :
   `whd_types.h <whd__types_8h.html#a9911a5f34d75551d1c25fa68396eafc4>`__

