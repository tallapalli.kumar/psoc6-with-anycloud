API Reference
==============

         The following provides a list of driver API documentation

      

         +----------------------------------+----------------------------------+
         | `Common middleware               |                                  |
         | utiliti\                         |                                  |
         | es <group__group__utils.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Middleware utilities            | Cypress middleware APIs return   |
         | enumerated                       | results of type cy_rslt_t and    |
         | types <gro                       | comprise of three parts:         |
         | up__group__utils__enums.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `generic middleware              |                                  |
         | results/error                    |                                  |
         | codes <grou                      |                                  |
         | p__generic__mw__defines.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `TCP/IP socket results/error     |                                  |
         | codes <gr                        |                                  |
         | oup__tcpip__mw__defines.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `TLS results/error               |                                  |
         | codes <g                         |                                  |
         | roup__tls__mw__defines.html>`__  |                                  |
         +----------------------------------+----------------------------------+
         | `Logging                         |                                  |
         | utilities <g                     |                                  |
         | roup__logging__utils.html>`__    |                                  |
         +----------------------------------+----------------------------------+
         | `Logging enumerated              |                                  |
         | types <group                     |                                  |
         | __group__logging__enums.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Logging data                    |                                  |
         | structures <group__gro           |                                  |
         | up__logging__structures.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Logging                         | A logging subsystem that allows  |
         | functions <grou                  | run time control for the logging |
         | p__group__logging__func.html>`__ | level                            |
         +----------------------------------+----------------------------------+
         | `JSON parser                     |                                  |
         | utilit\                          |                                  |
         | ies <group__json__utils.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `JSON parser enumerated          |                                  |
         | types <gr                        |                                  |
         | oup__group__json__enums.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `JSON parser data                |                                  |
         | structures <group__              |                                  |
         | group__json__structures.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `JSON parser                     | The JSON parser utility library  |
         | functions <g                     | provides helper functions to     |
         | roup__group__json__func.html>`__ | parse JSON objects               |
         +----------------------------------+----------------------------------+
         | `Linked list                     |                                  |
         | utilities <g                     |                                  |
         | roup__linkedlist__utils.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Linked list data                |                                  |
         | structures <group__group_        |                                  |
         | _linkedlist__structures.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Linked list                     | This is a generic linked list    |
         | functions <group__               | library with helper functions to |
         | group__linkedlist__func.html>`__ | add, insert, delete and find     |
         |                                  | nodes in a list                  |
         +----------------------------------+----------------------------------+
         | `Network helper                  |                                  |
         | utilities                        |                                  |
         | <group__nwhelper__utils.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Network helper enumerated       |                                  |
         | types <group_                    |                                  |
         | _group__nwhelper__enums.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Network helper data             |                                  |
         | structures <group__grou          |                                  |
         | p__nwhelper__structures.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Network helper                  | This is a collection of network  |
         | functions <group                 | helper functions which would be  |
         | __group__nwhelper__func.html>`__ | used by various Cypress          |
         |                                  | Middleware libraries             |
         +----------------------------------+----------------------------------+
         | `String conversion               |                                  |
         | utilitie\                        |                                  |
         | s <group__string__utils.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `String conversion               | The string utilities module is a |
         | functions <gro                   | collection of string conversion  |
         | up__group__string__func.html>`__ | helpers to convert between       |
         |                                  | integer and strings              |
         +----------------------------------+----------------------------------+

.. toctree::
   :hidden:

   group__group__utils.rst
  
