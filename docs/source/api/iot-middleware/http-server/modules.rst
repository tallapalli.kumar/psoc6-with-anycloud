===============
API Reference
===============

The following provides a list of API documentation. This library
provides both C and C++ APIs. C++ library APIs are typically used in
Mbed OS framework and C APIs are used in AnyCloud framework. Refer the
below sections for details.

+-----------------------------------+-----------------------------------+
| `C API <g                         |                                   |
| roup__group__c__api.html>`__      |                                   |
+-----------------------------------+-----------------------------------+
| `Structures &                     | HTTP server data structures and   |
| Enumerations <group__http__server | type definitions                  |
| __struct.html>`__                 |                                   |
+-----------------------------------+-----------------------------------+
| `Macros <group__http__server__    | HTTP server preprocessor          |
| defines.html>`__                  | directives such as results and    |
|                                   | error codes                       |
+-----------------------------------+-----------------------------------+
| `Functions <group__group__c__a    | C API provided by HTTP server     |
| pi__functions.html>`__            | library                           |
+-----------------------------------+-----------------------------------+
| `C++ Class                        | This section contains details     |
| Interface <group__group__api__cpp | about C++ base class interface    |
| .html>`__                         | provided by the library           |
+-----------------------------------+-----------------------------------+

.. toctree::
   :hidden:

   group__group__c__api.rst
   group__group__api__cpp.rst




