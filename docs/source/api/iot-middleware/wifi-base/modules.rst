===============
API Reference
===============

The following provides a list of API documentation

+-----------------------------------+-----------------------------------+
| `lwIP and WHD                     | Functions for dealing with        |
| port <group__group__lwip__whd__po | linking the lwIP TCP/IP stack     |
| rt.html>`__                       | with WiFi Host Driver             |
+-----------------------------------+-----------------------------------+
| `Enumerated                       |                                   |
| Types <group__group__lwip__whd__e |                                   |
| nums.html>`__                     |                                   |
+-----------------------------------+-----------------------------------+
| `Functions <group__group__lwip    |                                   |
| __whd__port__functions.html>`__   |                                   |
+-----------------------------------+-----------------------------------+
| `Structures <group__group__lwi    |                                   |
| p__whd__port__structures.html>`__ |                                   |
+-----------------------------------+-----------------------------------+
| `CY generic lwIP WHD glue         | WiFi middleware core APIs return  |
| results/error                     | results of type cy_rslt_t and     |
| codes <group__generic__lwip__whd_ | comprise of three parts:          |
| _port__defines.html>`__           |                                   |
+-----------------------------------+-----------------------------------+
| `Optimization of Wi-Fi            | For PSoC 6 MCU family of chips    |
| Middleware                        | and Wi-Fi/BT connectivity chips   |
| Core <group__group__optimization_ | that have limited flash and RAM   |
| _config.html>`__                  | footprint, there is a need to     |
|                                   | optimize the various connectivity |
|                                   | middleware components to fit into |
|                                   | kits that use these chips         |
+-----------------------------------+-----------------------------------+

.. toctree::
   :hidden:

   group__group__lwip__whd__port.rst
   group__generic__lwip__whd__port__defines.rst
   group__group__optimization__config.rst




