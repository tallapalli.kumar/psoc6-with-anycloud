AWS IoT Device SDK
===================

.. doxygengroup:: mqtt_aws_iot_device_sdk_function
   :project: MQTT
   :members:
   :protected-members:
   :private-members:
   :undoc-members: