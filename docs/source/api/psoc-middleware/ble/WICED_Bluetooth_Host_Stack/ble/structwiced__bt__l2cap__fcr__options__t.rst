===================================
wiced_bt_l2cap_fcr_options_t Struct
===================================

.. doxygenstruct:: wiced_bt_l2cap_fcr_options_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: