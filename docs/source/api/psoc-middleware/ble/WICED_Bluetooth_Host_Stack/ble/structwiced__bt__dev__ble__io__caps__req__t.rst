=============================================
wiced_bt_dev_ble_io_caps_req_t struct
=============================================

.. doxygenstruct:: wiced_bt_dev_ble_io_caps_req_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
