=====================
GATT Database
=====================

.. doxygengroup:: gattdb_api_functions
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: