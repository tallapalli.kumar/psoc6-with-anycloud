==========================================
wiced_bt_set_adv_tx_power_result_t Struct
==========================================

.. doxygenstruct:: wiced_bt_set_adv_tx_power_result_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: