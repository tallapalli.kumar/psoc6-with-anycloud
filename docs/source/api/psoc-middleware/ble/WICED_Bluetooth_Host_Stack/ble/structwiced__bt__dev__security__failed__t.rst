==========================================
wiced_bt_dev_security_failed_t struct
==========================================

.. doxygenstruct:: wiced_bt_dev_security_failed_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: