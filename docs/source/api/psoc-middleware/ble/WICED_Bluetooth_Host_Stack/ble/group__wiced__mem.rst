==================
Memory Management
==================

.. doxygengroup:: wiced_mem
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: