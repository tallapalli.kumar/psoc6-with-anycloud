=============================================
wiced_bt_device_edr_packet_types_t struct
=============================================

.. doxygenstruct:: wiced_bt_device_edr_packet_types_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
