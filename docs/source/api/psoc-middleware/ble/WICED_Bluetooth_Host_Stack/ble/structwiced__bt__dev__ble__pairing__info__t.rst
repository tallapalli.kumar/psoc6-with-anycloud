==========================================
wiced_bt_dev_ble_pairing_info_t Struct
==========================================

.. doxygenstruct:: wiced_bt_dev_ble_pairing_info_t
   :project: WICED_Bluetooth_Host_Stack-ble
   :members:
   :protected-members:
   :private-members:
   :undoc-members: