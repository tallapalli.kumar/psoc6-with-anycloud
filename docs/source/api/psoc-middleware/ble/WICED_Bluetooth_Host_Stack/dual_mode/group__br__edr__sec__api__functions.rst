=========================
BR/EDR Security Function
=========================

.. doxygengroup:: br_edr_sec_api_functions
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
 