===============
Connection API
===============

.. raw:: html

   <h3 class='simpleheader'>API Reference</h3>
   <hr style="margin-top:0px;border-top: 1px solid #404040 !important;">


.. toctree::
   :titlesonly:
   
   group__gatt__le.rst
   group__gatt__br.rst
   group__gatt__eatt__functions.rst
   
   
.. doxygengroup:: gatt_common_api
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
