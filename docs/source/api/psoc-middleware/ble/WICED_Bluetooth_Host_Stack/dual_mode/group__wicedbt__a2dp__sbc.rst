=====================
A2DP SBC Support
=====================

.. doxygengroup:: wicedbt_a2dp_sbc
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members: