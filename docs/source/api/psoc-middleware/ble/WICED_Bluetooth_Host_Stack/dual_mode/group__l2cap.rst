====================================================
Logical Link Control and Adaptation Protocol (L2CAP)
====================================================


.. raw:: html

   <h3 class='simpleheader'>API Reference</h3>
   <hr style="margin-top:0px;border-top: 1px solid #404040 !important;">


.. toctree::
   :titlesonly:
   
   group__l2cap__common__api__functions.rst
   group__l2cap__br__edr__api__functions.rst
   group__l2cap__le__api__functions.rst
   
   
.. doxygengroup:: l2cap
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
