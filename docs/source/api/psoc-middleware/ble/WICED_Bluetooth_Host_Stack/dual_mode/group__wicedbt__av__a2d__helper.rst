=================
Helper Functions
=================

.. raw:: html

   <h3 class='simpleheader'>API Reference</h3>
   <hr style="margin-top:0px;border-top: 1px solid #404040 !important;">


.. toctree::
   :titlesonly:
   
   group__wicedbt__a2dp__mpeg__1__2.rst
   group__wicedbt__a2dp__mpeg__2__4.rst
   group__wicedbt__a2dp__sbc.rst
   
   
.. doxygengroup:: wicedbt_av_a2d_helper
   :project: WICED_Bluetooth_Host_Stack-dual_mode
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
