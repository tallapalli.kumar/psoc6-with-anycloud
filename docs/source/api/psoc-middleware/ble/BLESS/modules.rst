==================
API Reference
==================

         Provides the list of API Reference

         +----------------------------------+----------------------------------+
         | `BLE Common                      | The common API act as a general  |
         | API <group__                     | interface between the BLE        |
         | group__ble__common__api.html>`__ | application and the BLE Stack    |
         |                                  | module                           |
         +----------------------------------+----------------------------------+
         | `BLE Common Core                 | The common core API are used for |
         | Functions <group__group__ble_    | general BLE configuration        |
         | _common__api__functions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Whitelist                       | The API are used for enable user |
         | API <g                           | to use Whitelist feature of BLE  |
         | roup__group__ble__common___wh    | Stack                            |
         | itelist__api__functions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Link Layer Privacy              | The API are used for enable user |
         | A                                | to use Link Layer Privacy        |
         | PI <group__group__ble__common___ | feature of BLE Stack             |
         | privacy__api__functions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Data Length Extension (DLE)     | The API are used for enable user |
         | API <group__group                | to use Data Length Extension     |
         | __ble__common___data__length__ex | (DLE) feature of BLE Stack       |
         | tension__api__functions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `2Mbps Feature                   | The API are used for enable user |
         | AP                               | to use 2Mbps feature of BLE      |
         | I <group__group__ble__common__2_ | Stack                            |
         | m_b_p_s__api__functions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `LE Ping                         | The API are used for enable user |
         | API <g                           | to use LE Ping feature of BLE    |
         | roup__group__ble__common___l_    | Stack                            |
         | e__ping__api__functions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `AES Engine                      | BLE sub system AES Engine is     |
         | API                              | exposed through this API         |
         | <group__group__ble__common___enc |                                  |
         | ryption__api__functions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BLE HCI                         | API exposes BLE Stack's Host HCI |
         | API <group__group__ble__common_  | to user, if they want to do DTM  |
         | __h_c_i__api__functions.html>`__ | testing or use BLE Controller    |
         |                                  | alone                            |
         +----------------------------------+----------------------------------+
         | `BLE Interrupt Notification      | API exposes BLE interrupt        |
         | Callback <gr                     | notifications to the application |
         | oup__group__ble__common___intr__ | which indicates a different link |
         | feature__api__functions.html>`__ | layer and radio state            |
         |                                  | transitions to the user from the |
         |                                  | BLESS interrupt context          |
         +----------------------------------+----------------------------------+
         | `GAP                             | The GAP APIs allow access to the |
         | Functions <g                     | Generic Access Profile (GAP)     |
         | roup__group__ble__common__api__  | layer of the BLE Stack           |
         | gap__functions__section.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `GAP Central and Peripheral      | These are APIs common to both    |
         | Fu\                              | GAP Central role and GAP         |
         | nctions <group__group__ble__comm | Peripheral role                  |
         | on__api__gap__functions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `GAP Central                     | APIs unique to designs           |
         | Functions <g                     | configured as a GAP Central role |
         | roup__group__ble__common__api__  |                                  |
         | gap__central__functions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `GAP Peripheral                  | APIs unique to designs           |
         | Functions <gro                   | configured as a GAP Peripheral   |
         | up__group__ble__common__api__gap | role                             |
         | __peripheral__functions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `GATT                            | The GATT APIs allow access to    |
         | Functions <g                     | the Generic Attribute Profile    |
         | roup__group__ble__common__api__g | (GATT) layer of the BLE Stack    |
         | att__functions__section.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `GATT Client and Server          | These are APIs common to both    |
         | Fun\                             | GATT Client role and GATT Server |
         | ctions <group__group__ble__commo | role                             |
         | n__api__gatt__functions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `GATT Client                     | APIs unique to designs           |
         | Functions <g                     | configured as a GATT Client role |
         | roup__group__ble__common__api__  |                                  |
         | gatt__client__functions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `GATT Server                     | APIs unique to designs           |
         | Functions <g                     | configured as a GATT Server role |
         | roup__group__ble__common__api__  |                                  |
         | gatt__server__functions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `L2CAP                           | The L2CAP APIs allow access to   |
         | Func\                            | the Logical link control and     |
         | tions <group__group__ble__common | adaptation protocol (L2CAP)      |
         | __api__l2cap__functions.html>`__ | layer of the BLE Stack           |
         +----------------------------------+----------------------------------+
         | `BLE Common                      | The BLE Stack generates events   |
         | Events <group__group__b          | to notify the application on     |
         | le__common__api__events.html>`__ | various status alerts concerning |
         |                                  | the stack                        |
         +----------------------------------+----------------------------------+
         | `BLE Common Definitions and      | Contains definitions and         |
         | Data                             | structures that are common to    |
         | Structure\                       | all BLE common API               |
         | s <group__group__ble__common__ap |                                  |
         | i__definitions__section.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BLE Error                       | Contains definitions for all the |
         | Code <group__group__ble          | spec defined error code in Core  |
         | __common__macros__error.html>`__ | Spec 5.0, Vol2, Part D           |
         +----------------------------------+----------------------------------+
         | `Macros <group__group__ble__commo|                                  |
         | n__api__macros__section.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Common <group__group__b         | BLE Common macros                |
         | le__common__api__macros.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BLE GATT                        | BLE GATT Database macros         |
         | Dat\                             |                                  |
         | abase <group__group__ble__common |                                  |
         | __api__macros__gatt__db.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BLE Services                    | BLE Services Universal Unique    |
         | UUID <group                      | Identifier (UUID) macros         |
         | __group__ble__common__api__macro |                                  |
         | s__gatt__uuid__services.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BLE GATT Attribute Types        | BLE GATT Attribute Types defined |
         | UUID <group__group_              | by GATT Profile UUID macros      |
         | _ble__common__api__macros__gatt_ |                                  |
         | _uuid__char__gatt__type.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BLE GATT Characteristic         | BLE GATT Attribute Types defined |
         | Descriptors                      | by GATT Profile UUID macros      |
         | UUID <group__                    |                                  |
         | group__ble__common__api__macros_ |                                  |
         | _gatt__uuid__char__desc.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BLE GATT Characteristic         | BLE GATT Characteristic Types    |
         | Types                            | UUID macros                      |
         | UUID <group__                    |                                  |
         | group__ble__common__api__macros_ |                                  |
         | _gatt__uuid__char__type.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BLE Appearance                  | BLE Appearance values macros     |
         | values <gro                      |                                  |
         | up__group__ble__common__api__mac |                                  |
         | ros__appearance__values.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Data                            |                                  |
         | Structures <g                    |                                  |
         | roup__group__ble__common__api    |                                  |
         | __data__struct__section.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Common <group__group__ble__c    | Contains the common definitions  |
         | ommon__api__definitions.html>`__ | and data structures used in the  |
         |                                  | BLE                              |
         +----------------------------------+----------------------------------+
         | `GAP <group__group__ble__common  | Contains the GAP specific        |
         | __api__gap__definitions.html>`__ | definitions and data structures  |
         |                                  | used in the GAP APIs             |
         +----------------------------------+----------------------------------+
         | `GATT <group__group__ble__common_| Contains the GATT specific       |
         | _api__gatt__definitions.html>`__ | definitions and data structures  |
         |                                  | used in the GATT APIs            |
         +----------------------------------+----------------------------------+
         | `L2                              | Contains the L2CAP specific      |
         | CAP <group__group__ble__common__ | definitions and data structures  |
         | api__l2cap__definitions.html>`__ | used in the L2CAP APIs           |
         +----------------------------------+----------------------------------+
         | `Global                          |                                  |
         | Varia                            |                                  |
         | bles <group__group__ble__common_ |                                  |
         | _api__global__variables.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BLE Service-Specific            | This section describes BLE       |
         | API <group__g                    | Service-specific API             |
         | roup__ble__service__api.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BLE Service-Specific            | The BLE Stack generates          |
         | Events <group__group__bl         | service-specific events to       |
         | e__service__api__events.html>`__ | notify the application that a    |
         |                                  | service-specific status change   |
         |                                  | needs attention                  |
         +----------------------------------+----------------------------------+
         | `Apple Notification Center       | The Apple Notification Center    |
         | Service                          | Service provides iOS             |
         | (ANCS) <group__group__ble_       | notifications from Apple devices |
         | _service__api___a_n_c_s.html>`__ | for accessories                  |
         +----------------------------------+----------------------------------+
         | `ANCS Server and Client          | These are API common to both     |
         | Function <gr                     | GATT Client role and GATT Server |
         | oup__group__ble__service__api___ | role                             |
         | a_n_c_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `ANCS Server                     | API unique to ANCS designs       |
         | Funct\                           | configured as a GATT Server role |
         | ions <group__group__ble__service |                                  |
         | __api___a_n_c_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `ANCS Client                     | API unique to ANCS designs       |
         | Funct\                           | configured as a GATT Client role |
         | ions <group__group__ble__service |                                  |
         | __api___a_n_c_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `ANCS Definitions and Data       | Contains the ANCS specific       |
         | Structures <g                    | definitions and data structures  |
         | roup__group__ble__service__api   | used in the ANCS API             |
         | ___a_n_c_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Alert Notification Service      | The Alert Notification Service   |
         | (ANS) <group__group__bl          | exposes alert information in a   |
         | e__service__api___a_n_s.html>`__ | device                           |
         +----------------------------------+----------------------------------+
         | `ANS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __a_n_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `ANS Server                      | API unique to ANS designs        |
         | Functions <group__               | configured as a GATT Server role |
         | group__ble__servi                |                                  |
         | ce__api___a_n_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `ANS Client                      | API unique to ANS designs        |
         | Functions <group__               | configured as a GATT Client role |
         | group__ble__servi                |                                  |
         | ce__api___a_n_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `ANS Definitions and Data        | Contains the ANS specific        |
         | Structures                       | definitions and data structures  |
         | <group__group__ble__service__a   | used in the ANS API              |
         | pi___a_n_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Automation IO Service           | The Automation IO Service        |
         | (AIOS) <group__group__ble_       | enables a device to connect and  |
         | _service__api___a_i_o_s.html>`__ | interact with an Automation IO   |
         |                                  | Module (IOM) in order to access  |
         |                                  | digital and analog signals       |
         +----------------------------------+----------------------------------+
         | `AIOS Server and Client          | These are API common to both     |
         | Function <gr                     | GATT Client role and GATT Server |
         | oup__group__ble__service__api___ | role                             |
         | a_i_o_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `AIOS Server                     | API unique to AIOS designs       |
         | Functions <group__               | configured as a GATT Server role |
         | group__ble__service              |                                  |
         | __api___a_i_o_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `AIOS Client                     | API unique to AIOS designs       |
         | Functions <group__               | configured as a GATT Client role |
         | group__ble__service              |                                  |
         | __api___a_i_o_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `AIOS Definitions and Data       | Contains the AIOS specific       |
         | Structures                       | definitions and data structures  |
         | <group__group__ble__service__api | used in the AIOS API             |
         | ___a_i_o_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Battery Service                 | The Battery Service exposes the  |
         | (BAS) <group__group__bl          | battery level of a single        |
         | e__service__api___b_a_s.html>`__ | battery or set of batteries in a |
         |                                  | device                           |
         +----------------------------------+----------------------------------+
         | `BAS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __b_a_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BAS Server                      | API unique to BAS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___b_a_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BAS Client                      | API unique to BAS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___b_a_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BAS Definitions and Data        | Contains the BAS specific        |
         | Structures <group__              | definitions and data structures  |
         | group__ble__service__a           | used in the BAS API              |
         | pi___b_a_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Body Composition Service        | The Body Composition Service     |
         | (BCS) <group__group__bl          | exposes data related to body     |
         | e__service__api___b_c_s.html>`__ | composition from a body          |
         |                                  | composition analyzer (Server)    |
         |                                  | intended for consumer healthcare |
         |                                  | as well as sports/fitness        |
         |                                  | applications                     |
         +----------------------------------+----------------------------------+
         | `BCS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __b_c_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BCS Server                      | API unique to BCS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___b_c_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BCS Client                      | API unique to BCS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___b_c_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BCS Definitions and Data        | Contains the BCS specific        |
         | Structures <group__              | definitions and data structures  |
         | group__ble__service__a           | used in the BCS API              |
         | pi___b_c_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Blood Pressure Service          | The Blood Pressure Service       |
         | (BLS) <group__group__bl          | exposes blood pressure and other |
         | e__service__api___b_l_s.html>`__ | data related to a non-invasive   |
         |                                  | blood pressure monitor for       |
         |                                  | consumer and professional        |
         |                                  | healthcare applications          |
         +----------------------------------+----------------------------------+
         | `BLS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __b_l_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BLS Server                      | API unique to BLS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___b_l_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BLS Client                      | API unique to BLS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___b_l_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BLS Definitions and Data        | Contains the BLS specific        |
         | Structures <group__              | definitions and data structures  |
         | group__ble__service__a           | used in the BLS API              |
         | pi___b_l_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Bond Management Service         | The Bond Management Service      |
         | (BMS) <group__group__bl          | defines how a peer Bluetooth     |
         | e__service__api___b_m_s.html>`__ | device can manage the storage of |
         |                                  | bond information, especially the |
         |                                  | deletion of it, on the Bluetooth |
         |                                  | device supporting this service   |
         +----------------------------------+----------------------------------+
         | `BMS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __b_m_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BMS Server                      | API unique to BMS designs        |
         | Functions <group__               | configured as a GATT Server role |
         | group__ble__servi                |                                  |
         | ce__api___b_m_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BMS Client                      | API unique to BMS designs        |
         | Functions <group__               | configured as a GATT Client role |
         | group__ble__servi                |                                  |
         | ce__api___b_m_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `BMS Definitions and Data        | Contains the BMS specific        |
         | Structures <group__              | definitions and data structures  |
         | group__ble__service__a           | used in the BMS API              |
         | pi___b_m_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Continuous Glucose              | The Continuous Glucose           |
         | Monitoring Service               | Monitoring Service exposes       |
         | (CGMS) <group__group__ble_       | glucose measurement and other    |
         | _service__api___c_g_m_s.html>`__ | data related to a personal CGM   |
         |                                  | sensor for healthcare            |
         |                                  | applications                     |
         +----------------------------------+----------------------------------+
         | `CGMS Server and Client          | These are API common to both     |
         | Function <gr                     | GATT Client role and GATT Server |
         | oup__group__ble__service__api___ | role                             |
         | c_g_m_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `CGMS Server                     | API unique to CGMS designs       |
         | Functions <group__               | configured as a GATT Server role |
         | group__ble__service              |                                  |
         | __api___c_g_m_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `CGMS Client                     | API unique to CGMS designs       |
         | Functions <group__               | configured as a GATT Client role |
         | group__ble__service              |                                  |
         | __api___c_g_m_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `CGMS Definitions and Data       | Contains the CGMS specific       |
         | Structures                       | definitions and data structures  |
         | <group__group__ble__service__api | used in the CGMS API             |
         | ___c_g_m_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Cycling Power Service           | The Cycling Power Service (CPS)  |
         | (CPS) <group__group__bl          | exposes power- and force-related |
         | e__service__api___c_p_s.html>`__ | data and optionally speed- and   |
         |                                  | cadence-related data from a      |
         |                                  | Cycling Power sensor (GATT       |
         |                                  | Server) intended for sports and  |
         |                                  | fitness applications             |
         +----------------------------------+----------------------------------+
         | `CPS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __c_p_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `CPS Server                      | API unique to CPS designs        |
         | Functions <group__               | configured as a GATT Server role |
         | group__ble__servi                |                                  |
         | ce__api___c_p_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `CPS Client                      | API unique to CPS designs        |
         | Functions <group__               | configured as a GATT Client role |
         | group__ble__servi                |                                  |
         | ce__api___c_p_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `CPS Definitions and Data        | Contains the CPS specific        |
         | Structures <group__              | definitions and data structures  |
         | group__ble__service__a           | used in the CPS API              |
         | pi___c_p_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Cycling Speed and Cadence       | The Cycling Speed and Cadence    |
         | Service                          | (CSC) Service exposes            |
         | (CSCS) <group__group__ble_       | speed-related data and/or        |
         | _service__api___c_s_c_s.html>`__ | cadence-related data while using |
         |                                  | the Cycling Speed and Cadence    |
         |                                  | sensor (Server)                  |
         +----------------------------------+----------------------------------+
         | `CSCS Server and Client          | These are API common to both     |
         | Function <gr                     | GATT Client role and GATT Server |
         | oup__group__ble__service__api___ | role                             |
         | c_s_c_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `CSCS Server                     | API unique to CSCS designs       |
         | Functions <group__               | configured as a GATT Server role |
         | group__ble__service              |                                  |
         | __api___c_s_c_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `CSCS Client                     | API unique to CSCS designs       |
         | Functions <group__               | configured as a GATT Client role |
         | group__ble__service              |                                  |
         | __api___c_s_c_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `CSCS Definitions and Data       | Contains the CSCS specific       |
         | Structures                       | definitions and data structures  |
         | <group__group__ble__service__api | used in the CSCS API             |
         | ___c_s_c_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Current Time Service            | The Current Time Service defines |
         | (CTS) <group__group__bl          | how a Bluetooth device can       |
         | e__service__api___c_t_s.html>`__ | expose time information to other |
         |                                  | Bluetooth devices                |
         +----------------------------------+----------------------------------+
         | `CTS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __c_t_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `CTS Server                      | API unique to CTS designs        |
         | Functions <group__               | configured as a GATT Server role |
         | group__ble__servi                |                                  |
         | ce__api___c_t_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `CTS Client                      | API unique to CTS designs        |
         | Functions <group__               | configured as a GATT Client role |
         | group__ble__servi                |                                  |
         | ce__api___c_t_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `CTS Definitions and Data        | Contains the CTS specific        |
         | Structures <group__              | definitions and data structures  |
         | group__ble__service__a           | used in the CTS API              |
         | pi___c_t_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Device Information Service      | The Device Information Service   |
         | (DIS) <group__group__bl          | exposes manufacturer and/or      |
         | e__service__api___d_i_s.html>`__ | vendor information about a       |
         |                                  | device                           |
         +----------------------------------+----------------------------------+
         | `DIS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __d_i_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `DIS Server                      | API unique to DIS designs        |
         | Functions <group__               | configured as a GATT Server role |
         | group__ble__servi                |                                  |
         | ce__api___d_i_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `DIS Client                      | API unique to DIS designs        |
         | Functions <group__               | configured as a GATT Client role |
         | group__ble__servi                |                                  |
         | ce__api___d_i_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `DIS Definitions and Data        | Contains the DIS specific        |
         | Structures <group__              | definitions and data structures  |
         | group__ble__service__a           | used in the DIS API              |
         | pi___d_i_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Environmental Sensing           | The Environmental Sensing        |
         | Service                          | Service exposes measurement data |
         | (ESS) <group__group__bl          | from an environmental sensor     |
         | e__service__api___e_s_s.html>`__ | intended for sports and fitness  |
         |                                  | applications                     |
         +----------------------------------+----------------------------------+
         | `ESS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __e_s_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `ESS Server                      | API unique to ESS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___e_s_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `ESS Client                      | API unique to ESS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___e_s_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `ESS Definitions and Data        | Contains the ESS specific        |
         | Structure\                       | definitions and data structures  |
         | s <group__group__ble__service__a | used in the ESS API              |
         | pi___e_s_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Glucose Service                 | The Glucose Service exposes      |
         | (GLS) <group__group__bl          | glucose and other data related   |
         | e__service__api___g_l_s.html>`__ | to a personal glucose sensor for |
         |                                  | consumer healthcare applications |
         |                                  | and is not designed for clinical |
         |                                  | use                              |
         +----------------------------------+----------------------------------+
         | `GLS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __g_l_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `GLS Server                      | API unique to GLS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___g_l_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `GLS Client                      | API unique to GLS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___g_l_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `GLS Definitions and Data        | Contains the GLS specific        |
         | Structure\                       | definitions and data structures  |
         | s <group__group__ble__service__a | used in the GLS API              |
         | pi___g_l_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HID Service                     | The HID Service exposes data and |
         | (HIDS) <group__group__ble_       | associated formatting for HID    |
         | _service__api___h_i_d_s.html>`__ | Devices and HID Hosts            |
         +----------------------------------+----------------------------------+
         | `HIDS Server and Client          | These are API common to both     |
         | Functions <gr                    | GATT Client role and GATT Server |
         | oup__group__ble__service__api___ | role                             |
         | h_i_d_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HIDS Server                     | API unique to HID designs        |
         | Funct\                           | configured as a GATT Server role |
         | ions <group__group__ble__service |                                  |
         | __api___h_i_d_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HIDS Client                     | API unique to HID designs        |
         | Funct\                           | configured as a GATT Client role |
         | ions <group__group__ble__service |                                  |
         | __api___h_i_d_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HIDS Definitions and Data       | Contains the HID specific        |
         | Structures                       | definitions and data structures  |
         | <group__group__ble__service__api | used in the HID API              |
         | ___h_i_d_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Heart Rate Service              | The Heart Rate Service exposes   |
         | (HRS) <group__group__bl          | heart rate and other data        |
         | e__service__api___h_r_s.html>`__ | related to a heart rate sensor   |
         |                                  | intended for fitness             |
         |                                  | applications                     |
         +----------------------------------+----------------------------------+
         | `HRS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __h_r_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HRS Server                      | API unique to HRS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___h_r_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HRS Client                      | API unique to HRS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___h_r_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HRS Definitions and Data        | Contains the HRS specific        |
         | Structure\                       | definitions and data structures  |
         | s <group__group__ble__service__a | used in the HRS API              |
         | pi___h_r_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HTTP Proxy Service              | The HTTP Proxy Service allows a  |
         | (HPS) <group__group__bl          | Client device, typically a       |
         | e__service__api___h_p_s.html>`__ | sensor, to communicate with a    |
         |                                  | Web Server through a gateway     |
         |                                  | device                           |
         +----------------------------------+----------------------------------+
         | `HPS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __h_p_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HPS Server                      | API unique to HPS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___h_p_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HPS Client                      | API unique to HPS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___h_p_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HPS Definitions and Data        | Contains the HPS specific        |
         | Structure\                       | definitions and data structures  |
         | s <group__group__ble__service__a | used in the HPS API              |
         | pi___h_p_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Health Thermometer Service      | The Health Thermometer Service   |
         | (HTS) <group__group__bl          | exposes temperature and other    |
         | e__service__api___h_t_s.html>`__ | data related to a thermometer    |
         |                                  | used for healthcare applications |
         +----------------------------------+----------------------------------+
         | `HTS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __h_t_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HTS Server                      | API unique to HTS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___h_t_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HTS Client                      | API unique to HTS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___h_t_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `HTS Definitions and Data        | Contains the HTS specific        |
         | Structure\                       | definitions and data structures  |
         | s <group__group__ble__service__a | used in the HTS API              |
         | pi___h_t_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Immediate Alert Service         | The Immediate Alert Service      |
         | (IAS) <group__group__bl          | exposes a control point to allow |
         | e__service__api___i_a_s.html>`__ | a peer device to cause the       |
         |                                  | device to immediately alert      |
         +----------------------------------+----------------------------------+
         | `IAS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __i_a_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `IAS Server                      | API unique to IAS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___i_a_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `IAS Client                      | API unique to IAS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___i_a_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `IAS Definitions and Data\       | Contains the IAS specific        |
         | Structure\                       | definitions and data structures  |
         | s <group__group__ble__service__a | used in the IAS API              |
         | pi___i_a_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Indoor Positioning Service      | The Indoor Positioning exposes   |
         | (IPS) <group__group__bl          | coordinates and other location   |
         | e__service__api___i_p_s.html>`__ | related information via an       |
         |                                  | advertisement or indicates that  |
         |                                  | the device address can be used   |
         |                                  | for location look-up, enabling   |
         |                                  | mobile devices to find their     |
         |                                  | position                         |
         +----------------------------------+----------------------------------+
         | `IPS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __i_p_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `IPS Server                      | API unique to IPS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___i_p_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `IPS Client                      | API unique to IPS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___i_p_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `IPS Definitions and Data        | Contains the IPS specific        |
         | Structure\                       | definitions and data structures  |
         | s <group__group__ble__service__a | used in the IPS API              |
         | pi___i_p_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Link Loss Service               | The Link Loss Service uses the   |
         | (LLS) <group__group__bl          | Alert Level Characteristic to    |
         | e__service__api___l_l_s.html>`__ | cause an alert in the device     |
         |                                  | when the link is lost            |
         +----------------------------------+----------------------------------+
         | `LLS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __l_l_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `LLS Server                      | API unique to LLS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___l_l_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `LLS Client                      | API unique to LLS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___l_l_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `LLS Definitions and Data        | Contains the LLS specific        |
         | Structure\                       | definitions and data structures  |
         | s <group__group__ble__service__a | used in the LLS API              |
         | pi___l_l_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Location and Navigation         | The Location and Navigation      |
         | Service                          | Service exposes location and     |
         | (LNS) <group__group__bl          | navigation-related data from a   |
         | e__service__api___l_n_s.html>`__ | Location and Navigation sensor   |
         |                                  | (Server) intended for outdoor    |
         |                                  | activity applications            |
         +----------------------------------+----------------------------------+
         | `LNS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __l_n_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `LNS Server                      | API unique to LNS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___l_n_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `LNS Client                      | API unique to LNS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___l_n_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `LNS Definitions and Data        | Contains the LNS specific        |
         | Structure\                       | definitions and data structures  |
         | s <group__group__ble__service__a | used in the LNS API              |
         | pi___l_n_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Next DST Change Service         | The Next DST Change Service      |
         | (NDCS) <group__group__ble_       | enables a BLE device that has    |
         | _service__api___n_d_c_s.html>`__ | knowledge about the next         |
         |                                  | occurrence of a DST change to    |
         |                                  | expose this information to       |
         |                                  | another Bluetooth device         |
         +----------------------------------+----------------------------------+
         | `NDCS Server and Client          | These are API common to both     |
         | Functions <gr                    | GATT Client role and GATT Server |
         | oup__group__ble__service__api___ | role                             |
         | n_d_c_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `NDCS Server                     | API unique to NDCS designs       |
         | Funct\                           | configured as a GATT Server role |
         | ions <group__group__ble__service |                                  |
         | __api___n_d_c_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `NDCS Client                     | API unique to NDCS designs       |
         | Funct\                           | configured as a GATT Client role |
         | ions <group__group__ble__service |                                  |
         | __api___n_d_c_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `NDCS Definitions and Data       | Contains the NDCS specific       |
         | Structures                       | definitions and data structures  |
         | <group__group__ble__service__api | used in the NDCS API             |
         | ___n_d_c_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Phone Alert Status Service      | The Phone Alert Status Service   |
         | (PASS) <group__group__ble_       | uses the Alert Status            |
         | _service__api___p_a_s_s.html>`__ | Characteristic and Ringer        |
         |                                  | Setting Characteristic to expose |
         |                                  | the phone alert status and uses  |
         |                                  | the Ringer Control Point         |
         |                                  | Characteristic to control the    |
         |                                  | phone's ringer into mute or      |
         |                                  | enable                           |
         +----------------------------------+----------------------------------+
         | `PASS Server and Client          | These are API common to both     |
         | Function <gr                     | GATT Client role and GATT Server |
         | oup__group__ble__service__api___ | role                             |
         | p_a_s_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `PASS Server                     | API unique to PASS designs       |
         | Funct\                           | configured as a GATT Server role |
         | ions <group__group__ble__service |                                  |
         | __api___p_a_s_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `PASS Client                     | API unique to PASS designs       |
         | Funct\                           | configured as a GATT Client role |
         | ions <group__group__ble__service |                                  |
         | __api___p_a_s_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `PASS Definitions and Data       | Contains the PASS specific       |
         | Structures                       | definitions and data structures  |
         | <group__group__ble__service__api | used in the PASS API             |
         | ___p_a_s_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Pulse Oximeter Service          | The Pulse Oximeter Service       |
         | (PLXS) <group__group__ble_       | enables a Collector device to    |
         | _service__api___p_l_x_s.html>`__ | connect and interact with a      |
         |                                  | pulse oximeter intended for      |
         |                                  | healthcare applications          |
         +----------------------------------+----------------------------------+
         | `PLXS Server and Client          | These are API common to both     |
         | Function <gr                     | GATT Client role and GATT Server |
         | oup__group__ble__service__api___ | role                             |
         | p_l_x_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `PLXS Server                     | API unique to PLXS designs       |
         | Funct\                           | configured as a GATT Server role |
         | ions <group__group__ble__service |                                  |
         | __api___p_l_x_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `PLXS Client                     | API unique to PLXS designs       |
         | Funct\                           | configured as a GATT Client role |
         | ions <group__group__ble__service |                                  |
         | __api___p_l_x_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `PLXS Definitions and Data       | Contains the PLXS specific       |
         | Structures                       | definitions and data structures  |
         | <group__group__ble__service__api | used in the PLXS API             |
         | ___p_l_x_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Running Speed and Cadence       | The Running Speed and Cadence    |
         | Service                          | (RSC) Service exposes speed,     |
         | (RSCS) <group__group__ble_       | cadence and other data related   |
         | _service__api___r_s_c_s.html>`__ | to fitness applications such as  |
         |                                  | the stride length and the total  |
         |                                  | distance the user has travelled  |
         |                                  | while using the Running Speed    |
         |                                  | and Cadence sensor (Server)      |
         +----------------------------------+----------------------------------+
         | `RSCS Server and Client          | These are API common to both     |
         | Functions <gr                    | GATT Client role and GATT Server |
         | oup__group__ble__service__api___ | role                             |
         | r_s_c_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `RSCS Server                     | API unique to RSCS designs       |
         | Funct\                           | configured as a GATT Server role |
         | ions <group__group__ble__service |                                  |
         | __api___r_s_c_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `RSCS Client                     | API unique to RSCS designs       |
         | Funct\                           | configured as a GATT Client role |
         | ions <group__group__ble__service |                                  |
         | __api___r_s_c_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `RSCS Definitions and Data       | Contains the RSCS specific       |
         | Structures                       | definitions and data structures  |
         | <group__group__ble__service__api | used in the RSCS API             |
         | ___r_s_c_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Reference Time Update           | The Reference Time Update        |
         | Service                          | Service enables a Bluetooth      |
         | (RTUS) <group__group__ble_       | device that can update the       |
         | _service__api___r_t_u_s.html>`__ | system time using the reference  |
         |                                  | time such as a GPS receiver to   |
         |                                  | expose a control point and       |
         |                                  | expose the accuracy (drift) of   |
         |                                  | the local system time compared   |
         |                                  | to the reference time source     |
         +----------------------------------+----------------------------------+
         | `RTUS Server and Client          | These are API common to both     |
         | Function <gr                     | GATT Client role and GATT Server |
         | oup__group__ble__service__api___ | role                             |
         | r_t_u_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `RTUS Server                     | API unique to RTUS designs       |
         | Funct\                           | configured as a GATT Server role |
         | ions <group__group__ble__service |                                  |
         | __api___r_t_u_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `RTUS Client                     | API unique to RTUS designs       |
         | Funct\                           | configured as a GATT Client role |
         | ions <group__group__ble__service |                                  |
         | __api___r_t_u_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `RTUS Definitions and Data       | Contains the RTUS specific       |
         | Structures <g                    | definitions and data structures  |
         | roup__group__ble__service__api   | used in the RTUS API             |
         | ___r_t_u_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Scan Parameters Service         | The Scan Parameters Service      |
         | (ScPS) <group__group__ble_       | enables a Server device to       |
         | _service__api___s_c_p_s.html>`__ | expose a Characteristic for the  |
         |                                  | GATT Client to write its scan    |
         |                                  | interval and scan window on the  |
         |                                  | Server device, and enables a     |
         |                                  | Server to request a refresh of   |
         |                                  | the GATT Client scan interval    |
         |                                  | and scan window                  |
         +----------------------------------+----------------------------------+
         | `ScPS Server and Client          | These are API common to both     |
         | Functions <gr                    | GATT Client role and GATT Server |
         | oup__group__ble__service__api___ | role                             |
         | s_c_p_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `ScPS Server                     | API unique to ScPS designs       |
         | Funct\                           | configured as a GATT Server role |
         | ions <group__group__ble__service |                                  |
         | __api___s_c_p_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `ScPS Client                     | API unique to ScPS designs       |
         | Funct\                           | configured as a GATT Client role |
         | ions <group__group__ble__service |                                  |
         | __api___s_c_p_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `ScPS Definitions and Data       | Contains the ScPS specific       |
         | Structures                       | definitions and data structures  |
         | <group__group__ble__service__api | used in the ScPS API             |
         | ___s_c_p_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `TX Power Service                | The Tx Power Service uses the Tx |
         | (TPS) <group__group__bl          | Power Level Characteristic to    |
         | e__service__api___t_p_s.html>`__ | expose the current transmit      |
         |                                  | power level of a device when in  |
         |                                  | a connection                     |
         +----------------------------------+----------------------------------+
         | `TPS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __t_p_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `TPS Server                      | API unique to TPS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___t_p_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `TPS Client                      | API unique to TPS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___t_p_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `TPS Definitions and Data        | Contains the TPS specific        |
         | Structure\                       | definitions and data structures  |
         | s <group__group__ble__service__a | used in the TPS API              |
         | pi___t_p_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `User Data Service               | The User Data Service exposes    |
         | (UDS) <group__group__bl          | user-related data in the sports  |
         | e__service__api___u_d_s.html>`__ | and fitness environment          |
         +----------------------------------+----------------------------------+
         | `UDS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __u_d_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `UDS Server                      | API unique to UDS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___u_d_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `UDS Client                      | API unique to UDS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___u_d_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `UDS Definitions and Data        | Contains the UDS specific        |
         | Structure\                       | definitions and data structures  |
         | s <group__group__ble__service__a | used in the UDS API              |
         | pi___u_d_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Wireless Power Transfer         | The Wireless Power Transfer      |
         | Service                          | Service enables communication    |
         | (WPTS) <group__group__ble_       | between Power Receiver Unit and  |
         | _service__api___w_p_t_s.html>`__ | Power Transmitter Unit in the    |
         |                                  | Wireless Power Transfer systems  |
         +----------------------------------+----------------------------------+
         | `WPTS Server and Client          | These are API common to both     |
         | Function <gr                     | GATT Client role and GATT Server |
         | oup__group__ble__service__api___ | role                             |
         | w_p_t_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `WPTS Server                     | API unique to WPTS designs       |
         | Funct\                           | configured as a GATT Server role |
         | ions <group__group__ble__service |                                  |
         | __api___w_p_t_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `WPTS Client                     | API unique to WPTS designs       |
         | Funct\                           | configured as a GATT Client role |
         | ions <group__group__ble__service |                                  |
         | __api___w_p_t_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `WPTS Definitions and Data       | Contains the WPTS specific       |
         | Structures <g                    | definitions and data structures  |
         | roup__group__ble__service__api   | used in the WPTS API             |
         | ___w_p_t_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Weight Scale Service            | The Weight Scale Service exposes |
         | (WSS) <group__group__bl          | weight and related data from a   |
         | e__service__api___w_s_s.html>`__ | weight scale (Server) intended   |
         |                                  | for consumer healthcare as well  |
         |                                  | as sports/fitness applications   |
         +----------------------------------+----------------------------------+
         | `WSS Server and Client           | These are API common to both     |
         | Function <g                      | GATT Client role and GATT Server |
         | roup__group__ble__service__api_  | role                             |
         | __w_s_s__server__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `WSS Server                      | API unique to WSS designs        |
         | Fun\                             | configured as a GATT Server role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___w_s_s__server.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `WSS Client                      | API unique to WSS designs        |
         | Fun\                             | configured as a GATT Client role |
         | ctions <group__group__ble__servi |                                  |
         | ce__api___w_s_s__client.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `WSS Definitions and Data        | Contains the WSS specific        |
         | Structure\                       | definitions and data structures  |
         | s <group__group__ble__service__a | used in the WSS API              |
         | pi___w_s_s__definitions.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         | `Custom                          | This section contains the data   |
         | Service <group__group__bl        | structures used for Custom       |
         | e__service__api__custom.html>`__ | Services                         |
         +----------------------------------+----------------------------------+

.. toctree::
   :hidden:

   group__group__ble__common__api.rst
   group__group__ble__service__api.rst 
