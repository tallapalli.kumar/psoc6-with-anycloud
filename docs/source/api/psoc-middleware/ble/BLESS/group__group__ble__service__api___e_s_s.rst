====================================
Environmental Sensing Service (ESS)
====================================

.. doxygengroup:: group_ble_service_api_ESS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___e_s_s__server__client.rst
   group__group__ble__service__api___e_s_s__server.rst
   group__group__ble__service__api___e_s_s__client.rst
   group__group__ble__service__api___e_s_s__definitions.rst