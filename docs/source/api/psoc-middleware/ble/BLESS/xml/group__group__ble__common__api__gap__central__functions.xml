<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ble__common__api__gap__central__functions" kind="group">
    <compoundname>group_ble_common_api_gap_central_functions</compoundname>
    <title>GAP Central Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ble__common__api__gap__central__functions_1ga06ad7c5a0f265897c0f8a9d1f79695b4" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_GAPC_StartScan</definition>
        <argsstring>(uint8_t scanningIntervalType, uint8_t scanParamIndex)</argsstring>
        <name>Cy_BLE_GAPC_StartScan</name>
        <param>
          <type>uint8_t</type>
          <declname>scanningIntervalType</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>scanParamIndex</declname>
        </param>
        <briefdescription>
<para>This function is used for discovering GAP peripheral devices that are available for connection. </para>        </briefdescription>
        <detaileddescription>
<para>It performs the scanning routine using the parameters entered in the Component's customizer indicated by scanParamIndex parameter.</para><para>As soon as the discovery operation starts, <ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a91ec76ad9c1f8866290a86da2e4ff207">CY_BLE_EVT_GAPC_SCAN_START_STOP</ref> event is generated. The <ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86adb28261868379e739fcd60959ce3c040">CY_BLE_EVT_GAPC_SCAN_PROGRESS_RESULT</ref> event is generated when a GAP peripheral device is located. There are three discovery procedures can be selected in the customizer's GUI:</para><para><itemizedlist>
<listitem><para>Observation procedure: A device performing the observer role receives only advertisement data from devices irrespective of their discoverable mode settings. Advertisement data received is provided by the event, <ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86adb28261868379e739fcd60959ce3c040">CY_BLE_EVT_GAPC_SCAN_PROGRESS_RESULT</ref>. This procedure requires the scanType sub parameter to be passive scanning.</para></listitem><listitem><para>Discovery procedure: A device performing the discovery procedure receives the advertisement data and scan response data from devices in both limited discoverable mode and the general discoverable mode. Received data is provided by the event, <ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86adb28261868379e739fcd60959ce3c040">CY_BLE_EVT_GAPC_SCAN_PROGRESS_RESULT</ref>. This procedure requires the scanType sub-parameter to be active scanning.</para></listitem></itemizedlist>
</para><para>Every Advertisement / Scan response packet is received in a new event, <ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86adb28261868379e739fcd60959ce3c040">CY_BLE_EVT_GAPC_SCAN_PROGRESS_RESULT</ref>. If 'scanTo' sub-parameter is a non-zero value, then upon commencement of discovery procedure and elapsed time = 'scanTo', <ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a5faab937dcd719a5cc5cb8e8b749dcaa">CY_BLE_EVT_TIMEOUT</ref> event is generated with the event parameter indicating <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga9401245e7b20db19f6b735d6295a6113a7d5643d4803c97c1555c62f9358d67cd">CY_BLE_GAP_SCAN_TO</ref>. Possible generated events are:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a91ec76ad9c1f8866290a86da2e4ff207">CY_BLE_EVT_GAPC_SCAN_START_STOP</ref>: If a device started or stopped scanning. Use <ref kindref="member" refid="group__group__ble__common__api__functions_1ga5e2cf92628baacf34cf9c6569792cdb4">Cy_BLE_GetScanState()</ref> to determine the state. Sequential scanning could be started when <ref kindref="member" refid="group__group__ble__common__api__gap__definitions_1gga1af3aeb3bb744f0d9b7fe0b6859b8edcace0f9cadd252b1959497c6340b95385a">CY_BLE_ADV_STATE_STOPPED</ref> state is returned.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86adb28261868379e739fcd60959ce3c040">CY_BLE_EVT_GAPC_SCAN_PROGRESS_RESULT</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a5faab937dcd719a5cc5cb8e8b749dcaa">CY_BLE_EVT_TIMEOUT</ref> (CY_BLE_GAP_SCAN_TO)</para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>scanningIntervalType</parametername>
</parameternamelist>
<parameterdescription>
<para>Fast or slow scanning interval with timings entered in Scan settings section of the customizer.<itemizedlist>
<listitem><para>CY_BLE_SCANNING_FAST 0x00u</para></listitem><listitem><para>CY_BLE_SCANNING_SLOW 0x01u</para></listitem><listitem><para>CY_BLE_SCANNING_CUSTOM 0x02u</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>scanParamIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the central and scan configuration in customizer.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref> : Return value indicates whether the function succeeded or failed. The following are possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On passing an invalid parameter. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On calling this function not in Stopped state. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;CY_BLE_ERROR_INVALID_OPERATION | </verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="955" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_gap.c" bodystart="907" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_gap.h" line="503" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common__api__gap__central__functions_1gaf05df8db69912fcab86883b2618eb66e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_GAPC_StopScan</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_BLE_GAPC_StopScan</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This function used to stop the discovery of devices. </para>        </briefdescription>
        <detaileddescription>
<para>On stopping discovery operation, <ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a91ec76ad9c1f8866290a86da2e4ff207">CY_BLE_EVT_GAPC_SCAN_START_STOP</ref> event is generated. Application layer needs to keep track of the function call made before receiving this event to associate this event with either the start or stop discovery function.</para><para>Possible events generated are:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a91ec76ad9c1f8866290a86da2e4ff207">CY_BLE_EVT_GAPC_SCAN_START_STOP</ref></para></listitem></itemizedlist>
</para><para><simplesect kind="return"><para><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref> : Return value indicates whether the function succeeded or failed. The following are possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On calling this function not in Scanning state. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1000" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_gap.c" bodystart="982" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_gap.h" line="504" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common__api__gap__central__functions_1ga76386b6b464a3783f190157202db9b82" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_GAPC_ConnectDevice</definition>
        <argsstring>(const cy_stc_ble_gap_bd_addr_t *address, uint8_t centralParamIndex)</argsstring>
        <name>Cy_BLE_GAPC_ConnectDevice</name>
        <param>
          <type>const <ref kindref="member" refid="group__group__ble__common__api__gap__definitions_1ga07cab4d5612bd17499ed3ddee947bfdb">cy_stc_ble_gap_bd_addr_t</ref> *</type>
          <declname>address</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>centralParamIndex</declname>
        </param>
        <briefdescription>
<para>This function is used to send a connection request to the remote device with the connection parameters set in the BLE Component customizer. </para>        </briefdescription>
        <detaileddescription>
<para>This function needs to be called only once after the target device is discovered by <ref kindref="member" refid="group__group__ble__common__api__gap__central__functions_1ga06ad7c5a0f265897c0f8a9d1f79695b4">Cy_BLE_GAPC_StartScan()</ref> and further scanning has stopped. Scanning is successfully stopped on invoking <ref kindref="member" refid="group__group__ble__common__api__gap__central__functions_1gaf05df8db69912fcab86883b2618eb66e">Cy_BLE_GAPC_StopScan()</ref> and then receiving the event <ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a91ec76ad9c1f8866290a86da2e4ff207">CY_BLE_EVT_GAPC_SCAN_START_STOP</ref> with sub-parameter 'success' = 0x01u.</para><para>On successful connection, the following events are generated at the GAP Central device (as well as the GAP Peripheral device), in the following order.<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86aff4f3e61e6d3125ac81bf0c92be85ab5">CY_BLE_EVT_GATT_CONNECT_IND</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86af55b50a1ddedf1aed59932c035fd4e0a">CY_BLE_EVT_GAP_DEVICE_CONNECTED</ref> - If the device connects to a GAP Central and Link Layer Privacy is disabled.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a348367e4e95447eba8e6011050c7fac9">CY_BLE_EVT_GAP_ENHANCE_CONN_COMPLETE</ref> - If the device connects to a GAP Central and Link Layer Privacy is enabled.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86af55b50a1ddedf1aed59932c035fd4e0a">CY_BLE_EVT_GAP_DEVICE_CONNECTED</ref></para></listitem></itemizedlist>
</para><para>A procedure is considered to have timed out if a connection response packet is not received within time set by cy_ble_connectingTimeout global variable (30 seconds by default). <ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a5faab937dcd719a5cc5cb8e8b749dcaa">CY_BLE_EVT_TIMEOUT</ref> event with <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga9401245e7b20db19f6b735d6295a6113a337fd92ce11151df7eaab75a89fe9694">CY_BLE_GENERIC_APP_TO</ref> parameter will indicate about connection procedure timeout. Connection will automatically be canceled and state will be changed to <ref kindref="member" refid="group__group__ble__common__api__gap__definitions_1gga9fb144d5b94e09d019db4fb7b40dc677a0197fe0e4f2c19437eaa905ecb3a8f37">CY_BLE_STATE_ON</ref>.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>address</parametername>
</parameternamelist>
<parameterdescription>
<para>The device address of the remote device to connect to. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>centralParamIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the central configuration in customizer. For example:<itemizedlist>
<listitem><para>CY_BLE_CENTRAL_CONFIGURATION_0_INDEX 0x00</para></listitem><listitem><para>CY_BLE_CENTRAL_CONFIGURATION_1_INDEX 0x01</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref> : Return value indicates whether the function succeeded or failed. The following are possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On passing an invalid parameter. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On calling this function not in Disconnected state. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The operation is not permitted due to connection limit exceeded. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;Note: Please refer the description of Cy_BLE_GAPC_InitConnection() for recommended Connection Interval values. </verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1129" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_gap.c" bodystart="1051" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_gap.h" line="506" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common__api__gap__central__functions_1ga8405648f65b8364ad8cc77521c7a8e36" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_GAPC_CancelDeviceConnection</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_BLE_GAPC_CancelDeviceConnection</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This function cancels a previously initiated connection with the remote device. </para>        </briefdescription>
        <detaileddescription>
<para>It is a blocking function. No event is generated on calling this function. If the devices are already connected then this function should not be used. If you intend to disconnect from an existing connection, the function <ref kindref="member" refid="group__group__ble__common__api__gap__functions_1ga7bc79b7fd5102619c87101fc89b3d3dd">Cy_BLE_GAP_Disconnect()</ref> should be used.</para><para><simplesect kind="return"><para><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref> : Return value indicates whether the function succeeded or failed. The following are possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On calling this function not in Connecting state. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1176" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_gap.c" bodystart="1152" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_gap.h" line="507" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common__api__gap__central__functions_1gad34d0337e5353d3f60dcc5aeaa31b9fb" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_GAPC_ResolveDevice</definition>
        <argsstring>(cy_stc_ble_gapc_resolve_peer_info_t *param)</argsstring>
        <name>Cy_BLE_GAPC_ResolveDevice</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__gapc__resolve__peer__info__t">cy_stc_ble_gapc_resolve_peer_info_t</ref> *</type>
          <declname>param</declname>
        </param>
        <briefdescription>
<para>This function enables the GAP Central application to start a address resolution procedure for a peer device that is connected using a resolvable private address. </para>        </briefdescription>
        <detaileddescription>
<para>This is a non-blocking function.</para><para>Refer to Bluetooth 5.0 Core specification, Volume 3, Part C, section 10.8.2.3 Resolvable Private Address Resolution Procedure to understand the usage of Private addresses.</para><para>'CY_BLE_EVT_GAP_RESOLVE_DEVICE_COMPLETE' event is generated to inform about the completion of address resolution procedure.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>param</parametername>
</parameternamelist>
<parameterdescription>
<para>Parameter is of type <ref kindref="compound" refid="structcy__stc__ble__gapc__resolve__peer__info__t">cy_stc_ble_gapc_resolve_peer_info_t</ref> param-&gt;bdAddr: Peer Bluetooth device address of 6 bytes length, not NULL terminated. param-&gt;peerIrk: 128-bit IRK to be used for resolving the peer's private resolvable address.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_en_ble_api_result_t : Return value indicates if the function succeeded or failed. Following are the possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Errors codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On specifying NULL as input parameter for 'param' or 'bdAddr'. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INSUFFICIENT_RESOURCES &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;BLE Stack resources are unavailable. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;: Note1: param should point to a valid memory location until completion of function is indicated via 'CY_BLE_EVT_GAP_RESOLVE_DEVICE_COMPLETE' event. Note2: If application is using White List filtering, update the White List with the new address using &lt;a href="group__group__ble__common___whitelist__api__functions.html#group__group__ble__common___whitelist__api__functions_1ga6e2997c70484c232096b5efed77a8315"&gt;Cy_BLE_AddDeviceToWhiteList()&lt;/a&gt; function. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_stack_gap_central.h" line="591" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common__api__gap__central__functions_1gada4e13c47119514e3573b70e5c014605" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_GAPC_SetRemoteAddr</definition>
        <argsstring>(cy_stc_ble_gapc_peer_bd_addr_info_t *param)</argsstring>
        <name>Cy_BLE_GAPC_SetRemoteAddr</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__gapc__peer__bd__addr__info__t">cy_stc_ble_gapc_peer_bd_addr_info_t</ref> *</type>
          <declname>param</declname>
        </param>
        <briefdescription>
<para>This function is used by GAP Central application to set the new address of peer device identified by bdHandle. </para>        </briefdescription>
        <detaileddescription>
<para>No event is generated on calling this function. This is a blocking function.</para><para>This function should be used when:<orderedlist>
<listitem><para>A peer device bonded previously with a public address and changes its BD address to a resolvable private address. The application should first resolve the device address by calling the '<ref kindref="member" refid="group__group__ble__common__api__gap__central__functions_1gad34d0337e5353d3f60dcc5aeaa31b9fb">Cy_BLE_GAPC_ResolveDevice()</ref>' function. If address resolution is successful then set the new address using this function.</para></listitem><listitem><para>A peer device is previously bonded with a random address, then application should call this function to set the new address(public/random).</para></listitem></orderedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>param</parametername>
</parameternamelist>
<parameterdescription>
<para>Parameter of type '<ref kindref="compound" refid="structcy__stc__ble__gapc__peer__bd__addr__info__t">cy_stc_ble_gapc_peer_bd_addr_info_t</ref>'</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_en_ble_api_result_t : Return value indicates whether the function succeeded or failed. Following are the possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Errors codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On invalid bdHandle. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_stack_gap_central.h" line="625" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common__api__gap__central__functions_1ga6cb851d11763ced036a1e41cd0a4b707" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_GAPC_ConnectionParamUpdateRequest</definition>
        <argsstring>(cy_stc_ble_gap_conn_update_param_info_t *param)</argsstring>
        <name>Cy_BLE_GAPC_ConnectionParamUpdateRequest</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__gap__conn__update__param__info__t">cy_stc_ble_gap_conn_update_param_info_t</ref> *</type>
          <declname>param</declname>
        </param>
        <briefdescription>
<para>This function is used by GAP Central application to initiate the Connection Parameter Update Procedure. </para>        </briefdescription>
        <detaileddescription>
<para>This function updates parameters to local BLE Controller. Local BLE Controller follows the connection update procedure as defined in Bluetooth Core Specification 4.0. This function does not perform parameter negotiation with peer device through LL topology as defined in Bluetooth Core Specification 4.1.</para><para>CY_BLE_EVT_GAP_CONNECTION_UPDATE_COMPLETE event is generated to inform about completion of Connection Parameter Update procedure.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>param</parametername>
</parameternamelist>
<parameterdescription>
<para>Parameter is of type '<ref kindref="compound" refid="structcy__stc__ble__gap__conn__update__param__info__t">cy_stc_ble_gap_conn_update_param_info_t</ref>'</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_en_ble_api_result_t : Return value indicates if the function succeeded or failed. Following are the possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Errors codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On specifying NULL for input parameter. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_NO_DEVICE_ENTITY &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;If connection does not exist for corresponding 'bdHandle'. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INSUFFICIENT_RESOURCES &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;BLE Stack resources are unavailable. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_stack_gap_central.h" line="658" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>APIs unique to designs configured as a GAP Central role. </para>    </briefdescription>
    <detaileddescription>
<para>A letter 'C' is appended to the API name: Cy_BLE_GAPC_ </para>    </detaileddescription>
  </compounddef>
</doxygen>