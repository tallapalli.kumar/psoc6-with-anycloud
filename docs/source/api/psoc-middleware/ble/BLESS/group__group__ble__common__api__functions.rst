==========================
BLE Common Core Functions
==========================

.. doxygengroup:: group_ble_common_api_functions
   :project: bless

.. toctree::
   :hidden:

   group__group__ble__common___whitelist__api__functions.rst
   group__group__ble__common___privacy__api__functions.rst
   group__group__ble__common___data__length__extension__api__functions.rst
   group__group__ble__common__2_m_b_p_s__api__functions.rst
   group__group__ble__common___l_e__ping__api__functions.rst
   group__group__ble__common___encryption__api__functions.rst
   group__group__ble__common___h_c_i__api__functions.rst
   group__group__ble__common___intr__feature__api__functions.rst