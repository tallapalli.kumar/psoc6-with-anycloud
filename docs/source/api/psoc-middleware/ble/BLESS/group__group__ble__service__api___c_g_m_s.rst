=============================================
Continuous Glucose Monitoring Service (CGMS)
=============================================

.. doxygengroup:: group_ble_service_api_CGMS
   :project: bless
   
   
.. toctree::
   :hidden:

   group__group__ble__service__api___c_g_m_s__server__client.rst
   group__group__ble__service__api___c_g_m_s__server.rst
   group__group__ble__service__api___c_g_m_s__client.rst
   group__group__ble__service__api___c_g_m_s__definitions.rst