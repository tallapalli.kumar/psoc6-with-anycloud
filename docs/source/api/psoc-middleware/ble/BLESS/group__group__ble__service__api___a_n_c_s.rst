=========================================
Apple Notification Center Service (ANCS)
=========================================

.. doxygengroup:: group_ble_service_api_ANCS
   :project: bless
   

.. toctree::
   :hidden:

   group__group__ble__service__api___a_n_c_s__server__client.rst
   group__group__ble__service__api___a_n_c_s__server.rst
   group__group__ble__service__api___a_n_c_s__client.rst
   group__group__ble__service__api___a_n_c_s__definitions.rst