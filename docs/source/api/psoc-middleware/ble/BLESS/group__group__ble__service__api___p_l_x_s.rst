===============================
Pulse Oximeter Service (PLXS)
===============================

.. doxygengroup:: group_ble_service_api_PLXS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___p_l_x_s__server__client.rst
   group__group__ble__service__api___p_l_x_s__server.rst
   group__group__ble__service__api___p_l_x_s__client.rst
   group__group__ble__service__api___p_l_x_s__definitions.rst