<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="_r_e_a_d_m_e_8doxygen_8md" kind="file" language="Markdown">
    <compoundname>README.doxygen.md</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">#<sp />CY8CKIT-028-TFT<sp />shield<sp />support<sp />library</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />TFT<sp />Display<sp />Shield<sp />Board<sp />(CY8CKIT-028-TFT)<sp />has<sp />been<sp />designed<sp />such<sp />that<sp />a<sp />TFT<sp />display,<sp />audio<sp />devices,<sp />and<sp />sensors<sp />can<sp />interface<sp />with<sp />PSoC<sp />MCUs.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">It<sp />comes<sp />with<sp />the<sp />features<sp />below<sp />to<sp />enable<sp />everyday<sp />objects<sp />to<sp />connect<sp />to<sp />the<sp />Internet<sp />of<sp />Things<sp />(IoT).</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />2.4<sp />inch<sp />TFT<sp />Display<sp />(ST7789V)</highlight></codeline>
<codeline><highlight class="normal">*<sp />Motion<sp />Sensor<sp />(BMI-160)</highlight></codeline>
<codeline><highlight class="normal">*<sp />Ambient<sp />Light<sp />Sensor<sp />(TEMT6000X01)</highlight></codeline>
<codeline><highlight class="normal">*<sp />PDM<sp />Microphone<sp />example<sp />code<sp />(SPK0838HT4HB)</highlight></codeline>
<codeline><highlight class="normal">*<sp />Audio<sp />Codec<sp />(AK4954A)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">This<sp />shield<sp />has<sp />a<sp />Newhaven<sp />2.4″<sp />240×320<sp />TFT<sp />display<sp />with<sp />a<sp />Sitronix<sp />ST7789<sp />display<sp />controller<sp />and<sp />uses<sp />the<sp />8080-Series<sp />Parallel<sp />Interface.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />shield<sp />library<sp />provides<sp />support<sp />for:</highlight></codeline>
<codeline><highlight class="normal">*<sp />Initializing/freeing<sp />all<sp />of<sp />the<sp />hardware<sp />peripheral<sp />resources<sp />on<sp />the<sp />board</highlight></codeline>
<codeline><highlight class="normal">*<sp />Defining<sp />all<sp />pin<sp />mappings<sp />from<sp />the<sp />Arduino<sp />interface<sp />to<sp />the<sp />different<sp />peripherals</highlight></codeline>
<codeline><highlight class="normal">*<sp />Providing<sp />access<sp />to<sp />each<sp />of<sp />the<sp />underlying<sp />peripherals<sp />on<sp />the<sp />board</highlight></codeline>
<codeline />
<codeline><highlight class="normal">This<sp />library<sp />makes<sp />use<sp />of<sp />support<sp />libraries:<sp />[display-tft-st7789v](https://github.com/cypresssemiconductorco/display-tft-st7789v),<sp />[sensor-light](https://github.com/cypresssemiconductorco/sensor-light),<sp />[sensor-motion-bmi160](https://github.com/cypresssemiconductorco/sensor-motion-bmi160),<sp />and<sp />[audio-codec-ak4954a](https://github.com/cypresssemiconductorco/audio-codec-ak4954a).<sp />These<sp />can<sp />be<sp />seen<sp />in<sp />the<sp />libs<sp />directory<sp />and<sp />can<sp />be<sp />used<sp />directly<sp />instead<sp />of<sp />through<sp />the<sp />shield<sp />if<sp />desired.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">The<sp />TFT<sp />Display<sp />Shield<sp />Board<sp />uses<sp />the<sp />Arduino<sp />Uno<sp />pin<sp />layout<sp />plus<sp />an<sp />additional<sp />6<sp />pins.<sp />It<sp />is<sp />compatible<sp />with<sp />the<sp />PSoC<sp />4<sp />and<sp />PSoC<sp />6<sp />Pioneer<sp />Kits.<sp />Refer<sp />to<sp />the<sp />respective<sp />kit<sp />guides<sp />for<sp />more<sp />details.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">![](board.png)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">#<sp />Quick<sp />Start<sp />Guide</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />[Display<sp />usage](https://github.com/cypresssemiconductorco/display-tft-st7789v#quick-start)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Motion<sp />Sensor<sp />usage](https://github.com/cypresssemiconductorco/sensor-motion-bmi160#quick-start)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Light<sp />Sensor<sp />usage](https://github.com/cypresssemiconductorco/sensor-light#quick-start)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Audio<sp />Codec<sp />usage](https://github.com/cypresssemiconductorco/audio-codec-ak4954a#quick-start)</highlight></codeline>
<codeline />
<codeline />
<codeline><highlight class="normal">###<sp />More<sp />information</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />[API<sp />Reference<sp />Guide](https://cypresssemiconductorco.github.io/CY8CKIT-028-TFT/html/index.html)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[CY8CKIT-028-TFT<sp />Documentation](https://www.cypress.com/documentation/development-kitsboards/tft-display-shield-board-cy8ckit-028-tft)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[SEGGER<sp />emWin<sp />Middleware<sp />Library](https://github.com/cypresssemiconductorco/emwin)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor,<sp />an<sp />Infineon<sp />Technologies<sp />Company](http://www.cypress.com)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor<sp />GitHub](https://github.com/cypresssemiconductorco)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[ModusToolbox](https://www.cypress.com/products/modustoolbox-software-environment)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">---</highlight></codeline>
<codeline><highlight class="normal">©<sp />Cypress<sp />Semiconductor<sp />Corporation,<sp />2019-2020.</highlight></codeline>
    </programlisting>
    <location file="bsp/shields/CY8CKIT-028-TFT/README.doxygen.md" />
  </compounddef>
</doxygen>