<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="indexpage" kind="group">
    <compoundname>index</compoundname>
    <title>Thermistor (Temperature Sensor)</title>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
<para><bold>Overview</bold>
</para><para>This library provides functions for reading a thermistor temperature sensor as used on some shields.</para><para><bold>Quick Start</bold>
</para><para><orderedlist>
<listitem><para>Create an empty PSoC6 application</para></listitem><listitem><para>Add this library to the application</para></listitem><listitem><para>Add retarget-io library using Library Manager</para></listitem><listitem><para>Place following code in the main.c file:</para></listitem><listitem><para>Note: CY8CKIT_028_EPD_ items are all defined in the CY8CKIT-028-EPD library. If you are using different hardware, pick appropriate values to substitute for your hardware. <programlisting filename=".cpp"><codeline><highlight class="preprocessor">#include<sp />"cy_pdl.h"</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal.h"</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cybsp.h"</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_retarget_io.h"</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy8ckit_028_epd.h"</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal">cyhal_adc_t<sp />adc;</highlight></codeline>
<codeline><highlight class="normal"><ref kindref="compound" refid="structmtb__thermistor__ntc__gpio__t">mtb_thermistor_ntc_gpio_t</ref><sp />thermistor;</highlight></codeline>
<codeline><highlight class="normal"><ref kindref="compound" refid="structmtb__thermistor__ntc__gpio__cfg__t">mtb_thermistor_ntc_gpio_cfg_t</ref><sp />thermistor_cfg<sp />=<sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.<ref kindref="member" refid="structmtb__thermistor__ntc__gpio__cfg__t_1a8e0394b410ee31e8f6a8e3096ce9948b">r_ref</ref><sp />=<sp />CY8CKIT_028_EPD_THERM_R_REF,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.b_const<sp />=<sp />CY8CKIT_028_EPD_THERM_B_CONST,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />.r_infinity<sp />=<sp />CY8CKIT_028_EPD_THERM_R_INFINITY,</highlight></codeline>
<codeline><highlight class="normal">};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keywordtype">int</highlight><highlight class="normal"><sp />main(</highlight><highlight class="keywordtype">void</highlight><highlight class="normal">)</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cy_rslt_t<sp />result;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />the<sp />device<sp />and<sp />board<sp />peripherals<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cybsp_init();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />__enable_irq();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />retarget-io<sp />to<sp />use<sp />the<sp />debug<sp />UART<sp />port<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cy_retarget_io_init(CYBSP_DEBUG_UART_TX,<sp />CYBSP_DEBUG_UART_RX,<sp />CY_RETARGET_IO_BAUDRATE);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Intialize<sp />adc<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_adc_init(&amp;adc,<sp />CY8CKIT_028_EPD_PIN_THERM_OUT1,<sp />NULL);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />thermistor<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__board__libs_1gad8550ff1adedfd91648b41f29bc8ba6c">mtb_thermistor_ntc_gpio_init</ref>(&amp;thermistor,<sp />&amp;adc,<sp />CY8CKIT_028_EPD_PIN_THERM_GND,<sp />CY8CKIT_028_EPD_PIN_THERM_VDD,<sp />CY8CKIT_028_EPD_PIN_THERM_OUT1,<sp />&amp;thermistor_cfg);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(result<sp />==<sp />CY_RSLT_SUCCESS);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">for</highlight><highlight class="normal"><sp />(;;)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Measure<sp />the<sp />temperature<sp />and<sp />send<sp />the<sp />value<sp />via<sp />UART<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />printf(</highlight><highlight class="stringliteral">"Temperature<sp />=<sp />%fC\n\r"</highlight><highlight class="normal">,<sp /><ref kindref="member" refid="group__group__board__libs_1ga03a63ba0021b2039c22414336629aa91">mtb_thermistor_ntc_gpio_get_temp</ref>(&amp;thermistor));</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cyhal_system_delay_ms(1000);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
</programlisting></para></listitem><listitem><para>Build the application and program the kit.</para></listitem></orderedlist>
</para><para><bold>More information</bold>
</para><para><itemizedlist>
<listitem><para><ulink url="https://cypresssemiconductorco.github.io/thermistor/html/index.html">API Reference Guide</ulink></para></listitem><listitem><para><ulink url="http://www.cypress.com">Cypress Semiconductor, an Infineon Technologies Company</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco">Cypress Semiconductor GitHub</ulink></para></listitem><listitem><para><ulink url="https://www.cypress.com/products/modustoolbox-software-environment">ModusToolbox</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco/Code-Examples-for-ModusToolbox-Software">PSoC 6 Code Examples using ModusToolbox IDE</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco/psoc6-middleware">PSoC 6 Middleware</ulink></para></listitem><listitem><para><ulink url="https://community.cypress.com/docs/DOC-14644">PSoC 6 Resources - KBA223067</ulink> <hruler />
 © Cypress Semiconductor Corporation, 2019-2020. </para></listitem></itemizedlist>
</para>    </detaileddescription>
  </compounddef>
</doxygen>