==========================================
ModusToolbox CapSense Configurator Guide
==========================================


Overview
========

CapSense is a Cypress capacitive sensing solution. Capacitive sensing
can be used in a variety of applications and products where sleek human
interfaces replace conventional mechanical buttons to transform the way
users interact with electronic systems. These include home appliances,
automotive, IoT, and industrial applications. CapSense supports multiple
interfaces (widgets) using both CSX and CSD sensing methods, with robust
performance.

The CapSense Configurator is part of a collection of tools included in
ModusToolbox. Use it to create and configure CapSense widgets, and
generate code to control the application firmware. There is a separate
CapSense Tuner application for tuning, testing, and debugging for easy
and smooth design of human interfaces on customer products.

|image0|

Supported Middleware
--------------------

+---------------------------+-----------+----------------------------------------------------+
| Name                      | Version   | Link                                               |
+===========================+===========+====================================================+
| Cypress PSoC 6 CapSense   | 2.0, 2.10 | https://github.com/cypresssemiconductorco/capsense |
| Middleware Library        |           |                                                    |
+---------------------------+-----------+----------------------------------------------------+

Launch the CapSense Configurator
================================

You can launch the CapSense Configurator as a GUI with or without the
Eclipse IDE for ModusToolbox or from the Device Configurator. You can
also run the tool from the command line. The CapSense Configurator GUI
contains `menus <#menus-commands>`__ and `tabs <#tabs>`__ to configure
CapSense settings. The command line tool has various options. Then, you
can either use the generated source with an Eclipse IDE application, or
use it in any software environment you choose.

Launch without the Eclipse IDE
------------------------------

To run the CapSense Configurator GUI without the Eclipse IDE, navigate
to the install location and run the executable. The default install
location for the CapSense Configurator is:

	*<install_dir>/tools_<version>/capsense-configurator*

The CapSense Configurator opens with an untitled configuration file
(\*\ *.cycapsense*). Save it as a new file and provide a file name, or
open another existing \*\ *.cycapsense* file.

Launch with the Eclipse IDE
---------------------------

If your Eclipse IDE application already includes a *design.cycapsense*
file in the Board Support Package (BSP), right-click on a project in the
IDE Project Explorer, and select **ModusToolbox > CapSense
Configurator** to open the tool.

|image1|

You can also open the CapSense Configurator GUI by clicking the link in
the Eclipse IDE Quick Panel.

Launch from the Device Configurator
-----------------------------------

If your Eclipse IDE application does **not** include a
*design.cycapsense* file:

1. Open the Device Configurator.

   |image2|

2. On the **Peripherals** tab, select the **CSD (CapSense)** resource.

3. On the **Parameters** pane, select an appropriate input **Clock**.

4. On the **Parameters** pane, click the **Launch CapSense
   Configurator** button.

|image3|

The Device Configurator is based on the *design.modus* file, which
contains all the required information about the device and the
application. This information is passed to the CapSense Configurator.
When you save changes, it generates/updates firmware in the Eclipse IDE
application’s “GeneratedSource” folder.

From the Command Line
---------------------

You can run the capsense-configurator executable from the command line.
There is also a capsense-configurator-cli executable, which re-generates
source code based on the latest configuration settings from a
command-line prompt or from within batch files or shell scripts. The
exit code for the capsense-configurator-cli executable is zero if the
operation is successful, or non-zero if the operation encounters an
error. In order to use the capsense-configurator-cli executable, you
must provide at least the \-\-\config argument with a path to the
configuration file.

For more information about command-line options, run the
capsense-configurator or capsense-configurator-cli executable using the
-h option.

Quick Start
===========

This section provides a simple workflow for how to use the CapSense
Configurator.

1. Create a CapSense application using the Eclipse IDE. It provides a
   simple CapSense Linear Slider example to get started.

2. Launch the Device Configurator.

3.  Enable and configure a communication peripheral. The example uses an
    SCB configured as EZI2C.

4.  On the **Peripherals** tab, select the **CSD (CapSense)** resource.

5.  On the **Parameters** pane, click the **Launch CapSense
    Configurator** button.

6. Add and configure widgets on the `Basic tab <#basic-tab>`__.

7. Configure parameters on the `Advanced tab <#advanced-tab>`__.

8. Assign pins on the `Pins tab <#pins-tab>`__.

9. Save to generate code.

   The CapSense Configurator generates code into a “GeneratedSource”
   directory in your Eclipse IDE application, or in the location you
   specified for non-Eclipse IDE applications. That directory contains the
   necessary source (.c) and header (.h) files for the generated firmware,
   which uses the relevant driver APIs to configure the hardware.

   .. note::
      The tool generates code every time you save the configuration
      file.

10. Include *cycfg_capsense.h* in the *main.c* file.

11. Build the application in the Eclipse IDE, and program the device.

12. Launch the CapSense Tuner. Refer to the *CapSense Tuner Guide*.

Menus/Commands
==============

The menus include:

-  **File** – Provides basic commands to open, close, and save files, as
   well as exit the configurator.

	-  **New** – Creates a new file with new configuration.

	-  **Open** – Opens a specified *<file_name>.cycapsense* configuration
	   file. The current file, if any, will be closed.

	-  **Close** – Closes the current file.

	-  **Save** – Saves the current configuration file and generates
	   CapSense middleware configuration code. If there are errors in the
	   application, a dialog will indicate such. The file will still be
	   saved.

	-  **Save As** – Saves the existing file under a different name.

	-  **Import** – Imports a specified configuration file.

	-  **Export** – Exports the current configuration file into a specified
	   file.

	-  **Export Register Map to PDF** – Exports the current configuration
	   register map in PDF format.

	-  **Exit** – Closes the configurator. You will be prompted to save any
	   pending changes.

-  **Edit**

	-  **Undo** – Undoes the last action or sequence of actions.

	-  **Redo** – Redoes the undone action or sequence of undone actions.

-  **View**

	-  **Notice List** – Hides or shows the Notice List pane. The pane is
	   shown by default.

	-  **Toolbar** – Hides or shows the Toolbar.

	-  **Reset View** – Resets the view to the default.

-  **Help**

	-  **View Help** – Opens this document.

	-  **About CapSense Configurator** – Opens the About box for version
	   information.

Notice List
===========

The Notice List pane combines notices (errors, warnings, tasks, and
notes) from many places in the configuration into a centralized list. If
a notice shows a location, you can double-click the entry to show the
error or warning.

|image4|

The Notice List pane contains the following columns:

-  **Icon** – Displays the icons for the error, warning, task, or note.

-  **Fix** – This may display a wrench icon, which can be used to
   automatically address the required notice.

-  **Description** – Displays a brief description of the notice.

-  **Location** – Displays the specific tab of the message, when
   applicable.

Tabs
====

The CapSense Configurator contains the following tabs, each of which
provides access to specific parameters. Separate sections in this
document provide more descriptions of these tabs.

-  `Basic Tab <#basic-tab>`__

-  `Advanced Tab <#advanced-tab>`__

-  `Pins Tab <#pins-tab>`__

Basic Tab
---------

The **Basic** tab defines the high-level middleware configuration. Use
this tab to add various *Widget Type* and assign *Sensing mode* , *Widget
Sensing Element(s)* and *Finger capacitance* for each widget.

|image5|

The following table contains descriptions of the various **Basic** tab
parameters:

+---------------------------+-----------------------------------------+
| Name                      | Description                             |
+===========================+=========================================+
| CSD tuning mode           | Tuning is a process of finding          |
|                           | appropriate values for configurable     |
|                           | parameters (Hardware parameters and     |
|                           | Threshold parameters) for proper        |
|                           | functionality and optimized performance |
|                           | of the CapSense system.                 |
|                           |                                         |
|                           | The SmartSense Auto-tuning is an        |
|                           | algorithm embedded in the CapSense      |
|                           | middleware. The algorithm automatically |
|                           | finds optimum values for configurable   |
|                           | parameters basing on the hardware       |
|                           | properties of capacitive sensors. This  |
|                           | allows the user to avoid the            |
|                           | manual-tuning process.                  |
|                           |                                         |
|                           | Configurable parameters that affect the |
|                           | operation of the sensing hardware are   |
|                           | called Hardware parameters. Parameters  |
|                           | that affect the operation of the        |
|                           | touch-detection firmware algorithm are  |
|                           | called Threshold parameters.            |
|                           |                                         |
|                           | This parameter is a drop-down box to    |
|                           | select the tuning mode for CSD widgets  |
|                           | only.                                   |
|                           |                                         |
|                           | -  **SmartSense (Full Auto-Tune)** –    |
|                           |    This is the quickest way to tune a   |
|                           |    design. Most hardware and threshold  |
|                           |    parameters are automatically tuned   |
|                           |    by the middleware and the            |
|                           |    configurator GUI displays them as    |
|                           |    *Set by SmartSense* mode. In this    |
|                           |    mode, the following parameters are   |
|                           |    automatically tuned:                 |
|                           |                                         |
|                           |    -  *CSD Settings* tab: *Enable IDAC  |
|                           |       auto-calibration*                 |
|                           |                                         |
|                           |    -  *Widget Details* tab: The         |
|                           |       CSD-related parameters of the     |
|                           |       *Widget Hardware Parameters* and  |
|                           |       *Widget Threshold Parameters*     |
|                           |       groups                            |
|                           |                                         |
|                           |    -  *Widget Details* tab: the         |
|                           |       *Compensation IDAC value parameter|
|                           |       if *Enable compensation IDAC*     |
|                           |       is set.                           |
|                           |                                         |
|                           | -  **SmartSense (Hardware parameters    |
|                           |    only)** – The Hardware parameters    |
|                           |    are automatically set by the         |
|                           |    middleware. The Threshold parameters |
|                           |    are set manually by the user. This   |
|                           |    mode consumes less memory and less   |
|                           |    CPU processing time, this leads to   |
|                           |    consuming lower average power. In    |
|                           |    this mode, the following parameters  |
|                           |    are automatically tuned:             |
|                           |                                         |
|                           |    -  *CSD Settings* tab: *Enable IDAC  |
|                           |       auto-calibration*                 |
|                           |                                         |
|                           |    -  *Widget Details* tab: The         |
|                           |       CSD-related parameters of the     |    
|                           |       *Widget Hardware Parameters*      |
|                           |       group.                            |
|                           |                                         |
|                           |    -  *Widget Details* tab:             |
|                           |       *Compensation IDAC* value         |
|                           |       parameter if *Enable compensation |
|                           |       IDAC* is set.                     |
|                           |                                         |
|                           | -  **Manual** – The SmartSense          |
|                           |    auto-tuning is disabled, the         |
|                           |    **Widget Hardware Parameters** and   |
|                           |    **Widget Threshold Parameters** are  |
|                           |    tuned manually. The lowest memory    |
|                           |    and CPU process-time consumption.    |
|                           |                                         |
|                           | The SmartSense Auto-tuning (both Full   |
|                           | Auto-Tune and Hardware parameters only) |
|                           | supports the *IDAC Sourcing*            |
|                           | configuration only.                     |
|                           |                                         |
|                           | *SmartSense Auto-tuning* requires       |
|                           | Modulator clock frequency *Modulator    |
|                           | clock* set to 6000 kHz or higher.       |
|                           |                                         |
|                           | SmartSense operating conditions:        |
|                           |                                         |
|                           | -  Sensor capacitance Cp range 5 pF to  |
|                           |    61 pF                                |
|                           |                                         |
|                           | -  Maximum external series resistance   |
|                           |    on a sensor Rext < 1.1 kOhm          |
+---------------------------+-----------------------------------------+
| Widget Type               | A widget is one sensor or a group of    |
|                           | sensors that perform a specific         |
|                           | user-interface functionality. The       |
|                           | following widgets types consist:        |
|                           |                                         |
|                           | -  **Button** – One or more sensors.    |
|                           |    Each sensor in the widget can detect |
|                           |    the presence or absence (i.e. only   |
|                           |    two states) of a finger on the       |
|                           |    sensor.                              |
|                           |                                         |
|                           | -  **Linear Slider** – More than one    |
|                           |    sensor arranged in the specific      |
|                           |    order to detect the presence and     |
|                           |    movement of a finger on a linear     |
|                           |    axis. If a finger is present, Linear |
|                           |    Slider detects the physical position |
|                           |    (single axis position) of the        |
|                           |    finger.                              |
|                           |                                         |
|                           | -  **Radial Slider** – More than one    |
|                           |    sensor arranged in the circular      |
|                           |    order to detect the presence and     |
|                           |    radial movement of a finger. If a    |
|                           |    finger is present, the Radial Slider |
|                           |    detects the physical position of the |
|                           |    finger.                              |
|                           |                                         |
|                           | -  **Matrix Buttons** – Two or more     |
|                           |    sensors arranged in the specific     |
|                           |    horizontal and vertical order to     |
|                           |    detect the presence or absence of a  |
|                           |    finger on the intersections of       |
|                           |    vertically and horizontally arranged |
|                           |    sensors.                             |
|                           |                                         |
|                           |    If M and N are the numbers of the    |
|                           |    sensors in the horizontal and        |
|                           |    vertical axis respectively, the      |
|                           |    total of the M x N intersection      |
|                           |    positions can detect a finger touch. |
|                           |    When using the *CSD sensing method*, |
|                           |    a simultaneous finger touch on more  |
|                           |    than one intersection is invalid and |
|                           |    produces invalid results. This       |
|                           |    limitation does not apply when using |
|                           |    the *CSX sensing method* and all     |
|                           |    intersections can detect a valid     |
|                           |    touch simultaneously.                |
|                           |                                         |
|                           | -  **Touchpad** – Multiple sensors      |
|                           |    arranged in the specific horizontal  |
|                           |    and vertical order to detect the     |
|                           |    presence or absence of a human       |
|                           |    finger. If a finger is present, the  |
|                           |    widget will detect the physical      |
|                           |    position (both X and Y axis          |
|                           |    position) of the touch. The *CSD     |
|                           |    sensing method* supports detection   |
|                           |    of up to 2 simultaneous touches      |
|                           |    (when Advanced Centroid is enabled). |
|                           |    The *CSX sensing method* supports    |
|                           |    detection of up to 3 simultaneous    |
|                           |    finger touches.                      |
|                           |                                         |
|                           | -  **Proximity Sensor** – One or more   |
|                           |    sensors. Each sensor in the widget   |
|                           |    can detect the proximity of          |
|                           |    conductive objects, such as a human  |
|                           |    hand or finger to the sensors. The   |
|                           |    proximity sensor has two thresholds: |
|                           |                                         |
|                           |    -  *Proximity threshold* – To detect |
|                           |       an approaching hand or finger.    |
|                           |                                         |
|                           |    -  *Touch threshold* – To detect a   |
|                           |       finger touch on the sensor.       |
+---------------------------+-----------------------------------------+
| Widget Name               | A widget name can be defined to aid in  |
|                           | referring to a specific widget in a     |
|                           | design. A widget name does not affect   |
|                           | functionality or performance. A widget  |
|                           | name is used throughout source code to  |
|                           | generate macro definitions. A maximum   |
|                           | of 16 alphanumeric characters (the      |
|                           | first letter must be an alphabetic      |
|                           | character) is acceptable for a widget   |
|                           | name.                                   |
+---------------------------+-----------------------------------------+
| Sensing mode              | The parameter to select the sensing     |
|                           | mode for each widget:                   |
|                           |                                         |
|                           | -  **CSD** sensing method (Capacitive   |
|                           |    Sigma Delta) – A Cypress patented    |
|                           |    method of performing                 |
|                           |    self‑capacitance measurements. All   |
|                           |    widget types support the CSD         |
|                           |    sensing.                             |
|                           |                                         |
|                           | -  **CSX** sensing method – A Cypress   |
|                           |    patented method of performing        |
|                           |    mutual‑capacitance measurements;     |
|                           |    only buttons, matrix buttons, and    |
|                           |    touchpad widgets support CSX         |
|                           |    sensing.                             |
+---------------------------+-----------------------------------------+
| Widget Sensing Element(s) | A sensing element refers to the sensing |
|                           | terminals assigned to port pins to      |
|                           | connect to physical sensors on a        |
|                           | user-interface panel (such as a pad or  |
|                           | layer on a PCB, ITO, or FPCB).          |
|                           |                                         |
|                           | The following element numbers are       |
|                           | supported by the *CSD sensing method*:  |
|                           |                                         |
|                           | -  *Button* – Supports 1 to 32 sensors  |
|                           |    within a widget.                     |
|                           |                                         |
|                           | -  *Linear Slider* – Supports 3 to 32   |
|                           |    segments within a widget.            |
|                           |                                         |
|                           | -  *Radial Slider* – Supports 3 to 32   |
|                           |    segments within a widget.            |
|                           |                                         |
|                           | -  *Matrix Buttons* – Support 2 to 16   |
|                           |    rows and columns.                    |
|                           |                                         |
|                           | -  *Touchpad* – Supports 3 to 16 rows   |
|                           |    and columns.                         |
|                           |                                         |
|                           | -  *Proximity* –Supports 1 to 16        |
|                           |    sensors within a widget.             |
|                           |                                         |
|                           | The following element numbers are       |
|                           | supported by the *CSX sensing method*:  |
|                           |                                         |
|                           | -  *Button* – 1 to 32 Rx electrodes     |
|                           |    (for 1 to 32 sensors) and Tx is      |
|                           |    fixed to 1.                          |
|                           |                                         |
|                           | -  *Matrix Buttons* – 2 to 16 Tx and    |
|                           |    Rx.                                  |
|                           |                                         |
|                           | -  *Touchpad* – Supports 3 to 16 Tx and |
|                           |    Rx. The total intersections (node)   |
|                           |    number is equal to Tx × Rx. The      |
|                           |    maximum number of nodes is 256.      |
+---------------------------+-----------------------------------------+
| Finger capacitance        | Finger capacitance is defined as        |
|                           | capacitance introduced by a user touch  |
|                           | on the sensors. This parameter is used  |
|                           | to indicate how a sensitive CSD widget  |
|                           | is tuned by the *SmartSense             |
|                           | Auto-tuning* algorithm.                 |
|                           |                                         |
|                           | The supported Finger capacitance range: |
|                           |                                         |
|                           | -  *SmartSense (Full Auto-Tune)* mode – |
|                           |    0.1 pF to 1 pF.                      |
|                           |                                         |
|                           | -  *SmartSense (Hardware parameters     |
|                           |    only)* mode – 0.02 pF to 20.48 pF on |
|                           |    the exponential scale.               |
|                           |                                         |
|                           | CapSense sensor sensitivity is          |
|                           | inversely proportional to a finger      |
|                           | capacitance value. A smaller value of   |
|                           | finger capacitance provides higher      |
|                           | sensitivity for a sensor. To detect a   |
|                           | user touch on a thick overlay (4-mm     |
|                           | plastic overlay), finger capacitance is |
|                           | set to a small value, e.g. 0.1 pF. For  |
|                           | a sensor with a thin overlay or no      |
|                           | overlay, the 0.1 pF finger capacitance  |
|                           | setting makes the sensor too sensitive  |
|                           | and may cause false touches. For the    |
|                           | robust operation, it is important to    |
|                           | set the appropriate finger capacitance  |
|                           | value by considering the sensor size    |
|                           | and overlay thickness of the design.    |
|                           | Refer to the `CapSense design guide     |
|                           | <http://www.cypress.com/an85951>`__     |
|                           | for more information.                   |
+---------------------------+-----------------------------------------+
| Move up / Move down       | Moves the selected widget up or down by |
|                           | one on the list. It defines the widget  |
|                           | scanning order.                         |
+---------------------------+-----------------------------------------+
| Delete                    | Deletes the selected widget from the    |
|                           | list.                                   |
+---------------------------+-----------------------------------------+
| CSD electrodes            | Indicates the total number of           |
|                           | electrodes (port pins) used by the CSD  |
|                           | widgets.                                |
+---------------------------+-----------------------------------------+
| CSX electrodes            | Indicates the total number of           |
|                           | electrodes (port pins) used by the CSX  |
|                           | widgets.                                |
+---------------------------+-----------------------------------------+
| Pins required             | Indicates the total number of port pins |
|                           | required for the design. This does not  |
|                           | include port pins used by other         |
|                           | peripherals in the application or SWD   |
|                           | pins in Debug mode. **Pins required**   |
|                           | includes the number of CSD and CSX      |
|                           | electrodes, Cmod, Csh, Shield, CintA    |
|                           | and CintB electrodes.                   |
+---------------------------+-----------------------------------------+

Advanced Tab
------------

The **Advanced** tab provides advanced configuration parameters. In
*SmartSense Auto-tuning,* most of the advanced parameters are
automatically tuned by the algorithm and the user does not need to set
values for these parameters by the *Manual* tuning process. When Manual
tuning mode is selected, the **Advanced** tab allows the user to control
and configure the CapSense middleware parameters.

The parameters in the **Advanced** tab are systematically arranged in
the following sub-tabs.

-  *General* – Contains the parameters common for all widgets respective
   of the sensing method used for the widgets.

-  *CSD Settings* – Contains the parameters common for all widgets using
   the CSD sensing method. This tab is relevant only if one or more
   widgets use the CSD sensing method.

-  *CSX Settings* – Contains the parameters common for all widgets using
   the CSX sensing method. This tab is relevant only if one or more
   widgets use the CSX sensing method.

-  *Widget Details* – Contains parameters specific to widgets and/or
   sensors.

General Sub-tab
~~~~~~~~~~~~~~~

Contains the parameters common for all widgets respective of *Sensing
mode* used for widgets.

|image6|

The **General** sub-tab contains the following sections:

Regular widget raw count filter type
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Regular widget raw count filter type applies to raw counts of
sensors belonging to non-proximity widgets. These parameters can be
enabled only when one or more non-proximity widgets are added to the
**Basic** tab. The filter algorithm is executed when any processing
function is called by the application layer. When enabled, each filter
consumes RAM to store a previous raw count (filter history). If multiple
filters are enabled, the total filter history correspondingly increases
so that the size of the total filter history is equal to a sum of all
enabled filter histories.

+----------------------------------+----------------------------------+
| Name                             | Description                      |
+==================================+==================================+
| Enable IIR filter (First order)  | Enables the infinite-impulse     |
|                                  | response filter (See equation    |
|                                  | below) with a step response      |
|                                  | similar to an RC low-pass        |
|                                  | filter, thereby passing the      |
|                                  | low-frequency signals (finger    |
|                                  | touch responses).                |
|                                  |                                  |
|                                  | .. math::                        |
|                                  |                                  |
|                                  |    output = \frac{N}{K}          |
|                                  |    \times input +                |
|                                  |    \frac{K-N}{K} \times          |
|                                  |    previous Output               |
|                                  |                                  |
|                                  | where:                           |
|                                  |                                  |
|                                  | *K* is always 256.               |
|                                  |                                  |
|                                  | *N* is the IIR filter raw count  |
|                                  | coefficient selectable from 1 to |
|                                  | 128 in the configurator.         |
|                                  |                                  |
|                                  | A lower N (set in the *IIR       |
|                                  | filter raw count coefficient*    |
|                                  | parameter) results in lower      |
|                                  | noise, but slows down the        |
|                                  | response. This filter eliminates |
|                                  | high-frequency noise.            |
|                                  |                                  |
|                                  | Consumes 2 bytes of RAM per each |
|                                  | sensor to store a previous raw   |
|                                  | count (filter history).          |
+----------------------------------+----------------------------------+
| IIR filter raw count coefficient | The coefficient (N) of IIR       |
|                                  | filter for raw counts is         |
|                                  | explained in the *Enable IIR     |
|                                  | filter (First order)* parameter. |
|                                  |                                  |
|                                  | The range of valid values:       |
|                                  | 1-128.                           |
+----------------------------------+----------------------------------+
| Enable median filter (3-sample)  | Enables a non-linear filter that |
|                                  | takes three of most recent       |
|                                  | samples and computes the median  |
|                                  | value. This filter eliminates    |
|                                  | spike noise typically caused by  |
|                                  | motors and switching power       |
|                                  | supplies.                        |
|                                  |                                  |
|                                  | Consumes 4 bytes of RAM per each |
|                                  | sensor to store a previous raw   |
|                                  | count (filter history).          |
+----------------------------------+----------------------------------+
| Enable average filter (4-sample) | The finite-impulse response      |
|                                  | filter (no feedback) with        |
|                                  | equally weighted coefficients.   |
|                                  | It takes four of most recent     |
|                                  | samples and computes their       |
|                                  | average. Eliminates periodic     |
|                                  | noise (e.g. noise from AC        |
|                                  | mains).                          |
|                                  |                                  |
|                                  | Consumes 6 bytes of RAM per each |
|                                  | sensor to store a previous raw   |
|                                  | count (filter history).          |
+----------------------------------+----------------------------------+

.. note::
   If multiple filters are enabled, the execution order is the
   following:

   -  Median filter

   -  IIR filter

   -  Average filter

Proximity widget raw count filter type
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The proximity widget raw count filter applies to raw counts of sensors
belonging to the proximity widgets, these parameters can be enabled only
when one or more proximity widgets are added on the *Basic Tab*.

+----------------------------------+----------------------------------+
| Parameter Name                   | Description                      |
+==================================+==================================+
| Enable IIR filter (First order)  | The design of these parameters   |
|                                  | is the same as the *Regular      |
|                                  | widget raw count filter* type    |
|                                  | parameters. The *Proximity*      |
|                                  | sensors require high-noise       |
|                                  | reduction. These dedicated       |
|                                  | parameters allow for setting the |
|                                  | proximity filter configuration   |
|                                  | and behavior differently         |
|                                  | compared to other widgets.       |
+----------------------------------+----------------------------------+
| IIR filter raw count coefficient |                                  |
+----------------------------------+----------------------------------+
| Enable median filter (3-sample)  |                                  |
+----------------------------------+----------------------------------+
| Enable average filter (4-sample) |                                  |
+----------------------------------+----------------------------------+

Baseline filter settings
^^^^^^^^^^^^^^^^^^^^^^^^

Baseline filter settings are applied to all sensors baselines. But,
filter coefficients for the proximity and regular widgets can be
controlled independently from each other.

The design baseline IIR filter is the same as the raw count *Enable IIR
filter (First order)* parameter. But, filter coefficients can be
separate for both baseline filter and raw count filters to produce a
different roll-off. The baseline filter is applied to a filtered raw
count (if the widget raw count filters are enabled).

+----------------------------------+----------------------------------+
| Name                             | Description                      |
+==================================+==================================+
| Regular widget baseline          | Baseline IIR filter coefficient  |
| coefficient                      | selection for sensors in         |
|                                  | non-proximity widgets. The range |
|                                  | of valid values: 1-255.          |
+----------------------------------+----------------------------------+
| Proximity widget baseline        | The design of these parameters   |
| coefficient                      | is the same as the *Regular      |
|                                  | widget baseline* coefficient,    |
|                                  | but with a dedicated parameter   |
|                                  | allows controlling the baseline  |
|                                  | update-rate of the proximity     |
|                                  | sensors differently compared to  |
|                                  | other widgets.                   |
+----------------------------------+----------------------------------+

General settings
^^^^^^^^^^^^^^^^

The general settings are applicable to the whole CapSense middleware
behavior.

+-----------------------------+---------------------------------------+
| Name                        | Description                           |
+=============================+=======================================+
| Enable sensor auto‑reset    | When enabled, the baseline is always  |
|                             | updated and when disabled, the        |
|                             | baseline is updated only when the     |
|                             | difference between the baseline and   |
|                             | raw count is less than the noise      |
|                             | threshold.                            |
|                             |                                       |
|                             | When enabled, the feature prevents    |
|                             | the sensors from permanently turning  |
|                             | on when the raw count accidentally    |
|                             | rises due to a large power supply     |
|                             | voltage fluctuation or other spurious |
|                             | conditions.                           |
+-----------------------------+---------------------------------------+
| Enable self-test library    | The CapSense middleware provides the  |
|                             | **B**\ uilt-**I**\ n                  |
|                             | **S**\ elf-**T**\ est (BIST) library  |
|                             | to support the design compliant with  |
|                             | the safety-integrity level of Class B |
|                             | (IEC-60730) white goods and           |
|                             | automotive, and design for            |
|                             | manufacturing testing. The library    |
|                             | includes a set of tests for board     |
|                             | validation, middleware configuration, |
|                             | and operation. The feature includes   |
|                             | safety functions to reduce the risk,  |
|                             | validate boards at manufacturing, and |
|                             | verify the middleware operation at    |
|                             | run-time.                             |
|                             |                                       |
|                             | The BIST tests are classified into    |
|                             | two categories:                       |
|                             |                                       |
|                             | 1. HW Tests – To confirm the CSD      |
|                             | block and sensor hardware (external   |
|                             | to chip) function correctly:          |
|                             |                                       |
|                             | -  Chip analog-routing verification   |
|                             |                                       |
|                             | -  Pin faults checking                |
|                             |                                       |
|                             | -  PCB-trace opens / shorts checking  |
|                             |                                       |
|                             | -  External capacitors and sensors    |
|                             |    capacitance measurement            |
|                             |                                       |
|                             | -  VDDA measurement.                  |
|                             |                                       |
|                             | 2. FW Tests – To confirm the          |
|                             | integrity of data used for            |
|                             | decision-making on the sensor status: |
|                             |                                       |
|                             | -  Global and widget specific         |
|                             |    configuration verification         |
|                             |                                       |
|                             | -  Sensor baseline duplication        |
|                             |                                       |
|                             | -  Sensor raw count and baseline are  |
|                             |    in the specified range.            |
|                             |                                       |
|                             | The middleware is responsible for     |
|                             | running each test at start and        |
|                             | run-time as required by the product   |
|                             | requirements.                         |
|                             |                                       |
|                             | .. note::                             |
|                             |    -  If *SmartSense (Full Auto-Tune)*|
|                             |       is enabled, the self-test       |
|                             |       library cannot be enabled.      |
|                             |                                       |
|                             |    -  This option is supported by     |
|                             |       CapSense Middleware 2.1         |
|                             |       and later.                      |
+-----------------------------+---------------------------------------+
| Enable multi-frequency scan | The Multi-frequency scan (MFS)        |
|                             | provides superior immunity against    |
|                             | external noises and is suitable for   |
|                             | applications subjected to harsh       |
|                             | environments.                         |
|                             |                                       |
|                             | When the Multi-frequency scan is      |
|                             | enabled, each sensor is scanned three |
|                             | times with three different sensor     |
|                             | frequencies. The base frequency F0    |
|                             | (zero channel) is the nominal sensor  |
|                             | frequency. The second F1 and the      |
|                             | third F2 frequencies are obtained by  |
|                             | increasing the sense clock-divider by |
|                             | 1 and by 2 correspondingly:           |
|                             |                                       |
|                             | .. math::                             |
|                             |                                       |
|                             |    F_N = \frac{ModClk}{\frac{ModClk}  |
|                             |    {SnsClk} + N}                      |                                    
|                             |                                       |
|                             |                                       |
|                             |                                       |
|                             | where:                                |
|                             |                                       |
|                             | *N* is a frequency channel 0, 1, or   |
|                             | 2;                                    |
|                             |                                       |
|                             | F\ :sub:`N` is a scanning frequency;  |    
|                             |                                       |
|                             | *ModClk* is the CSD Modulator clock   |
|                             | frequency or CSX Modulator clock      |
|                             | frequency;                            |
|                             |                                       |
|                             | *SnsClk* is the CSD Sense clock       |
|                             | frequency or CSX Tx clock frequency.  |
|                             |                                       |
|                             | The *SmartSense (Full Auto-Tune)* and |
|                             | the Multi-frequency scan features are |
|                             | mutually exclusive. If the SmartSense |
|                             | (Full Auto-Tune) is enabled,          |
|                             | Multi-frequency scan cannot be        |
|                             | enabled.                              |
|                             |                                       |
|                             | .. note::                             |
|                             |                                       |
|                             |    -  Enabling the MFS increases RAM  |
|                             |       usage by three times            |
|                             |       approximately.                  |
|                             |                                       |
|                             | Enabling the MFS increases the sensor |
|                             | scan duration by three times.         |
+-----------------------------+---------------------------------------+

CSD Settings Sub-tab
~~~~~~~~~~~~~~~~~~~~

Contains the parameters common for all widgets using the *CSD sensing
method*, is relevant only if at least one widget uses the CSD sensing
method.

|image7|

The **CSD Settings** sub-tab contains the following parameters:

+----------------------------------+----------------------------------+
| Name                             | Description                      |
+==================================+==================================+
| Modulator clock divider          | Selects the modulator clock      |
|                                  | divider used for the *CSD        |
|                                  | sensing method*. It defines the  |
|                                  | operating frequency of the CSD   |
|                                  | block.                           |
+----------------------------------+----------------------------------+
| Actual modulator clock frequency | The modulator clock frequency    |
| (kHz)                            | depends on the CSD peripheral    |
|                                  | clock frequency and the          |
|                                  | modulator clock divider. The     |
|                                  | read-only value is displayed     |
|                                  | only when the CapSense           |
|                                  | Configurator is launched from    |
|                                  | the Device Configurator.         |
+----------------------------------+----------------------------------+
| Inactive sensor connection       | Selects the state of the sensor  |
|                                  | when it is not scanned.          |
|                                  |                                  |
|                                  | -  **Ground (default)** –        |
|                                  |    Inactive sensors are          |
|                                  |    connected to the ground.      |
|                                  |                                  |
|                                  | -  **High-Z** – Inactive sensors |
|                                  |    are floating (not connected   |
|                                  |    to GND or Shield).            |
|                                  |                                  |
|                                  | -  **Shield** - Inactive sensors |
|                                  |    are connected to Shield. This |
|                                  |    option is available only if   |
|                                  |    the *Enable shield electrode* |
|                                  |    check box is set.             |
|                                  |                                  |
|                                  | **Ground** is the recommended    |
|                                  | selection for this parameter     |
|                                  | when water tolerance is not      |
|                                  | required for the design. Select  |
|                                  | Shield when the design needs     |
|                                  | water tolerance or to reduce the |
|                                  | sensor parasitic capacitance in  |
|                                  | the design.                      |
+----------------------------------+----------------------------------+
| IDAC sensing configuration       | Selects the type of IDAC         |
|                                  | switching:                       |
|                                  |                                  |
|                                  | -  **IDAC Sourcing (default)** – |
|                                  |    Sources current into the      |
|                                  |    modulator capacitor (Cmod).   |
|                                  |    The analog switches are       |
|                                  |    configured to alternate       |
|                                  |    between the Cmod and GND.     |
|                                  |    IDAC Sourcing is recommended  |
|                                  |    for most designs because of   |
|                                  |    the better signal-to-noise    |
|                                  |    ratio                         |
|                                  |                                  |
|                                  | -  **IDAC sinking** – Sinks      |
|                                  |    current from the modulator    |
|                                  |    capacitor (Cmod). The analog  |
|                                  |    switches are configured to    |
|                                  |    alternate between             |
|                                  |    V\ :sub:`DD` and Cmod.        |
+----------------------------------+----------------------------------+
| Enable IDAC auto-calibration     | When enabled, values of the CSD  |
|                                  | widget IDACs are automatically   |
|                                  | set by the middleware. Select    |
|                                  | the Enable IDAC Auto-calibration |
|                                  | parameter for robust operation.  |
|                                  | The *SmartSense Auto-tuning*     |
|                                  | parameter can be enabled only    |
|                                  | when the Enable IDAC             |
|                                  | auto-calibration is selected.    |
+----------------------------------+----------------------------------+
| Enable compensation IDAC         | The compensation IDAC is used to |
|                                  | compensate for sensor parasitic  |
|                                  | capacitance to improve           |
|                                  | performance. Enabling the        |
|                                  | compensation IDAC is recommended |
|                                  | unless one IDAC is required for  |
|                                  | general purpose (other than      |
|                                  | CapSense) in the application.    |
+----------------------------------+----------------------------------+
| Enable shield electrode          | The shield electrode is used to  |
|                                  | reduce the sensor parasitic      |
|                                  | capacitance, enable              |
|                                  | water-tolerant CapSense designs  |
|                                  | and enhance the detection range  |
|                                  | for the *Proximity* sensors.     |
|                                  | When the shield electrode is     |
|                                  | disabled, configurable           |
|                                  | parameters associated with the   |
|                                  | shield electrode are hidden.     |
+----------------------------------+----------------------------------+
| Enable shield tank (Csh)         | The shield tank capacitor is     |
| capacitor                        | used to increase the drive       |
|                                  | capacity of the shield electrode |
|                                  | driver. It should be enabled     |
|                                  | when the shield electrode        |
|                                  | capacitance is higher than 100   |
|                                  | pF. The recommended value for a  |
|                                  | shield tank capacitor is         |
|                                  | 10nF/5V/X7R or an NP0 capacitor. |
|                                  |                                  |
|                                  | The shield tank capacitor is not |
|                                  | supported in configuration which |
|                                  | includes both CSD and CSX        |
|                                  | sensing-based widgets.           |
+----------------------------------+----------------------------------+
| Shield SW resistance             | Selects the resistance of        |
|                                  | switches used to drive the       |
|                                  | shield electrode. The four       |
|                                  | options:                         |
|                                  |                                  |
|                                  | -  **Low**                       |
|                                  |                                  |
|                                  | -  **Medium (default)**          |
|                                  |                                  |
|                                  | -  **High**                      |
|                                  |                                  |
|                                  | -  **Low EMI**                   |
+----------------------------------+----------------------------------+
| Total shield count               | Selects the number of shield     |
|                                  | electrodes required in the       |
|                                  | design.                          |
|                                  |                                  |
|                                  | Most designs work with one       |
|                                  | dedicated shield electrode but,  |
|                                  | some designs require multiple    |
|                                  | dedicated shield electrodes to   |
|                                  | ease the PCB layout routing or   |
|                                  | to minimize the PCB area used    |
|                                  | for the shield layer.            |
|                                  |                                  |
|                                  | The minimum value is 0 (i.e.     |
|                                  | shield signal could be routed to |
|                                  | sensors using the *Inactive      |
|                                  | sensor connection* parameter)    |
|                                  | and the maximum value is equal   |
|                                  | to the total number of           |
|                                  | CapSense-enabled port pins       |
|                                  | available for the selected       |
|                                  | device.                          |
+----------------------------------+----------------------------------+

CSX Settings Sub-tab
~~~~~~~~~~~~~~~~~~~~

| The parameters in this sub-tab apply to all widgets that use the *CSX
  sensing method*, is relevant only if at least one widget uses the CSX
  sensing method.
| |image8|

The **CSX Settings** sub-tab contains the following parameters:

+----------------------------------+----------------------------------+
| Name                             | Description                      |
+==================================+==================================+
| Modulator clock divider          | Selects the modulator clock      |
|                                  | divider used for the *CSX        |
|                                  | sensing method*. It defines the  |
|                                  | operating frequency of the CSD   |
|                                  | block.                           |
|                                  |                                  |
|                                  | A higher modulator clock         |
|                                  | frequency reduces the sensor     |
|                                  | scan time, results in lower      |
|                                  | power, and reduces the noise in  |
|                                  | raw counts, so use the highest   |
|                                  | possible frequency.              |
+----------------------------------+----------------------------------+
| Actual modulator clock frequency | The modulator clock frequency    |
| (kHz)                            | depends on the CSX peripheral    |
|                                  | clock frequency and the          |
|                                  | modulator clock divider. The     |
|                                  | read-only value is displayed     |
|                                  | only when the CapSense           |
|                                  | Configurator is launched from    |
|                                  | the Device Configurator.         |
+----------------------------------+----------------------------------+
| Number of reported fingers       | Sets the number of reported      |
|                                  | fingers for a CSX Touchpad       |
|                                  | widget only. The available       |
|                                  | options are from 1 to 3.         |
+----------------------------------+----------------------------------+
| Enable IDAC auto-calibration     | When enabled, IDAC values are    |
|                                  | automatically set by the         |
|                                  | middleware. It is recommended to |
|                                  | select the Enable IDAC           |
|                                  | auto-calibration for robust      |
|                                  | operation.                       |
+----------------------------------+----------------------------------+

Widget Details Sub-tab
~~~~~~~~~~~~~~~~~~~~~~

This sub-tab contains parameters specific to each widget and sensor.
These parameters must be set when *SmartSense Auto-tuning* is not
enabled. The parameters are unique for each widget type.

|image9|

The **Widget Details** sub-tab contains the following parameters:


+----------------------------------+----------------------------------+
| Name                             | Description                      |
+==================================+==================================+
| **Widget General Parameters**                                       |
+----------------------------------+----------------------------------+
| Diplexing                        | Enabling Diplexing allows        |
|                                  | doubling the slider physical     |
|                                  | touch sensing area by using the  |
|                                  | specific duplexing sensor        |
|                                  | pattern and without using        |
|                                  | additional port pins and         |
|                                  | sensors.                         |
+----------------------------------+----------------------------------+
| Maximum position                 | Represents the maximum Centroid  |
|                                  | position for the slider. A touch |
|                                  | on the slider would produce a    |
|                                  | position value from 0 to the     |
|                                  | maximum position-value set. **No |
|                                  | Touch** would produce 0x0000.    |
+----------------------------------+----------------------------------+
| Maximum X-axis position          | Represents the maximum column    |
|                                  | (X-axis) Centroid position and   |
|                                  | row (Y-axis) Centroid positions  |
|                                  | for a touchpad. A touch on the   |
|                                  | touchpad would produce a         |
|                                  | position value from 0 to the     |
|                                  | maximum position set. **No       |
|                                  | Touch** would produce 0x0000.    |
+----------------------------------+----------------------------------+
| Maximum Y-axis position          |                                  |
+----------------------------------+----------------------------------+
| **Widget Hardware Parameters**                                      |
|                                                                     |
| .. note::                                                           |
|    All the Widget Hardware parameters for the CSD widgets are       |
|    automatically set when *SmartSense Auto-tuning* is               |
|    selected in the *CSD tuning mode*.                               |
+----------------------------------+----------------------------------+
| Sense clock divider              | Sets the CSD Sense clock         |
|                                  | divider.                         |
|                                  |                                  |
|                                  | When SmartSense is selected in   |
|                                  | *CSD tuning mode*, the Sense     |
|                                  | Clock divider is automatically   |
|                                  | set by the middleware to an      |
|                                  | optimal value by following the   |
|                                  | 2*5*R*C rule (refer to `CapSense |
|                                  | design                           |
|                                  | guide <ht                        |
|                                  | tp://www.cypress.com/an85951>`__ |
|                                  | for more information on this     |
|                                  | rule) and this control is not    |
|                                  | editable.                        |
+----------------------------------+----------------------------------+
| Row sense clock divider          | Sets the CSD Sense clock divider |
|                                  | for row and column sensors of    |
|                                  | the *Matrix Buttons* and         |
|                                  | *Touchpad* widgets.              |
+----------------------------------+                                  |
| Column sense clock divider       |                                  |
+----------------------------------+----------------------------------+
| Tx clock divider                 | Sets the Tx Clock divider for    |
|                                  | the CSX widgets.                 |
+----------------------------------+----------------------------------+
| Sense clock source               | The Sense clock frequency is     |
|                                  | derived from the Modulator clock |
|                                  | frequency using a clock-divider  |
|                                  | and is used to sample the        |
|                                  | sensor. Both the clock source    |
|                                  | and clock divider are            |
|                                  | configurable.                    |
|                                  |                                  |
|                                  | The Spread Spectrum Clock (SSC)  |
|                                  | provides a dithering clock       |
|                                  | source with a center frequency   |
|                                  | equal to the Sense clock         |
|                                  | frequency. The PRS clock source  |
|                                  | spreads the clock using the      |
|                                  | pseudo-random sequencer and the  |
|                                  | Direct source disables both SSC  |
|                                  | and PRS sources and uses a       |
|                                  | fixed-frequency clock.           |
|                                  |                                  |
|                                  | Both PRS and SSC reduce the      |
|                                  | radiated noise by spreading the  |
|                                  | clock and improve the immunity   |
|                                  | against external noise. Using a  |
|                                  | higher number of bits of SSC and |
|                                  | PRS lowers the radiation and     |
|                                  | increases the immunity against   |
|                                  | external noise.                  |
|                                  |                                  |
|                                  | The following sources are        |
|                                  | available:                       |
|                                  |                                  |
|                                  | *Direct* – PRS and SSC are       |
|                                  | disabled and a fixed clock is    |
|                                  | used.                            |
|                                  |                                  |
|                                  | *PRS8* – The clock spreads using |
|                                  | PRS to Modulator Clock / 256.    |
|                                  |                                  |
|                                  | *PRS12* – The clock spreads      |
|                                  | using PRS to Modulator Clock /   |
|                                  | 4096.                            |
|                                  |                                  |
|                                  | *SSC6, SSC7, SSC9* and *SSC10* – |
|                                  | The clock spreads using from 6   |
|                                  | to 10 bits of the sense-clock    |
|                                  | divider respectively.            |
|                                  |                                  |
|                                  | *Auto* – The middleware          |
|                                  | automatically selects optimal    |
|                                  | SSC, PRS or Direct sources       |
|                                  | individually for each widget.    |
|                                  | The Auto is the recommended      |
|                                  | sense clock source selection.    |
|                                  |                                  |
|                                  | The following rules and          |
|                                  | recommendations for the SSC      |
|                                  | selection:                       |
|                                  |                                  |
|                                  | The ratio between the Modulator  |
|                                  | clock frequencyModulator clock   |
|                                  | and Sense clock frequency must   |
|                                  | be greater than or equal to 20.  |
|                                  |                                  |
|                                  | 20% of the ratio between the     |
|                                  | Modulator clock                  |
|                                  | frequencyModulator clock and     |
|                                  | Sense clock frequency should be  |
|                                  | greater or equal to the SSC      |
|                                  | frequency range = 32. It allows  |
|                                  | varying the ratio between the    |
|                                  | Modulator and Sense clock        |
|                                  | frequencies to 32 different      |
|                                  | clocks evenly spaced over +/-    |
|                                  | 10% from the center frequency.   |
|                                  |                                  |
|                                  | .. math::                        |
|                                  |                                  |
|                                  |    160 \leq \frac{ModClk}{SnsClk}|
|                                  |                                  |
|                                  | Where *ModClk* is the Modulator  |
|                                  | clock frequency and *SnsClk* is  |
|                                  | Sense clock frequency.           |
|                                  |                                  |
|                                  | It is recommended that at least  |
|                                  | one full-spread spectrum         |
|                                  | polynomial should end during the |
|                                  | scan time:                       |
|                                  |                                  |
|                                  | .. math::                        |
|                                  |                                  |
|                                  |    \frac{2^N -1}{ModClk} \geq    |
|                                  |    \frac{2^{SSCN} -1}{SnsClk}    |
|                                  |                                  |
|                                  |                                  |
|                                  | where *N* is the *Scan           |
|                                  | resolution*, *SSCN* is the       |
|                                  | number of bits used for SSC (6,  |
|                                  | 7, 9 and 10),                    |
|                                  |                                  |
|                                  | *ModClk* is Modulator clock      |
|                                  | frequency and *SnsClk* is Sense  |
|                                  | clock frequency.                 |
|                                  |                                  |
|                                  | It is recommended that the       |
|                                  | number of sub-conversions for    |
|                                  | the widget should be an integer  |
|                                  | multiple of the SSC polynomial   |
|                                  | selected. For example, if SSC6   |
|                                  | is selected, the number of the   |
|                                  | sub-conversion should be         |
|                                  | multiple of (2\ :sup:`SSC6`-1) = |
|                                  | 63.                              |
|                                  |                                  |
|                                  | The recommendation for the PRS   |
|                                  | selection:                       |
|                                  |                                  |
|                                  | At least one full PRS polynomial |
|                                  | should finish during the scan    |
|                                  | time:                            |
|                                  |                                  |
|                                  | .. math::                        |
|                                  |                                  |
|                                  |    \frac{2^N -1}{ModClk} \geq    |
|                                  |    \frac{2^{PRSN} -1}{SnsClk}    |
|                                  |                                  |
|                                  |                                  |
|                                  | where *N* is the *Scan           |
|                                  | resolution*, *PRSN* is the       |
|                                  | number of bits used for PRS (8   |
|                                  | and 12),                         |
|                                  |                                  |
|                                  | *ModClk* is the Modulator clock  |
|                                  | frequency Modulator clock and    |
|                                  | *SnsClk* is the average Sense    |
|                                  | clock frequency.                 |
+----------------------------------+----------------------------------+
| Tx clock source                  | The Tx clock frequency derives   |
|                                  | from the Modulator clock         |
|                                  | frequency using a clock-divider  |
|                                  | and is used to sample the        |
|                                  | sensor. Both the clock source    |
|                                  | and clock divider are            |
|                                  | configurable.                    |
|                                  |                                  |
|                                  | The Spread Spectrum Clock (SSC)  |
|                                  | provides a dithering clock       |
|                                  | source with a center frequency   |
|                                  | equal to the Tx clock frequency  |
|                                  | and the Direct source disables   |
|                                  | the SSC source and uses a fixed  |
|                                  | frequency clock. The SSC reduces |
|                                  | the radiated noise by spreading  |
|                                  | the clock and improves the       |
|                                  | immunity against external noise. |
|                                  | Using a higher number of bits of |
|                                  | SSC lowers the radiation and     |
|                                  | increases the immunity against   |
|                                  | external noise.                  |
|                                  |                                  |
|                                  | The following clock sources are  |
|                                  | available:                       |
|                                  |                                  |
|                                  | *Direct* – SSC is disabled and a |
|                                  | fixed clock is used.             |
|                                  |                                  |
|                                  | *SSC6, SSC7, SSC9* and *SSC10* – |
|                                  | The clock spreads using from 6   |
|                                  | to 10 bits of the sense-clock    |
|                                  | divider respectively.            |
|                                  |                                  |
|                                  | *Auto* – The middleware          |
|                                  | automatically selects optimal    |
|                                  | SSC or Direct sources            |
|                                  | individually for each widget.    |
|                                  | Auto is the recommended Sense    |
|                                  | clock source selection.          |
|                                  |                                  |
|                                  | The rules and recommendations    |
|                                  | for the SSC selection:           |
|                                  |                                  |
|                                  | The ratio between the Modulator  |
|                                  | clock frequency and Tx clock     |
|                                  | frequency must be greater than   |
|                                  | or equal to 20.                  |
|                                  |                                  |
|                                  | 20% of the ratio between the     |
|                                  | Modulator clock frequency and Tx |
|                                  | clock frequency should be        |
|                                  | greater or equal to the SSC      |
|                                  | frequency range = 32. It allows  |
|                                  | varying the ratio between the    |
|                                  | Modulator and Tx clock           |
|                                  | frequencies to 32 different      |
|                                  | clocks evenly spaced over +/-    |
|                                  | 10% from the center frequency.   |
|                                  |                                  |
|                                  | .. math::                        |
|                                  |                                  |
|                                  |    160 \leq \frac{ModClk}        |                              
|                                  |    {TxClk}                       |
|                                  |                                  |
|                                  | where *ModClk* is the Modulator  |
|                                  | clock frequency and *TxClk* is   |
|                                  | Tx clock frequency.              |
|                                  |                                  |
|                                  | It is recommended that at least  |
|                                  | one full-spread spectrum         |
|                                  | polynomial should end during the |
|                                  | scan time.                       |
|                                  |                                  |
|                                  | .. math::                        |
|                                  |                                  |
|                                  |    \frac{N_{sub}}{ModClk} \geq   |
|                                  |    \frac{2^{SSCN} - 1}{TxClk}    |
|                                  |                                  |
|                                  | where *N\ Sub* is the *Number of |
|                                  | sub-conversions*, *SSCN* is the  |
|                                  | number of bits used for SSC (6,  |
|                                  | 7, 9 and 10), *ModClk* is the    |
|                                  | Modulator clock                  |
|                                  | frequencyModulator clock and     |
|                                  | *TxClk* is the Tx clock          |
|                                  | frequency.                       |
|                                  |                                  |
|                                  | It is recommended that *Number   |
|                                  | of sub-conversions* for the      |
|                                  | widget should be an integer      |
|                                  | multiple of the SSC polynomial   |
|                                  | selected. For example, if SSC6   |
|                                  | is selected, the number of       |
|                                  | sub-conversion should be         |
|                                  | multiple of (2\ :sup:`SSC6`-1) = |
|                                  | 63.                              |
+----------------------------------+----------------------------------+
| Scan resolution                  | Selects the scan resolution of   |
|                                  | the CSD widgets (Resolution of   |
|                                  | capacitance to digital           |
|                                  | conversion). Acceptable values   |
|                                  | are from 6 to 16 bits.           |
+----------------------------------+----------------------------------+
| Number of sub-conversions        | Selects the number of            |
|                                  | sub-conversions in the *CSX      |
|                                  | sensing method*.                 |
+----------------------------------+----------------------------------+
| Modulator IDAC                   | Sets the modulator IDAC value    |
|                                  | for the CSD Button, Slider, or   |
|                                  | Proximity widget.                |
|                                  |                                  |
|                                  | The value of this parameter is   |
|                                  | automatically set when *Enable   |
|                                  | IDAC auto-calibration* is        |
|                                  | selected in the *CSD Settings*   |
|                                  | tab.                             |
+----------------------------------+----------------------------------+
| Row modulator IDAC               | Sets a separate modulator IDAC   |
|                                  | value for the row and column     |
|                                  | sensors of the CSD *Matrix       |
|                                  | Buttons* and *Touchpad* widget.  |
|                                  |                                  |
|                                  | These parameters values are      |
|                                  | automatically set when *Enable   |
|                                  | IDAC auto-calibration* is        |
|                                  | checked in the *CSD Settings*    |
|                                  | tab.                             |
+----------------------------------+                                  |
| Column modulator IDAC            |                                  |
+----------------------------------+----------------------------------+
| IDAC gain index                  | Sets the IDAC gain index.        |
|                                  | Options include:                 |
|                                  |                                  |
|                                  | -  Index 0 – 37.5 nA             |
|                                  |                                  |
|                                  | -  Index 1 - 75 nA               |
|                                  |                                  |
|                                  | -  Index 2 - 300 nA (default for |
|                                  |    CSX widgets)                  |
|                                  |                                  |
|                                  | -  Index 3 - 600 nA              |
|                                  |                                  |
|                                  | -  Index 4 - 2400 nA (default    |
|                                  |    for CSD widgets)              |
|                                  |                                  |
|                                  | -  Index 5 - 4800 nA             |
|                                  |                                  |
|                                  | The value of this parameter is   |
|                                  | automatically set when Enable    |
|                                  | IDAC auto-calibration is         |
|                                  | selected.                        |
+----------------------------------+----------------------------------+
| **Widget Threshold Parameters**                                     |
|                                                                     |
| .. note::                                                           |
|    All the threshold parameters for the CSD widge                   |
|    are automatically set when                                       |
|    *SmartSense (Full Auto-Tune)* is                                 |
|    selected in the *CSD tuning                                      |
|    mode* parameter.                                                 |
+----------------------------------+----------------------------------+
| Finger threshold                 | The finger threshold parameter   |
|                                  | is used along with the           |
|                                  | hysteresis parameter to          |
|                                  | determine the sensor state as    |
|                                  | follows:                         |
|                                  |                                  |
|                                  | -  ON – Signal > (Finger         |
|                                  |    Threshold + Hysteresis)       |
|                                  |                                  |
|                                  | -  OFF – Signal ≤ (Finger        |
|                                  |    Threshold – Hysteresis).      |
|                                  |                                  |
|                                  | Note that “Signal” in the above  |
|                                  | equations refers to:             |
|                                  |                                  |
|                                  | Difference Count = Raw Count –   |
|                                  | Baseline.                        |
|                                  |                                  |
|                                  | It is recommended to set the     |
|                                  | Finger threshold parameter value |
|                                  | equal to the 80% of the touch    |
|                                  | signal.                          |
|                                  |                                  |
|                                  | The Finger Threshold parameter   |
|                                  | is not available for the         |
|                                  | *Proximity* widget. Instead,     |
|                                  | Proximity has two thresholds:    |
|                                  |                                  |
|                                  | -  *Proximity threshold*         |
|                                  |                                  |
|                                  | -  *Touch threshold*             |
+----------------------------------+----------------------------------+
| Noise threshold                  | Sets a raw count limit below     |
|                                  | which a raw count is considered  |
|                                  | as noise. When a raw count is    |
|                                  | above the Noise Threshold, a     |
|                                  | difference count is produced and |
|                                  | the baseline is updated only if  |
|                                  | *Enable sensor auto‑reset* is    |
|                                  | selected (In other words, the    |
|                                  | baseline remains constant as     |
|                                  | long as the raw count is above   |
|                                  | the baseline + noise threshold.  |
|                                  | This prevents the baseline from  |
|                                  | following raw counts during a    |
|                                  | finger touch detection event).   |
|                                  |                                  |
|                                  | It is recommended to set the     |
|                                  | noise threshold parameter value  |
|                                  | equal to 2x noise in the raw     |
|                                  | count or the 40% of the signal.  |
+----------------------------------+----------------------------------+
| Negative noise threshold         | Sets a raw count limit below     |
|                                  | which the baseline is not        |
|                                  | updated for the number of        |
|                                  | samples specified by the *Low    |
|                                  | baseline reset* parameter.       |
|                                  |                                  |
|                                  | The negative noise threshold     |
|                                  | ensures that the baseline does   |
|                                  | not fall low because of any      |
|                                  | high-amplitude repeated          |
|                                  | negative-noise spikes on a raw   |
|                                  | count caused by different noise  |
|                                  | sources such as ESD events.      |
|                                  |                                  |
|                                  | It is recommended to set the     |
|                                  | negative noise threshold         |
|                                  | parameter value equal to the     |
|                                  | *Noise threshold* parameter      |
|                                  | value.                           |
+----------------------------------+----------------------------------+
| Low baseline reset               | This parameter is used along     |
|                                  | with the *Negative noise         |
|                                  | threshold* parameter. It counts  |
|                                  | the number of abnormally low raw |
|                                  | counts required to reset the     |
|                                  | baseline.                        |
|                                  |                                  |
|                                  | If a finger is placed on the     |
|                                  | sensor during a device startup,  |
|                                  | the baseline gets initialized to |
|                                  | a high raw count value at a      |
|                                  | startup. When the finger is      |
|                                  | removed, the raw count falls to  |
|                                  | a lower value. In this case, the |
|                                  | baseline should track low raw    |
|                                  | counts. The Low Baseline Reset   |
|                                  | parameter helps handle this      |
|                                  | event. It resets the baseline to |
|                                  | a low raw count value when the   |
|                                  | number of low samples reaches    |
|                                  | the low-baseline reset number.   |
|                                  |                                  |
|                                  | .. note::                        |
|                                  |    After a finger is removed     |
|                                  |    from the sensor, the          |
|                                  |    sensor will not respond to    |
|                                  |    finger touches for low        |
|                                  |    baseline-reset time.          |
|                                  |                                  |
|                                  | The recommended value is 30      |
|                                  | which works for most designs.    |
+----------------------------------+----------------------------------+
| Hysteresis                       | The hysteresis parameter is used |
|                                  | along with the *Finger           |
|                                  | threshold* parameter (*Proximity |
|                                  | threshold* and *Touch threshold* |
|                                  | for Proximity sensor) to         |
|                                  | determine the sensor state. The  |
|                                  | hysteresis provides immunity     |
|                                  | against noisy transitions of the |
|                                  | sensor state.                    |
|                                  |                                  |
|                                  | See the description of the       |
|                                  | *Finger threshold* parameter for |
|                                  | details.                         |
|                                  |                                  |
|                                  | The recommend value for the      |
|                                  | hysteresis is the 10% *Finger    |
|                                  | threshold*.                      |
+----------------------------------+----------------------------------+
| ON debounce                      | Selects a number of consecutive  |
|                                  | CapSense scans during which a    |
|                                  | sensor must be active to         |
|                                  | generate an ON state from the    |
|                                  | middleware. The Debounce ensures |
|                                  | that high-frequency,             |
|                                  | high-amplitude noise does not    |
|                                  | cause false detection            |
|                                  |                                  |
|                                  | -  Buttons/Matrix                |
|                                  |    buttons/Proximity – An ON     |
|                                  |    status is reported only when  |
|                                  |    the sensor is touched for a   |
|                                  |    consecutive debounce number   |
|                                  |    of samples.                   |
|                                  |                                  |
|                                  | -  Sliders/Touchpads – The       |
|                                  |    position status is reported   |
|                                  |    only when any of the sensors  |
|                                  |    is touched for a consecutive  |
|                                  |    debounce number of samples.   |
|                                  |                                  |
|                                  | The recommended value for the    |
|                                  | Debounce parameter is 3 for      |
|                                  | reliable sensor status           |
|                                  | detection.                       |
+----------------------------------+----------------------------------+
| Proximity threshold              | The design of these parameters   |
|                                  | is the same as for the *Finger   |
|                                  | threshold* parameters. The       |
|                                  | proximity sensor requires a      |
|                                  | higher noise reduction, and      |
|                                  | supports two levels of           |
|                                  | detection:                       |
|                                  |                                  |
|                                  | -  The proximity level to detect |
|                                  |    an approaching hand or        |
|                                  |    finger.                       |
|                                  |                                  |
|                                  | -  The touch level to detect a   |
|                                  |    finger touch on the sensor    |
|                                  |    similarly to other *Widget    |
|                                  |    Type* sensors.                |
|                                  |                                  |
|                                  | Note that for valid operation,   |
|                                  | the Proximity threshold must be  |
|                                  | higher than the Touch threshold. |
|                                  |                                  |
|                                  | The threshold parameters such as |
|                                  | *Hysteresis* and *ON* debounce   |
|                                  | are applicable to both detection |
|                                  | levels.                          |
+----------------------------------+                                  |
| Touch threshold                  |                                  |
+----------------------------------+----------------------------------+
| Velocity                         | Defines the maximum speed of a   |
|                                  | finger movement in terms of the  |
|                                  | squared distance of the touchpad |
|                                  | resolution. The parameter is     |
|                                  | applicable for a multi-touch     |
|                                  | touchpad (CSX Touchpad) only. If |
|                                  | the detected position of the     |
|                                  | next scan is further than the    |
|                                  | defined squared distance, then   |
|                                  | this touch is considered as a    |
|                                  | separate touch with a new touch  |
|                                  | ID.                              |
+----------------------------------+----------------------------------+
| **Position Filter Parameters**                                      |
|                                                                     |
| These parameters enable firmware                                    |
| filters on a centroid position                                      |
| to reduce noise. These filters                                      |
| are available for Slider and                                        |
| Touchpad widgets only. If                                           |
| multiple filters are enabled,                                       |
| the execution order corresponds                                     |
| to the listed below and the                                         |
| total RAM consumption increases                                     |
| so that the size of the total                                       |
| filter history is equal to a sum                                    |
| of all enabled filter histories.                                    |
+----------------------------------+----------------------------------+
| IIR filter                       | Enables the infinite-impulse     |
|                                  | response filter (see equation    |
|                                  | below) with a step response.     |
|                                  |                                  |
|                                  | .. math::                        |
|                                  |                                  |
|                                  |    Output = \frac{N}{K}          |
|                                  |    \times Input +\frac{K-N}      |                              
|                                  |    {K} \times prev Output        |
|                                  |                                  |
|                                  | where:                           |
|                                  |                                  |
|                                  | *K* is always 256;               |
|                                  |                                  |
|                                  | *N* is the IIR filter raw count  |
|                                  | coefficient selectable from 1 to |
|                                  | 255 in the configurator.         |
|                                  |                                  |
|                                  | A lower N (set in the *IIR       |
|                                  | filter coefficient* parameter)   |
|                                  | results in lower noise, but      |
|                                  | slows down the response. This    |
|                                  | filter eliminates high-frequency |
|                                  | noise.                           |
|                                  |                                  |
|                                  | Consumes 2 bytes of RAM per each |
|                                  | position (filter history).       |
+----------------------------------+----------------------------------+
| IIR filter coefficient           | The coefficient (N) of the IIR   |
|                                  | filter for a position as         |
|                                  | explained in the *IIR filter*    |
|                                  | parameter.                       |
|                                  |                                  |
|                                  | The range of valid values:       |
|                                  | 1-255.                           |
+----------------------------------+----------------------------------+
| Median filter                    | Enables a non-linear filter that |
|                                  | takes three of most recent       |
|                                  | samples and computes the median  |
|                                  | value. This filter eliminates    |
|                                  | the spikes noise typically       |
|                                  | caused by motors and switching   |
|                                  | power supplies. Consumes 4 bytes |
|                                  | of RAM per each position (filter |
|                                  | history).                        |
+----------------------------------+----------------------------------+
| Average filter                   | Enables the finite-impulse       |
|                                  | response filter (no feedback)    |
|                                  | with equally weighted            |
|                                  | coefficients. It takes two of    |
|                                  | most recent samples and computes |
|                                  | their average. Eliminates        |
|                                  | periodic noise (e.g. noise from  |
|                                  | AC mains). Consumes 2 bytes of   |
|                                  | RAM per each position (filter    |
|                                  | history).                        |
+----------------------------------+----------------------------------+
| Jitter filter                    | This filter eliminates the noise |
|                                  | in the position data that        |
|                                  | toggles between the two most     |
|                                  | recent values. If the most       |
|                                  | recent position value is greater |
|                                  | than the previous one, the       |
|                                  | current position is decremented  |
|                                  | by 1; if it is less, the current |
|                                  | position is incremented by 1.    |
|                                  | The filter is most effective at  |
|                                  | low noise. Consumes 2 bytes of   |
|                                  | RAM per each position (filter    |
|                                  | history).                        |
+----------------------------------+----------------------------------+
| **Adaptive IIR Filter                                               |
| Parameters**                                                        |
|                                                                     |
| |image14|                                                           |
+----------------------------------+----------------------------------+
| Adaptive IIR filter              | Enables the Adaptive IIR filter. |
|                                  | It is the IIR filter that        |
|                                  | changes its IIR coefficient      |
|                                  | according to the speed of the    |
|                                  | finger movement. This is done to |
|                                  | smooth the fast movement of the  |
|                                  | finger and at the same time      |
|                                  | control properly the position    |
|                                  | movement. The filter             |
|                                  | coefficients are automatically   |
|                                  | adjusted by the adaptive         |
|                                  | algorithm with the speed of the  |
|                                  | finger movement. If the finger   |
|                                  | moves slowly, the IIR            |
|                                  | coefficient decreases; if the    |
|                                  | finger moves fast, the IIR       |
|                                  | coefficient increases from the   |
|                                  | existing value.                  |
|                                  |                                  |
|                                  | Consumes 3 bytes of RAM per each |
|                                  | position (filter history).       |
+----------------------------------+----------------------------------+
| Position movement threshold      | Defines the position threshold   |
|                                  | below which a position           |
|                                  | displacement is ignored or       |
|                                  | considered as no movement.       |
+----------------------------------+----------------------------------+
| Position slow movement threshold | Defines the position threshold   |
|                                  | below which (and above Position  |
|                                  | movement threshold) a position   |
|                                  | displacement (the difference     |
|                                  | between the current and previous |
|                                  | position) is considered as slow  |
|                                  | movement. If the position        |
|                                  | displacement is within the       |
|                                  | threshold limits, the IIR filter |
|                                  | coefficient decreases during     |
|                                  | each new scan. So, the filter    |
|                                  | impact on the position becomes   |
|                                  | less intensive.                  |
+----------------------------------+----------------------------------+
| Position fast movement threshold | Defines the position threshold   |
|                                  | above which a position           |
|                                  | displacement is considered as    |
|                                  | fast movement. If the position   |
|                                  | displacement is above the        |
|                                  | threshold limit, the IIR filter  |
|                                  | impact on the position becomes   |
|                                  | more intensive during each new   |
|                                  | scan as the filter coefficient   |
|                                  | increases.                       |
+----------------------------------+----------------------------------+
| IIR coefficient maximum limit    | Defines the maximum limit of the |
|                                  | IIR coefficient when the finger  |
|                                  | moves fast. The fast movement    |
|                                  | event is defined by the Position |
|                                  | fast movement threshold.         |
+----------------------------------+----------------------------------+
| IIR coefficient minimum limit    | Defines the minimum limit of the |
|                                  | IIR coefficient when the finger  |
|                                  | moves slowly. The slow movement  |
|                                  | event is defined by the Position |
|                                  | slow movement threshold.         |
+----------------------------------+----------------------------------+
| IIR coefficient divisor          | This parameter acts as the scale |
|                                  | factor for the filter IIR        |
|                                  | coefficient.                     |
|                                  |                                  |
|                                  | .. math::                        |
|                                  |                                  |
|                                  |    Output = \frac{Coeff}{Divisor}|
|                                  |    \times Input + \frac{Divisor -|
|                                  |    Coeff}{Divisor} \times        |
|                                  |    previous Output               |
|                                  | where:                           |
|                                  |                                  |
|                                  | *Inpu*\ t\ *, Output*, and       |
|                                  | *Previous Output* are the touch  |
|                                  | positions;                       |
|                                  |                                  |
|                                  | *Coeff* is the automatically     |
|                                  | adjusted IIR filter coefficient; |
|                                  |                                  |
|                                  | *Divisor* is the IIR coefficient |
|                                  | divisor (this parameter).        |
+----------------------------------+----------------------------------+
| **Centroid Parameters**                                             |
|                                                                     |
| These parameters are available                                      |
| for the CSD Touchpad widgets                                        |
| only.                                                               |
+----------------------------------+----------------------------------+
| Centroid type                    | Selects a sensor matrix size for |
|                                  | centroid calculation. The 5x5    |
|                                  | centroid (also known as Advanced |
|                                  | Centroid) provides benefits such |
|                                  | as Two-finger detection, Edge    |
|                                  | correction and improved          |
|                                  | accuracy.                        |
|                                  |                                  |
|                                  | If Advanced Centroid is          |
|                                  | selected, the below parameters   |
|                                  | are configured as well.          |
+----------------------------------+----------------------------------+
| Cross-coupling position          | Defines the cross-coupling       |
| threshold                        | threshold. This value is         |
|                                  | subtracted from the sensor       |
|                                  | signal used for centroid         |
|                                  | position calculation to improve  |
|                                  | the accuracy.                    |
|                                  |                                  |
|                                  | The threshold should be equal to |
|                                  | a sensor signal when a finger is |
|                                  | near the sensor but is not       |
|                                  | touching the sensor. This can be |
|                                  | determined by slowly dragging    |
|                                  | the finger across the panel and  |
|                                  | finding the inflection point of  |
|                                  | the difference counts at the     |
|                                  | base of the curve. The           |
|                                  | difference value at this point   |
|                                  | is the Cross-coupling threshold. |
|                                  | The default value is 5.          |
+----------------------------------+----------------------------------+
| Edge correction                  | This feature is available if the |
|                                  | Centroid Type is configured to   |
|                                  | 5x5.                             |
|                                  |                                  |
|                                  | When enabled, a matrix of        |
|                                  | centroid calculation is updated  |
|                                  | with virtual sensors on the      |
|                                  | edges of a touchpad. It improves |
|                                  | the accuracy of the reported     |
|                                  | position on the edges. When      |
|                                  | enabled, two more parameters     |
|                                  | must be configured: Virtual      |
|                                  | Sensor threshold and Penultimate |
|                                  | threshold.                       |
+----------------------------------+----------------------------------+
| Virtual sensor threshold         | This parameter is applicable     |
|                                  | only if Edge correction is       |
|                                  | enabled and it is used to        |
|                                  | calculate a signal (difference   |
|                                  | count) for a virtual sensor used |
|                                  | for the edge correction          |
|                                  | algorithm.                       |
|                                  |                                  |
|                                  | A touch position on a slider or  |
|                                  | touchpad is calculated using a   |
|                                  | signal from the local-maxima     |
|                                  | sensor and its neighboring       |
|                                  | sensors. A touch on the edge     |
|                                  | sensor of a slider or touchpad   |
|                                  | does not accurately report a     |
|                                  | position because the edge sensor |
|                                  | lacks signal from one side of    |
|                                  | neighboring sensors of the       |
|                                  | local-maxima sensor.             |
|                                  |                                  |
|                                  | |image15|                        |
|                                  |                                  |
|                                  | If the Edge correction is        |
|                                  | enabled, the algorithm adds a    |
|                                  | virtual neighbor sensor to       |
|                                  | correct the deviation in the     |
|                                  | reported position. The Virtual   |
|                                  | sensor signal is defined by the  |
|                                  | Virtual sensor threshold:        |
|                                  |                                  |
|                                  | .. math::                        |
|                                  |                                  |
|                                  |    DiffCount_{VIRTUAL} =         |
|                                  |    (Threshold_{VIRTIAL}  -       |
|                                  |    DiffCount_{SNS0}) \times 2    |
|                                  |                                  |
|                                  | where:                           |
|                                  |                                  |
|                                  | *DiffCount VIRTUAL* is the       |
|                                  | virtual sensor difference count; |
|                                  |                                  |
|                                  | *Threshold VIRTUAL* is the       |
|                                  | virtual sensor threshold;        |
|                                  |                                  |
|                                  | *DiffCount SNS0* is the sensor 0 |
|                                  | difference count.                |
|                                  |                                  |
|                                  | The conditions for a virtual     |
|                                  | sensor (and Edge correction      |
|                                  | algorithm) to be applied:        |
|                                  |                                  |
|                                  | -  Local-maxima detected on the  |
|                                  |    edge sensor                   |
|                                  |                                  |
|                                  | Difference count from the        |
|                                  | penultimate sensor less than the |
|                                  | Penultimate threshold.           |
+----------------------------------+----------------------------------+
| Penultimate threshold            | This parameter is applicable     |
|                                  | only if the Edge correction is   |
|                                  | enabled and it works along with  |
|                                  | the Virtual sensor threshold     |
|                                  | parameter.                       |
|                                  |                                  |
|                                  | This parameter defines the       |
|                                  | threshold of penultimate sensor  |
|                                  | signal. If the signal from       |
|                                  | penultimate sensor is below the  |
|                                  | Penultimate threshold, the edge  |
|                                  | correction algorithm is applied  |
|                                  | to the centroid calculation.     |
|                                  |                                  |
|                                  | The conditions for the edge      |
|                                  | correction to be applied:        |
|                                  |                                  |
|                                  | -  Local-maxima detected on the  |
|                                  |    edge sensor                   |
|                                  |                                  |
|                                  | -  The difference count of the   |
|                                  |    penultimate sensor (SNS 1 in  |
|                                  |    the figure below) less than   |
|                                  |    the Penultimate threshold.    |
|                                  |                                  |
|                                  |    |image15|                     |
|                                  |                                  |
+----------------------------------+----------------------------------+
| Two-finger detection             | Enables the detection of the     |
|                                  | second finger on a CSD touchpad. |
|                                  |                                  |
|                                  | In general, a CSD touchpad can   |
|                                  | detect only one true touch       |
|                                  | position. A CSD touchpad widget  |
|                                  | consists of two Linear Sliders   |
|                                  | and each slider reports the X    |
|                                  | and Y coordinates of a finger    |
|                                  | touch. If there are two touches  |
|                                  | on the touchpad, there are four  |
|                                  | possible touch positions as      |
|                                  | shown in the figure below. The   |
|                                  | two of these touches are real    |
|                                  | touches and two are known as     |
|                                  | “ghost” touches. There is no     |
|                                  | possibility to differentiate     |
|                                  | between ghost and real touches   |
|                                  | in a CSD widget (to get true     |
|                                  | multi-touch performance, use the |
|                                  | CSX Touchpad widget).            |
|                                  |                                  |
|                                  | |image12|                        |
|                                  |                                  |
|                                  | But, if this feature is enabled, |
|                                  | the CSD touchpad can report up   |
|                                  | to two touches, mainly to be     |
|                                  | used in conjunction with         |
|                                  | two-finger gestures where real   |
|                                  | and ghost touches do not need to |
|                                  | be fully differentiated. It is   |
|                                  | available for the CSD touchpad   |
|                                  | only when the Centroid type is   |
|                                  | configured to 5x5.               |
|                                  |                                  |
|                                  | The Advanced centroid (Centroid  |
|                                  | type is 5x5) uses the 3x3        |
|                                  | centroid matrix when detects two |
|                                  | touches.                         |
+----------------------------------+----------------------------------+
| **Ballistic Multiplier**                                            |
| **Parameters**                                                      |
|                                                                     |
| These parameters are available                                      |
| for the CSD Touchpad widgets                                        |
| only.                                                               |
+----------------------------------+----------------------------------+
| Ballistic multiplier             | Enables the Ballistic multiplier |
|                                  | filter used to provide better    |
|                                  | user experience of the pointer   |
|                                  | movement. Fast movement will     |
|                                  | move the cursor by more pixels.  |
|                                  | Consumes 16 bytes of RAM when    |
|                                  | enabled.                         |
|                                  |                                  |
|                                  | The simplified diagram of the    |
|                                  | Ballistic Multiplier filter      |
|                                  | operation:                       |
|                                  |                                  |
|                                  | |image16|                        |
|                                  |                                  |
|                                  | where,                           |
|                                  |                                  |
|                                  | *dPos* is an input position      |
|                                  | displacement either in the X     |
|                                  | axis or Y axis,                  |
|                                  |                                  |
|                                  | *dPosFiltered* is the filtered   |
|                                  | displacement;                    |
|                                  |                                  |
|                                  | *SpeedThreshold* is either the   |
|                                  | X-axis speed threshold or Y-axis |
|                                  | speed threshold;                 |
|                                  |                                  |
|                                  | *A* is the Acceleration          |
|                                  | coefficient;                     |
|                                  |                                  |
|                                  | *S* is the Speed coefficient;    |
|                                  |                                  |
|                                  | *D* is the Divisor value.        |
+----------------------------------+----------------------------------+
| Acceleration coefficient         | Defines the value at which the   |
|                                  | position movement needs to be    |
|                                  | interpolated when the movement   |
|                                  | is classified as fast movement.  |
|                                  | The reported position            |
|                                  | displacement is multiplied by    |
|                                  | this parameter.                  |
+----------------------------------+----------------------------------+
| Speed coefficient                | Defines the value at which the   |
|                                  | position movement is             |
|                                  | interpolated when the movement   |
|                                  | is classified as slow movement.  |
|                                  | The reported position            |
|                                  | displacement is multiplied by    |
|                                  | this parameter.                  |
+----------------------------------+----------------------------------+
| Divisor value                    | Defines the divisor value used   |
|                                  | to create a fraction for the     |
|                                  | acceleration and speed           |
|                                  | coefficients. The interpolated   |
|                                  | position coordinates are divided |
|                                  | by the value of this parameter.  |
+----------------------------------+----------------------------------+
| X-axis speed threshold           | Defines the threshold to         |
|                                  | distinguish fast and slow        |
|                                  | movement on the X axis. If the   |
|                                  | X-axis position displacement     |
|                                  | reported between two consecutive |
|                                  | scans exceeds this threshold,    |
|                                  | then it is considered as fast    |
|                                  | movement, otherwise as slow      |
|                                  | movement.                        |
+----------------------------------+----------------------------------+
| Y-axis speed threshold           | Defines the threshold to         |
|                                  | distinguish fast and slow        |
|                                  | movement on the Y axis. If the   |
|                                  | Y-axis position displacement     |
|                                  | reported between two consecutive |
|                                  | scans exceeds this threshold,    |
|                                  | then it is considered as fast    |
|                                  | movement, otherwise as slow      |
|                                  | movement.                        |
+----------------------------------+----------------------------------+
| **Gesture Parameters**                                              |
+----------------------------------+----------------------------------+
| Enable gestures                  | Master enable for gestures       |
|                                  | feature.                         |
|                                  |                                  |
|                                  | Each gesture consists of a       |
|                                  | sequence of Touchdown and Lift   |
|                                  | Off events.                      |
|                                  |                                  |
|                                  | A simple touch on a widget is    |
|                                  | reported as a Touchdown event.   |
|                                  |                                  |
|                                  | Removal of a finger from a       |
|                                  | widget reported as a Lift Off    |
|                                  | event. If the Lift Off event     |
|                                  | triggers another higher-level    |
|                                  | Gesture, then the Lift Off event |
|                                  | is not reported.                 |
+----------------------------------+----------------------------------+
| Enable one-finger single click   | One-finger single click gesture  |
| gestures                         | is a combination of a Touchdown  |
|                                  | and Lift Off events with the     |
|                                  | conditions to be met:            |
|                                  |                                  |
|                                  | -  A touchdown event is followed |
|                                  |    by a Lift Off event.          |
|                                  |                                  |
|                                  | -  The touch duration (duration  |
|                                  |    between touchdown and lift    |
|                                  |    off) must be greater than     |
|                                  |    Minimum click timeout and     |
|                                  |    less than Maximum click       |
|                                  |    timeout.                      |
|                                  |                                  |
|                                  | -  Position displacements        |
|                                  |    between the Touchdown and     |
|                                  |    Lift Off events must be       |
|                                  |    within the Maximum click      |
|                                  |    distance.                     |
+----------------------------------+----------------------------------+
| Enable one-finger double click   | A One-finger double click        |
| gestures                         | gesture is a combination of two  |
|                                  | sequential one-finger single     |
|                                  | click gestures under specific    |
|                                  | conditions:                      |
|                                  |                                  |
|                                  | -  Both clicks in the sequence   |
|                                  |    must meet one-finger single   |
|                                  |    click conditions.             |
|                                  |                                  |
|                                  | -  The touch duration between    |
|                                  |    the two touchdown events must |
|                                  |    be within the Minimum second  |
|                                  |    click interval and Maximum    |
|                                  |    second click interval timeout |
|                                  |    limits.                       |
|                                  |                                  |
|                                  | -  The distance between two      |
|                                  |    clicks must not exceed the    |
|                                  |    Maximum second click distance |
+----------------------------------+----------------------------------+
| Enable one-finger click & drag   | This gesture is a one-finger     |
| gestures                         | click and then a hold, followed  |
|                                  | by a drag. A typical use case is |
|                                  | while moving items on the screen |
|                                  | from one point to another. It is |
|                                  | triggered when the finger        |
|                                  | movement follows this sequence:  |
|                                  | Touchdown -> Lift Off -> \       |
|                                  | Touchdown -> Drag                |
|                                  |                                  |
|                                  | Gesture triggering condition: A  |
|                                  | one-finger click gesture and a   |
|                                  | subsequent touchdown were        |
|                                  | detected within the Minimum      |
|                                  | click timeout and Maximum click  |
|                                  | timeout limits and within        |
|                                  | Maximum second click distance.   |
|                                  | Then the finger exceeds the      |
|                                  | Maximum click distance from a    |
|                                  | drag touchdown.                  |
+----------------------------------+----------------------------------+
| Enable two-finger single click   | A Two-finger single click        |
| gestures                         | gesture is a combination of a    |
|                                  | Touchdown and Lift Off events    |
|                                  | with under specific conditions:  |
|                                  |                                  |
|                                  | -  Two simultaneous finger       |
|                                  |    touches (touchdown and lift   |
|                                  |    off) should be detected.      |
|                                  |                                  |
|                                  | -  The duration between the      |
|                                  |    second finger touchdown and   |
|                                  |    lift off events of both       |
|                                  |    fingers must be within the    |
|                                  |    Minimum second click interval |
|                                  |    and Maximum second click      |
|                                  |    interval timeout limits. The  |
|                                  |    duration counting starts when |
|                                  |    the settling time elapsed for |
|                                  |    the second finger touchdown   |
|                                  |    event.                        |
|                                  |                                  |
|                                  | -  A position displacement       |
|                                  |    between the touchdown and     |
|                                  |    lift off events must be less  |
|                                  |    than the Maximum second click |
|                                  |    distance.                     |
+----------------------------------+----------------------------------+
| Enable one-finger scroll         | A One-finger Scroll gesture is a |
| gestures                         | combination of a touchdown       |
|                                  | followed by a displacement in a  |
|                                  | specific direction under         |
|                                  | specific conditions:             |
|                                  |                                  |
|                                  | -  For a slider, the position    |
|                                  |    displacement between two      |
|                                  |    consecutive scans must exceed |
|                                  |    the Minimum scroll distance.  |
|                                  |                                  |
|                                  | -  The Scroll debounce number of |
|                                  |    a scroll gesture in the same  |
|                                  |    direction is already          |
|                                  |    detected.                     |
+----------------------------------+----------------------------------+
| Enable two-finger scroll         | The design of a two-finger       |
| gestures                         | scroll gesture is the same as of |
|                                  | a one-finger scroll gesture,     |
|                                  | except for the conditions below. |
|                                  |                                  |
|                                  | -  The conditions of a           |
|                                  |    one-finger scroll are met.    |
|                                  |                                  |
|                                  | -  There must be two             |
|                                  |    simultaneous finger touches   |
|                                  |    detected on a widget for a    |
|                                  |    scroll to be considered as a  |
|                                  |    two-finger scroll.            |
|                                  |                                  |
|                                  | -  The displacement of both      |
|                                  |    finger touches must be on     |
|                                  |    same direction for a          |
|                                  |    two-finger scroll to be       |
|                                  |    valid.                        |
+----------------------------------+----------------------------------+
| Enable one-finger edge swipe     | An edge swipe gesture is a       |
| gestures                         | combination of a touchdown on an |
|                                  | edge followed by a displacement  |
|                                  | towards the center.              |
|                                  |                                  |
|                                  | The conditions for an edge swipe |
|                                  | gesture:                         |
|                                  |                                  |
|                                  | -  A touchdown event must occur  |
|                                  |    in the edge area defined by   |
|                                  |    the Edge size.                |
|                                  |                                  |
|                                  | -  A finger displacement must    |
|                                  |    occur from the edge towards   |
|                                  |    the center within the Maximum |
|                                  |    edge angle.                   |
|                                  |                                  |
|                                  | -  The displacement must exceed  |
|                                  |    the Minimum edge distance     |
|                                  |    within the Maximum edge       |
|                                  |    timeout duration.             |
+----------------------------------+----------------------------------+
| Enable one-finger flick gestures | A flick gesture is a combination |
|                                  | of a touchdown followed by a     |
|                                  | high-speed displacement and a    |
|                                  | lift off event.                  |
|                                  |                                  |
|                                  | A flick gesture starts at a      |
|                                  | touchdown and ends and reported  |
|                                  | at a lift off event. The         |
|                                  | conditions for a flick gesture.  |
|                                  |                                  |
|                                  | -  The displacement must exceed  |
|                                  |    the Minimum flick distance.   |
|                                  |                                  |
|                                  | -  The duration between a        |
|                                  |    touchdown and lift off events |
|                                  |    must be less than the Maximum |
|                                  |    flick timeout.                |
|                                  |                                  |
|                                  | .. note::                        |
|                                  |    The flick gesture is detected |
|                                  |    in 8 directions:              |
|                                  |                                  |
|                                  |    -  Up                         |
|                                  |                                  |
|                                  |    -  Down                       |
|                                  |                                  |
|                                  |    -  Left                       |
|                                  |                                  |
|                                  |    -  Right                      |
|                                  |                                  |
|                                  |    -  Up-Right                   |
|                                  |                                  |
|                                  |    -  Down-Left                  |
|                                  |                                  |
|                                  |    -  Up-Left                    |
|                                  |                                  |
|                                  |    -  Down-Right                 |
+----------------------------------+----------------------------------+
| Enable one-finger rotate         | A one-finger rotate gesture is   |
| gestures                         | reported when a circular         |
|                                  | displacement is detected. The    |
|                                  | decoding algorithm uses four     |
|                                  | directions to identify a         |
|                                  | circular displacement. A         |
|                                  | displacement in all four         |
|                                  | directions must be in the        |
|                                  | succession order to report a     |
|                                  | rotate gesture. The rotation     |
|                                  | direction can be clockwise or    |
|                                  | counter-clockwise.               |
+----------------------------------+----------------------------------+
| Enable two-finger zoom gestures  | A two-finger zoom gesture is     |
|                                  | reported when two touches move   |
|                                  | towards each other (Zoom Out) or |
|                                  | move away from each other (Zoom  |
|                                  | In).                             |
|                                  |                                  |
|                                  | The conditions for a zoom        |
|                                  | gesture:                         |
|                                  |                                  |
|                                  | -  An increase or decrease in    |
|                                  |    distance between two-finger   |
|                                  |    touch positions must exceed   |
|                                  |    the Minimum zoom distance.    |
|                                  |                                  |
|                                  | -  The zoom debounce number of a |
|                                  |    Zoom In or Zoom Out gesture   |
|                                  |    must be sequentially detected |
|                                  |    for a Zoom gesture to be      |
|                                  |    reported.                     |
|                                  |                                  |
|                                  | A scroll to the zoom debounce    |
|                                  | number of a zoom gestures must   |
|                                  | be sequentially detected for a   |
|                                  | Zoom gesture to be reported. If  |
|                                  | a Zoom gesture occurred after a  |
|                                  | scroll, the gesture is reported  |
|                                  | and there was no lift off event  |
|                                  | between the scroll and Zoom      |
|                                  | gestures.                        |
+----------------------------------+----------------------------------+
| Enable gesture filtering         | Enables filtering of the         |
|                                  | detected gestures.               |
|                                  |                                  |
|                                  | The gesture priority defined as  |
|                                  | follow (starting from the most   |
|                                  | important):                      |
|                                  |                                  |
|                                  | -  Two-finger zoom               |
|                                  |                                  |
|                                  | -  Two-finger scroll             |
|                                  |                                  |
|                                  | -  One-finger rotate             |
|                                  |                                  |
|                                  | -  One-finger edge swipe         |
|                                  |                                  |
|                                  | -  One-finger flick              |
|                                  |                                  |
|                                  | -  One-finger scroll             |
|                                  |                                  |
|                                  | -  Two-finger single click       |
|                                  |                                  |
|                                  | -  One-finger click and drag     |
|                                  |                                  |
|                                  | -  One-finger double click       |
|                                  |                                  |
|                                  | -  One-finger single click       |
|                                  |                                  |
|                                  | -  Touchdown                     |
|                                  |                                  |
|                                  | -  Liftoff                       |
+----------------------------------+----------------------------------+
| Maximum click timeout            | Defines the maximum duration     |
|                                  | between a touchdown and lift off |
|                                  | events of a click event. This    |
|                                  | parameter is used in all         |
|                                  | click-based gestures.            |
+----------------------------------+----------------------------------+
| Minimum click timeout            | Defines the minimum duration     |
|                                  | between a touchdown and lift off |
|                                  | events of a click event. This    |
|                                  | parameter is used in all         |
|                                  | click-based gestures.            |
+----------------------------------+----------------------------------+
| Maximum click distance           | Defines the maximum displacement |
|                                  | between a touchdown and lift off |
|                                  | events of a click event. This    |
|                                  | parameter is used in all         |
|                                  | click-based gestures.            |
+----------------------------------+----------------------------------+
| Maximum second click interval    | Defines the maximum displacement |
|                                  | between a touchdown and lift off |
|                                  | events of a click event. This    |
|                                  | parameter is used in all         |
|                                  | click-based gestures.            |
+----------------------------------+----------------------------------+
| Minimum second click interval    | This parameter defines the       |
|                                  | minimum duration between the     |
|                                  | first lift off and the second    |
|                                  | touchdown events. If the second  |
|                                  | click occurs early this limit,   |
|                                  | the double click and click&drag  |
|                                  | gestures are not reported.       |
+----------------------------------+----------------------------------+
| Maximum second click distance    | Defines the maximum distance     |
|                                  | between the first lift off event |
|                                  | and the second touchdown event.  |
|                                  | If the second click occurs       |
|                                  | outside this limit, the double   |
|                                  | click and click&drag gestures    |
|                                  | are not reported.                |
+----------------------------------+----------------------------------+
| Scroll debounce                  | Defines the minimum number       |
|                                  | sequential scroll steps in the   |
|                                  | same direction to be detected    |
|                                  | prior to the scroll is           |
|                                  | considered valid. A widget must  |
|                                  | detect scroll steps, at the      |
|                                  | minimum of Debounce times in the |
|                                  | same direction to be considered  |
|                                  | as a scroll in that direction.   |
+----------------------------------+----------------------------------+
| Minimum scroll distance          | Defines the minimum displacement |
|                                  | to recognize a single scroll     |
|                                  | step. A scroll step is           |
|                                  | calculated between two           |
|                                  | consecutive scans.               |
+----------------------------------+----------------------------------+
| Rotate debounce                  | Defines a maximum number of      |
|                                  | sequential rotate steps in the   |
|                                  | same direction to deem a rotate  |
|                                  | gesture invalid. For example, if |
|                                  | the Debounce value is set to 5,  |
|                                  | then the touch cannot continue   |
|                                  | in the same direction for 5      |
|                                  | rotate steps and still have a    |
|                                  | valid rotate gesture. After this |
|                                  | threshold, the reported gesture  |
|                                  | stops being a rotate gesture. If |
|                                  | this parameter is set to 0, then |
|                                  | the Debounce is disabled.        |
+----------------------------------+----------------------------------+
| Minimum rotate distance          | Defines the minimum displacement |
|                                  | to recognize a single rotate     |
|                                  | step.                            |
+----------------------------------+----------------------------------+
| Zoom debounce                    | Defines the minimum number of    |
|                                  | zoom steps in a particular       |
|                                  | direction (in or out) to report  |
|                                  | a zoom gesture.                  |
+----------------------------------+----------------------------------+
| Minimum zoom distance            | Defines the minimum displacement |
|                                  | to recognize a single zoom step. |
+----------------------------------+----------------------------------+
| Maximum flick timeout            | Defines the maximum duration of  |
|                                  | how long a flick gesture is      |
|                                  | searched after a touchdown       |
|                                  | event. A position displacement   |
|                                  | and lift off event must happen   |
|                                  | within the duration defined by   |
|                                  | this parameter for a flick to be |
|                                  | valid.                           |
+----------------------------------+----------------------------------+
| Minimum flick distance           | Defines the minimum displacement |
|                                  | to be detected for a one-finger  |
|                                  | flick to be valid.               |
+----------------------------------+----------------------------------+
| Edge size                        | Defines the maximum edge area    |
|                                  | where a touchdown must be        |
|                                  | detected for an edge swipe to be |
|                                  | reported.                        |
+----------------------------------+----------------------------------+
| Minimum edge distance            | Defines the minimum displacement |
|                                  | to be detected from an edge to   |
|                                  | the center for an edge swipe to  |
|                                  | be reported.                     |
+----------------------------------+----------------------------------+
| Maximum edge timeout             | Defines the maximum duration     |
|                                  | within which an edge swipe must  |
|                                  | occur to be reported. The        |
|                                  | displacement must exceed the     |
|                                  | displacement threshold within    |
|                                  | the duration defined by this     |
|                                  | parameter for the edge swipe to  |
|                                  | be reported.                     |
+----------------------------------+----------------------------------+
| Maximum edge angle               | To report this gesture a finger  |
|                                  | movement should start from an    |
|                                  | edge and moves in the center     |
|                                  | direction. This is an ideal      |
|                                  | line. This parameters defines    |
|                                  | the maximum angle deviation (in  |
|                                  | degree) from this ideal line for |
|                                  | edge swipe to be valid. Degree 1 |
|                                  | means that the user can do       |
|                                  | gestures only on a single ideal  |
|                                  | line.                            |
+----------------------------------+----------------------------------+
| **Sensing Parameters**                                              |
+----------------------------------+----------------------------------+
| Compensation IDAC value          | Sets the Compensation IDAC value |
|                                  | for each CSD sensor when *Enable |
|                                  | compensation IDAC* is selected   |
|                                  | on the *CSD Settings* tab. If    |
|                                  | the *CSD tuning mode* is set to  |
|                                  | *SmartSense Auto-tuning* or      |
|                                  | *Enable IDAC auto-calibration*   |
|                                  | is selected on the *CSD          |
|                                  | Settings* tab, the value of this |
|                                  | parameter is set equal to the    |
|                                  | Modulator IDAC value at a device |
|                                  | power-up for the maximum         |
|                                  | performance from the sensor.     |
|                                  |                                  |
|                                  | Select the *Enable IDAC          |
|                                  | auto-calibration* for robust     |
|                                  | operation.                       |
+----------------------------------+----------------------------------+
| IDAC Values                      | Sets the IDAC value for each CSX |
|                                  | sensor/node, a lower IDAC value  |
|                                  | without saturating raw counts    |
|                                  | provides better performance for  |
|                                  | sensor/nodes.                    |
|                                  |                                  |
|                                  | When *Enable IDAC                |
|                                  | auto-calibration* is selected on |
|                                  | the *CSX Settings* tab, the      |
|                                  | value of this parameter is       |
|                                  | automatically set to the lowest  |
|                                  | possible value at a device       |
|                                  | power-up for better performance. |
|                                  |                                  |
|                                  | It is recommended to select      |
|                                  | *Enable IDAC auto-calibration*   |
|                                  | for robust operation.            |
+----------------------------------+----------------------------------+
| Selected pins                    | Selects a port pin for the       |
|                                  | sensor (CSD sensing) and         |
|                                  | electrode (CSX sensing). The     |
|                                  | available options use a          |
|                                  | dedicated pin for a sensor or    |
|                                  | re-use one or more pins from any |
|                                  | other sensor. Re-using the pins  |
|                                  | of any other sensor from any     |
|                                  | widgets helps create a ganged    |
|                                  | sensor.                          |
+----------------------------------+----------------------------------+

The following table shows which Widget / Sensor parameters belong to a
given widget type:


.. rst-class:: align-center

+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Param |                       **Widget Type**                                 |
| meters|                                                                       |
|       +-------+-------+-------+-------+-------+-------+-------+-------+-------+
|       | CSD   | CSX   |       |       |       |       |       |       |       |
|       | Widget| Widget|       |       |       |       |       |       |       |
|       +-------+-------+-------+-------+-------+-------+-------+-------+-------+
|       | *But  | *L\   | *R\   | *M\   | *Touc\| *\    | *Bu\  | *M\   | *Touc\|
|       | ton*  | inear | adial | atrix | hpad* | Proxi\| tton* | atrix | hpad* |
|       |       | Sl\   | Sl\   | But\  |       | mity* |       | But\  |       |
|       |       | ider* | ider* | tons* |       |       |       | tons* |       |
+=======+=======+=======+=======+=======+=======+=======+=======+=======+=======+
|                           **Widget General**                                  |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Dipl\ |       | √     |       |       |       |       |       |       |       |
| exing |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ma\   |       | √     | √     |       |       |       |       |       |       |
| ximum |       |       |       |       |       |       |       |       |       |
| pos\  |       |       |       |       |       |       |       |       |       |
| ition |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ma\   |       |       |       |       | √     |       |       |       | √     |
| ximum |       |       |       |       |       |       |       |       |       |
| X\    |       |       |       |       |       |       |       |       |       |
| -axis |       |       |       |       |       |       |       |       |       |
| pos\  |       |       |       |       |       |       |       |       |       |
| ition |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ma\   |       |       |       |       | √     |       |       |       | √     |
| ximum |       |       |       |       |       |       |       |       |       |
| Y\    |       |       |       |       |       |       |       |       |       |
| -axis |       |       |       |       |       |       |       |       |       |
| pos\  |       |       |       |       |       |       |       |       |       |
| ition |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
|                           **Widget Hardware**                                 |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Sense | √     | √     | √     |       |       | √     |       |       |       |
| clock |       |       |       |       |       |       |       |       |       |
| di\   |       |       |       |       |       |       |       |       |       |
| vider |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| C\    |       |       |       | √     | √     |       |       |       |       |
| olumn |       |       |       |       |       |       |       |       |       |
| sense |       |       |       |       |       |       |       |       |       |
| clock |       |       |       |       |       |       |       |       |       |
| di\   |       |       |       |       |       |       |       |       |       |
| vider |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Row   |       |       |       | √     | √     |       |       |       |       |
| sense |       |       |       |       |       |       |       |       |       |
| clock |       |       |       |       |       |       |       |       |       |
| di\   |       |       |       |       |       |       |       |       |       |
| vider |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Sense | √     | √     | √     | √     | √     | √     |       |       |       |
| clock |       |       |       |       |       |       |       |       |       |
| s\    |       |       |       |       |       |       |       |       |       |
| ource |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Tx    |       |       |       |       |       |       | √     | √     | √     |
| clock |       |       |       |       |       |       |       |       |       |
| di\   |       |       |       |       |       |       |       |       |       |
| vider |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Tx    |       |       |       |       |       |       | √     | √     | √     |
| clock |       |       |       |       |       |       |       |       |       |
| s\    |       |       |       |       |       |       |       |       |       |
| ource |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Scan  | √     | √     | √     | √     | √     | √     |       |       |       |
| resol\|       |       |       |       |       |       |       |       |       |
| ution |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| N\    |       |       |       |       |       |       | √     | √     | √     |
| umber |       |       |       |       |       |       |       |       |       |
| of    |       |       |       |       |       |       |       |       |       |
| sub-c\|       |       |       |       |       |       |       |       |       |
| onver\|       |       |       |       |       |       |       |       |       |
| sions |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Modu\ | √     | √     | √     |       |       | √     |       |       |       |
| lator |       |       |       |       |       |       |       |       |       |
| IDAC  |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| C\    |       |       |       | √     | √     |       |       |       |       |
| olumn |       |       |       |       |       |       |       |       |       |
| modu\ |       |       |       |       |       |       |       |       |       |
| lator |       |       |       |       |       |       |       |       |       |
| IDAC  |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Row   |       |       |       | √     | √     |       |       |       |       |
| modu\ |       |       |       |       |       |       |       |       |       |
| lator |       |       |       |       |       |       |       |       |       |
| IDAC  |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| IDAC  | √     | √     | √     | √     | √     | √     | √     | √     | √     |
| gain  |       |       |       |       |       |       |       |       |       |
| index |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
|                           **Widget Threshold**                                |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Prox\ |       |       |       |       |       | √     |       |       |       |
| imity |       |       |       |       |       |       |       |       |       |
| thre\ |       |       |       |       |       |       |       |       |       |
| shold |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Touch |       |       |       |       |       | √     |       |       |       |
| thre\ |       |       |       |       |       |       |       |       |       |
| shold |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| F\    | √     | √     | √     | √     | √     |       | √     | √     | √     |
| inger |       |       |       |       |       |       |       |       |       |
| thre\ |       |       |       |       |       |       |       |       |       |
| shold |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Noise | √     | √     | √     | √     | √     | √     | √     | √     | √     |
| thre\ |       |       |       |       |       |       |       |       |       |
| shold |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Neg\  | √     | √     | √     | √     | √     | √     | √     | √     | √     |
| ative |       |       |       |       |       |       |       |       |       |
| noise |       |       |       |       |       |       |       |       |       |
| thre\ |       |       |       |       |       |       |       |       |       |
| shold |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Low   | √     | √     | √     | √     | √     | √     | √     | √     | √     |
| bas\  |       |       |       |       |       |       |       |       |       |
| eline |       |       |       |       |       |       |       |       |       |
| reset |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Hyste\| √     | √     | √     | √     | √     | √     | √     | √     | √     |
| resis |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| ON    | √     | √     | √     | √     | √     | √     | √     | √     | √     |
| deb\  |       |       |       |       |       |       |       |       |       |
| ounce |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Vel\  |       |       |       |       |       |       |       |       | √     |
| ocity |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
|                           **Sensing Parameters**                              |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Co\   | √     | √     | √     | √     | √     | √     |       |       |       |
| mpens\|       |       |       |       |       |       |       |       |       |
| ation |       |       |       |       |       |       |       |       |       |
| IDAC  |       |       |       |       |       |       |       |       |       |
| value |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| IDAC  |       |       |       |       |       |       | √     | √     | √     |
| V\    |       |       |       |       |       |       |       |       |       |
| alues |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Sel\  | √     | √     | √     | √     | √     | √     | √     | √     | √     |
| ected |       |       |       |       |       |       |       |       |       |
| pins  |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
|                         **Position Filter Parameters**                        |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| IIR   |       | √     | √     |       | √     |       |       |       | √     |
| f\    |       |       |       |       |       |       |       |       |       |
| ilter |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| IIR   |       | √     | √     |       | √     |       |       |       | √     |
| f\    |       |       |       |       |       |       |       |       |       |
| ilter |       |       |       |       |       |       |       |       |       |
| c\    |       |       |       |       |       |       |       |       |       |
| oeffi\|       |       |       |       |       |       |       |       |       |
| cient |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| M\    |       | √     | √     |       | √     |       |       |       | √     |
| edian |       |       |       |       |       |       |       |       |       |
| f\    |       |       |       |       |       |       |       |       |       |
| ilter |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Av\   |       | √     | √     |       | √     |       |       |       | √     |
| erage |       |       |       |       |       |       |       |       |       |
| f\    |       |       |       |       |       |       |       |       |       |
| ilter |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| J\    |       | √     | √     |       | √     |       |       |       | √     |
| itter |       |       |       |       |       |       |       |       |       |
| f\    |       |       |       |       |       |       |       |       |       |
| ilter |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
|                      **Adaptive IIR Filter Parameters**                       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ada\  |       | √     | √     |       | √     |       |       |       | √     |
| ptive |       |       |       |       |       |       |       |       |       |
| IIR   |       |       |       |       |       |       |       |       |       |
| f\    |       |       |       |       |       |       |       |       |       |
| ilter |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Pos\  |       | √     | √     |       | √     |       |       |       | √     |
| ition |       |       |       |       |       |       |       |       |       |
| mov\  |       |       |       |       |       |       |       |       |       |
| ement |       |       |       |       |       |       |       |       |       |
| thre\ |       |       |       |       |       |       |       |       |       |
| shold |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Pos\  |       | √     | √     |       | √     |       |       |       | √     |
| ition |       |       |       |       |       |       |       |       |       |
| slow  |       |       |       |       |       |       |       |       |       |
| mov\  |       |       |       |       |       |       |       |       |       |
| ement |       |       |       |       |       |       |       |       |       |
| thre\ |       |       |       |       |       |       |       |       |       |
| shold |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Pos\  |       | √     | √     |       | √     |       |       |       | √     |
| ition |       |       |       |       |       |       |       |       |       |
| fast  |       |       |       |       |       |       |       |       |       |
| mov\  |       |       |       |       |       |       |       |       |       |
| ement |       |       |       |       |       |       |       |       |       |
| thre\ |       |       |       |       |       |       |       |       |       |
| shold |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| IIR   |       | √     | √     |       | √     |       |       |       | √     |
| c\    |       |       |       |       |       |       |       |       |       |
| oeffi\|       |       |       |       |       |       |       |       |       |
| cient |       |       |       |       |       |       |       |       |       |
| ma\   |       |       |       |       |       |       |       |       |       |
| ximum |       |       |       |       |       |       |       |       |       |
| limit |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| IIR   |       | √     | √     |       | √     |       |       |       | √     |
| c\    |       |       |       |       |       |       |       |       |       |
| oeffi\|       |       |       |       |       |       |       |       |       |
| cient |       |       |       |       |       |       |       |       |       |
| mi\   |       |       |       |       |       |       |       |       |       |
| nimum |       |       |       |       |       |       |       |       |       |
| limit |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| IIR   |       | √     | √     |       | √     |       |       |       | √     |
| c\    |       |       |       |       |       |       |       |       |       |
| oeffi\|       |       |       |       |       |       |       |       |       |
| cient |       |       |       |       |       |       |       |       |       |
| di\   |       |       |       |       |       |       |       |       |       |
| visor |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
|                             **Centroid Parameters**                           |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Cen\  |       |       |       |       | √     |       |       |       |       |
| troid |       |       |       |       |       |       |       |       |       |
| type  |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Cros\ |       |       |       |       | √     |       |       |       |       |
| s-cou\|       |       |       |       |       |       |       |       |       |
| pling |       |       |       |       |       |       |       |       |       |
| pos\  |       |       |       |       |       |       |       |       |       |
| ition |       |       |       |       |       |       |       |       |       |
| thre\ |       |       |       |       |       |       |       |       |       |
| shold |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Edge  |       |       |       |       | √     |       |       |       |       |
| corre\|       |       |       |       |       |       |       |       |       |
| ction |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Vi\   |       |       |       |       | √     |       |       |       |       |
| rtual |       |       |       |       |       |       |       |       |       |
| s\    |       |       |       |       |       |       |       |       |       |
| ensor |       |       |       |       |       |       |       |       |       |
| thre\ |       |       |       |       |       |       |       |       |       |
| shold |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| P\    |       |       |       |       | √     |       |       |       |       |
| enult |       |       |       |       |       |       |       |       |       |
| imate\|       |       |       |       |       |       |       |       |       |
| thre\ |       |       |       |       |       |       |       |       |       |
| shold |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Two-f\|       |       |       |       | √     |       |       |       |       |
| inger |       |       |       |       |       |       |       |       |       |
| dete\ |       |       |       |       |       |       |       |       |       |
| ction |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
|                      **Ballistic Multiplier Parameters**                      |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ball\ |       |       |       |       | √     |       |       |       |       |
| istic |       |       |       |       |       |       |       |       |       |
| multi\|       |       |       |       |       |       |       |       |       |
| plier |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ac\   |       |       |       |       | √     |       |       |       |       |
| celer\|       |       |       |       |       |       |       |       |       |
| ation |       |       |       |       |       |       |       |       |       |
| c\    |       |       |       |       |       |       |       |       |       |
| oeffi\|       |       |       |       |       |       |       |       |       |
| cient |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Speed |       |       |       |       | √     |       |       |       |       |
| c\    |       |       |       |       |       |       |       |       |       |
| oeffi\|       |       |       |       |       |       |       |       |       |
| cient |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Di\   |       |       |       |       | √     |       |       |       |       |
| visor |       |       |       |       |       |       |       |       |       |
| value |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| X\    |       |       |       |       | √     |       |       |       |       |
| -axis |       |       |       |       |       |       |       |       |       |
| speed |       |       |       |       |       |       |       |       |       |
| thre\ |       |       |       |       |       |       |       |       |       |
| shold |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Y\    |       |       |       |       | √     |       |       |       |       |
| -axis |       |       |       |       |       |       |       |       |       |
| speed |       |       |       |       |       |       |       |       |       |
| thre\ |       |       |       |       |       |       |       |       |       |
| shold |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
|                             **Gesture Parameters**                            |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| E\    |       | √     |       |       | √     |       |       |       | √     |
| nable |       |       |       |       |       |       |       |       |       |
| ges\  |       |       |       |       |       |       |       |       |       |
| tures |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| E\    |       | √     |       |       | √     |       |       |       | √     |
| nable |       |       |       |       |       |       |       |       |       |
| one-f\|       |       |       |       |       |       |       |       |       |
| inger |       |       |       |       |       |       |       |       |       |
| s\    |       |       |       |       |       |       |       |       |       |
| ingle |       |       |       |       |       |       |       |       |       |
| click |       |       |       |       |       |       |       |       |       |
| ges\  |       |       |       |       |       |       |       |       |       |
| tures |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| E\    |       | √     |       |       | √     |       |       |       | √     |
| nable |       |       |       |       |       |       |       |       |       |
| one-f\|       |       |       |       |       |       |       |       |       |
| inger |       |       |       |       |       |       |       |       |       |
| d\    |       |       |       |       |       |       |       |       |       |
| ouble |       |       |       |       |       |       |       |       |       |
| click |       |       |       |       |       |       |       |       |       |
| ges\  |       |       |       |       |       |       |       |       |       |
| tures |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| E\    |       | √     |       |       | √     |       |       |       | √     |
| nable |       |       |       |       |       |       |       |       |       |
| one-f\|       |       |       |       |       |       |       |       |       |
| inger |       |       |       |       |       |       |       |       |       |
| click |       |       |       |       |       |       |       |       |       |
| &     |       |       |       |       |       |       |       |       |       |
| drag  |       |       |       |       |       |       |       |       |       |
| ges\  |       |       |       |       |       |       |       |       |       |
| tures |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| E\    |       | √     |       |       | √     |       |       |       | √     |
| nable |       |       |       |       |       |       |       |       |       |
| two-f\|       |       |       |       |       |       |       |       |       |
| inger |       |       |       |       |       |       |       |       |       |
| s\    |       |       |       |       |       |       |       |       |       |
| ingle |       |       |       |       |       |       |       |       |       |
| click |       |       |       |       |       |       |       |       |       |
| ges\  |       |       |       |       |       |       |       |       |       |
| tures |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| E\    |       | √     |       |       | √     |       |       |       | √     |
| nable |       |       |       |       |       |       |       |       |       |
| one-f\|       |       |       |       |       |       |       |       |       |
| inger |       |       |       |       |       |       |       |       |       |
| s\    |       |       |       |       |       |       |       |       |       |
| croll |       |       |       |       |       |       |       |       |       |
| ges\  |       |       |       |       |       |       |       |       |       |
| tures |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| E\    |       | √     |       |       | √     |       |       |       | √     |
| nable |       |       |       |       |       |       |       |       |       |
| two-f\|       |       |       |       |       |       |       |       |       |
| inger |       |       |       |       |       |       |       |       |       |
| s\    |       |       |       |       |       |       |       |       |       |
| croll |       |       |       |       |       |       |       |       |       |
| ges\  |       |       |       |       |       |       |       |       |       |
| tures |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| E\    |       | √     |       |       | √     |       |       |       | √     |
| nable |       |       |       |       |       |       |       |       |       |
| one-f\|       |       |       |       |       |       |       |       |       |
| inger |       |       |       |       |       |       |       |       |       |
| edge  |       |       |       |       |       |       |       |       |       |
| swipe |       |       |       |       |       |       |       |       |       |
| ges\  |       |       |       |       |       |       |       |       |       |
| tures |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| E\    |       | √     |       |       | √     |       |       |       | √     |
| nable |       |       |       |       |       |       |       |       |       |
| one-f\|       |       |       |       |       |       |       |       |       |
| inger |       |       |       |       |       |       |       |       |       |
| flick |       |       |       |       |       |       |       |       |       |
| ges\  |       |       |       |       |       |       |       |       |       |
| tures |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| E\    |       | √     |       |       | √     |       |       |       | √     |
| nable |       |       |       |       |       |       |       |       |       |
| one-f\|       |       |       |       |       |       |       |       |       |
| inger |       |       |       |       |       |       |       |       |       |
| r\    |       |       |       |       |       |       |       |       |       |
| otate |       |       |       |       |       |       |       |       |       |
| ges\  |       |       |       |       |       |       |       |       |       |
| tures |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| E\    |       | √     |       |       | √     |       |       |       | √     |
| nable |       |       |       |       |       |       |       |       |       |
| one-f\|       |       |       |       |       |       |       |       |       |
| inger |       |       |       |       |       |       |       |       |       |
| zoom  |       |       |       |       |       |       |       |       |       |
| ges\  |       |       |       |       |       |       |       |       |       |
| tures |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| E\    |       | √     |       |       | √     |       |       |       | √     |
| nable |       |       |       |       |       |       |       |       |       |
| ge\   |       |       |       |       |       |       |       |       |       |
| sture |       |       |       |       |       |       |       |       |       |
| filt\ |       |       |       |       |       |       |       |       |       |
| ering |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ma\   |       | √     |       |       | √     |       |       |       | √     |
| ximum |       |       |       |       |       |       |       |       |       |
| click |       |       |       |       |       |       |       |       |       |
| ti\   |       |       |       |       |       |       |       |       |       |
| meout |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Mi\   |       | √     |       |       | √     |       |       |       | √     |
| nimum |       |       |       |       |       |       |       |       |       |
| click |       |       |       |       |       |       |       |       |       |
| ti\   |       |       |       |       |       |       |       |       |       |
| meout |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ma\   |       | √     |       |       | √     |       |       |       | √     |
| ximum |       |       |       |       |       |       |       |       |       |
| click |       |       |       |       |       |       |       |       |       |
| dis\  |       |       |       |       |       |       |       |       |       |
| tance |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ma\   |       | √     |       |       | √     |       |       |       | √     |
| ximum |       |       |       |       |       |       |       |       |       |
| s\    |       |       |       |       |       |       |       |       |       |
| econd |       |       |       |       |       |       |       |       |       |
| click |       |       |       |       |       |       |       |       |       |
| int\  |       |       |       |       |       |       |       |       |       |
| erval |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ma\   |       | √     |       |       | √     |       |       |       | √     |
| ximum |       |       |       |       |       |       |       |       |       |
| s\    |       |       |       |       |       |       |       |       |       |
| econd |       |       |       |       |       |       |       |       |       |
| click |       |       |       |       |       |       |       |       |       |
| int\  |       |       |       |       |       |       |       |       |       |
| erval |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ma\   |       | √     |       |       | √     |       |       |       | √     |
| ximum |       |       |       |       |       |       |       |       |       |
| s\    |       |       |       |       |       |       |       |       |       |
| econd |       |       |       |       |       |       |       |       |       |
| click |       |       |       |       |       |       |       |       |       |
| dis\  |       |       |       |       |       |       |       |       |       |
| tance |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| S\    |       | √     |       |       | √     |       |       |       | √     |
| croll |       |       |       |       |       |       |       |       |       |
| deb\  |       |       |       |       |       |       |       |       |       |
| ounce |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Mi\   |       | √     |       |       | √     |       |       |       | √     |
| nimum |       |       |       |       |       |       |       |       |       |
| s\    |       |       |       |       |       |       |       |       |       |
| croll |       |       |       |       |       |       |       |       |       |
| dis\  |       |       |       |       |       |       |       |       |       |
| tance |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| R\    |       | √     |       |       | √     |       |       |       | √     |
| otate |       |       |       |       |       |       |       |       |       |
| deb\  |       |       |       |       |       |       |       |       |       |
| ounce |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Mi\   |       | √     |       |       | √     |       |       |       | √     |
| nimum |       |       |       |       |       |       |       |       |       |
| r\    |       |       |       |       |       |       |       |       |       |
| otate |       |       |       |       |       |       |       |       |       |
| dis\  |       |       |       |       |       |       |       |       |       |
| tance |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Zoom  |       | √     |       |       | √     |       |       |       | √     |
| deb\  |       |       |       |       |       |       |       |       |       |
| ounce |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Mi\   |       | √     |       |       | √     |       |       |       | √     |
| nimum |       |       |       |       |       |       |       |       |       |
| zoom  |       |       |       |       |       |       |       |       |       |
| dis\  |       |       |       |       |       |       |       |       |       |
| tance |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ma\   |       | √     |       |       | √     |       |       |       | √     |
| ximum |       |       |       |       |       |       |       |       |       |
| flick |       |       |       |       |       |       |       |       |       |
| ti\   |       |       |       |       |       |       |       |       |       |
| meout |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ma\   |       | √     |       |       | √     |       |       |       | √     |
| ximum |       |       |       |       |       |       |       |       |       |
| flick |       |       |       |       |       |       |       |       |       |
| dis\  |       |       |       |       |       |       |       |       |       |
| tance |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Edge  |       | √     |       |       | √     |       |       |       | √     |
| size  |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Mi\   |       | √     |       |       | √     |       |       |       | √     |
| nimum |       |       |       |       |       |       |       |       |       |
| edge  |       |       |       |       |       |       |       |       |       |
| dis\  |       |       |       |       |       |       |       |       |       |
| tance |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ma\   |       | √     |       |       | √     |       |       |       | √     |
| ximum |       |       |       |       |       |       |       |       |       |
| edge  |       |       |       |       |       |       |       |       |       |
| ti\   |       |       |       |       |       |       |       |       |       |
| meout |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+
| Ma\   |       | √     |       |       | √     |       |       |       | √     |
| ximum |       |       |       |       |       |       |       |       |       |
| edge  |       |       |       |       |       |       |       |       |       |
| angle |       |       |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+-------+-------+

Pins Tab
--------

Use the **Pins** tab to assign pins to each sensor. Select the
appropriate signal from the pull-down menu.

|image13|

.. note::
   The **Pins** tab is only enabled if the CapSense Configurator
   is launched from the Device Configurator.

**Migration of Configuration File Format**

Versions of the CapSense Configurator prior to 2.0 used a C header file
to store its configuration as a comment in the XML format. In version
2.0 and later, the configuration is stored in a separate .cycapsense
file in the XML format. Use the following instructions to migrate to the
.cycapsense file as appropriate:

**From the** \ *Eclipse* **IDE**

1. Launch the CapSense Configurator. If there is no .cycapsense file,
   and if the .h file contains the XML configuration, it will be loaded
   automatically.

2. Click **Save** to create the .cycapsense file and update the C file.

**Without the** \ *Eclipse* **IDE**

1. Launch the CapSense Configurator.

2. Click **Open**.

3. Select the **Obsolete configurator files (\*.h)** file extension and
   choose a header file.

4. After the configuration is loaded, click **Save** to create the
   .cycapsense file and update the C file.

.. note::

   -  The .cycapsense file is located one directory up related to the
      header file.

   -  The command-line argument --config does not accept the obsolete
      configuration files (\*.h).

References
==========

Refer to the following documents for more information, as needed:

-  Eclipse IDE for ModusToolbox User Guide

-  ModusToolbox Device Configurator Guide

-  ModusToolbox CapSense Tuner Guide

-  CapSense Middleware API Reference Guide

-  Device Datasheets

-  Device Technical Reference Manuals

Version Changes
===============

This section lists and describes the changes for each version of this
tool.

+---------+-----------------------------------------------------------+
| Version | Description                                               |
+=========+===========================================================+
| 1.0     | New tool.                                                 |
+---------+-----------------------------------------------------------+
| 1.1     | Added the Notice List.                                    |
|         +-----------------------------------------------------------+
|         | Added more configuration parameters validation.           |
|         +-----------------------------------------------------------+
|         | Fixed minor issues.                                       |
+---------+-----------------------------------------------------------+
| 2.0     | Added "IDAC gain index" parameter.                        |
|         +-----------------------------------------------------------+
|         |    Changed the data storage location from the header (.h) |
|         |    file to XML-based file with the .cycapsense extension. |
|         |                                                           |
|         |    For backward compatibility, the configurator is still  |
|         |    able to load the header (.h) file that contains the    |
|         |    legacy format configuration. But, if the legacy header |
|         |    (.h) with the configuration is passed via a            |
|         |    command-line parameter, a message appears saying that  |
|         |    the .h file is not supported.                          |
|         +-----------------------------------------------------------+
|         |    Added the **Import** and **Export** options to the     |
|         |    **File** menu that enable importing and exporting the  |
|         |    configuration file from and into the external file.    |
|         +-----------------------------------------------------------+
|         | Added the **Reset View** command to the **View** menu     |
|         | that resets the view to the default.                      |
|         +-----------------------------------------------------------+
|         | Changed the Widget / Sensor parameters and Widget Type    |
|         | table to align with the actual CapSense Configurator      |
|         | widget parameters and types.                              |
|         |                                                           |
|         | Changed the name of Section “Sensor Parameters” to        |
|         | “Sensing Parameters” to align with the tool.              |
|         +-----------------------------------------------------------+
|         | Changed generation of the middleware initialization       |
|         | structure according to the changes in CapSense v2.0       |
|         | middleware (adding fields for flash memory optimization,  |
|         | fixed the defect with the rawcount filters config, IDAC   |
|         | gain index, etc.)                                         |
|         +-----------------------------------------------------------+
|         | Added verification if the provided MPNs match the         |
|         | contents of the design.modus/xml config file.             |
|         +-----------------------------------------------------------+
|         | Added the warning about opening a broken configuration    |
|         | file.                                                     |
|         +-----------------------------------------------------------+
|         | Added highlighting bold of modified properties in the     |
|         | property grid.                                            |
|         +-----------------------------------------------------------+
|         | Added handling of invalid command-line arguments.         |
|         +-----------------------------------------------------------+
|         | Fixed the pin assignment issues.                          |
+---------+-----------------------------------------------------------+
| 3.0     | Added the self-test library support.                      |
|         +-----------------------------------------------------------+
|         | Added the Undo / Redo feature.                            |
|         +-----------------------------------------------------------+
|         | Improved configuration validation. Added new validation   |
|         | rules.                                                    |
+---------+-----------------------------------------------------------+

.. |image0| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config1.png
.. |image1| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config2.png
.. |image2| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config3.png
.. |image3| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config4.png
.. |image4| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config5.png
.. |image5| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config6.png
.. |image6| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config7.png
.. |image7| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config10.png
.. |image8| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config11.png
.. |image9| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config12.png
.. |image12| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config13.png
.. |image13| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config14.png
.. |image14| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config15.png
.. |image15| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config16.png
.. |image16| image:: ../../../../_static/image/api/psoc-middleware/sensing/capsense/ModusToolbox-CapSense-Configurator-Guide/capsense-config17.png