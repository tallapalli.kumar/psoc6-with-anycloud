================
Data Structures
================

.. doxygengroup:: group_em_eeprom_data_structures
   :project: eeprom

.. toctree::
   :hidden:

   structcy__stc__eeprom__config__t.rst
   structcy__stc__eeprom__context__t.rst
   
