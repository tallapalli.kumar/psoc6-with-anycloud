==============
API Reference
==============


.. toctree::
   :hidden:

   group__group__usb__device.rst
   group__group__usb__dev__audio.rst
   group__group__usb__dev__cdc.rst
   group__group__usb__dev__hid.rst


Provides the list of API Reference

+-----------------------------------+-----------------------------------+
| `Device <group__group__usb__d     | This section provides an API      |
| evice.html>`__                    | description for the core          |
|                                   | functionality of the USB Device:  |
|                                   | initialization, status            |
|                                   | information, data transfers, and  |
|                                   | class support                     |
+-----------------------------------+-----------------------------------+
| `Macros <group__group__usb__d     |                                   |
| ev__macros.html>`__               |                                   |
+-----------------------------------+-----------------------------------+
| `Descriptors <group__group__us    |                                   |
| b__dev__macros__device__descr.htm |                                   |
| l>`__                             |                                   |
+-----------------------------------+-----------------------------------+
| `Functions <group__group__usb     |                                   |
| __dev__functions.html>`__         |                                   |
+-----------------------------------+-----------------------------------+
| `Initialization                   |                                   |
| Functions <group__group__usb__dev |                                   |
| __functions__common.html>`__      |                                   |
+-----------------------------------+-----------------------------------+
| `Service                          |                                   |
| Functions <group__group__usb__dev |                                   |
| __functions__service.html>`__     |                                   |
+-----------------------------------+-----------------------------------+
| `Data Transfer                    |                                   |
| Functions <group__group__usb__dev |                                   |
| __functions__data__transfer       |                                   |
| .html>`__                         |                                   |
+-----------------------------------+-----------------------------------+
| `Vendor Request Support           |                                   |
| Functions <group__group__usb__dev |                                   |
| __functions__vendor__support.h    |                                   |
| tml>`__                           |                                   |
+-----------------------------------+-----------------------------------+
| `Class Support                    |                                   |
| Functions <group__group__usb__dev |                                   |
| __functions__class__support       |                                   |
| .html>`__                         |                                   |
+-----------------------------------+-----------------------------------+
| `Data                             |                                   |
| Structures <group__group__usb__de |                                   |
| v__data__structures.html>`__      |                                   |
+-----------------------------------+-----------------------------------+
| `Device                           |                                   |
| Information <group__group__usb__d |                                   |
| ev__structures__device.html>`__   |                                   |
+-----------------------------------+-----------------------------------+
| `Device                           |                                   |
| Descriptors <group__group__usb__d |                                   |
| ev__structures__device__descr.htm |                                   |
| l>`__                             |                                   |
+-----------------------------------+-----------------------------------+
| `Control                          |                                   |
| Transfer <group__group__usb__dev_ |                                   |
| _structures__control.html>`__     |                                   |
+-----------------------------------+-----------------------------------+
| `Class                            |                                   |
| Support <group__group__usb__dev__ |                                   |
| structures__class.html>`__        |                                   |
+-----------------------------------+-----------------------------------+
| `Function                         |                                   |
| Pointers <group__group__usb__dev_ |                                   |
| _structures__func__ptr.html>`__   |                                   |
+-----------------------------------+-----------------------------------+
| `Enumerated                       |                                   |
| Types <group__group__usb__dev__en |                                   |
| ums.html>`__                      |                                   |
+-----------------------------------+-----------------------------------+
| `Audio                            | This section provides API         |
| Class <group__group__usb__dev__au | description for the Audio class   |
| dio.html>`__                      |                                   |
+-----------------------------------+-----------------------------------+
| `Macros <group__group__usb__de    |                                   |
| v__audio__macros.html>`__         |                                   |
+-----------------------------------+-----------------------------------+
| `Functions <group__group__usb_    |                                   |
| _dev__audio__functions.html>`__   |                                   |
+-----------------------------------+-----------------------------------+
| `Data                             |                                   |
| Structures <group__group__usb__de |                                   |
| v__audio__data__structures.       |                                   |
| html>`__                          |                                   |
+-----------------------------------+-----------------------------------+
| `CDC                              | This section provides API         |
| Class <group__group__usb__dev__cd | description for the CDC class     |
| c.html>`__                        |                                   |
+-----------------------------------+-----------------------------------+
| `Macros <group__group__usb__de    |                                   |
| v__cdc__macros.html>`__           |                                   |
+-----------------------------------+-----------------------------------+
| `Functions <group__group__usb_    |                                   |
| _dev__cdc__functions.html>`__     |                                   |
+-----------------------------------+-----------------------------------+
| `Data                             |                                   |
| Structures <group__group__usb__de |                                   |
| v__cdc__data__structures.html>`__ |                                   |
+-----------------------------------+-----------------------------------+
| `Enumerated                       |                                   |
| Types <group__group__usb__dev__cd |                                   |
| c__enums.html>`__                 |                                   |
+-----------------------------------+-----------------------------------+
| `HID                              | This section provides the API     |
| Class <group__group__usb__dev__hi | description for the HID class     |
| d.html>`__                        |                                   |
+-----------------------------------+-----------------------------------+
| `Macros <group__group__usb__de    |                                   |
| v__hid__macros.html>`__           |                                   |
+-----------------------------------+-----------------------------------+
| `Functions <group__group__usb_    |                                   |
| _dev__hid__functions.html>`__     |                                   |
+-----------------------------------+-----------------------------------+
| `Data                             |                                   |
| Structures <group__group__usb__de |                                   |
| v__hid__data__structures.html>`__ |                                   |
+-----------------------------------+-----------------------------------+
| `Enumerated                       |                                   |
| Types <group__group__usb__dev__hi |                                   |
| d__enums.html>`__                 |                                   |
+-----------------------------------+-----------------------------------+












